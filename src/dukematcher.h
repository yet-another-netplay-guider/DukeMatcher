/*************************************************************************

Copyright 2008-2009 - NY00123

This file is part of DUKEMATCHER.

DUKEMATCHER is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

DUKEMATCHER is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DUKEMATCHER; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*************************************************************************/

#ifndef _DUKEMATCHER_H_
#define _DUKEMATCHER_H_

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "common.h"
#include "config.h"
#include "main_frame.h"
#include "host_frame.h"
#include "client_frame.h"
#include "sp_dialog.h"
#include "mp_dialog.h"
#include "mp_common.h"
//#include "manualjoin_dialog.h"
#include "srcports_dialog.h"
#include "network_dialog.h"
#include "adv_dialog.h"

#ifdef ENABLE_UPNP
#include "upnp.h"
#endif

#define WEBSITE_HOSTNAME wxT("127.0.0.1")
#define WEBSITE_ADDRESS (wxString(wxT("http://")) + WEBSITE_HOSTNAME + wxT('/'))

#define DN3D_MAX_SKILLNUM 4
#define SW_MAX_SKILLNUM 4
#define DN3D_DEFAULT_SKILL 2
#define SW_DEFAULT_SKILL 2
#define DN3D_DEFAULT_LEVEL 0
#define SW_DEFAULT_LEVEL 0
//#define MAX_NUM_OF_MOD_FILES 10
#define DEFAULT_GAME_PORT_NUM 23513
#define DEFAULT_SERVER_PORT_NUM 8501

#define GAMENAME_DN3D wxT("Duke Nukem 3D")
#define GAMENAME_SW wxT("Shadow Warrior")
#define SRCPORTNAME_EDUKE32 wxT("EDuke32")
#define SRCPORTNAME_JFDUKE3D wxT("JFDuke3D")
#define SRCPORTNAME_IDUKE3D wxT("hDuke")
#define SRCPORTNAME_DUKE3DW32 wxT("NDuke")
#define SRCPORTNAME_XDUKE wxT("xDuke")
#define SRCPORTNAME_DOSDUKE wxT("DOS Duke")
#define SRCPORTNAME_JFSW wxT("JFShadowWarrior")
#define SRCPORTNAME_SWP wxT("SWP")
#define SRCPORTNAME_DOSSW wxT("DOS SW")

extern wxArrayString *g_dn3d_skill_list, *g_sw_skill_list, *g_dn3d_level_list, *g_sw_level_list,
                     *g_dn3d_gametype_list, *g_dn3d_spawn_list, *g_serverlists_addrs;
extern wxArrayLong *g_serverlists_portnums;
extern LauncherConfig *g_configuration;
extern MainFrame *g_main_frame;
extern HostedRoomFrame *g_host_frame;
extern ClientRoomFrame *g_client_frame;
extern SRCPortsDialog *g_srcports_dialog;
extern NetworkDialog *g_network_dialog;
extern AdvDialog *g_adv_dialog;
extern SPDialog *g_sp_dialog;
extern MPDialog *g_mp_dialog;
//extern ManualJoinDialog *g_manualjoin_dialog;
//extern wxString *g_launcher_user_profile_path, *g_launcher_resources_path;
#if (defined __WXMAC__) || (defined __WXCOCOA__)
extern DUKEMATCHERMenuBar *g_empty_menubar;
#endif
#ifdef ENABLE_UPNP
#if !wxCHECK_VERSION(2, 9, 0)
#error "ERROR: UPnP support should be used when compiled with wxWidgets 2.9 or later!"
#endif
extern UPNPData *g_upnpdata;
#endif

class UPNPDetectThread : public wxThread
{
public:
    virtual void *Entry();
private:
};

class UPNPForwardThread : public wxThread
{
public:
    UPNPForwardThread(long port_num, bool is_tcp);
    virtual void *Entry();
private:
  long m_port_num;
  bool m_is_tcp;
};

class UPNPUnForwardThread : public wxThread
{
public:
    UPNPUnForwardThread(long port_num, bool is_tcp);
    virtual void *Entry();
private:
  long m_port_num;
  bool m_is_tcp;
};

void g_CheckForUpdates();

size_t g_GetDN3DLevelSelectionByNum(long dn3d_level);
size_t g_GetSWLevelSelectionByNum(long sw_level);
size_t g_GetDN3DLevelNumBySelection(long dn3d_selection);
size_t g_GetSWLevelNumBySelection(long sw_selection);
void g_OpenURL(const wxString& url);
void g_PlaySound(const wxString& filename);
void g_AddAllDN3DMapFiles(wxArrayString *file_list);
void g_AddAllSWMapFiles(wxArrayString *file_list);
void g_AddAllDN3DModFiles(wxArrayString *file_list);
void g_AddAllSWModFiles(wxArrayString *file_list);
void g_AddAllDemoFiles(wxArrayString *file_list, const wxString& demo_dir);

bool g_MakeLaunchScript(const wxString& script_filename,
                        RoomPlayersTable* players_table, size_t player_index,
                        GameType game, SourcePortType src_port,
                        bool have_bots, size_t num_bots,
                        bool play_demo, const wxString& playdemo_filename,
                        bool record_demo, const wxString& recorddemo_filename,
                        size_t skill_num,
                        DN3DMPGameT_Type gametype, DN3DSpawnType spawn,
                        bool usemasterslave, bool launch_usermap,
                        const wxString& user_map, size_t episode_map,
                        const wxArrayString& mod_files, const wxString& team, const wxString& extra_args);

class DUKEMATCHERApp : public wxApp
{
public:
    // Called on application startup
    virtual bool OnInit();
//private:
//  DECLARE_EVENT_TABLE()
};

// Implements DUKEMATCHERApp& GetApp()
//DECLARE_APP(DUKEMATCHERApp)

#endif
