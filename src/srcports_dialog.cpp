/*************************************************************************

Copyright 2008-2009 - NY00123

This file is part of DUKEMATCHER.

DUKEMATCHER is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

DUKEMATCHER is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DUKEMATCHER; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*************************************************************************/

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/wx.h>
#include <wx/notebook.h>
//#include <wx/listbook.h>
#include <wx/gbsizer.h>
#include <wx/filename.h>
#endif

#include "srcports_dialog.h"
#include "network_dialog.h"
#include "sp_dialog.h"
#include "config.h"
#include "dukematcher.h"
#include "theme.h"
#include "dukematcher.xpm"

BEGIN_EVENT_TABLE(SRCPortsDialog, DUKEMATCHERDialog)

EVT_BUTTON(wxID_OK, SRCPortsDialog::OnOk)
//EVT_BUTTON(wxID_APPLY, SRCPortsDialog::OnApply)
EVT_BUTTON(wxID_CANCEL, SRCPortsDialog::OnCancel)

EVT_BUTTON(ID_LOCATEEDUKE32, SRCPortsDialog::OnLocateFile)
EVT_BUTTON(ID_LOCATEJFDUKE3D, SRCPortsDialog::OnLocateFile)
EVT_BUTTON(ID_LOCATEIDUKE3D, SRCPortsDialog::OnLocateFile)
EVT_BUTTON(ID_LOCATEDUKE3DW32, SRCPortsDialog::OnLocateFile)
EVT_BUTTON(ID_LOCATEXDUKE, SRCPortsDialog::OnLocateFile)
EVT_BUTTON(ID_LOCATEDOSDUKE, SRCPortsDialog::OnLocateFile)
EVT_BUTTON(ID_LOCATEJFSW, SRCPortsDialog::OnLocateFile)
EVT_BUTTON(ID_LOCATESWP, SRCPortsDialog::OnLocateFile)
EVT_BUTTON(ID_LOCATEDOSSW, SRCPortsDialog::OnLocateFile)
EVT_BUTTON(ID_LOCATEDOSBOX, SRCPortsDialog::OnLocateFile)
EVT_BUTTON(ID_LOCATEDOSBOXCONF, SRCPortsDialog::OnLocateFile)
EVT_BUTTON(ID_LOCATEDOSBOXCDIMAGE, SRCPortsDialog::OnLocateFile)
#ifndef __WXMSW__
EVT_BUTTON(ID_LOCATEWINE, SRCPortsDialog::OnLocateFile)
#endif

EVT_BUTTON(ID_SELECTDUKEMAPSFOLDER, SRCPortsDialog::OnSelectDir)
EVT_BUTTON(ID_SELECTDUKEMODSFOLDER, SRCPortsDialog::OnSelectDir)
EVT_BUTTON(ID_SELECTSWMAPSFOLDER, SRCPortsDialog::OnSelectDir)
EVT_BUTTON(ID_SELECTSWMODSFOLDER, SRCPortsDialog::OnSelectDir)
EVT_BUTTON(ID_SELECTDOSBOXCDLOCATION, SRCPortsDialog::OnSelectDir)

EVT_CHECKBOX(ID_EDUKE32CHECKBOX, SRCPortsDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_JFDUKE3DCHECKBOX, SRCPortsDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_IDUKE3DCHECKBOX, SRCPortsDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_DUKE3DW32CHECKBOX, SRCPortsDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_XDUKECHECKBOX, SRCPortsDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_DOSDUKECHECKBOX, SRCPortsDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_JFSWCHECKBOX, SRCPortsDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_SWPCHECKBOX, SRCPortsDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_DOSSWCHECKBOX, SRCPortsDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_DOSBOXCHECKBOX, SRCPortsDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_DOSBOXCONFCHECKBOX, SRCPortsDialog::OnCheckBoxClick)
#ifndef __WXMSW__
EVT_CHECKBOX(ID_WINECHECKBOX, SRCPortsDialog::OnCheckBoxClick)
#endif

EVT_RADIOBUTTON(ID_DOSBOXNOCDRADIOBTN, SRCPortsDialog::OnRadioBtnClick)
EVT_RADIOBUTTON(ID_DOSBOXCDLOCATIONRADIOBTN, SRCPortsDialog::OnRadioBtnClick)
EVT_RADIOBUTTON(ID_DOSBOXCDIMAGERADIOBTN, SRCPortsDialog::OnRadioBtnClick)

END_EVENT_TABLE()

SRCPortsDialog::SRCPortsDialog() : DUKEMATCHERDialog(NULL, wxID_ANY, wxT("Source ports"), wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE|wxDIALOG_NO_PARENT, wxEmptyString)
{
	wxBoxSizer* SRCPortsDialogbSizer = new wxBoxSizer( wxVERTICAL );
	
	m_srcportsnotebook = new DUKEMATCHERNotebook( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
//	m_srcportslistbook = new wxListbook( this, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(88, -1)), wxLB_DEFAULT|wxTAB_TRAVERSAL );
	m_srcportduke3dpanel = new DUKEMATCHERPanel( m_srcportsnotebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* duke3dpanelbSizer = new wxBoxSizer( wxVERTICAL );
	
	wxGridBagSizer* duke3dgbSizer = new wxGridBagSizer( 0, 0 );
	duke3dgbSizer->AddGrowableCol( 1 );
	duke3dgbSizer->SetFlexibleDirection( wxBOTH );
	duke3dgbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	m_eduke32availcheckBox = new DUKEMATCHERCheckBox( m_srcportduke3dpanel, ID_EDUKE32CHECKBOX, wxT("EDuke32"), wxDefaultPosition, wxDefaultSize, 0 );
	
	duke3dgbSizer->Add( m_eduke32availcheckBox, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_eduke32bintextCtrl = new DUKEMATCHERTextCtrl( m_srcportduke3dpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	duke3dgbSizer->Add( m_eduke32bintextCtrl, wxGBPosition( 0, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_eduke32locatebutton = new DUKEMATCHERButton( m_srcportduke3dpanel, ID_LOCATEEDUKE32, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	duke3dgbSizer->Add( m_eduke32locatebutton, wxGBPosition( 0, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_jfduke3davailcheckBox = new DUKEMATCHERCheckBox( m_srcportduke3dpanel, ID_JFDUKE3DCHECKBOX, wxT("JFDuke3D"), wxDefaultPosition, wxDefaultSize, 0 );
	
	duke3dgbSizer->Add( m_jfduke3davailcheckBox, wxGBPosition( 1, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_jfduke3dbintextCtrl = new DUKEMATCHERTextCtrl( m_srcportduke3dpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	duke3dgbSizer->Add( m_jfduke3dbintextCtrl, wxGBPosition( 1, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_jfduke3dlocatebutton = new DUKEMATCHERButton( m_srcportduke3dpanel, ID_LOCATEJFDUKE3D, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	duke3dgbSizer->Add( m_jfduke3dlocatebutton, wxGBPosition( 1, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_iduke3davailcheckBox = new DUKEMATCHERCheckBox( m_srcportduke3dpanel, ID_IDUKE3DCHECKBOX, wxT("hDuke"), wxDefaultPosition, wxDefaultSize, 0 );
	
	duke3dgbSizer->Add( m_iduke3davailcheckBox, wxGBPosition( 2, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_iduke3dbintextCtrl = new DUKEMATCHERTextCtrl( m_srcportduke3dpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	duke3dgbSizer->Add( m_iduke3dbintextCtrl, wxGBPosition( 2, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_iduke3dlocatebutton = new DUKEMATCHERButton( m_srcportduke3dpanel, ID_LOCATEIDUKE3D, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	duke3dgbSizer->Add( m_iduke3dlocatebutton, wxGBPosition( 2, 2 ), wxGBSpan( 1, 1 ), wxALL, 5 );
	
	m_duke3dw32availcheckBox = new DUKEMATCHERCheckBox( m_srcportduke3dpanel, ID_DUKE3DW32CHECKBOX, wxT("NDuke"), wxDefaultPosition, wxDefaultSize, 0 );
	
	duke3dgbSizer->Add( m_duke3dw32availcheckBox, wxGBPosition( 3, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_duke3dw32bintextCtrl = new DUKEMATCHERTextCtrl( m_srcportduke3dpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	duke3dgbSizer->Add( m_duke3dw32bintextCtrl, wxGBPosition( 3, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_duke3dw32locatebutton = new DUKEMATCHERButton( m_srcportduke3dpanel, ID_LOCATEDUKE3DW32, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	duke3dgbSizer->Add( m_duke3dw32locatebutton, wxGBPosition( 3, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_xdukeavailcheckBox = new DUKEMATCHERCheckBox( m_srcportduke3dpanel, ID_XDUKECHECKBOX, wxT("xDuke"), wxDefaultPosition, wxDefaultSize, 0 );
	
	duke3dgbSizer->Add( m_xdukeavailcheckBox, wxGBPosition( 4, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_xdukebintextCtrl = new DUKEMATCHERTextCtrl( m_srcportduke3dpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	duke3dgbSizer->Add( m_xdukebintextCtrl, wxGBPosition( 4, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_xdukelocatebutton = new DUKEMATCHERButton( m_srcportduke3dpanel, ID_LOCATEXDUKE, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	duke3dgbSizer->Add( m_xdukelocatebutton, wxGBPosition( 4, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_dosdukeavailcheckBox = new DUKEMATCHERCheckBox( m_srcportduke3dpanel, ID_DOSDUKECHECKBOX, wxT("DOS Duke (Requires DOSBox)"), wxDefaultPosition, wxDefaultSize, 0 );
	
	duke3dgbSizer->Add( m_dosdukeavailcheckBox, wxGBPosition( 5, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_dosdukebintextCtrl = new DUKEMATCHERTextCtrl( m_srcportduke3dpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	duke3dgbSizer->Add( m_dosdukebintextCtrl, wxGBPosition( 5, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_dosdukelocatebutton = new DUKEMATCHERButton( m_srcportduke3dpanel, ID_LOCATEDOSDUKE, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	duke3dgbSizer->Add( m_dosdukelocatebutton, wxGBPosition( 5, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_prefduke3dstaticline = new wxStaticLine( m_srcportduke3dpanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	duke3dgbSizer->Add( m_prefduke3dstaticline, wxGBPosition( 6, 0 ), wxGBSpan( 1, 3 ), wxEXPAND|wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_duke3dmapsstaticText = new wxStaticText( m_srcportduke3dpanel, wxID_ANY, wxT("User maps directory"), wxDefaultPosition, wxDefaultSize, 0 );
	duke3dgbSizer->Add( m_duke3dmapsstaticText, wxGBPosition( 7, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_duke3dmapstextCtrl = new DUKEMATCHERTextCtrl( m_srcportduke3dpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	duke3dgbSizer->Add( m_duke3dmapstextCtrl, wxGBPosition( 7, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_duke3dmapsselectbutton = new DUKEMATCHERButton( m_srcportduke3dpanel, ID_SELECTDUKEMAPSFOLDER, wxT("Select"), wxDefaultPosition, wxDefaultSize, 0 );
	duke3dgbSizer->Add( m_duke3dmapsselectbutton, wxGBPosition( 7, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_duke3dmodsstaticText = new wxStaticText( m_srcportduke3dpanel, wxID_ANY, wxT("TCs/MODs directory (GRP, ZIP, CON files)"), wxDefaultPosition, wxDefaultSize, 0 );
	duke3dgbSizer->Add( m_duke3dmodsstaticText, wxGBPosition( 8, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_duke3dmodstextCtrl = new DUKEMATCHERTextCtrl( m_srcportduke3dpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	duke3dgbSizer->Add( m_duke3dmodstextCtrl, wxGBPosition( 8, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_duke3dmodsselectbutton = new DUKEMATCHERButton( m_srcportduke3dpanel, ID_SELECTDUKEMODSFOLDER, wxT("Select"), wxDefaultPosition, wxDefaultSize, 0 );
	duke3dgbSizer->Add( m_duke3dmodsselectbutton, wxGBPosition( 8, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	duke3dpanelbSizer->Add( duke3dgbSizer, 1, wxEXPAND, 0 );
	
	m_srcportduke3dpanel->SetSizer( duke3dpanelbSizer );
	m_srcportduke3dpanel->Layout();
	duke3dpanelbSizer->Fit( m_srcportduke3dpanel );

	m_srcportsnotebook->AddPage( m_srcportduke3dpanel, wxT("Duke Nukem 3D"), true );

	m_srcportswpanel = new DUKEMATCHERPanel( m_srcportsnotebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* swpanelbSizer = new wxBoxSizer( wxVERTICAL );
	
	wxGridBagSizer* swgbSizer = new wxGridBagSizer( 0, 0 );
	swgbSizer->AddGrowableCol( 1 );
	swgbSizer->SetFlexibleDirection( wxBOTH );
	swgbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	m_jfswavailcheckBox = new DUKEMATCHERCheckBox( m_srcportswpanel, ID_JFSWCHECKBOX, wxT("JFShadowWarrior"), wxDefaultPosition, wxDefaultSize, 0 );
	
	swgbSizer->Add( m_jfswavailcheckBox, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_jfswbintextCtrl = new DUKEMATCHERTextCtrl( m_srcportswpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	swgbSizer->Add( m_jfswbintextCtrl, wxGBPosition( 0, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_jfswlocatebutton = new DUKEMATCHERButton( m_srcportswpanel, ID_LOCATEJFSW, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	swgbSizer->Add( m_jfswlocatebutton, wxGBPosition( 0, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_swpavailcheckBox = new DUKEMATCHERCheckBox( m_srcportswpanel, ID_SWPCHECKBOX, wxT("SWP"), wxDefaultPosition, wxDefaultSize, 0 );
	
	swgbSizer->Add( m_swpavailcheckBox, wxGBPosition( 1, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_swpbintextCtrl = new DUKEMATCHERTextCtrl( m_srcportswpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	swgbSizer->Add( m_swpbintextCtrl, wxGBPosition( 1, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_swplocatebutton = new DUKEMATCHERButton( m_srcportswpanel, ID_LOCATESWP, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	swgbSizer->Add( m_swplocatebutton, wxGBPosition( 1, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_dosswavailcheckBox = new DUKEMATCHERCheckBox( m_srcportswpanel, ID_DOSSWCHECKBOX, wxT("DOS SW (Requires DOSBox)"), wxDefaultPosition, wxDefaultSize, 0 );
	
	swgbSizer->Add( m_dosswavailcheckBox, wxGBPosition( 2, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_dosswbintextCtrl = new DUKEMATCHERTextCtrl( m_srcportswpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	swgbSizer->Add( m_dosswbintextCtrl, wxGBPosition( 2, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_dosswlocatebutton = new DUKEMATCHERButton( m_srcportswpanel, ID_LOCATEDOSSW, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	swgbSizer->Add( m_dosswlocatebutton, wxGBPosition( 2, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_prefswstaticline = new wxStaticLine( m_srcportswpanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	swgbSizer->Add( m_prefswstaticline, wxGBPosition( 3, 0 ), wxGBSpan( 1, 3 ), wxEXPAND|wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_swmapsstaticText = new wxStaticText( m_srcportswpanel, wxID_ANY, wxT("User maps directory"), wxDefaultPosition, wxDefaultSize, 0 );
	swgbSizer->Add( m_swmapsstaticText, wxGBPosition( 4, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_swmapstextCtrl = new DUKEMATCHERTextCtrl( m_srcportswpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	swgbSizer->Add( m_swmapstextCtrl, wxGBPosition( 4, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_swmapsselectbutton = new DUKEMATCHERButton( m_srcportswpanel, ID_SELECTSWMAPSFOLDER, wxT("Select"), wxDefaultPosition, wxDefaultSize, 0 );
	swgbSizer->Add( m_swmapsselectbutton, wxGBPosition( 4, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_swmodsstaticText = new wxStaticText( m_srcportswpanel, wxID_ANY, wxT("TCs/MODs directory (GRP, ZIP, CON files)"), wxDefaultPosition, wxDefaultSize, 0 );
	swgbSizer->Add( m_swmodsstaticText, wxGBPosition( 5, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_swmodstextCtrl = new DUKEMATCHERTextCtrl( m_srcportswpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	swgbSizer->Add( m_swmodstextCtrl, wxGBPosition( 5, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_swmodsselectbutton = new DUKEMATCHERButton( m_srcportswpanel, ID_SELECTSWMODSFOLDER, wxT("Select"), wxDefaultPosition, wxDefaultSize, 0 );
	swgbSizer->Add( m_swmodsselectbutton, wxGBPosition( 5, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	swpanelbSizer->Add( swgbSizer, 1, wxEXPAND, 0 );
	
	m_srcportswpanel->SetSizer( swpanelbSizer );
	m_srcportswpanel->Layout();
	swpanelbSizer->Fit( m_srcportswpanel );
	m_srcportsnotebook->AddPage( m_srcportswpanel, wxT("Shadow Warrior"), false );
	m_srcportdosboxpanel = new DUKEMATCHERPanel( m_srcportsnotebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* dosboxpanelbSizer = new wxBoxSizer( wxVERTICAL );
	
	wxGridBagSizer* dosboxgbSizer = new wxGridBagSizer( 0, 0 );
	dosboxgbSizer->AddGrowableCol( 1 );
	dosboxgbSizer->SetFlexibleDirection( wxBOTH );
	dosboxgbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	m_dosboxavailcheckBox = new DUKEMATCHERCheckBox( m_srcportdosboxpanel, ID_DOSBOXCHECKBOX, wxT("I have DOSBox"), wxDefaultPosition, wxDefaultSize, 0 );
	
	dosboxgbSizer->Add( m_dosboxavailcheckBox, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_dosboxbintextCtrl = new DUKEMATCHERTextCtrl( m_srcportdosboxpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	dosboxgbSizer->Add( m_dosboxbintextCtrl, wxGBPosition( 0, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_dosboxlocatebutton = new DUKEMATCHERButton( m_srcportdosboxpanel, ID_LOCATEDOSBOX, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	dosboxgbSizer->Add( m_dosboxlocatebutton, wxGBPosition( 0, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_dosboxconfcheckBox = new DUKEMATCHERCheckBox( m_srcportdosboxpanel, ID_DOSBOXCONFCHECKBOX, wxT("Use the following configuration file as a base"), wxDefaultPosition, wxDefaultSize, 0 );
	
	dosboxgbSizer->Add( m_dosboxconfcheckBox, wxGBPosition( 1, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_dosboxconftextCtrl = new DUKEMATCHERTextCtrl( m_srcportdosboxpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	dosboxgbSizer->Add( m_dosboxconftextCtrl, wxGBPosition( 1, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_dosboxconflocatebutton = new DUKEMATCHERButton( m_srcportdosboxpanel, ID_LOCATEDOSBOXCONF, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	dosboxgbSizer->Add( m_dosboxconflocatebutton, wxGBPosition( 1, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_dosboxnocdradioBtn = new DUKEMATCHERRadioButton( m_srcportdosboxpanel, ID_DOSBOXNOCDRADIOBTN, wxT("Don't mount a CD-ROM"), wxDefaultPosition, wxDefaultSize, 0 );
	dosboxgbSizer->Add( m_dosboxnocdradioBtn, wxGBPosition( 2, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_dosboxcdlocationradioBtn = new DUKEMATCHERRadioButton( m_srcportdosboxpanel, ID_DOSBOXCDLOCATIONRADIOBTN, wxT("Mount a CD-ROM location"), wxDefaultPosition, wxDefaultSize, 0 );
	dosboxgbSizer->Add( m_dosboxcdlocationradioBtn, wxGBPosition( 3, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_dosboxcdlocationtextCtrl = new DUKEMATCHERTextCtrl( m_srcportdosboxpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	dosboxgbSizer->Add( m_dosboxcdlocationtextCtrl, wxGBPosition( 3, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_dosboxcdlocationselectbutton = new DUKEMATCHERButton( m_srcportdosboxpanel, ID_SELECTDOSBOXCDLOCATION, wxT("Select"), wxDefaultPosition, wxDefaultSize, 0 );
	dosboxgbSizer->Add( m_dosboxcdlocationselectbutton, wxGBPosition( 3, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_dosboxcdimageradioBtn = new DUKEMATCHERRadioButton( m_srcportdosboxpanel, ID_DOSBOXCDIMAGERADIOBTN, wxT("Mount a CD-ROM image/block device"), wxDefaultPosition, wxDefaultSize, 0 );
	dosboxgbSizer->Add( m_dosboxcdimageradioBtn, wxGBPosition( 4, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_dosboxcdimagetextCtrl = new DUKEMATCHERTextCtrl( m_srcportdosboxpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	dosboxgbSizer->Add( m_dosboxcdimagetextCtrl, wxGBPosition( 4, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_dosboxcdimagelocatebutton = new DUKEMATCHERButton( m_srcportdosboxpanel, ID_LOCATEDOSBOXCDIMAGE, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	dosboxgbSizer->Add( m_dosboxcdimagelocatebutton, wxGBPosition( 4, 2 ), wxGBSpan( 1, 1 ), wxALL, 5 );
	
	dosboxpanelbSizer->Add( dosboxgbSizer, 1, wxEXPAND, 0 );
	
	m_srcportdosboxpanel->SetSizer( dosboxpanelbSizer );
	m_srcportdosboxpanel->Layout();
	dosboxpanelbSizer->Fit( m_srcportdosboxpanel );
	m_srcportsnotebook->AddPage( m_srcportdosboxpanel, wxT("DOSBox"), false );
#ifndef __WXMSW__
	m_srcportwinepanel = new DUKEMATCHERPanel( m_srcportsnotebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* winepanelbSizer = new wxBoxSizer( wxVERTICAL );
	
	wxGridBagSizer* winepanelgbSizer = new wxGridBagSizer( 0, 0 );
	winepanelgbSizer->AddGrowableCol( 0 );
	winepanelgbSizer->SetFlexibleDirection( wxBOTH );
	winepanelgbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	m_usewinecheckBox = new DUKEMATCHERCheckBox( m_srcportwinepanel, ID_WINECHECKBOX, wxT("Use a Windows compatibility pre-loader* for .EXE files"), wxDefaultPosition, wxDefaultSize, 0 );
	
	winepanelgbSizer->Add( m_usewinecheckBox, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_winebintextCtrl = new DUKEMATCHERTextCtrl( m_srcportwinepanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	winepanelgbSizer->Add( m_winebintextCtrl, wxGBPosition( 1, 0 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_winestaticText = new wxStaticText( m_srcportwinepanel, wxID_ANY, wxT("* That means some kind of a layer or emulator like Wine.\nIt isn't used with DOS games.\n\nBefore the binary you may add some kind\nof a sound wrapper like aoss or padsp."), wxDefaultPosition, wxDefaultSize, 0 );
	winepanelgbSizer->Add( m_winestaticText, wxGBPosition( 2, 0 ), wxGBSpan( 1, 1 ), wxALL, 5 );
	
	m_winelocatebutton = new DUKEMATCHERButton( m_srcportwinepanel, ID_LOCATEWINE, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	winepanelgbSizer->Add( m_winelocatebutton, wxGBPosition( 1, 1 ), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	winepanelbSizer->Add( winepanelgbSizer, 1, wxEXPAND, 0 );
	
	m_srcportwinepanel->SetSizer( winepanelbSizer );
	m_srcportwinepanel->Layout();
	winepanelbSizer->Fit( m_srcportwinepanel );
	m_srcportsnotebook->AddPage( m_srcportwinepanel, wxT("Windows compatibility"), false );
#endif
	SRCPortsDialogbSizer->Add( m_srcportsnotebook, 1, wxEXPAND | wxALL, 5 );
	
	m_srcportssdbSizer = new wxStdDialogButtonSizer();
	m_srcportssdbSizerOK = new DUKEMATCHERButton( this, wxID_OK );
	m_srcportssdbSizer->AddButton( m_srcportssdbSizerOK );
	m_srcportssdbSizerCancel = new DUKEMATCHERButton( this, wxID_CANCEL );
	m_srcportssdbSizer->AddButton( m_srcportssdbSizerCancel );
	m_srcportssdbSizer->Realize();
	SRCPortsDialogbSizer->Add( m_srcportssdbSizer, 0, wxALIGN_CENTER_HORIZONTAL, 5 );
	
//	m_srcportsnotebook->Layout();
	SetSizer( SRCPortsDialogbSizer );
	Layout();
	SRCPortsDialogbSizer->Fit( this );

  Centre();
  SetIcon(wxIcon(dukematcher_xpm));

  m_eduke32availcheckBox->SetValue(g_configuration->have_eduke32);
  m_eduke32bintextCtrl->SetValue(g_configuration->eduke32_exec);
  m_eduke32bintextCtrl->Enable(g_configuration->have_eduke32);
  m_eduke32locatebutton->Enable(g_configuration->have_eduke32);

  m_jfduke3davailcheckBox->SetValue(g_configuration->have_jfduke3d);
  m_jfduke3dbintextCtrl->SetValue(g_configuration->jfduke3d_exec);
  m_jfduke3dbintextCtrl->Enable(g_configuration->have_jfduke3d);
  m_jfduke3dlocatebutton->Enable(g_configuration->have_jfduke3d);

  m_iduke3davailcheckBox->SetValue(g_configuration->have_iduke3d);
  m_iduke3dbintextCtrl->SetValue(g_configuration->iduke3d_exec);
  m_iduke3dbintextCtrl->Enable(g_configuration->have_iduke3d);
  m_iduke3dlocatebutton->Enable(g_configuration->have_iduke3d);

  m_duke3dw32availcheckBox->SetValue(g_configuration->have_duke3dw32);
  m_duke3dw32bintextCtrl->SetValue(g_configuration->duke3dw32_exec);
  m_duke3dw32bintextCtrl->Enable(g_configuration->have_duke3dw32);
  m_duke3dw32locatebutton->Enable(g_configuration->have_duke3dw32);

  m_xdukeavailcheckBox->SetValue(g_configuration->have_xduke);
  m_xdukebintextCtrl->SetValue(g_configuration->xduke_exec);
  m_xdukebintextCtrl->Enable(g_configuration->have_xduke);
  m_xdukelocatebutton->Enable(g_configuration->have_xduke);

  m_dosdukeavailcheckBox->SetValue(g_configuration->have_dosduke);
  m_dosdukeavailcheckBox->Enable(g_configuration->have_dosbox);
  m_dosdukebintextCtrl->SetValue(g_configuration->dosduke_exec);
  m_dosdukebintextCtrl->Enable(g_configuration->have_dosduke && g_configuration->have_dosbox);
  m_dosdukelocatebutton->Enable(g_configuration->have_dosduke && g_configuration->have_dosbox);

  m_duke3dmapstextCtrl->SetValue(g_configuration->dn3d_maps_dir);
  m_duke3dmodstextCtrl->SetValue(g_configuration->dn3d_mods_dir);

  m_jfswavailcheckBox->SetValue(g_configuration->have_jfsw);
  m_jfswbintextCtrl->SetValue(g_configuration->jfsw_exec);
  m_jfswbintextCtrl->Enable(g_configuration->have_jfsw);
  m_jfswlocatebutton->Enable(g_configuration->have_jfsw);

  m_swpavailcheckBox->SetValue(g_configuration->have_swp);
  m_swpbintextCtrl->SetValue(g_configuration->swp_exec);
  m_swpbintextCtrl->Enable(g_configuration->have_swp);
  m_swplocatebutton->Enable(g_configuration->have_swp);

  m_dosswavailcheckBox->SetValue(g_configuration->have_dossw);
  m_dosswavailcheckBox->Enable(g_configuration->have_dosbox);
  m_dosswbintextCtrl->SetValue(g_configuration->dossw_exec);
  m_dosswbintextCtrl->Enable(g_configuration->have_dossw && g_configuration->have_dosbox);
  m_dosswlocatebutton->Enable(g_configuration->have_dossw && g_configuration->have_dosbox);

  m_swmapstextCtrl->SetValue(g_configuration->sw_maps_dir);
  m_swmodstextCtrl->SetValue(g_configuration->sw_mods_dir);

  m_dosboxavailcheckBox->SetValue(g_configuration->have_dosbox);
  m_dosboxbintextCtrl->SetValue(g_configuration->dosbox_exec);
  m_dosboxbintextCtrl->Enable(g_configuration->have_dosbox);
  m_dosboxlocatebutton->Enable(g_configuration->have_dosbox);

  m_dosboxconfcheckBox->SetValue(g_configuration->dosbox_use_conf);
  m_dosboxconfcheckBox->Enable(g_configuration->have_dosbox);
  m_dosboxconftextCtrl->SetValue(g_configuration->dosbox_conf);
  m_dosboxconftextCtrl->Enable(g_configuration->have_dosbox && g_configuration->dosbox_use_conf);
  m_dosboxconflocatebutton->Enable(g_configuration->have_dosbox && g_configuration->dosbox_use_conf);

  m_dosboxnocdradioBtn->SetValue((g_configuration->dosbox_cdmount == CDROMMOUNT_NONE));
  m_dosboxnocdradioBtn->Enable(g_configuration->have_dosbox);

  bool l_temp_bool = (g_configuration->dosbox_cdmount == CDROMMOUNT_DIR);
  m_dosboxcdlocationradioBtn->SetValue(l_temp_bool);
  m_dosboxcdlocationradioBtn->Enable(g_configuration->have_dosbox);
  m_dosboxcdlocationtextCtrl->SetValue(g_configuration->dosbox_cdrom_location);
  l_temp_bool = (g_configuration->have_dosbox && l_temp_bool);
  m_dosboxcdlocationtextCtrl->Enable(l_temp_bool);
  m_dosboxcdlocationselectbutton->Enable(l_temp_bool);

  l_temp_bool = (g_configuration->dosbox_cdmount == CDROMMOUNT_IMG);
  m_dosboxcdimageradioBtn->SetValue(l_temp_bool);
  m_dosboxcdimageradioBtn->Enable(g_configuration->have_dosbox);
  m_dosboxcdimagetextCtrl->SetValue(g_configuration->dosbox_cdrom_image);
  l_temp_bool = (g_configuration->have_dosbox && l_temp_bool);
  m_dosboxcdimagetextCtrl->Enable(l_temp_bool);
  m_dosboxcdimagelocatebutton->Enable(l_temp_bool);
#ifndef __WXMSW__
  m_usewinecheckBox->SetValue(g_configuration->have_wine);
  m_winebintextCtrl->SetValue(g_configuration->wine_exec);
  m_winebintextCtrl->Enable(g_configuration->have_wine);
  m_winelocatebutton->Enable(g_configuration->have_wine);
#endif
}

SRCPortsDialog::~SRCPortsDialog()
{
  if (g_main_frame)
    g_main_frame->ShowMainFrame();
  else
  {
    g_network_dialog = new NetworkDialog;
    g_network_dialog->Show(true);
    g_network_dialog->SetFocus();
//  g_network_dialog->Raise();
  }
//g_srcports_dialog = NULL;
}

void SRCPortsDialog::OnLocateFile(wxCommandEvent& event)
{
  wxFileDialog l_dialog(NULL, wxT("Select a file"), wxEmptyString, wxEmptyString,
                        wxT("All files|*"), wxFD_OPEN|wxFD_FILE_MUST_EXIST);
  if (l_dialog.ShowModal() == wxID_OK)
  {
    wxString l_fullpath = l_dialog.GetPath();
    switch (event.GetId())
    {
      case ID_LOCATEEDUKE32: m_eduke32bintextCtrl->SetValue(l_fullpath); break;
      case ID_LOCATEJFDUKE3D: m_jfduke3dbintextCtrl->SetValue(l_fullpath); break;
      case ID_LOCATEIDUKE3D: m_iduke3dbintextCtrl->SetValue(l_fullpath); break;
      case ID_LOCATEDUKE3DW32: m_duke3dw32bintextCtrl->SetValue(l_fullpath); break;
      case ID_LOCATEXDUKE: m_xdukebintextCtrl->SetValue(l_fullpath); break;
      case ID_LOCATEDOSDUKE: m_dosdukebintextCtrl->SetValue(l_fullpath); break;
      case ID_LOCATEJFSW: m_jfswbintextCtrl->SetValue(l_fullpath); break;
      case ID_LOCATESWP: m_swpbintextCtrl->SetValue(l_fullpath); break;
      case ID_LOCATEDOSSW: m_dosswbintextCtrl->SetValue(l_fullpath); break;
      case ID_LOCATEDOSBOX: m_dosboxbintextCtrl->SetValue(l_fullpath);
                            l_fullpath = ((wxFileName)l_fullpath).GetPath(wxPATH_GET_VOLUME|wxPATH_GET_SEPARATOR) + wxT("dosbox.conf");
                            // A TINY TRICK HERE... Continue and set the configuration file
                            if (!wxFileExists(l_fullpath))
                              break;
      case ID_LOCATEDOSBOXCONF: m_dosboxconftextCtrl->SetValue(l_fullpath); break;
#ifndef __WXMSW__
      case ID_LOCATEWINE: m_winebintextCtrl->SetValue(l_fullpath); break;
#endif
      default: /*ID_LOCATEDOSBOXCDIMAGE*/ m_dosboxcdimagetextCtrl->SetValue(l_fullpath);
    }
  }
}

void SRCPortsDialog::OnSelectDir(wxCommandEvent& event)
{
  wxDirDialog l_dialog(NULL, wxT("Select a directory"), wxEmptyString,
                       wxDD_DEFAULT_STYLE|wxDD_DIR_MUST_EXIST);
  if (l_dialog.ShowModal() == wxID_OK)
  {
    wxString l_path = l_dialog.GetPath();
    switch (event.GetId())
    {
      case ID_SELECTDUKEMAPSFOLDER: m_duke3dmapstextCtrl->SetValue(l_path); break;
      case ID_SELECTDUKEMODSFOLDER: m_duke3dmodstextCtrl->SetValue(l_path); break;
      case ID_SELECTSWMAPSFOLDER: m_swmapstextCtrl->SetValue(l_path); break;
      case ID_SELECTSWMODSFOLDER: m_swmodstextCtrl->SetValue(l_path); break;
      default: /*ID_SELECTDOSBOXCDLOCATION*/ m_dosboxcdlocationtextCtrl->SetValue(l_path);
    }
  }
}

void SRCPortsDialog::OnCheckBoxClick(wxCommandEvent& event)
{
  bool l_selection = event.GetInt();
  switch (event.GetId())
  {
    case ID_EDUKE32CHECKBOX:            m_eduke32bintextCtrl->Enable(l_selection);
                                        m_eduke32locatebutton->Enable(l_selection);
                                        break;
    case ID_JFDUKE3DCHECKBOX:           m_jfduke3dbintextCtrl->Enable(l_selection);
                                        m_jfduke3dlocatebutton->Enable(l_selection);
                                        break;
    case ID_IDUKE3DCHECKBOX:            m_iduke3dbintextCtrl->Enable(l_selection);
                                        m_iduke3dlocatebutton->Enable(l_selection);
                                        break;
    case ID_DUKE3DW32CHECKBOX:          m_duke3dw32bintextCtrl->Enable(l_selection);
                                        m_duke3dw32locatebutton->Enable(l_selection);
                                        break;
    case ID_XDUKECHECKBOX:              m_xdukebintextCtrl->Enable(l_selection);
                                        m_xdukelocatebutton->Enable(l_selection);
                                        break;
    case ID_DOSDUKECHECKBOX:            m_dosdukebintextCtrl->Enable(l_selection);
                                        m_dosdukelocatebutton->Enable(l_selection);
                                        break;
    case ID_JFSWCHECKBOX:               m_jfswbintextCtrl->Enable(l_selection);
                                        m_jfswlocatebutton->Enable(l_selection);
                                        break;
    case ID_SWPCHECKBOX:                m_swpbintextCtrl->Enable(l_selection);
                                        m_swplocatebutton->Enable(l_selection);
                                        break;
    case ID_DOSSWCHECKBOX:              m_dosswbintextCtrl->Enable(l_selection);
                                        m_dosswlocatebutton->Enable(l_selection);
                                        break;
    case ID_DOSBOXCHECKBOX:             m_dosboxbintextCtrl->Enable(l_selection);
                                        m_dosboxlocatebutton->Enable(l_selection);
                                        m_dosboxconfcheckBox->Enable(l_selection);
                                        bool l_temp_bool;
                                        l_temp_bool = (l_selection && (m_dosboxconfcheckBox->GetValue()));
                                        m_dosboxconftextCtrl->Enable(l_temp_bool);
                                        m_dosboxconflocatebutton->Enable(l_temp_bool);
                                        m_dosboxnocdradioBtn->Enable(l_selection);
                                        m_dosboxcdlocationradioBtn->Enable(l_selection);
                                        l_temp_bool = (l_selection && m_dosboxcdlocationradioBtn->GetValue());
                                        m_dosboxcdlocationtextCtrl->Enable(l_temp_bool);
                                        m_dosboxcdlocationselectbutton->Enable(l_temp_bool);
                                        m_dosboxcdimageradioBtn->Enable(l_selection);
                                        l_temp_bool = (l_selection && m_dosboxcdimageradioBtn->GetValue());
                                        m_dosboxcdimagetextCtrl->Enable(l_temp_bool);
                                        m_dosboxcdimagelocatebutton->Enable(l_temp_bool);
                                        l_temp_bool = (l_selection && m_dosdukeavailcheckBox->GetValue());
                                        m_dosdukeavailcheckBox->Enable(l_selection);
                                        m_dosdukebintextCtrl->Enable(l_temp_bool);
                                        m_dosdukelocatebutton->Enable(l_temp_bool);
                                        l_temp_bool = (l_selection && m_dosswavailcheckBox->GetValue());
                                        m_dosswavailcheckBox->Enable(l_selection);
                                        m_dosswbintextCtrl->Enable(l_temp_bool);
                                        m_dosswlocatebutton->Enable(l_temp_bool);
                                        break;
#ifndef __WXMSW__
    case ID_WINECHECKBOX:               m_winebintextCtrl->Enable(l_selection);
                                        m_winelocatebutton->Enable(l_selection);
                                        break;
#endif
    default: /*ID_DOSBOXCONFCHECKBOX:*/ m_dosboxconftextCtrl->Enable(l_selection);
                                        m_dosboxconflocatebutton->Enable(l_selection);
  }
}

void SRCPortsDialog::OnRadioBtnClick(wxCommandEvent& WXUNUSED(event))
{
  m_dosboxcdlocationtextCtrl->Enable(m_dosboxcdlocationradioBtn->GetValue());
  m_dosboxcdlocationselectbutton->Enable(m_dosboxcdlocationradioBtn->GetValue());
  m_dosboxcdimagetextCtrl->Enable(m_dosboxcdimageradioBtn->GetValue());
  m_dosboxcdimagelocatebutton->Enable(m_dosboxcdimageradioBtn->GetValue());
}
/*
void SRCPortsDialog::OnApply(wxCommandEvent& WXUNUSED(event))
{
  ApplySettings();
}
*/
void SRCPortsDialog::OnOk(wxCommandEvent& WXUNUSED(event))
{
  ApplySettings();
  Destroy();
}

void SRCPortsDialog::OnCancel(wxCommandEvent& WXUNUSED(event))
{
  Destroy();
}

void SRCPortsDialog::ApplySettings()
{
  g_configuration->have_eduke32 = m_eduke32availcheckBox->GetValue();
  g_configuration->eduke32_exec = m_eduke32bintextCtrl->GetValue();
  g_configuration->have_jfduke3d = m_jfduke3davailcheckBox->GetValue();
  g_configuration->jfduke3d_exec = m_jfduke3dbintextCtrl->GetValue();
  g_configuration->have_iduke3d = m_iduke3davailcheckBox->GetValue();
  g_configuration->iduke3d_exec = m_iduke3dbintextCtrl->GetValue();
  g_configuration->have_duke3dw32 = m_duke3dw32availcheckBox->GetValue();
  g_configuration->duke3dw32_exec = m_duke3dw32bintextCtrl->GetValue();
  g_configuration->have_xduke = m_xdukeavailcheckBox->GetValue();
  g_configuration->xduke_exec = m_xdukebintextCtrl->GetValue();
  g_configuration->have_dosduke = m_dosdukeavailcheckBox->GetValue();
  g_configuration->dosduke_exec = m_dosdukebintextCtrl->GetValue();
  g_configuration->dn3d_maps_dir = m_duke3dmapstextCtrl->GetValue();
  g_configuration->dn3d_mods_dir = m_duke3dmodstextCtrl->GetValue();
  g_configuration->have_jfsw = m_jfswavailcheckBox->GetValue();
  g_configuration->jfsw_exec = m_jfswbintextCtrl->GetValue();
  g_configuration->have_swp = m_swpavailcheckBox->GetValue();
  g_configuration->swp_exec = m_swpbintextCtrl->GetValue();
  g_configuration->have_dossw = m_dosswavailcheckBox->GetValue();
  g_configuration->dossw_exec = m_dosswbintextCtrl->GetValue();
  g_configuration->sw_maps_dir = m_swmapstextCtrl->GetValue();
  g_configuration->sw_mods_dir = m_swmodstextCtrl->GetValue();
  g_configuration->have_dosbox = m_dosboxavailcheckBox->GetValue();
  g_configuration->dosbox_exec = m_dosboxbintextCtrl->GetValue();
  g_configuration->dosbox_use_conf = m_dosboxconfcheckBox->GetValue();
  g_configuration->dosbox_conf = m_dosboxconftextCtrl->GetValue();
  if (m_dosboxnocdradioBtn->GetValue())
    g_configuration->dosbox_cdmount = CDROMMOUNT_NONE;
  else
    if (m_dosboxcdlocationradioBtn->GetValue())
      g_configuration->dosbox_cdmount = CDROMMOUNT_DIR;
  else
    g_configuration->dosbox_cdmount = CDROMMOUNT_IMG;
  g_configuration->dosbox_cdrom_location = m_dosboxcdlocationtextCtrl->GetValue();
  g_configuration->dosbox_cdrom_image = m_dosboxcdimagetextCtrl->GetValue();
#ifndef __WXMSW__
  g_configuration->have_wine = m_usewinecheckBox->GetValue();
  g_configuration->wine_exec = m_winebintextCtrl->GetValue();
#endif
  g_configuration->Save();
}

