/*************************************************************************

Copyright 2008-2009 - NY00123

This file is part of DUKEMATCHER.

DUKEMATCHER is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

DUKEMATCHER is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DUKEMATCHER; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*************************************************************************/

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/wx.h>
#include <wx/filename.h>
#include <wx/listctrl.h>
#include <wx/sstream.h>
#include <wx/protocol/http.h>
#include <wx/tokenzr.h>
#include <wx/socket.h>
#include <wx/datetime.h>
#include <wx/utils.h>
#include <wx/timer.h>
#include <wx/choice.h>
#endif

#include <string.h>
#include "client_frame.h"
#include "config.h"
#include "common.h"
#include "mapselection_dialog.h"
#include "comm_txt.h"
#include "dukematcher.h"
#include "theme.h"
#include "dukematcher.xpm"

BEGIN_EVENT_TABLE(ClientRoomFrame, DUKEMATCHERFrame)
  EVT_BUTTON(ID_CLIENTTRANSFERUPLOAD, ClientRoomFrame::OnUploadButtonClick)
  EVT_BUTTON(ID_CLIENTGENERALTRANSFERPURPOSE, ClientRoomFrame::OnTransferButtonClick)

  EVT_BUTTON(ID_CLIENTSENDTEXT, ClientRoomFrame::OnEnterText)
  EVT_BUTTON(wxID_EXIT, ClientRoomFrame::OnLeaveRoom)

  EVT_BUTTON(ID_MAPSELECTIONCLOSE, ClientRoomFrame::OnMapSelectionClose)

  EVT_BUTTON(ID_TRANSFERTHREADEND, ClientRoomFrame::OnTransferThreadEnd)

  EVT_CLOSE(ClientRoomFrame::OnCloseWindow)

  EVT_TEXT_URL(ID_CLIENTOUTPUTTEXT, ClientRoomFrame::OnTextURLEvent)

  EVT_TEXT_ENTER(ID_CLIENTINPUTTEXT, ClientRoomFrame::OnEnterText)

  EVT_SOCKET(ID_CLIENTSOCKET, ClientRoomFrame::OnSocketEvent)
  EVT_SOCKET(ID_CLIENTFILETRANSFERSOCKET, ClientRoomFrame::OnFileTransferSocketEvent)

  EVT_TIMER(ID_CLIENTTIMER, ClientRoomFrame::OnTimer)
  EVT_TIMER(ID_CLIENTMSGTIMER, ClientRoomFrame::OnClientMsgTimer)
  EVT_TIMER(ID_CLIENTFILETRANSFERTIMER, ClientRoomFrame::OnFileTransferTimer)
  EVT_CHECKBOX(ID_CLIENTCHANGEREADY, ClientRoomFrame::OnChangeReady)
  EVT_CHOICE(ID_CLIENTCHANGETEAM, ClientRoomFrame::OnChangeTeam)
//EVT_CHECKBOX(ID_CLIENTNATFREE, ClientRoomFrame::OnNatFreeCheck)
END_EVENT_TABLE()

ClientRoomFrame::ClientRoomFrame() : DUKEMATCHERFrame(NULL, wxID_ANY, wxEmptyString)
{
	wxBoxSizer* ClientRoomFramebSizer = new wxBoxSizer( wxVERTICAL );
	
	m_clientroompanel = new DUKEMATCHERPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* clientroompanelbSizer = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* clientroompanelinfo1bSizer = new wxBoxSizer( wxHORIZONTAL );
	
	m_clientgamenamestaticText = new wxStaticText( m_clientroompanel, wxID_ANY, wxT("Game:"), wxDefaultPosition, wxDefaultSize, 0 );
	clientroompanelinfo1bSizer->Add( m_clientgamenamestaticText, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_clientgamenametextCtrl = new DUKEMATCHERTextCtrl( m_clientroompanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(64, -1)), wxTE_READONLY );
	clientroompanelinfo1bSizer->Add( m_clientgamenametextCtrl, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_clientsrcportstaticText = new wxStaticText( m_clientroompanel, wxID_ANY, wxT("Port:"), wxDefaultPosition, wxDefaultSize, 0 );
	clientroompanelinfo1bSizer->Add( m_clientsrcportstaticText, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_clientsrcporttextCtrl = new DUKEMATCHERTextCtrl( m_clientroompanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(60, -1)), wxTE_READONLY );
	clientroompanelinfo1bSizer->Add( m_clientsrcporttextCtrl, 1, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_clientmapstaticText = new wxStaticText( m_clientroompanel, wxID_ANY, wxT("Map:"), wxDefaultPosition, wxDefaultSize, 0 );
	clientroompanelinfo1bSizer->Add( m_clientmapstaticText, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_clientmaptextCtrl = new DUKEMATCHERTextCtrl( m_clientroompanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)), wxTE_READONLY );
	clientroompanelinfo1bSizer->Add( m_clientmaptextCtrl, 1, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_clientspawnstaticText = new wxStaticText( m_clientroompanel, wxID_ANY, wxT("Spawn:"), wxDefaultPosition, wxDefaultSize, 0 );
	clientroompanelinfo1bSizer->Add( m_clientspawnstaticText, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_clientspawntextCtrl = new DUKEMATCHERTextCtrl( m_clientroompanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(36, -1)), wxTE_READONLY );
	clientroompanelinfo1bSizer->Add( m_clientspawntextCtrl, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	clientroompanelbSizer->Add( clientroompanelinfo1bSizer, 0, wxEXPAND, 5 );
	
	wxBoxSizer* clientroompanelinfo2bSizer = new wxBoxSizer( wxHORIZONTAL );
	
	m_clientgametypestaticText = new wxStaticText( m_clientroompanel, wxID_ANY, wxT("Game Type:"), wxDefaultPosition, wxDefaultSize, 0 );
	clientroompanelinfo2bSizer->Add( m_clientgametypestaticText, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_clientgametypetextCtrl = new DUKEMATCHERTextCtrl( m_clientroompanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(96, -1)), wxTE_READONLY );
	clientroompanelinfo2bSizer->Add( m_clientgametypetextCtrl, 1, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_clientteamstaticText = new wxStaticText( m_clientroompanel, wxID_ANY, wxT("Choose Team:"), wxDefaultPosition, wxDefaultSize, 0 );
	clientroompanelinfo2bSizer->Add( m_clientteamstaticText, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_clientteamchoice = new DUKEMATCHERChoice( m_clientroompanel, ID_CLIENTCHANGETEAM, wxDefaultPosition, wxDefaultSize, 0 );
	clientroompanelinfo2bSizer->Add( m_clientteamchoice, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_clientskillstaticText = new wxStaticText( m_clientroompanel, wxID_ANY, wxT("Skill:"), wxDefaultPosition, wxDefaultSize, 0 );
	clientroompanelinfo2bSizer->Add( m_clientskillstaticText, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_clientskilltextCtrl = new DUKEMATCHERTextCtrl( m_clientroompanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(64, -1)), wxTE_READONLY );
	clientroompanelinfo2bSizer->Add( m_clientskilltextCtrl, 1, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_clientfraglimitstaticText = new wxStaticText( m_clientroompanel, wxID_ANY, wxT("Frag Limit:"), wxDefaultPosition, wxDefaultSize, 0 );
	clientroompanelinfo2bSizer->Add( m_clientfraglimitstaticText, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_clientfraglimittextCtrl = new DUKEMATCHERTextCtrl( m_clientroompanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY );
	clientroompanelinfo2bSizer->Add( m_clientfraglimittextCtrl, 1, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	clientroompanelbSizer->Add( clientroompanelinfo2bSizer, 0, wxEXPAND, 5 );
	
	wxBoxSizer* clientroompanelinfo3bSizer = new wxBoxSizer( wxHORIZONTAL );
	
	m_clientmodstaticText = new wxStaticText( m_clientroompanel, wxID_ANY, wxT("TC/MOD:"), wxDefaultPosition, wxDefaultSize, 0 );
	clientroompanelinfo3bSizer->Add( m_clientmodstaticText, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_clientmodtextCtrl = new DUKEMATCHERTextCtrl( m_clientroompanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)), wxTE_READONLY );
	clientroompanelinfo3bSizer->Add( m_clientmodtextCtrl, 1, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_clientnumplayersstaticText = new wxStaticText( m_clientroompanel, wxID_ANY, wxT("Players:"), wxDefaultPosition, wxDefaultSize, 0 );
	clientroompanelinfo3bSizer->Add( m_clientnumplayersstaticText, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_clientnumplayerstextCtrl = new DUKEMATCHERTextCtrl( m_clientroompanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY );
	clientroompanelinfo3bSizer->Add( m_clientnumplayerstextCtrl, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_clientconnectionstaticText = new wxStaticText( m_clientroompanel, wxID_ANY, wxT("Connection:"), wxDefaultPosition, wxDefaultSize, 0 );
	clientroompanelinfo3bSizer->Add( m_clientconnectionstaticText, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_clientconnectiontextCtrl = new DUKEMATCHERTextCtrl( m_clientroompanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(64, -1)), wxTE_READONLY );
	clientroompanelinfo3bSizer->Add( m_clientconnectiontextCtrl, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	clientroompanelbSizer->Add( clientroompanelinfo3bSizer, 0, wxEXPAND, 5 );

	m_clientplayerslistCtrl = new DUKEMATCHERListCtrl( m_clientroompanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLC_REPORT );

	wxListItem itemCol;
	itemCol.SetText(wxT("Nickname"));
	itemCol.SetWidth(220);
//	itemCol.SetAlign(wxLIST_FORMAT_LEFT);
//	itemCol.SetImage(-1);
	m_clientplayerslistCtrl->InsertColumn(0, itemCol);
	itemCol.SetText(wxT("Status"));
	itemCol.SetWidth(100);
	m_clientplayerslistCtrl->InsertColumn(1, itemCol);
	itemCol.SetText(wxT("Location"));
	itemCol.SetWidth(280);
	m_clientplayerslistCtrl->InsertColumn(2, itemCol);
	itemCol.SetText(wxT("Detected IP"));
	itemCol.SetWidth(150);
	m_clientplayerslistCtrl->InsertColumn(3, itemCol);
	itemCol.SetText(wxT("In-game Address"));
	itemCol.SetWidth(160);
	m_clientplayerslistCtrl->InsertColumn(4, itemCol);
//	RefreshListCtrlSize();

	clientroompanelbSizer->Add( m_clientplayerslistCtrl, 1, wxALL|wxEXPAND, 5 );

	wxBoxSizer* clientroompaneltransferbSizer = new wxBoxSizer( wxHORIZONTAL );
	
	m_clientreadycheckBox = new DUKEMATCHERCheckBox( m_clientroompanel, ID_CLIENTCHANGEREADY, wxT("Ready"), wxDefaultPosition, wxDefaultSize, 0 );
	clientroompaneltransferbSizer->Add( m_clientreadycheckBox, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	clientroompaneltransferbSizer->Add( 267, 0, 1, wxEXPAND, 5 );
	
	m_clienttransferstaticText = new wxStaticText( m_clientroompanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(128, -1)), 0 );
	clientroompaneltransferbSizer->Add( m_clienttransferstaticText, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_clienttransferuploadbutton = new DUKEMATCHERButton( m_clientroompanel, ID_CLIENTTRANSFERUPLOAD, wxT("Upload a map"), wxDefaultPosition, wxDefaultSize, 0 );
	clientroompaneltransferbSizer->Add( m_clienttransferuploadbutton, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_clienttransferdynamicbutton = new DUKEMATCHERButton( m_clientroompanel, ID_CLIENTGENERALTRANSFERPURPOSE, wxT("Download map"), wxDefaultPosition, wxDefaultSize, 0 );
	clientroompaneltransferbSizer->Add( m_clienttransferdynamicbutton, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_clienttransfergauge = new wxGauge( m_clientroompanel, wxID_ANY, 100, wxDefaultPosition, wxDLG_UNIT(this, wxSize(128, -1)), wxGA_HORIZONTAL );
	clientroompaneltransferbSizer->Add( m_clienttransfergauge, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	clientroompanelbSizer->Add( clientroompaneltransferbSizer, 0, wxEXPAND, 5 );

	m_clientoutputtextCtrl = new DUKEMATCHERTextCtrl( m_clientroompanel, ID_CLIENTOUTPUTTEXT, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(160, 110)), wxTE_AUTO_URL|wxTE_MULTILINE|wxTE_READONLY|wxTE_RICH );
	clientroompanelbSizer->Add( m_clientoutputtextCtrl, 1, wxALL|wxEXPAND, 5 );
	
	wxBoxSizer* clientroompanelinputbSizer = new wxBoxSizer( wxHORIZONTAL );
	
	m_clientinputtextCtrl = new DUKEMATCHERTextCtrl( m_clientroompanel, ID_CLIENTINPUTTEXT, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)), wxTE_PROCESS_ENTER );
#if ((defined __WXMAC__) || (defined __WXCOCOA__))
        m_clientinputtextCtrl->MacCheckSpelling(true);
#endif 
	clientroompanelinputbSizer->Add( m_clientinputtextCtrl, 1, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_clientsendbutton = new DUKEMATCHERButton( m_clientroompanel, ID_CLIENTSENDTEXT, wxT("Send"), wxDefaultPosition, wxDefaultSize, 0 );
	clientroompanelinputbSizer->Add( m_clientsendbutton, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	clientroompanelbSizer->Add( clientroompanelinputbSizer, 0, wxEXPAND, 5 );

/*	m_clientroomnatfreecheckBox = new wxCheckBox( m_clientroompanel, ID_CLIENTNATFREE, wxT("Use UDP hole-punching in game (NAT free)."), wxDefaultPosition, wxDefaultSize, 0 );

	clientroompanelbSizer->Add( m_clientroomnatfreecheckBox, 0, wxALL, 5 );*/

	wxBoxSizer* clientroompanelbuttonsbSizer = new wxBoxSizer( wxHORIZONTAL );

	m_clientleaveroombutton = new DUKEMATCHERButton( m_clientroompanel, wxID_EXIT, wxT("Leave Room"), wxDefaultPosition, wxDefaultSize, 0 );
	clientroompanelbuttonsbSizer->Add( m_clientleaveroombutton, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );


	clientroompanelbuttonsbSizer->Add( 0, 0, 1, wxEXPAND, 5 );

//	m_clientsendpmbutton = new DUKEMATCHERButton( m_clientroompanel, ID_CLIENTSENDPM, wxT("Send private message"), wxDefaultPosition, wxDefaultSize, 0 );
//	clientroompanelbuttonsbSizer->Add( m_clientsendpmbutton, 0, wxALL, 5 );

	clientroompanelbSizer->Add( clientroompanelbuttonsbSizer, 0, wxEXPAND, 5 );

	m_clientroompanel->SetSizer( clientroompanelbSizer );
	m_clientroompanel->Layout();
//	clientroompanelbSizer->Fit( m_clientroompanel );
        clientroompanelbSizer->SetSizeHints(m_clientroompanel);
	ClientRoomFramebSizer->Add( m_clientroompanel, 1, wxEXPAND | wxALL, 0 );

	SetSizer( ClientRoomFramebSizer );
	Layout();
//	ClientRoomFramebSizer->Fit( this );
        ClientRoomFramebSizer->SetSizeHints(this);

  Centre();
  SetIcon(wxIcon(dukematcher_xpm));

//m_clientroomnatfreecheckBox->SetValue(g_configuration->gamelaunch_enable_natfree);

  m_in_room = false;
//m_in_game = false;
  m_allow_sounds = true;
  m_timer = NULL;

  m_transfer_client = NULL;
  m_transfer_state = TRANSFERSTATE_NONE;
  m_transfer_request = TRANSFERREQUEST_NONE;
  m_mapselection_dialog = NULL;
//m_transfer_awaiting_download = false;
  // May prevent a few problems in case the player becomes in-room
  // before actually getting information about the room
  // (e.g. game and level) for some reason.
  m_launch_usermap = false;

  // We aren't really in room, yet.
  m_clienttransferuploadbutton->Disable();
  m_clienttransferdynamicbutton->Disable();
  m_clientsendbutton->Disable();
//m_clientsendpmbutton->Disable();
// ready checkbox should be enabled by default.
m_clientreadycheckBox->SetValue(true);
// disable mod text control until the information has been retrieved.
m_clientmodtextCtrl->Disable();
oldver = false;
// Add teams to the choice
	m_clientteamchoice->Clear();
	m_clientteamchoice->Append("No Team");
	m_clientteamchoice->Append("Blue Team");
	m_clientteamchoice->Append("Red Team");
	m_clientteamchoice->Append("Green Team");
	m_clientteamchoice->Append("Grey Team");
	m_clientteamchoice->Append("Dark Grey Team");
	m_clientteamchoice->Append("Dark Green Team");
	m_clientteamchoice->Append("Brown Team");
	m_clientteamchoice->Append("Dark Blue Team");
	m_clientteamchoice->Append("Hell Grey Team");
	m_clientteamchoice->SetStringSelection("No Team");
}

/*
ClientRoomFrame::~ClientRoomFrame()
{
  g_main_frame->ShowMainFrame();
}

void ClientRoomFrame::RefreshListCtrlSize()
{
  int l_width;
  if (m_clientplayerslistCtrl->GetItemCount())
    l_width = wxLIST_AUTOSIZE;
  else
    l_width = wxLIST_AUTOSIZE_USEHEADER;
  m_clientplayerslistCtrl->SetColumnWidth(0, l_width);
  m_clientplayerslistCtrl->SetColumnWidth(1, l_width);
  m_clientplayerslistCtrl->SetColumnWidth(2, l_width);
}
*/
void ClientRoomFrame::FocusFrame()
{
  m_clientinputtextCtrl->SetFocus();
}

void ClientRoomFrame::AddMessage(const wxString& msg)
{
//m_clientoutputtextCtrl->AppendText(wxT('\n') + msg + wxT('\n'));
  (*m_clientoutputtextCtrl) << msg << wxT('\n');
  while (m_clientoutputtextCtrl->GetNumberOfLines() > MAX_NUM_MSG_LINES)
    m_clientoutputtextCtrl->Remove(0, (m_clientoutputtextCtrl->GetLineText(0)).Len()+1);
  // Without that, text scrolling may not work as expected.
#ifdef __WXMSW__
  m_clientoutputtextCtrl->ScrollLines(-1);
#elif ((defined __WXMAC__) || (defined __WXCOCOA__))
  ((wxWindow*)m_clientoutputtextCtrl)->SetScrollPos(wxVERTICAL, ((wxWindow*)m_clientoutputtextCtrl)->GetScrollRange(wxVERTICAL));
#endif
}

void ClientRoomFrame::OnTextURLEvent(wxTextUrlEvent& event)
{
  if (event.GetMouseEvent().GetEventType() == wxEVT_LEFT_DOWN)
    g_OpenURL(m_clientoutputtextCtrl->GetRange(event.GetURLStart(), event.GetURLEnd()));
}

void ClientRoomFrame::UpdateGameSettings(const wxArrayString& l_info_input)
/*                                       GameType game,
                                         SourcePortType srcport,
                                         const wxString& roomname,
                                         size_t max_num_players,
                                         size_t skill,
                                         DN3DMPGameT_Type dn3dgametype,
                                         DN3DSpawnType dn3dspawn,
                                         const wxString& extra_args,
                                         bool launch_usermap,
                                         size_t epimap_num,
                                         const wxString& usermap,
                                         const wxString& modname,
                                         bool showmodurl,
                                         const wxString& modurl,
                                         bool usemasterslave)*/
{
  wxString l_temp_str;
  long l_temp_num;
  size_t l_num_of_items = l_info_input.GetCount(), l_temp_nonneg_num, l_loop_var;

  GameType l_last_game = m_game;
  wxString l_last_usermap = m_usermap;
  bool l_last_launch_usermap = m_launch_usermap;

  m_roomname = l_info_input[2];
  SetTitle(m_roomname);
//m_num_players = l_info_input[3];
  if (l_info_input[3].ToLong(&l_temp_num))
  {
    if ((l_temp_num < 0) || (l_temp_num > MAX_NUM_PLAYERS))
      m_max_num_players = DEFAULT_NUM_PLAYERS;
    else
      m_max_num_players = l_temp_num;
  }
  else
    m_max_num_players = DEFAULT_NUM_PLAYERS;
  m_clientnumplayerstextCtrl->SetValue(wxString::Format(wxT("%d/%d"),
                                                        m_players_table.num_players, m_max_num_players));
//m_in_game = (l_info_input[4] == wxT("yes"));

  l_temp_str = l_info_input[5];
  if (l_info_input[1] == wxT("dn3d"))
  {
    m_game = GAME_DN3D;
    m_clientgamenametextCtrl->SetValue(GAMENAME_DN3D);
    if (l_temp_str == wxT("eduke32"))
    {
      m_srcport = SOURCEPORT_EDUKE32;
      m_clientsrcporttextCtrl->SetValue(SRCPORTNAME_EDUKE32);
    }
    else
      if (l_temp_str == wxT("jfduke3d"))
      {
        m_srcport = SOURCEPORT_JFDUKE3D;
        m_clientsrcporttextCtrl->SetValue(SRCPORTNAME_JFDUKE3D);
      }
    else
      if (l_temp_str == wxT("iduke3d"))
      {
        m_srcport = SOURCEPORT_IDUKE3D;
        m_clientsrcporttextCtrl->SetValue(SRCPORTNAME_IDUKE3D);
      }
    else
      if (l_temp_str == wxT("duke3dw32"))
      {
        m_srcport = SOURCEPORT_DUKE3DW32;
        m_clientsrcporttextCtrl->SetValue(SRCPORTNAME_DUKE3DW32);
      }
    else
      if (l_temp_str == wxT("xduke"))
      {
        m_srcport = SOURCEPORT_XDUKE;
        m_clientsrcporttextCtrl->SetValue(SRCPORTNAME_XDUKE);
      }
    else
    {
      m_srcport = SOURCEPORT_DOSDUKE;
      m_clientsrcporttextCtrl->SetValue(SRCPORTNAME_DOSDUKE);
    }
    if (l_info_input[6].ToLong(&l_temp_num))
    {
      if ((l_temp_num < 0) && (l_temp_num > DN3D_MAX_SKILLNUM))
        m_skill = DN3D_DEFAULT_SKILL;
      else
        m_skill = l_temp_num;
    }
    else
      m_skill = DN3D_DEFAULT_SKILL;
    m_clientskilltextCtrl->SetValue((*g_dn3d_skill_list)[m_skill]);
    l_temp_str = l_info_input[7];
    if (l_temp_str == wxT("dmspawn"))
      m_dn3dgametype = DN3DMPGAMETYPE_DMSPAWN;
    else
      if (l_temp_str == wxT("coop"))
        m_dn3dgametype = DN3DMPGAMETYPE_COOP;
    else
      if (l_temp_str == wxT("dmnospawn"))
        m_dn3dgametype = DN3DMPGAMETYPE_DMNOSPAWN;
    else
      if (l_temp_str == wxT("tdmspawn"))
        m_dn3dgametype = DN3DMPGAMETYPE_TDMSPAWN;
    else
      m_dn3dgametype = DN3DMPGAMETYPE_TDMNOSPAWN;
    m_clientgametypetextCtrl->SetValue((*g_dn3d_gametype_list)[m_dn3dgametype-1]);
    l_temp_str = l_info_input[8];
    if (l_temp_str == wxT("monsters"))
      m_dn3dspawn = DN3DSPAWN_MONSTERS;
    else
      if (l_temp_str == wxT("items"))
        m_dn3dspawn = DN3DSPAWN_ITEMS;
    else
      if (l_temp_str == wxT("inventory"))
        m_dn3dspawn = DN3DSPAWN_INVENTORY;
    else
      m_dn3dspawn = DN3DSPAWN_ALL;
    m_clientspawntextCtrl->SetValue((*g_dn3d_spawn_list)[m_dn3dspawn-1]);
	 m_clientfraglimittextCtrl->SetValue("0");
    m_extra_args = l_info_input[9];
	// get frag limit for the display if source port is hduke.
	if (m_srcport != SOURCEPORT_IDUKE3D)
		m_clientfraglimittextCtrl->Disable();
	else {
	m_clientfraglimittextCtrl->Enable();
	wxStringTokenizer tokens(m_extra_args.Lower(), wxT(" "));
	long x = 0;
	while ( tokens.HasMoreTokens() )
{
	token = tokens.GetNextToken();
	if (!token.Find(wxT("/y")))
	  m_clientfraglimittextCtrl->SetValue(token.Mid(2));
	x++;
}
}
    if (l_num_of_items >= 12)
    {
      m_launch_usermap = (l_info_input[10] == wxT("usermap"));
      if (m_launch_usermap)
      {
        m_usermap = l_info_input[11];
        m_clientmaptextCtrl->SetValue(m_usermap);
      }
      else
      {
        if (l_info_input[11].ToLong(&l_temp_num))
        {
          m_epimap_num = l_temp_num;
          l_temp_nonneg_num = g_GetDN3DLevelSelectionByNum(m_epimap_num);
          if ((l_temp_nonneg_num <= 0) || (l_temp_nonneg_num >= g_dn3d_level_list->GetCount()))
          {
            m_epimap_num = 0;
            l_temp_nonneg_num = 0;
          }
        }
        else
        {
          m_epimap_num = 0;
          l_temp_nonneg_num = 0;
        }
        m_clientmaptextCtrl->SetValue((*g_dn3d_level_list)[l_temp_nonneg_num]);
      }
      m_usemasterslave = false;
      if (l_num_of_items >= 13)
        if ((l_info_input[12] == wxT("masterslave")) &&
            ((m_srcport == SOURCEPORT_EDUKE32) || (m_srcport == SOURCEPORT_JFDUKE3D)))
          m_usemasterslave = true;
      if (m_usemasterslave)
        m_clientconnectiontextCtrl->SetValue(wxT("Master/Slave"));
      else
        if (m_srcport == SOURCEPORT_DOSDUKE)
          m_clientconnectiontextCtrl->SetValue(wxT("Emulated IPX/SPX"));
      else
        m_clientconnectiontextCtrl->SetValue(wxT("Peer-2-Peer"));
    }
  }
  else
  {
    m_game = GAME_SW;
    m_clientgamenametextCtrl->SetValue(GAMENAME_SW);
    if (l_temp_str == wxT("jfsw"))
    {
      m_srcport = SOURCEPORT_JFSW;
      m_clientsrcporttextCtrl->SetValue(SRCPORTNAME_JFSW);
    }
    else
      if (l_temp_str == wxT("swp"))
      {
        m_srcport = SOURCEPORT_SWP;
        m_clientsrcporttextCtrl->SetValue(SRCPORTNAME_SWP);
      }
    else
    {
      m_srcport = SOURCEPORT_DOSSW;
      m_clientsrcporttextCtrl->SetValue(SRCPORTNAME_DOSSW);
    }
    if (l_info_input[6].ToLong(&l_temp_num))
    {
      if ((l_temp_num < 0) && (l_temp_num > SW_MAX_SKILLNUM))
        m_skill = SW_DEFAULT_SKILL;
      else
        m_skill = l_temp_num;
    }
    else
      m_skill = SW_DEFAULT_SKILL;
    m_clientskilltextCtrl->SetValue((*g_sw_skill_list)[m_skill]);
    m_clientgametypetextCtrl->Clear();
    m_clientspawntextCtrl->Clear();
    m_extra_args = l_info_input[7];
	// get frag limit for the display
	wxStringTokenizer tokens(m_extra_args, wxT(" "));
	long x = 0;
	while ( tokens.HasMoreTokens() )
{
	token = tokens.GetNextToken();
	if (!token.Find(wxT("/y")))
	m_clientfraglimittextCtrl->SetValue(token.Mid(2));
	x++;
}
	//
    m_launch_usermap = (l_info_input[8] == wxT("usermap"));
    if (m_launch_usermap)
    {
      m_usermap = l_info_input[9];
      m_clientmaptextCtrl->SetValue(m_usermap);
    }
    else
    {
      if (l_info_input[9].ToLong(&l_temp_num))
      {
        m_epimap_num = l_temp_num;
        l_temp_nonneg_num = g_GetSWLevelSelectionByNum(m_epimap_num);
        if ((l_temp_nonneg_num <= 0) || (l_temp_nonneg_num >= g_sw_level_list->GetCount()))
        {
          m_epimap_num = 0;
          l_temp_nonneg_num = 0;
        }
      }
      else
      {
        m_epimap_num = 0;
        l_temp_nonneg_num = 0;
      }
      m_clientmaptextCtrl->SetValue((*g_sw_level_list)[l_temp_nonneg_num]);
    }
    if (m_srcport == SOURCEPORT_DOSSW)
      m_clientconnectiontextCtrl->SetValue(wxT("Emulated IPX/SPX"));
    else
      m_clientconnectiontextCtrl->SetValue(wxT("Peer-2-Peer"));
  }

  // Stuff related to file transfer...

  if (m_mapselection_dialog && (l_last_game != m_game))
  {
    m_mapselection_dialog->m_game = m_game;
    m_mapselection_dialog->RefreshMapsList();
  }

  if (((l_last_game != m_game) || (l_last_usermap != m_usermap) || (l_last_launch_usermap != m_launch_usermap))
      && (m_transfer_state == TRANSFERSTATE_NONE))
  {
/*if ((l_last_game != m_game) || (l_last_usermap != m_usermap) || (l_last_launch_usermap != m_launch_usermap))
    switch (m_transfer_state)
    {
      case TRANSFERSTATE_NONE:*/
        if (m_transfer_request != TRANSFERREQUEST_NONE)
        {
          g_SendCStringToSocket(m_client, "1:requestcancel:");
          if (m_launch_usermap &&
              (((m_game == GAME_DN3D) && !wxFileExists(g_configuration->dn3d_maps_dir + wxFILE_SEP_PATH + m_usermap))
               || ((m_game == GAME_SW) && !wxFileExists(g_configuration->sw_maps_dir + wxFILE_SEP_PATH + m_usermap))))
          {
            g_SendCStringToSocket(m_client, "1:requestdownload:");
            m_transfer_filename = m_usermap;
            m_transfer_request = TRANSFERREQUEST_DOWNLOAD;
            m_clienttransferstaticText->SetLabel(m_transfer_filename);
          }
          else
          {
            m_transfer_request = TRANSFERREQUEST_NONE;
            m_clienttransferstaticText->SetLabel(wxEmptyString);
            m_clienttransferuploadbutton->Enable();
            m_clienttransferdynamicbutton->SetLabel(wxT("Download map"));
          }
        }
        else
          if ((m_mapselection_dialog == NULL) && m_in_room && m_launch_usermap &&
              (((m_game == GAME_DN3D) && !wxFileExists(g_configuration->dn3d_maps_dir + wxFILE_SEP_PATH + m_usermap))
               || ((m_game == GAME_SW) && !wxFileExists(g_configuration->sw_maps_dir + wxFILE_SEP_PATH + m_usermap))))
          {
            g_SendCStringToSocket(m_client, "1:requestdownload:");
            m_transfer_filename = m_usermap;
            m_transfer_request = TRANSFERREQUEST_DOWNLOAD;
            m_clienttransferstaticText->SetLabel(m_transfer_filename);
            m_clienttransferuploadbutton->Disable();
            m_clienttransferdynamicbutton->Enable();
            m_clienttransferdynamicbutton->SetLabel(wxT("Abort transfer"));
          }
          else
            m_clienttransferdynamicbutton->Disable();
  }
/*      break;
      case TRANSFERSTATE_PENDING:
        m_transfer_state = TRANSFERSTATE_NONE;
        g_SendCStringToSocket(m_client, "1:requestcancel:");
        m_transfer_client->Destroy();
        m_transfer_client = NULL;
        if (m_launch_usermap &&
            (((m_game == GAME_DN3D) && !wxFileExists(g_configuration->dn3d_maps_dir + wxFILE_SEP_PATH + m_usermap))
             || ((m_game == GAME_SW) && !wxFileExists(g_configuration->sw_maps_dir + wxFILE_SEP_PATH + m_usermap))))
        {
          g_SendCStringToSocket(m_client, "1:requestdownload:");
          m_transfer_filename = m_usermap;
          m_transfer_request = TRANSFERREQUEST_DOWNLOAD;
          m_clienttransferstaticText->SetLabel(m_transfer_filename);
        }
        else
        {
          m_transfer_request = TRANSFERREQUEST_NONE;
          m_clienttransferstaticText->SetLabel(wxEmptyString);
          m_clienttransferuploadbutton->Enable();
          m_clienttransferdynamicbutton->SetLabel(wxT("Download map"));
        }
        break;
      default: // BUSY, ABORTED. In the case of BUSY, the host should ask for abortion.
        if (m_transfer_state == TRANSFERSTATE_BUSY)
        {
          m_transfer_state = TRANSFERSTATE_ABORTED;
          g_SendCStringToSocket(m_client, "1:aborttransfer:");
        }
        m_transfer_awaiting_download = (m_launch_usermap &&
                                        (((m_game == GAME_DN3D) && !wxFileExists(g_configuration->dn3d_maps_dir + wxFILE_SEP_PATH + m_usermap))
                                         || ((m_game == GAME_SW) && !wxFileExists(g_configuration->sw_maps_dir + wxFILE_SEP_PATH + m_usermap))));
    }*/
	// Disable team choice if not Team DM
	if ((m_dn3dgametype != DN3DMPGAMETYPE_DMSPAWN) && (m_dn3dgametype != DN3DMPGAMETYPE_COOP) && (m_dn3dgametype != DN3DMPGAMETYPE_DMNOSPAWN) && (m_srcport == SOURCEPORT_IDUKE3D)) 
	{
		m_clientteamchoice->Enable();
		// loop through each player and set team to display name if team was previously specified.
		for (l_loop_var = 0; l_loop_var < m_players_table.num_players; l_loop_var++)
				m_clientplayerslistCtrl->SetItem(l_loop_var, 0, m_players_table.players[l_loop_var].nickname + wxT(" [") + m_players_table.players[l_loop_var].team + wxT("]"));
	}
	else {
		m_clientteamchoice->Disable();
		for (l_loop_var = 0; l_loop_var < m_players_table.num_players; l_loop_var++)
			m_clientplayerslistCtrl->SetItem(l_loop_var, 0, m_players_table.players[l_loop_var].nickname);
}}

void ClientRoomFrame::UpdateModFilesInfo(const wxArrayString& l_info_input)
{
  wxString l_temp_str;
  size_t l_num_of_items = l_info_input.GetCount(), l_loop_var;
  if (l_num_of_items >= 3)
  {
    m_modname = l_info_input[1];
    
    m_showmodurl = (l_info_input[2] == wxT("withurl"));
    if (m_showmodurl && (l_num_of_items >= 4))
    {
      m_modurl = l_info_input[3];
      l_loop_var = 4;
    }
    else
      l_loop_var = 3;
    m_modfiles.Empty();
    while (l_loop_var < l_num_of_items)
    {
      m_modfiles.Add(l_info_input[l_loop_var]);
	  l_temp_str += l_info_input[l_loop_var];
	  // if not last item, add a comma as there is another file to come.
	  if (l_loop_var != (l_num_of_items-1))
		l_temp_str += ", ";
      l_loop_var++;
    }
	
	// if there is a mod name and files add to the text control.
	if ((l_info_input[1] != "") && (l_temp_str != ""))
	   m_clientmodtextCtrl->SetValue(m_modname + wxT(" - ") + l_temp_str);
	// otherwise if there are files but no mod name, add the files to the text control.
	else if ((l_info_input[1] == "") && (l_temp_str != ""))
		m_clientmodtextCtrl->SetValue(l_temp_str);
	// if there is a mod name but no files, the host may be playing with custom cons so will still need to display just the name.
	else if ((l_info_input[1] != "") && (l_temp_str == ""))
		m_clientmodtextCtrl->SetValue(l_info_input[1]);
	// Finally if there is no mod name and no files, clear the control.
	else
		m_clientmodtextCtrl->Clear();
	// if there is content in the text control, enable it otherwise disable it.
		if (m_clientmodtextCtrl->GetValue() != "")
			m_clientmodtextCtrl->Enable();
		else
			m_clientmodtextCtrl->Disable();
  }
}

bool ClientRoomFrame::ConnectToServer(wxString ip_address, long port_num)
//void ClientRoomFrame::ConnectToServer(wxString ip_address, long port_num)
{
  wxIPV4address l_addr;
  l_addr.Hostname(ip_address);
  l_addr.Service(port_num);
  hostip = ip_address;
  hostport = port_num;

  m_client = new wxSocketClient();
  m_client->SetTimeout(10);
  m_client->SetEventHandler(*this, ID_CLIENTSOCKET);
  m_client->SetFlags(wxSOCKET_NONE|wxSOCKET_BLOCK);
  m_client->SetNotify(wxSOCKET_CONNECTION_FLAG | wxSOCKET_INPUT_FLAG | wxSOCKET_LOST_FLAG);
  //m_client->SetNotify(wxSOCKET_INPUT_FLAG | wxSOCKET_LOST_FLAG);
  m_client->Notify(true);
  if (m_client->Connect(l_addr, true))
  {
	m_client->SetTimeout(1);
    m_timer = new wxTimer(this, ID_CLIENTTIMER);
    m_timer->Start(45000, true); // If hasn't connected after 45 secs, it has failed to join.
    g_SendCStringToSocket(m_client, "1:requestroominfo:");
    return true;
  }  
/*m_client->Connect(l_addr, false);
  return true;
  if (!m_client->IsConnected());
  if ((m_client->Connect(l_addr, false))
      || (m_client->WaitOnConnect(10, 0)))
    if (m_client->IsConnected())
    {
      m_client->SetTimeout(10);
      m_timer = new wxTimer(this, ID_CLIENTTIMER);
      m_timer->Start(30000, wxTIMER_ONE_SHOT);
      g_SendCStringToSocket(m_client, "1:requestroominfo:");
      return true;
    }*/
  m_client->Destroy();
  return false;
}

void ClientRoomFrame::OnUploadButtonClick(wxCommandEvent& WXUNUSED(event))
{
  if (m_mapselection_dialog == NULL)
  {
    m_mapselection_dialog = new MapSelectionDialog(this, m_game);
    m_mapselection_dialog->RefreshMapsList();
    m_mapselection_dialog->ShowDialog();
    m_clienttransferdynamicbutton->Disable();
  }
  else
    m_mapselection_dialog->Raise();
}

void ClientRoomFrame::OnTransferButtonClick(wxCommandEvent& WXUNUSED(event))
{
  if (m_transfer_state == TRANSFERSTATE_BUSY)
  { // Abortion
    m_transfer_state = TRANSFERSTATE_ABORTED;
    g_SendCStringToSocket(m_client, "1:aborttransfer:");
  }
  else
    if (m_transfer_state == TRANSFERSTATE_PENDING)
    { // Ready to connect to the file-transfer server...
      m_transfer_state = TRANSFERSTATE_NONE;
      m_transfer_request = TRANSFERREQUEST_NONE;
      g_SendCStringToSocket(m_client, "1:requestcancel:");
      m_transfer_client->Destroy();
      m_transfer_client = NULL;
      m_clienttransferstaticText->SetLabel(wxEmptyString);
      m_clienttransferuploadbutton->Enable();
      m_clienttransferdynamicbutton->SetLabel(wxT("Download map"));
    }
    else
      if (m_transfer_state == TRANSFERSTATE_NONE)
      {
        if (m_transfer_request != TRANSFERREQUEST_NONE)
        {
          m_transfer_request = TRANSFERREQUEST_NONE;
          g_SendCStringToSocket(m_client, "1:requestcancel:");
          m_clienttransferstaticText->SetLabel(wxEmptyString);
          m_clienttransferuploadbutton->Enable();
          m_clienttransferdynamicbutton->SetLabel(wxT("Download map"));
        }
        else // "Download" button.
        {
          g_SendCStringToSocket(m_client, "1:requestdownload:");
          m_transfer_filename = m_usermap;
          m_transfer_request = TRANSFERREQUEST_DOWNLOAD;
          m_clienttransferstaticText->SetLabel(m_transfer_filename);
          m_clienttransferuploadbutton->Disable();
          m_clienttransferdynamicbutton->SetLabel(wxT("Abort transfer"));
        }
      } // In the case of ABORTED, we should wait for the file transfer thread end.

  // Disable button only when it should be a "Download" button afterwards,
  // and there's no missing map.
  // In other words, enable when the current state isn't NONE (or PENDING ==> NONE),
  // or there's a missing map.
  
  m_clienttransferdynamicbutton->Enable((m_transfer_state != TRANSFERSTATE_NONE) ||
                                        (m_launch_usermap &&
                                         (((m_game == GAME_DN3D) && (!wxFileExists(g_configuration->dn3d_maps_dir + wxFILE_SEP_PATH + m_usermap)))
                                          || ((m_game == GAME_SW) && (!wxFileExists(g_configuration->sw_maps_dir + wxFILE_SEP_PATH + m_usermap))))));
}
/*
void ClientRoomFrame::OnNatFreeCheck(wxCommandEvent& WXUNUSED(event))
{
  m_allow_sounds = true;
  g_configuration->gamelaunch_enable_natfree = m_clientroomnatfreecheckBox->GetValue();
  g_configuration->Save();
}
*/
void ClientRoomFrame::OnEnterText(wxCommandEvent& WXUNUSED(event))
{
  // get time if timestamp
  if (g_configuration->show_timestamp)
  {
	  DateTime = wxDateTime::Now();
	  time = DateTime.Format(wxT("%X"));
  }
  wxIPV4address l_addr;
  wxString l_text;
  wxArrayString l_str_list;
  size_t l_index, l_loop_var;
  m_allow_sounds = true;
  if (m_in_room)
  {
    l_text = m_clientinputtextCtrl->GetValue();
    if (l_text.IsEmpty())
    {
      if (g_configuration->play_snd_error)
        g_PlaySound(g_configuration->snd_error_file);
    }
    else
    {
		 // Is user wanting to change their nickname?
	if ((l_text.SubString(0,4) == wxT("/nick")) || (l_text.SubString(0,4) == wxT("/name"))){
		// Check that there isn't already a user with that nickname
		bool m_player_exists = false;
	for (l_loop_var = 0; l_loop_var < m_players_table.num_players; l_loop_var++){
     if (m_players_table.players[l_loop_var].nickname == l_text.SubString(6,0))
	 m_player_exists = true;}
	 if (!m_player_exists)
		{
		AddMessage(wxT("* ") + g_configuration->nickname + wxT(" is now known as ") + l_text.SubString(6,0) + wxT("."));
	l_str_list.Add(wxT("nickchange"));
	l_str_list.Add(g_configuration->nickname);
      l_str_list.Add(l_text.SubString(6,0));
	  // Update player list.
	  l_index = m_players_table.FindIndexByNick(g_configuration->nickname);
	  m_players_table.players[l_index].nickname = l_text.SubString(6,0);
	  g_configuration->nickname = l_text.SubString(6,0);
	  // If HDuke team DM, display player's team.
	  if ((m_dn3dgametype != DN3DMPGAMETYPE_DMSPAWN) && (m_dn3dgametype != DN3DMPGAMETYPE_COOP) && (m_dn3dgametype != DN3DMPGAMETYPE_DMNOSPAWN) && (m_srcport == SOURCEPORT_IDUKE3D)) 
		m_clientplayerslistCtrl->SetItem(l_index, 0, l_text.SubString(6,0) + wxT(" [") + m_players_table.players[l_index].team + wxT("]"));	
	else
		m_clientplayerslistCtrl->SetItem(l_index, 0, l_text.SubString(6,0));	
	  // Send to host
      g_SendStringListToSocket(m_client, l_str_list);
      m_clientinputtextCtrl->Clear();
	  }
	  else
		AddMessage(wxT("* Unable to change nickname as there is already a user in the room with that name."));  
	}
	else
	{ 
	l_str_list.Add(wxT("msg"));
      l_str_list.Add(l_text);
      g_SendStringListToSocket(m_client, l_str_list);
      m_clientinputtextCtrl->Clear();
	  // If timestamp
		if (g_configuration->show_timestamp)
		{
     AddMessage(wxT("[") + time + wxT("] ") + g_configuration->nickname + wxT(": ") + l_text);
	 }
	 else
	 {
	 AddMessage(g_configuration->nickname + wxT(": ") + l_text);
	 }
	}
      if (m_allow_sounds && g_configuration->play_snd_send)
        g_PlaySound(g_configuration->snd_send_file);
    }
  }
  else
  {
    AddMessage(wxT("* Can't send message while not really in room."));
    if (g_configuration->play_snd_error)
      g_PlaySound(g_configuration->snd_error_file);
  }
}

void ClientRoomFrame::OnMapSelectionClose(wxCommandEvent& WXUNUSED(event))
{
  if (m_mapselection_dialog->m_response && (m_game == m_mapselection_dialog->m_game))
  { // Second check is done in order to avoid a race condition!
    wxArrayString l_str_list;
    m_transfer_filename = m_mapselection_dialog->m_response_filename;
    m_transfer_request = TRANSFERREQUEST_UPLOAD;
    m_clienttransferstaticText->SetLabel(m_transfer_filename);
    m_clienttransferuploadbutton->Disable();
    m_clienttransferdynamicbutton->Enable();
    m_clienttransferdynamicbutton->SetLabel(wxT("Abort transfer"));
    l_str_list.Add(wxT("requestupload"));
    l_str_list.Add(m_transfer_filename);
    if (m_game == GAME_DN3D)
      m_transfer_filesize = (wxFileName::GetSize(g_configuration->dn3d_maps_dir +
                                                 wxFILE_SEP_PATH + m_transfer_filename)).ToULong();
    else
      m_transfer_filesize = (wxFileName::GetSize(g_configuration->sw_maps_dir +
                                                 wxFILE_SEP_PATH + m_transfer_filename)).ToULong();
    l_str_list.Add(wxString::Format(wxT("%d"), m_transfer_filesize));
    g_SendStringListToSocket(m_client, l_str_list);
  }
  // If we start an upload, we should enable the new "Abort transfer" button.
  // If we don't start an upload, should we enable the "Download" button?
  // Only when a map file selected by the host, if any, doesn't exist.
  m_clienttransferdynamicbutton->Enable((m_mapselection_dialog->m_response && (m_game == m_mapselection_dialog->m_game)) ||
                                        (m_launch_usermap &&
                                         (((m_game == GAME_DN3D) && (!wxFileExists(g_configuration->dn3d_maps_dir + wxFILE_SEP_PATH + m_usermap)))
                                          || ((m_game == GAME_SW) && (!wxFileExists(g_configuration->sw_maps_dir + wxFILE_SEP_PATH + m_usermap))))));
  m_mapselection_dialog->Destroy();
  m_mapselection_dialog = NULL;
}

void ClientRoomFrame::OnTransferThreadEnd(wxCommandEvent& WXUNUSED(event))
{
  wxString l_filename;
  bool l_no_download_completed = true;
  m_transfer_timer->Stop();
  delete m_transfer_timer;

  m_transfer_fp->Close();
  delete m_transfer_fp;
  m_transfer_fp = NULL;

  if (m_transfer_request == TRANSFERREQUEST_DOWNLOAD)
  {
    if (m_game == GAME_DN3D)
      l_filename = g_configuration->dn3d_maps_dir + wxFILE_SEP_PATH + m_transfer_filename;
    else
      l_filename = g_configuration->sw_maps_dir + wxFILE_SEP_PATH + m_transfer_filename;
    // Also make sure that the client hasn't left the room.
    if ((m_transfer_state == TRANSFERSTATE_BUSY) && (m_transfer_client != NULL)
        && (wxFileName::GetSize(l_filename) == m_transfer_filesize))
//  {
      l_no_download_completed = false;
//    m_usermap = m_transfer_filename;
//  }
    else
      if (wxFileExists(l_filename))
        wxRemoveFile(l_filename);
  }
  m_transfer_state = TRANSFERSTATE_NONE;

  if (m_transfer_client == NULL) // Close the room?
  {
    Destroy();
    return;
  }
  m_transfer_client = NULL;
  m_clienttransfergauge->SetValue(0);

/*if (m_transfer_awaiting_download)
  {
    m_transfer_awaiting_download = false;
    g_SendCStringToSocket(m_client, "1:requestdownload:");
    m_transfer_filename = m_usermap;
    m_transfer_request = TRANSFERREQUEST_DOWNLOAD;
    m_clienttransferstaticText->SetLabel(m_transfer_filename);
  }
  else
  {*/
    m_transfer_request = TRANSFERREQUEST_NONE;
    m_clienttransferstaticText->SetLabel(wxEmptyString);
    m_clienttransferuploadbutton->Enable();
    m_clienttransferdynamicbutton->SetLabel(wxT("Download map"));
    m_clienttransferdynamicbutton->Enable(l_no_download_completed && m_launch_usermap &&
                                          (((m_game == GAME_DN3D) && (!wxFileExists(g_configuration->dn3d_maps_dir + wxFILE_SEP_PATH + m_usermap)))
                                           || ((m_game == GAME_SW) && (!wxFileExists(g_configuration->sw_maps_dir + wxFILE_SEP_PATH + m_usermap)))));
//}
}

void ClientRoomFrame::OnFileTransferSocketEvent(wxSocketEvent& event)
{
//wxSocketBase *l_sock = event.GetSocket();
  if (!m_transfer_client) // Shutdown?
    return;
  wxString l_filename;
  char l_buffer[17]; // strlen("1:readyforupload:")
  if (m_transfer_state == TRANSFERSTATE_PENDING)
    switch (event.GetSocketEvent())
    {
      case wxSOCKET_CONNECTION: // Let's begin!
        if (m_transfer_request == TRANSFERREQUEST_DOWNLOAD)
        {
          m_transfer_client->Notify(false);
          if (m_game == GAME_DN3D)
          {
            if (!wxDirExists(g_configuration->dn3d_maps_dir))
              wxFileName::Mkdir(g_configuration->dn3d_maps_dir, 0777, wxPATH_MKDIR_FULL);
            l_filename = g_configuration->dn3d_maps_dir + wxFILE_SEP_PATH + m_transfer_filename;
          }
          else
          {
            if (!wxDirExists(g_configuration->sw_maps_dir))
              wxFileName::Mkdir(g_configuration->sw_maps_dir, 0777, wxPATH_MKDIR_FULL);
            l_filename = g_configuration->sw_maps_dir + wxFILE_SEP_PATH + m_transfer_filename;
          }
          m_transfer_fp = new wxFile(l_filename, wxFile::write);
          if (!m_transfer_fp->IsOpened())
          {
            wxLogError(wxT("Can't open file for writing!"));
            delete m_transfer_fp;
            m_transfer_fp = NULL;
            m_transfer_client->Notify(true);
          }
          else
          {
            m_transfer_thread = new FileReceiveThread(m_transfer_client, this, m_transfer_fp,
                                                      m_transfer_filesize, &m_transfer_state);
            if ( m_transfer_thread->Create() != wxTHREAD_NO_ERROR )
            {
              wxLogError(wxT("Can't create file-transfer thread!"));
              m_transfer_thread->Delete();
              m_transfer_fp->Close();
              delete m_transfer_fp;
              m_transfer_fp = NULL;
              m_transfer_client->Notify(true);
            }
            else
            {
              g_SendCStringToSocket(m_transfer_client, "1:readyfortransfer:");
              m_transfer_state = TRANSFERSTATE_BUSY;
              m_transfer_thread->Run();
              m_clienttransfergauge->SetRange(m_transfer_filesize);
              m_transfer_timer = new wxTimer(this, ID_CLIENTFILETRANSFERTIMER);
              m_transfer_timer->Start(500, false);
            }
          }
        }
        else
        {
          m_transfer_client->SetNotify(wxSOCKET_INPUT_FLAG | wxSOCKET_LOST_FLAG);
          g_SendCStringToSocket(m_transfer_client, "1:readyfortransfer:");
        }
        break;
      case wxSOCKET_INPUT:
        m_transfer_client->Read(l_buffer, 17); // strlen("1:readyforupload:")
        if ((m_transfer_request == TRANSFERREQUEST_UPLOAD)
            && (!memcmp(l_buffer, "1:readyforupload:", 17)))
        {
          m_transfer_client->Notify(false);
          if (m_game == GAME_DN3D)
            l_filename = g_configuration->dn3d_maps_dir + wxFILE_SEP_PATH + m_transfer_filename;
          else
            l_filename = g_configuration->sw_maps_dir + wxFILE_SEP_PATH + m_transfer_filename;
          m_transfer_fp = new wxFile(l_filename, wxFile::read);
          if (!m_transfer_fp->IsOpened())
          {
            wxLogError(wxT("Can't open file for reading!"));
            delete m_transfer_fp;
            m_transfer_fp = NULL;
            m_transfer_client->Notify(true);
          }
          else
          {
            m_transfer_thread = new FileSendThread(m_transfer_client, this, m_transfer_fp, &m_transfer_state);
            if ( m_transfer_thread->Create() != wxTHREAD_NO_ERROR )
            {
              wxLogError(wxT("Can't create file-transfer thread!"));
              m_transfer_thread->Delete();
              m_transfer_fp->Close();
              delete m_transfer_fp;
              m_transfer_fp = NULL;
              m_transfer_client->Notify(true);
            }
            else
            {
              m_transfer_state = TRANSFERSTATE_BUSY;
              m_transfer_thread->Run();
              m_clienttransfergauge->SetRange(m_transfer_filesize);
              m_transfer_timer = new wxTimer(this, ID_CLIENTFILETRANSFERTIMER);
              m_transfer_timer->Start(500, false);
            }
          }
        }
        break;
      case wxSOCKET_LOST:
        m_transfer_state = TRANSFERSTATE_NONE;
        m_transfer_request = TRANSFERREQUEST_NONE;
        m_transfer_client->Destroy();
        m_transfer_client = NULL;
        m_clienttransferstaticText->SetLabel(wxEmptyString);
        m_clienttransferuploadbutton->Enable();
        m_clienttransferdynamicbutton->SetLabel(wxT("Download map"));
        m_clienttransferdynamicbutton->Enable(m_launch_usermap &&
                                              (((m_game == GAME_DN3D) && (!wxFileExists(g_configuration->dn3d_maps_dir + wxFILE_SEP_PATH + m_usermap)))
                                               || ((m_game == GAME_SW) && (!wxFileExists(g_configuration->sw_maps_dir + wxFILE_SEP_PATH + m_usermap)))));
        break;
      default: ;
    }
}

void ClientRoomFrame::OnSocketEvent(wxSocketEvent& event)
{
  if (!m_client) // Shutdown?
    return;
//wxSocketBase *l_sock = event.GetSocket();
  wxArrayString l_str_list;
  size_t l_num_of_items, l_loop_var, l_index, l_num_of_read_attempts;
  long l_temp_num;
  bool l_script_is_ready;
  char l_short_buffer[MAX_COMM_TEXT_LENGTH];
  wxString l_temp_str, l_long_buffer;
  wxIPV4address l_addr;

  switch (event.GetSocketEvent())
  {
	case wxSOCKET_CONNECTION:
		// start automatic message to keep client connected.
		m_msg_timer = new wxTimer(this, ID_CLIENTMSGTIMER);
		m_msg_timer->Start(180000, wxTIMER_CONTINUOUS);
	break;
/*  case wxSOCKET_CONNECTION:
      m_client->SetTimeout(10);
      m_timer = new wxTimer(this, ID_CLIENTTIMER);
      m_timer->Start(30000, wxTIMER_ONE_SHOT);
      g_SendCStringToSocket(m_client, "1:requestroominfo:");
      AddMessage(wxT("Connected."));
//    Show(true);
//    if (g_manualjoin_dialog) // Should be.
//      g_manualjoin_dialog->Destroy();
      break;*/
    case wxSOCKET_INPUT:
      l_num_of_read_attempts = 0;
      do
      {
        m_client->Read(l_short_buffer, MAX_COMM_TEXT_LENGTH);
/*      if (m_client->Error())
        {
          AddMessage(wxString::Format(wxT("* A communication error has occured while trying to fetch data: %d"), m_client->LastError()));
          if (m_allow_sounds && g_configuration->play_snd_error)
            g_PlaySound(g_configuration->snd_error_file);
          break;
        }*/
        l_temp_num = m_client->LastCount();
        if (l_temp_num < MAX_COMM_TEXT_LENGTH)
          l_num_of_read_attempts = MAX_COMM_READ_ATTEMPTS;
        else
          l_num_of_read_attempts++;
        l_long_buffer << wxString(l_short_buffer, wxConvUTF8, l_temp_num);
        l_num_of_items = g_ConvertStringBufToStringList(l_long_buffer, l_str_list);
        while (l_num_of_items)
        {
          l_num_of_items = l_str_list.GetCount();
          if ((l_num_of_items == 2) && (l_str_list[0] == wxT("error")))
          {				
            m_client->SetNotify(0);
            m_client->Notify(false);
            m_client->Destroy();
            m_client = NULL;
            if (m_timer)
            {
              m_timer->Stop();
              delete m_timer;
              m_timer = NULL;
            }
            if (m_transfer_client)
            {
              if (m_transfer_state == TRANSFERSTATE_BUSY)
                m_transfer_state = TRANSFERSTATE_ABORTED;
              else
                if (m_transfer_state == TRANSFERSTATE_PENDING)
                {
                  m_transfer_client->Destroy();
                  m_transfer_client = NULL;
                }
//            m_transfer_awaiting_download = false;
            }
			// If there is a compatibility error, try joining as the net version.
			if (l_str_list[1] == "Server is hosted using an incompatible version of DUKEMATCHER.")
			{
			AddMessage(wxT("* ERROR: The host is using an incompatible version of Duke Matcher. Attempting to join the room in compatibility mode."));
			oldver = true;
			ConnectToServer(hostip,hostport);
			}
			else
			{		
            wxMessageBox(l_str_list[1], wxT("Error reported by host"), wxOK|wxICON_EXCLAMATION);
            if (m_transfer_client)
            {
              m_transfer_client = NULL;
              Hide();
            }
            else
              Destroy();
            g_main_frame->ShowMainFrame();
            return;
          }
		  }
          else
            if (((l_num_of_items == 10) || (l_num_of_items == 12) ||
                 (l_num_of_items == 13))
                && (l_str_list[0] == wxT("info")))
            {
              UpdateGameSettings(l_str_list);
              if (((m_srcport == SOURCEPORT_EDUKE32) && (!g_configuration->have_eduke32))
                  || ((m_srcport == SOURCEPORT_JFDUKE3D) && (!g_configuration->have_jfduke3d))
                  || ((m_srcport == SOURCEPORT_IDUKE3D) && (!g_configuration->have_iduke3d))
                  || ((m_srcport == SOURCEPORT_DUKE3DW32) && (!g_configuration->have_duke3dw32))
                  || ((m_srcport == SOURCEPORT_XDUKE) && (!g_configuration->have_xduke))
                  || ((m_srcport == SOURCEPORT_DOSDUKE) && (!g_configuration->have_dosduke))
                  || ((m_srcport == SOURCEPORT_JFSW) && (!g_configuration->have_jfsw))
                  || ((m_srcport == SOURCEPORT_SWP) && (!g_configuration->have_swp))
                  || ((m_srcport == SOURCEPORT_DOSSW) && (!g_configuration->have_dossw)))
              {
                if (m_in_room)
                {
                  AddMessage(wxT("* WARNING: You don't have the game source port currently selected by the host configured."));
                  if (m_allow_sounds && g_configuration->play_snd_error)
                    g_PlaySound(g_configuration->snd_error_file);
                }
                else
                {
                  m_client->SetNotify(0);
                  m_client->Notify(false);
                  m_client->Destroy();
                  m_client = NULL;
                  m_timer->Stop();
                  delete m_timer;
                  m_timer = NULL;
                  if (m_transfer_client)
                  {
                    if (m_transfer_state == TRANSFERSTATE_BUSY)
                      m_transfer_state = TRANSFERSTATE_ABORTED;
                    else
                      if (m_transfer_state == TRANSFERSTATE_PENDING)
                      {
                        m_transfer_client->Destroy();
                        m_transfer_client = NULL;
                      }
//                  m_transfer_awaiting_download = false;
                  }
                  wxMessageBox(wxT("The above game source port selected by the host is not configured."),
                               wxT("Game source port isn't configured."), wxOK|wxICON_EXCLAMATION);
                  if (m_transfer_client)
                  {
                    m_transfer_client = NULL;
                    Hide();
                  }
                  else
                    Destroy();
                  g_main_frame->ShowMainFrame();
                  return;
                }
              }
              else
                if (!m_in_room)
                {
                  m_timer->Start(3000, wxTIMER_ONE_SHOT);
                  l_str_list.Empty();
                  l_str_list.Add(wxT("join"));
				  if (oldver) {
					l_str_list.Add(DUKEMATCHER_STR_NET_VERSION);
					oldver = false;
					}
				  else
					l_str_list.Add(DUKEMATCHER_VERSION);
                  m_client->GetPeer(l_addr);
                  l_str_list.Add(l_addr.IPAddress());
                  m_client->GetLocal(l_addr);
                  l_str_list.Add(l_addr.IPAddress());
                  l_str_list.Add(wxString::Format(wxT("%d"), g_configuration->game_port_number));
                  l_str_list.Add(g_configuration->nickname);
                  g_SendStringListToSocket(m_client, l_str_list);
                }
            }
          else
            if ((!m_in_room) && (l_num_of_items % 4 == 1) && (l_num_of_items > 8)
                && (l_str_list[0] == wxT("playertable")))
            {
              m_timer->Stop();
              delete m_timer;
              m_timer = NULL;
              l_loop_var = 1;
              while (l_loop_var < l_num_of_items)
              {
                l_str_list[l_loop_var+3].ToLong(&l_temp_num);

                m_players_table.Add(l_str_list[l_loop_var],
                                    l_str_list[l_loop_var+2], l_temp_num);
                l_index = m_players_table.num_players - 1;

                m_clientplayerslistCtrl->InsertItem(l_index, m_players_table.players[l_index].nickname);
				if (g_configuration->theme_is_dark)
                  m_clientplayerslistCtrl->SetItemTextColour(l_index, *wxGREEN);
				 else
				 m_clientplayerslistCtrl->SetItemTextColour(l_index, *wxBLACK);	  
                m_clientplayerslistCtrl->SetItem(l_index, 3, l_str_list[l_loop_var+1]);
                m_clientplayerslistCtrl->SetItem(l_index, 4, m_players_table.players[l_index].ingameip +
                                                             wxString::Format(wxT(":%d"), m_players_table.players[l_index].game_port_number));
				m_players_table.players[l_index].peerdetectedip = l_str_list[l_loop_var+1];
//              RefreshListCtrlSize();
                l_loop_var += 4;
              }
              m_in_room = true;

              m_clientnumplayerstextCtrl->SetValue(wxString::Format(wxT("%d/%d"),
                                                                    m_players_table.num_players, m_max_num_players));

              m_clientsendbutton->Enable();
              if (m_launch_usermap &&
                  (((m_game == GAME_DN3D) && (!wxFileExists(g_configuration->dn3d_maps_dir + wxFILE_SEP_PATH + m_usermap)))
                   || ((m_game == GAME_SW) && (!wxFileExists(g_configuration->sw_maps_dir + wxFILE_SEP_PATH + m_usermap)))))
              {
                g_SendCStringToSocket(m_client, "1:requestdownload:");
                m_transfer_filename = m_usermap;
                m_transfer_request = TRANSFERREQUEST_DOWNLOAD;
                m_clienttransferstaticText->SetLabel(m_transfer_filename);
                m_clienttransferdynamicbutton->SetLabel(wxT("Abort transfer"));
                m_clienttransferdynamicbutton->Enable();
              }
              else
                m_clienttransferuploadbutton->Enable();
				// Ask host for ready status, team info and client version for all players and set our own
				 l_str_list.Empty();
                  l_str_list.Add(wxT("GetAdditionalPlayerInfo"));
                  g_SendStringListToSocket(m_client, l_str_list);
				  // Update player table with our version.
				  m_players_table.players[l_index].clientversion = DUKEMATCHER_VERSION;
				  // loop through all players to get location.
goto PULA;
				  for (l_loop_var = 0; l_loop_var < m_players_table.num_players; l_loop_var++)
				  {
					// get location from freegeoip.net unless was unable to obtain previously through this process.
					if (!locationerror)
					{
					wxHTTP get;
					get.SetHeader(_T("Content-type"), _T("text/html; charset=utf-8"));
					get.SetTimeout(3);
					get.Connect(wxT("freegeoip.net"));
					
					wxInputStream *httpStream = get.GetInputStream(_T("/csv/" + m_players_table.players[l_loop_var].peerdetectedip));

					if (get.GetError() == wxPROTO_NOERR)
					{
						wxString res, country, region, city;
						wxStringOutputStream out_stream(&res);
						httpStream->Read(out_stream);
						wxStringTokenizer tokens(res, wxT(","));
						long x = 0;
						while ( tokens.HasMoreTokens() )
					{
						wxString token = tokens.GetNextToken();
						if (x == 2){
							token.Remove(0,1);
							token.RemoveLast();
							country = token;
						}
						if (x == 4){
							token.Remove(0,1);
							token.RemoveLast();
							region = token;
						}
						if (x == 5){
							token.Remove(0,1);
							token.RemoveLast();
							city = token;
						}
						x++;
					}
					wxString location = city + ", " + region + ", " + country;
					m_clientplayerslistCtrl->SetItem(l_loop_var, 2, location);	
					}
					else
					{
						m_clientplayerslistCtrl->SetItem(l_loop_var, 2, "Unknown");
						locationerror=true;
					}

					wxDELETE(httpStream);
					get.Close();
					// end of getting location.	
					}
					else // if there was an error, display as unknown.
					{
					m_clientplayerslistCtrl->SetItem(l_loop_var, 2, "Unknown");
					}
				}
            }
          else
PULA:
            if ((l_num_of_items >= 3) && (l_str_list[0] == wxT("mod")))
            {
              UpdateModFilesInfo(l_str_list);
              l_num_of_items = m_modfiles.GetCount();
              l_str_list.Empty();
              l_temp_str = wxT("* The host has updated the game options.");
              if (m_game == GAME_DN3D)
                for (l_loop_var = 0; l_loop_var < l_num_of_items; l_loop_var++)
                {
                  l_temp_str << wxT(' ') << m_modfiles[l_loop_var];
                  if (!wxFileExists(g_configuration->dn3d_mods_dir + wxFILE_SEP_PATH + m_modfiles[l_loop_var]))
                    l_str_list.Add(m_modfiles[l_loop_var]);
                }
              else
                for (l_loop_var = 0; l_loop_var < l_num_of_items; l_loop_var++)
                {
                  l_temp_str << wxT(' ') << m_modfiles[l_loop_var];
                  if (!wxFileExists(g_configuration->sw_mods_dir + wxFILE_SEP_PATH + m_modfiles[l_loop_var]))
                    l_str_list.Add(m_modfiles[l_loop_var]);
                }
              AddMessage(l_temp_str);
              l_num_of_items = l_str_list.GetCount();
              if (l_num_of_items)
              {
                l_temp_str = wxT("* WARNING: The host has selected a TC/MOD that cannot not found in your TC/MOD directory: ");
                for (l_loop_var = 0; l_loop_var < l_num_of_items; l_loop_var++)
                  l_temp_str << wxT(' ') << l_str_list[l_loop_var];
                AddMessage(l_temp_str);
                if (m_showmodurl)
                  AddMessage(wxT("* The following URL is provided by the host: ") + m_modurl);
                if (m_allow_sounds && g_configuration->play_snd_error)
                  g_PlaySound(g_configuration->snd_error_file);
              }
            }
          else // The following cases are only relevant while in room.
            if (m_in_room)
            {
			  // Additional Player Info from host
				if (l_str_list[0] == wxT("AdditionalPlayerInfo"))
				{
				  l_loop_var = 1;
				  while (l_loop_var < l_num_of_items)
				  {
					l_index = m_players_table.FindIndexByNick(l_str_list[l_loop_var]);
					m_players_table.players[l_index].team = l_str_list[l_loop_var+1];
					if ((m_dn3dgametype != DN3DMPGAMETYPE_DMSPAWN) && (m_dn3dgametype != DN3DMPGAMETYPE_COOP) && (m_dn3dgametype != DN3DMPGAMETYPE_DMNOSPAWN) && (m_srcport == SOURCEPORT_IDUKE3D)) 
					  m_clientplayerslistCtrl->SetItem(l_index, 0, l_str_list[l_loop_var] + wxT(" [") + l_str_list[l_loop_var+1] + wxT("]"));
					m_players_table.players[l_index].status = l_str_list[l_loop_var+2];
					m_clientplayerslistCtrl->SetItem(l_index, 1, l_str_list[l_loop_var+2]);
					m_players_table.players[l_index].clientversion = l_str_list[l_loop_var+3];
					// If other player's client version differs, change font colour to grey.
					if (m_players_table.players[l_index].clientversion != DUKEMATCHER_VERSION){
					m_clientplayerslistCtrl->SetItemTextColour(l_index, *wxLIGHT_GREY);
					}
	//             	RefreshListCtrlSize();
					l_loop_var += 4;
				  }
				  // Change ready status to 'Not Ready'.
				l_index = m_players_table.FindIndexByNick(g_configuration->nickname);
				m_clientreadycheckBox->SetValue(false); 
				m_clientplayerslistCtrl->SetItem(l_index, 1, "Not Ready");
				l_str_list.Empty();
				l_str_list.Add(wxT("changeready"));
				l_str_list.Add("Not Ready");
				g_SendStringListToSocket(m_client, l_str_list);
				// If client version differs to the host, the client needs to be notified.
				if (m_players_table.players[l_index].clientversion != m_players_table.players[0].clientversion)
				{
					AddMessage(wxT("* WARNING: You are running Duke Matcher version ") + m_players_table.players[l_index].clientversion + wxT(" but the host is running version ") + m_players_table.players[0].clientversion + wxT(". Some features may be unavailable."));
					if (g_configuration->play_snd_error)
						g_PlaySound(g_configuration->snd_error_file);
				}
				}
				//end of additional player info
              if ((l_num_of_items == 3) && (l_str_list[0] == wxT("hostaddrupdate")))
              {
                m_players_table.players[0].ingameip = l_str_list[2];
                m_clientplayerslistCtrl->SetItem(l_index, 3, l_str_list[1]);
                m_clientplayerslistCtrl->SetItem(l_index, 4, l_str_list[2] +
                                                             wxString::Format(wxT(":%d"), m_players_table.players[0].game_port_number));
					// get location from freegeoip.net
					wxHTTP get;
					get.SetHeader(_T("Content-type"), _T("text/html; charset=utf-8"));
					get.SetTimeout(3);
					get.Connect(wxT("freegeoip.net"),false);
					
					wxInputStream *httpStream = get.GetInputStream(_T("/csv/" + l_str_list[1]));

					if (get.GetError() == wxPROTO_NOERR)
					{
						wxString res, country, region, city;
						wxStringOutputStream out_stream(&res);
						httpStream->Read(out_stream);
						wxStringTokenizer tokens(res, wxT(","));
						long x = 0;
						while ( tokens.HasMoreTokens() )
					{
						wxString token = tokens.GetNextToken();
						if (x == 2){
							token.Remove(0,1);
							token.RemoveLast();
							country = token;
						}
						if (x == 4){
							token.Remove(0,1);
							token.RemoveLast();
							region = token;
						}
						if (x == 5){
							token.Remove(0,1);
							token.RemoveLast();
							city = token;
						}
						x++;
					}
					wxString location = city + ", " + region + ", " + country;
					m_clientplayerslistCtrl->SetItem(l_index, 2, location);	
					}
					else
					{
						m_clientplayerslistCtrl->SetItem(l_index, 2, "Unknown");
					}

					wxDELETE(httpStream);
					get.Close();
					// end of getting location.	 
//              RefreshListCtrlSize();
              }

              if ((l_num_of_items == 3) && (l_str_list[0] == wxT("msg")) && (l_str_list[1] != g_configuration->nickname))
              {
				// get time if showing timestamp

				if (g_configuration->show_timestamp)
				{
				DateTime = wxDateTime::Now();
				time = DateTime.Format(wxT("%X"));
				}
				// Output message and sound if not automated message (heartbeat) from host.
				if (l_str_list[2] != wxT("#(HeartBeat)@ :"))
				{
				// If warning message, display no timestamp and output error sound.
				if (l_str_list[1] == wxT("* WARNING"))
				{
					AddMessage(l_str_list[1] + wxT(": ") + l_str_list[2]);
					if (g_configuration->play_snd_error)
						g_PlaySound(g_configuration->snd_error_file);
				}
				else
				{
				// If timestamp
				if (g_configuration->show_timestamp)
				{
					AddMessage(wxT("[") + time + wxT("] ") + l_str_list[1] + wxT(": ") + l_str_list[2]);
				}
				else
				{
					AddMessage(l_str_list[1] + wxT(": ") + l_str_list[2]);
				}
				
                if (m_allow_sounds && g_configuration->play_snd_receive)
                  g_PlaySound(g_configuration->snd_receive_file);
              }
			  }
			  }
			  else
			   if (l_str_list[0] == wxT("nickchange"))
                {
					AddMessage(wxT("* ") + l_str_list[1] + wxT(" is now known as ") + l_str_list[2] + wxT("."));
				l_index = m_players_table.FindIndexByNick(l_str_list[1]);
				if ((m_dn3dgametype != DN3DMPGAMETYPE_DMSPAWN) && (m_dn3dgametype != DN3DMPGAMETYPE_COOP) && (m_dn3dgametype != DN3DMPGAMETYPE_DMNOSPAWN) && (m_srcport == SOURCEPORT_IDUKE3D))
					m_clientplayerslistCtrl->SetItem(l_index, 0, l_str_list[2] + wxT(" [") + m_players_table.players[l_index].team + wxT("]"));
				else
					m_clientplayerslistCtrl->SetItem(l_index, 0, l_str_list[2]);
				m_players_table.players[l_index].nickname = l_str_list[2];
				}
				else
			// If user is changing team
			   if (l_str_list[0] == wxT("changeteam"))
                {
				l_index = m_players_table.FindIndexByNick(l_str_list[1]);
				l_temp_str = l_str_list[2];
				m_players_table.players[l_index].team = l_temp_str;
				m_clientplayerslistCtrl->SetItem(l_index, 0, m_players_table.players[l_index].nickname + wxT(" [") + l_temp_str + wxT("]"));	
				}
				else
			// If user is changing ready
			   if (l_str_list[0] == wxT("changeready"))
                {
				l_index = m_players_table.FindIndexByNick(l_str_list[1]);
				m_clientplayerslistCtrl->SetItem(l_index, 1, l_str_list[2]);	
				m_players_table.players[l_index].status = l_str_list[2];
				}
              else
                if ((l_num_of_items == 5) && (l_str_list[0] == wxT("joined")))
                {
				wxString l_temp = l_str_list[2];
                  l_str_list[4].ToLong(&l_temp_num);

                  m_players_table.Add(l_str_list[1], l_str_list[3], l_temp_num);
                  l_index = m_players_table.num_players - 1;
					
                  m_clientplayerslistCtrl->InsertItem(l_index, m_players_table.players[l_index].nickname);
				  m_clientplayerslistCtrl->SetItem(l_index, 1, "Ready");
                  m_clientplayerslistCtrl->SetItem(l_index, 3, l_str_list[2]);
                  m_clientplayerslistCtrl->SetItem(l_index, 4, m_players_table.players[l_index].ingameip + wxT(':') + l_str_list[4]);
//                RefreshListCtrlSize();
                  m_clientnumplayerstextCtrl->SetValue(wxString::Format(wxT("%d/%d"),
                                                                        m_players_table.num_players, m_max_num_players));
                  AddMessage(wxT("* ") + m_players_table.players[l_index].nickname + wxT(" has joined the room."));
                  if (m_allow_sounds && g_configuration->play_snd_join)
                    g_PlaySound(g_configuration->snd_join_file);
					m_players_table.players[l_index].team = "No Team";
					// If team dm, display [No Team] next to nickname, since user has not yet chosen a team.
				  if ((m_dn3dgametype != DN3DMPGAMETYPE_DMSPAWN) && (m_dn3dgametype != DN3DMPGAMETYPE_COOP) && (m_dn3dgametype != DN3DMPGAMETYPE_DMNOSPAWN) && (m_srcport == SOURCEPORT_IDUKE3D)) 
					m_clientplayerslistCtrl->SetItem(l_index, 0, m_players_table.players[l_index].nickname + wxT(" [") + m_players_table.players[l_index].team + wxT("]"));
					// ask host for new client's version.
					l_str_list.Empty();
					l_str_list.Add(wxT("GetClientVersion"));
					l_str_list.Add(m_players_table.players[l_index].nickname);
					g_SendStringListToSocket(m_client, l_str_list);
						  // get location from freegeoip.net
					wxHTTP get;
					get.SetHeader(_T("Content-type"), _T("text/html; charset=utf-8"));
					get.SetTimeout(3);
					get.Connect(wxT("freegeoip.net"),false);
					
					wxInputStream *httpStream = get.GetInputStream(_T("/csv/" + l_temp));

					if (get.GetError() == wxPROTO_NOERR)
					{
						wxString res, country, region, city;
						wxStringOutputStream out_stream(&res);
						httpStream->Read(out_stream);
						wxStringTokenizer tokens(res, wxT(","));
						long x = 0;
						while ( tokens.HasMoreTokens() )
					{
						wxString token = tokens.GetNextToken();
						if (x == 2){
							token.Remove(0,1);
							token.RemoveLast();
							country = token;
						}
						if (x == 4){
							token.Remove(0,1);
							token.RemoveLast();
							region = token;
						}
						if (x == 5){
							token.Remove(0,1);
							token.RemoveLast();
							city = token;
						}
						x++;
					}
					wxString location = city + ", " + region + ", " + country;
					m_clientplayerslistCtrl->SetItem(l_index, 2, location);	
					}
					else
					{
						m_clientplayerslistCtrl->SetItem(l_index, 2, "Unknown");
					}

					wxDELETE(httpStream);
					get.Close();
					// end of getting location.	 
                }
              else
                if ((l_num_of_items == 2) &&
                    ((l_str_list[0] == wxT("left")) || (l_str_list[0] == wxT("disconnect"))))
                {
                  l_temp_str = l_str_list[1];
                  l_index = m_players_table.FindIndexByNick(l_temp_str);
                  if (((l_index != m_players_table.num_players))
                      && (l_temp_str != g_configuration->nickname))
                  {
                    m_players_table.DelByIndex(l_index);

                    m_clientplayerslistCtrl->DeleteItem(l_index);
//                  RefreshListCtrlSize();

                    m_clientnumplayerstextCtrl->SetValue(wxString::Format(wxT("%d/%d"),
                                                                          m_players_table.num_players, m_max_num_players));
                    if (l_str_list[0] == wxT("left"))
                      AddMessage(wxT("* ") + l_temp_str + wxT(" has left the room."));
                    else
                      AddMessage(wxT("* ") + l_temp_str + wxT(" has got disconnected from the room."));
                    if (m_allow_sounds && g_configuration->play_snd_leave)
                      g_PlaySound(g_configuration->snd_leave_file);
                  }
                }
              else
                if ((l_num_of_items == 2) && (l_str_list[0] == wxT("kicked")))
                {
                  l_temp_str = l_str_list[1];
                  if (l_temp_str == g_configuration->nickname)
                  {
                    m_client->SetNotify(0);
                    m_client->Notify(false);
                    m_client->Destroy();
                    m_client = NULL;
                    if (m_transfer_client)
                    {
                      if (m_transfer_state == TRANSFERSTATE_BUSY)
                        m_transfer_state = TRANSFERSTATE_ABORTED;
                      else
                        if (m_transfer_state == TRANSFERSTATE_PENDING)
                        {
                          m_transfer_client->Destroy();
                          m_transfer_client = NULL;
                        }
//                    m_transfer_awaiting_download = false;
                    }
                    wxMessageBox(wxT("You've been kicked from the room."), wxT("Kick notification"), wxOK);
                    if (m_transfer_client)
                    {
                      m_transfer_client = NULL;
                      Hide();
                    }
                    else
                      Destroy();
                    g_main_frame->ShowMainFrame();
                    return;
                  }
                  else
                  {
                    l_index = m_players_table.FindIndexByNick(l_temp_str);
                    if (l_index != m_players_table.num_players)
                    {
                      m_players_table.DelByIndex(l_index);

                      m_clientplayerslistCtrl->DeleteItem(l_index);
//                    RefreshListCtrlSize();

                      m_clientnumplayerstextCtrl->SetValue(wxString::Format(wxT("%d/%d"),
                                                                            m_players_table.num_players, m_max_num_players));
                      AddMessage(wxT("* ") + l_temp_str + wxT(" has been kicked."));
                      if (m_allow_sounds && g_configuration->play_snd_leave)
                        g_PlaySound(g_configuration->snd_leave_file);
                    }
                  }
                }
				// If we have received client version for a specific nickname.
				else
			   if (l_str_list[0] == wxT("GetClientVersion"))
                {
					l_index = m_players_table.FindIndexByNick(l_str_list[1]);
					m_players_table.players[l_index].clientversion = l_str_list[2];
					// If other player's version differs, change font colour to grey.
					if (m_players_table.players[l_index].clientversion != DUKEMATCHER_VERSION){
					m_clientplayerslistCtrl->SetItemTextColour(l_index, *wxLIGHT_GREY);
					}
				}
              else
                if ((l_num_of_items == 2) && (l_str_list[0] == wxT("downloadaccept")) &&
                    (m_transfer_state == TRANSFERSTATE_NONE) && (m_transfer_request == TRANSFERREQUEST_DOWNLOAD))
                {
                  if (l_str_list[1].ToLong(&l_temp_num))
                    if (l_temp_num >= 0)
                    {
                      m_transfer_filesize = l_temp_num;
                      m_transfer_state = TRANSFERSTATE_PENDING;
                      m_client->GetPeer(l_addr);
                      m_transfer_client = new wxSocketClient();
                      m_transfer_client->SetEventHandler(*this, ID_CLIENTFILETRANSFERSOCKET);
                      m_transfer_client->SetFlags(wxSOCKET_NONE|wxSOCKET_BLOCK);
                      m_transfer_client->SetTimeout(10);
                      m_transfer_client->SetNotify(wxSOCKET_CONNECTION_FLAG | wxSOCKET_LOST_FLAG);
                      m_transfer_client->Notify(true);
                      m_transfer_client->Connect(l_addr, false);
                    }
                }
              else
                if ((l_num_of_items == 1) && (l_str_list[0] == wxT("uploadaccept")) &&
                    (m_transfer_state == TRANSFERSTATE_NONE) && (m_transfer_request == TRANSFERREQUEST_UPLOAD))
                {
                  m_transfer_state = TRANSFERSTATE_PENDING;
                  m_client->GetPeer(l_addr);
                  m_transfer_client = new wxSocketClient();
                  m_transfer_client->SetEventHandler(*this, ID_CLIENTFILETRANSFERSOCKET);
                  m_transfer_client->SetFlags(wxSOCKET_NONE|wxSOCKET_BLOCK);
                  m_transfer_client->SetTimeout(10);
                  m_transfer_client->SetNotify(wxSOCKET_CONNECTION_FLAG | wxSOCKET_LOST_FLAG);
                  m_transfer_client->Notify(true);
                  m_transfer_client->Connect(l_addr, false);
                }
              else
                if ((l_num_of_items == 1) && (l_str_list[0] == wxT("requestrefuse")) &&
                    ((m_transfer_state == TRANSFERSTATE_PENDING) ||
                     ((m_transfer_state == TRANSFERSTATE_NONE) && (m_transfer_request != TRANSFERREQUEST_NONE))))
                {
                  if (m_transfer_state == TRANSFERSTATE_PENDING)
                  {
                    m_transfer_state = TRANSFERSTATE_NONE;
                    m_transfer_client->Destroy();
                    m_transfer_client = NULL;
                  }
                  m_transfer_request = TRANSFERREQUEST_NONE;
                  m_clienttransferstaticText->SetLabel(wxEmptyString);
                  m_clienttransferuploadbutton->Enable();
                  m_clienttransferdynamicbutton->SetLabel(wxT("Download map"));
                  m_clienttransferdynamicbutton->Enable(m_launch_usermap &&
                                                        (((m_game == GAME_DN3D) && (!wxFileExists(g_configuration->dn3d_maps_dir + wxFILE_SEP_PATH + m_usermap)))
                                                         || ((m_game == GAME_SW) && (!wxFileExists(g_configuration->sw_maps_dir + wxFILE_SEP_PATH + m_usermap)))));
                }
              else
                if ((l_num_of_items == 1) && (l_str_list[0] == wxT("aborttransfer"))
                    && (m_transfer_state == TRANSFERSTATE_BUSY))
                  m_transfer_state = TRANSFERSTATE_ABORTED;
              else
                if ((l_num_of_items == 1) && (l_str_list[0] == wxT("launch")))
                {
//                m_in_game = true;
#ifdef __WXMSW__
                  l_temp_str = wxT("\\dukematcher.bat");
#elif (defined __WXMAC__) || (defined __WXCOCOA__)
                  l_temp_str = wxT("/dukematcher.command");
#else
                  l_temp_str = wxT("/dukematcher.sh");
#endif
                  m_allow_sounds = !(g_configuration->mute_sounds_while_ingame);
				  // get team
				  l_index = m_players_table.FindIndexByNick(g_configuration->nickname);
				    m_team = m_players_table.players[l_index].team;
                  l_script_is_ready = g_MakeLaunchScript(l_temp_str, &m_players_table,
                                                         m_players_table.FindIndexByNick(g_configuration->nickname),
                                                         m_game, m_srcport,
                                                         false, 0, false, wxEmptyString,
                                                         false, wxEmptyString, m_skill,
                                                         m_dn3dgametype, m_dn3dspawn, m_usemasterslave,
                                                         m_launch_usermap, m_usermap,
                                                         m_epimap_num, m_modfiles, m_team, m_extra_args);
                  if (l_script_is_ready)
                  {
                    AddMessage(wxT("* Launching game..."));
#ifdef __WXMSW__
                    wxExecute(wxT('"') + l_temp_str + wxT('"'));
#else
                    wxExecute(g_configuration->terminal_fullcmd + wxT(" \"") + l_temp_str + wxT('"'));
#endif
                  }
				  // Change ready status to 'Not Ready' for next game.
				m_clientreadycheckBox->SetValue(false); 
				m_clientplayerslistCtrl->SetItem(l_index, 1, "Not Ready");
				l_str_list.Empty();
				l_str_list.Add(wxT("changeready"));
				l_str_list.Add("Not Ready");
				g_SendStringListToSocket(m_client, l_str_list);
                }
              else
                if ((l_num_of_items == 1) && (l_str_list[0] == wxT("shutdown")))
                {
                  m_client->SetNotify(0);
                  m_client->Notify(false);
                  m_client->Destroy();
                  m_client = NULL;
                  if (m_transfer_client)
                  {
                    if (m_transfer_state == TRANSFERSTATE_BUSY)
                      m_transfer_state = TRANSFERSTATE_ABORTED;
                    else
                      if (m_transfer_state == TRANSFERSTATE_PENDING)
                      {
                        m_transfer_client->Destroy();
                        m_transfer_client = NULL;
                      }
//                  m_transfer_awaiting_download = false;
                  }
                  wxMessageBox(wxT("The host has closed the room."), wxT("Closed room notification"), wxOK|wxICON_INFORMATION);
                  if (m_transfer_client)
                  {
                    m_transfer_client = NULL;
                    Hide();
                  }
                  else
                    Destroy();
                  g_main_frame->ShowMainFrame();
                  return;
                }
            }
          l_num_of_items = g_ConvertStringBufToStringList(l_long_buffer, l_str_list);
        }
      }
      while (l_num_of_read_attempts < MAX_COMM_READ_ATTEMPTS);
      break;
    case wxSOCKET_LOST:
      m_client->SetNotify(0);
      m_client->Notify(false);
      m_client->Destroy();
      m_client = NULL;
      if (m_timer)
      {
        m_timer->Stop();
        delete m_timer;
        m_timer = NULL;
      }
/*    wxMessageBox(wxT("Connection timed out or lost."), wxEmptyString, wxOK);
      if (g_manualjoin_dialog)
        g_manualjoin_dialog->Enable();
      else
        g_main_frame->Show();*/
      if (m_transfer_client)
      {
        if (m_transfer_state == TRANSFERSTATE_BUSY)
          m_transfer_state = TRANSFERSTATE_ABORTED;
        else
          if (m_transfer_state == TRANSFERSTATE_PENDING)
          {
            m_transfer_client->Destroy();
            m_transfer_client = NULL;
          }
//      m_transfer_awaiting_download = false;
      }
	  if (m_transfer_client)
      {
        m_transfer_client = NULL;
      }
      break;
    default: ;
  }
}

void ClientRoomFrame::OnLeaveRoom(wxCommandEvent& WXUNUSED(event))
{
  Close();
}

void ClientRoomFrame::OnTimer(wxTimerEvent& WXUNUSED(event))
{
  if (m_timer != NULL)
  {
    delete m_timer;
    m_client->Destroy();
    m_client = NULL;
    wxMessageBox(wxT("Timed out while waiting for an appropriate response from the host."),
                 wxEmptyString, wxOK);
    Destroy();
    g_main_frame->ShowMainFrame();
  }
}

void ClientRoomFrame::OnClientMsgTimer(wxTimerEvent& WXUNUSED(event))
{
wxArrayString l_str_list2;
l_str_list2.Add(wxT("msg"));
l_str_list2.Add(wxT("#(HeartBeat)@ :"));
g_SendStringListToSocket(m_client, l_str_list2);
}

void ClientRoomFrame::OnChangeReady(wxCommandEvent& WXUNUSED(event))
{
size_t l_index;
wxString l_temp_str;
wxArrayString l_str_list;
if (m_clientreadycheckBox->GetValue())
l_temp_str = "Ready";
else
l_temp_str = "Not Ready";
l_index = m_players_table.FindIndexByNick(g_configuration->nickname);
m_players_table.players[l_index].status = l_temp_str;
m_clientplayerslistCtrl->SetItem(l_index, 1, l_temp_str);
// Send info to host
l_str_list.Empty();
l_str_list.Add(wxT("changeready"));
l_str_list.Add(l_temp_str);
g_SendStringListToSocket(m_client, l_str_list);
}

void ClientRoomFrame::OnChangeTeam(wxCommandEvent& WXUNUSED(event))
{
size_t l_index;
wxString l_temp_str;
wxArrayString l_str_list;
l_index = m_players_table.FindIndexByNick(g_configuration->nickname);
l_temp_str = m_clientteamchoice->GetStringSelection();
m_players_table.players[l_index].team = l_temp_str;
m_clientplayerslistCtrl->SetItem(l_index, 0, g_configuration->nickname + wxT(" [") + l_temp_str + wxT("]"));
// Send team info to host.
l_str_list.Empty();
l_str_list.Add(wxT("changeteam"));
l_str_list.Add(l_temp_str);
g_SendStringListToSocket(m_client, l_str_list);
}

void ClientRoomFrame::OnFileTransferTimer(wxTimerEvent& WXUNUSED(event))
{
  wxFileOffset l_position;
  if (m_transfer_fp)
  {
    l_position = m_transfer_fp->Tell();
    m_clienttransfergauge->SetValue(l_position);
// Temporary wrapper for a possible bug with MinGW?
#if (defined __WXMSW__) && (wxCHECK_VERSION(2, 8, 9)) && (!wxCHECK_VERSION(2, 8, 10))
    m_clienttransferstaticText->SetLabel(m_transfer_filename + wxString::Format(wxT(": %d / "), l_position)
                                                             + wxString::Format(wxT("%d"), m_clienttransfergauge->GetRange()));
#else
    m_clienttransferstaticText->SetLabel(m_transfer_filename + wxString::Format(wxT(": %d / %d"), l_position, m_clienttransfergauge->GetRange()));
#endif
  }
}

void ClientRoomFrame::OnCloseWindow(wxCloseEvent& WXUNUSED(event))
{
#ifdef ENABLE_UPNP
  UPNPUnForwardThread* l_thread = new UPNPUnForwardThread(g_configuration->game_port_number, false);
  if (l_thread->Create() == wxTHREAD_NO_ERROR )
    l_thread->Run();
  else
    l_thread->Delete();
#endif
  if (m_timer)
  {
    m_timer->Stop();
    delete m_timer;
    m_timer = NULL;
  }
  if (m_transfer_client)
  {
    if (m_transfer_state == TRANSFERSTATE_BUSY)
    {
      m_transfer_state = TRANSFERSTATE_ABORTED;
      g_SendCStringToSocket(m_client, "1:aborttransfer:");
      m_transfer_client = NULL;
      Hide();
    }
    else
      if (m_transfer_state == TRANSFERSTATE_PENDING)
      {
        m_transfer_client->Destroy();
        m_transfer_client = NULL;
        Destroy();
      }
//  m_transfer_awaiting_download = false;
  }
  else
    Destroy();
  g_SendCStringToSocket(m_client, "1:leave:");
  m_client->Destroy();
  m_client = NULL;
  g_main_frame->ShowMainFrame();
}
