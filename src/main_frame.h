/*************************************************************************

Copyright 2008-2009 - NY00123

This file is part of DUKEMATCHER.

DUKEMATCHER is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

DUKEMATCHER is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DUKEMATCHER; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*************************************************************************/

#ifndef _DUKEMATCHER_MAINFRAME_H_
#define _DUKEMATCHER_MAINFRAME_H_

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/wx.h>
#include <wx/socket.h>
#include <wx/listctrl.h>
#endif

#include "comm_txt.h"
#include "theme.h"

#define ID_OPENSPDIALOG 1000
#define ID_OPENMPDIALOG 1001
//#define ID_OPENMANUALJOINDIALOG 1002
#define ID_CHANGETHEME 1003
#define ID_OPENSRCPORTSETTINGS 1004
#define ID_OPENNETWORKSETTINGS 1005
#define ID_OPENADVANCEDSETTINGS 1006
#define ID_CHECKFORUPDATES 1007
#define ID_ALWAYSCHECKFORUPDATES 1008
#define ID_VISITWEBSITE 1009
#define ID_SERVERLIST 1010
#define ID_SERVERLISTAUTOCHECK 1011
#define ID_MANUALJOINADD 1012
#define ID_MANUALJOINLIST 1013
#define ID_MANUALJOINREM 1014
#define ID_GETLISTSOCKET 1015
#define ID_GETLISTTIMER 1016

WX_DECLARE_OBJARRAY(wxArrayString, ArrayOfStringArrays);

class MainFrame : public DUKEMATCHERFrame
{
public:
  MainFrame();
//void RefreshListCtrlSize();
  void OnChangeTheme(wxCommandEvent& event);
  void OnLoadSPDialog(wxCommandEvent& event);
  void OnLoadMPDialog(wxCommandEvent& event);
//void OnLoadManualJoinDialog(wxCommandEvent& event);
  void OnLoadSRCPortsDialog(wxCommandEvent& event);
  void OnLoadNetworkDialog(wxCommandEvent& event);
  void OnLoadAdvancedDialog(wxCommandEvent& event);
  void OnVisitWebsite(wxCommandEvent& event);
  void OnCheckForUpdates(wxCommandEvent& event);
  void OnAutoCheckForUpdates(wxCommandEvent& event);
  void OnAbout(wxCommandEvent& event);
  void OnQuit(wxCommandEvent& event);
  void OnServerListAutoCheck(wxCommandEvent& event);
  void Join();
  void OnJoin(wxCommandEvent& event);
  void OnHostActivate(wxListEvent& event);
  void OnHostSelect(wxListEvent& event);
  void OnAddHost(wxCommandEvent& event);
  void OnRemHost(wxCommandEvent& event);
  void OnManualJoinHostSelect(wxCommandEvent& event);
  void Connect();
  void TryNextServer();
  void RefreshList();
  void OnGetList(wxCommandEvent& event);
  void OnSocketEvent(wxSocketEvent& event);
  void OnTimer(wxTimerEvent& event);
  void OnClose(wxCloseEvent& event);

  void ShowMainFrame();
  void HideMainFrame();

private:
  wxMenuBar* m_Mainmenubar;
//DUKEMATCHERMenuBar* m_Mainmenubar;
  wxMenu* m_Actionmenu;
  wxMenu* m_Settingsmenu;
  wxMenu* m_Helpmenu;
  wxMenuItem* m_checkforupdatesonstartupmenuItem; // Has a CheckBox.
  DUKEMATCHERPanel* m_mainpanel;
  DUKEMATCHERListCtrl* m_serverlistCtrl;
  DUKEMATCHERListBox* m_playerlistBox;
  DUKEMATCHERButton* m_getlistbutton;
  wxStaticText* m_serverliststaticText;
  DUKEMATCHERCheckBox* m_autolookupcheckBox;
  wxStaticText* m_manualjoinnamestaticText;
  wxStaticText* m_manualjoinhostaddrstaticText;
  wxStaticText* m_manualjoinportnumstaticText;
  DUKEMATCHERTextCtrl* m_manualjoinnametextCtrl;
  DUKEMATCHERTextCtrl* m_manualjoinhostaddrtextCtrl;
  wxStaticText* m_manualjoinseparatorstaticText;
  DUKEMATCHERTextCtrl* m_manualjoinportnumtextCtrl;
  DUKEMATCHERButton* m_manualjoinaddbutton;
  DUKEMATCHERChoice* m_manualjoinchoice;
  DUKEMATCHERButton* m_manualjoinrembutton;
  DUKEMATCHERButton* m_joinbutton;
  wxStatusBar* m_MainstatusBar;
//DUKEMATCHERStatusBar* m_MainstatusBar;

  bool m_check_default_lists, m_wait_for_nicks;
  size_t m_curr_room, /*m_last_server, */m_server_in_curr_list, m_servers_to_count;
  wxTimer* m_serverlist_timer;
  wxSocketClient* m_client;

  ArrayOfStringArrays m_nicknames;
  wxArrayString m_addrs, m_numplayers_limits;
  wxArrayLong m_portnums;

  char m_short_buffer[MAX_COMM_TEXT_LENGTH];
  wxString m_long_buffer;

  DECLARE_EVENT_TABLE()
};

#endif
