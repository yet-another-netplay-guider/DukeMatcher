/*************************************************************************

Copyright 2008-2009 - NY00123

This file is part of DUKEMATCHER.

DUKEMATCHER is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

DUKEMATCHER is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DUKEMATCHER; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*************************************************************************/

#ifndef _DUKEMATCHER_CLIENTFRAME_H_
#define _DUKEMATCHER_CLIENTFRAME_H_

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/wx.h>
#include <wx/listctrl.h>
#include <wx/socket.h>
#include <wx/datetime.h>
#include <wx/utils.h>
#include <wx/timer.h>
#include <wx/choice.h>
#endif

#include "config.h"
#include "mp_common.h"
#include "mapselection_dialog.h"
#include "theme.h"

#define ID_CLIENTTRANSFERUPLOAD 1000
#define ID_CLIENTGENERALTRANSFERPURPOSE 1001
#define ID_CLIENTOUTPUTTEXT 1002
#define ID_CLIENTINPUTTEXT 1003
#define ID_CLIENTSENDTEXT 1004
//#define ID_CLIENTNATFREE 1005
//#define ID_CLIENTSENDPM 1006

enum { ID_CLIENTSOCKET = wxID_HIGHEST+1, ID_CLIENTFILETRANSFERSOCKET,
       ID_CLIENTTIMER, ID_CLIENTMSGTIMER, ID_CLIENTCHANGETEAM, ID_CLIENTCHANGEREADY, ID_CLIENTFILETRANSFERTIMER };

class ClientRoomFrame : public DUKEMATCHERFrame 
{
public:
  ClientRoomFrame();
  wxDateTime DateTime;
  wxString time, hostip, token;
  bool oldver;
  long hostport;
  bool locationerror;
//~ClientRoomFrame();
//void RefreshListCtrlSize();
  void FocusFrame();

/*void UpdateGameSettings(GameType game,
                          SourcePortType srcport,
                          const wxString& roomname,
                          size_t max_num_players,
                          size_t skill,
                          DN3DMPGameT_Type dn3dgametype,
                          DN3DSpawnType dn3dspawn,
                          const wxString& extra_args,
                          bool launch_usermap,
                          size_t epimap_num,
                          const wxString& usermap,
                          const wxString& modname,
                          bool showmodurl,
                          const wxString& modurl,
                          bool usemasterslave);
  void UpdateModFiles(const wxArrayString& modfiles);*/
  void UpdateGameSettings(const wxArrayString& l_info_input);
  void UpdateModFilesInfo(const wxArrayString& l_info_input);

  bool ConnectToServer(wxString ip_address, long port_num);
//void ConnectToServer(wxString ip_address, long port_num);
  void OnTextURLEvent(wxTextUrlEvent& event);
  void OnEnterText(wxCommandEvent& event);
  void OnUploadButtonClick(wxCommandEvent& event);
  void OnTransferButtonClick(wxCommandEvent& event);
//void OnNatFreeCheck(wxCommandEvent& event);
  void OnMapSelectionClose(wxCommandEvent& event);
  void OnTransferThreadEnd(wxCommandEvent& event);
  void OnFileTransferSocketEvent(wxSocketEvent& event);
  void OnSocketEvent(wxSocketEvent& event);
  void OnLeaveRoom(wxCommandEvent& event);
  void OnCloseWindow(wxCloseEvent& event);
  void OnTimer(wxTimerEvent& event);
  void OnClientMsgTimer(wxTimerEvent& event);
  void OnFileTransferTimer(wxTimerEvent& event);
  void OnChangeTeam(wxCommandEvent& event);
  void OnChangeReady(wxCommandEvent& event);
	
private:
  DUKEMATCHERPanel* m_clientroompanel;
  wxStaticText* m_clientgamenamestaticText;
  DUKEMATCHERTextCtrl* m_clientgamenametextCtrl;
  wxStaticText* m_clientskillstaticText;
  DUKEMATCHERTextCtrl* m_clientskilltextCtrl;
  wxStaticText* m_clientgametypestaticText;
  DUKEMATCHERTextCtrl* m_clientgametypetextCtrl;
  wxStaticText* m_clientspawnstaticText;
  DUKEMATCHERTextCtrl* m_clientspawntextCtrl;
  wxStaticText* m_clientsrcportstaticText;
  DUKEMATCHERTextCtrl* m_clientsrcporttextCtrl;
  wxStaticText* m_clientfraglimitstaticText;
  DUKEMATCHERTextCtrl* m_clientfraglimittextCtrl;
  wxStaticText* m_clientmapstaticText;
  DUKEMATCHERTextCtrl* m_clientmaptextCtrl;
  wxStaticText* m_clientmodstaticText;
  DUKEMATCHERTextCtrl* m_clientmodtextCtrl;
  wxStaticText* m_clientnumplayersstaticText;
  DUKEMATCHERTextCtrl* m_clientnumplayerstextCtrl;
  wxStaticText* m_clientconnectionstaticText;
  DUKEMATCHERTextCtrl* m_clientconnectiontextCtrl;
  DUKEMATCHERListCtrl* m_clientplayerslistCtrl;
  wxGauge* m_clienttransfergauge;
  wxStaticText* m_clienttransferstaticText;

  DUKEMATCHERButton* m_clienttransferuploadbutton;
  DUKEMATCHERButton* m_clienttransferdynamicbutton;
  DUKEMATCHERTextCtrl* m_clientoutputtextCtrl;
  DUKEMATCHERTextCtrl* m_clientinputtextCtrl;
  DUKEMATCHERButton* m_clientsendbutton;
//wxCheckBox* m_clientroomnatfreecheckBox;
  DUKEMATCHERButton* m_clientleaveroombutton;

//DUKEMATCHERButton* m_clientsendpmbutton;

DUKEMATCHERCheckBox* m_clientreadycheckBox;
wxStaticText* m_clientteamstaticText;
DUKEMATCHERChoice* m_clientteamchoice;

  void AddMessage(const wxString& msg);

  wxTimer* m_timer;
  wxTimer* m_msg_timer;

  wxSocketClient* m_client;
  bool m_in_room;

  TransferStateType m_transfer_state;
  TransferRequestType m_transfer_request;
  wxSocketClient* m_transfer_client;
  wxString m_transfer_filename;
  long m_transfer_filesize, m_transfer_port_num;
  wxThread* m_transfer_thread;
  wxFile* m_transfer_fp;
//bool m_transfer_awaiting_download;
  wxTimer* m_transfer_timer;

  MapSelectionDialog* m_mapselection_dialog;

  RoomPlayersTable m_players_table;
  
  bool m_allow_sounds;

  GameType m_game;
  SourcePortType m_srcport;
  wxString m_roomname;
  size_t m_max_num_players;
  size_t m_skill;
  DN3DMPGameT_Type m_dn3dgametype;
  DN3DSpawnType m_dn3dspawn;
  wxString m_extra_args, m_team;
  bool m_launch_usermap;
  size_t m_epimap_num;
  wxString m_usermap;
  wxArrayString m_modfiles;
  wxString m_modname;
  bool m_showmodurl;
  wxString m_modurl;
  bool m_usemasterslave;
//bool m_in_game;

  DECLARE_EVENT_TABLE()
};

#endif

