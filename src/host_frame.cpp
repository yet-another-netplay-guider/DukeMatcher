/*************************************************************************

Copyright 2008-2009 - NY00123

This file is part of DUKEMATCHER.

DUKEMATCHER is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

DUKEMATCHER is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DUKEMATCHER; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*************************************************************************/

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/wx.h>
#include <wx/filename.h>
#include <wx/listctrl.h>
#include <wx/sstream.h>
#include <wx/protocol/http.h>
#include <wx/tokenzr.h>
//#include <wx/gbsizer.h>
#include <wx/socket.h>
#include <wx/datetime.h>
#include <wx/textfile.h>
#include <wx/timer.h>
#endif

#include "host_frame.h"
#include "config.h"
#include "common.h"
#include "comm_txt.h"
#include "dukematcher.h"
#include "theme.h"
#include "dukematcher.xpm"

BEGIN_EVENT_TABLE(HostedRoomFrame, DUKEMATCHERFrame)
  EVT_BUTTON(ID_HOSTSENDTEXT, HostedRoomFrame::OnEnterText)
  EVT_BUTTON(wxID_EXIT, HostedRoomFrame::OnCloseRoom)
  EVT_BUTTON(ID_HOSTKICK, HostedRoomFrame::OnKickClient)
  EVT_BUTTON(ID_HOSTROOMSETTINGS, HostedRoomFrame::OnOpenRoomSettings)
  EVT_BUTTON(ID_HOSTROOMADVERTISE, HostedRoomFrame::OnAdvertiseButtonClick)
  EVT_BUTTON(wxID_OK, HostedRoomFrame::OnLaunchGame)

  EVT_BUTTON(ID_HOSTGENERALTRANSFERPURPOSE, HostedRoomFrame::OnTransferButtonClick)
  EVT_BUTTON(ID_HOSTREFUSESELECTIONS, HostedRoomFrame::OnRefuseButtonClick)

  EVT_BUTTON(ID_TRANSFERTHREADEND, HostedRoomFrame::OnTransferThreadEnd)

  EVT_CLOSE(HostedRoomFrame::OnCloseWindow)

  EVT_TEXT_URL(ID_HOSTOUTPUTTEXT, HostedRoomFrame::OnTextURLEvent)

  EVT_TEXT_ENTER(ID_HOSTINPUTTEXT, HostedRoomFrame::OnEnterText)

  EVT_SOCKET(ID_HOSTSERVER, HostedRoomFrame::OnServerEvent)
  EVT_SOCKET(ID_HOSTSOCKET, HostedRoomFrame::OnSocketEvent)
  EVT_SOCKET(ID_MASTERSOCKET, HostedRoomFrame::OnServerListSocketEvent)

  EVT_TIMER(ID_ADVERTISETIMER, HostedRoomFrame::OnServerListTimer)
  EVT_TIMER(ID_HOSTFILETRANSFERTIMER, HostedRoomFrame::OnFileTransferTimer)
  EVT_TIMER(ID_HOSTMSGTIMER, HostedRoomFrame::OnHostMsgTimer)  
  EVT_CHECKBOX(ID_HOSTCHANGEREADY, HostedRoomFrame::OnChangeReady)
  EVT_CHOICE(ID_HOSTCHANGETEAM, HostedRoomFrame::OnChangeTeam)
  EVT_CHECKBOX(ID_HOSTAUTOACCEPTCHECK, HostedRoomFrame::OnAutoAcceptCheck)
//EVT_CHECKBOX(ID_HOSTNATFREE, HostedRoomFrame::OnNatFreeCheck)

END_EVENT_TABLE()

HostedRoomFrame::HostedRoomFrame() : DUKEMATCHERFrame(NULL, wxID_ANY, wxEmptyString)
{
	wxBoxSizer* HostedRoomFramebSizer = new wxBoxSizer( wxVERTICAL );
	
	m_hostedroompanel = new DUKEMATCHERPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* hostedroompanelbSizer = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* hostedroompanelinfo1bSizer = new wxBoxSizer( wxHORIZONTAL );
	
	m_hostgamenamestaticText = new wxStaticText( m_hostedroompanel, wxID_ANY, wxT("Game:"), wxDefaultPosition, wxDefaultSize, 0 );
	hostedroompanelinfo1bSizer->Add( m_hostgamenamestaticText, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_hostgamenametextCtrl = new DUKEMATCHERTextCtrl( m_hostedroompanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(64, -1)), wxTE_READONLY );
	hostedroompanelinfo1bSizer->Add( m_hostgamenametextCtrl, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_hostsrcportstaticText = new wxStaticText( m_hostedroompanel, wxID_ANY, wxT("Port:"), wxDefaultPosition, wxDefaultSize, 0 );
	hostedroompanelinfo1bSizer->Add( m_hostsrcportstaticText, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_hostsrcporttextCtrl = new DUKEMATCHERTextCtrl( m_hostedroompanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(72, -1)), wxTE_READONLY );
	hostedroompanelinfo1bSizer->Add( m_hostsrcporttextCtrl, 1, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_hostmapstaticText = new wxStaticText( m_hostedroompanel, wxID_ANY, wxT("Map:"), wxDefaultPosition, wxDefaultSize, 0 );
	hostedroompanelinfo1bSizer->Add( m_hostmapstaticText, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_hostmaptextCtrl = new DUKEMATCHERTextCtrl( m_hostedroompanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)), wxTE_READONLY );
	hostedroompanelinfo1bSizer->Add( m_hostmaptextCtrl, 1, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_hostspawnstaticText = new wxStaticText( m_hostedroompanel, wxID_ANY, wxT("Spawn:"), wxDefaultPosition, wxDefaultSize, 0 );
	hostedroompanelinfo1bSizer->Add( m_hostspawnstaticText, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_hostspawntextCtrl = new DUKEMATCHERTextCtrl( m_hostedroompanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(48, -1)), wxTE_READONLY );
	hostedroompanelinfo1bSizer->Add( m_hostspawntextCtrl, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	hostedroompanelbSizer->Add( hostedroompanelinfo1bSizer, 0, wxEXPAND, 5 );
	
	wxBoxSizer* hostedroompanelinfo2bSizer = new wxBoxSizer( wxHORIZONTAL );
	
	m_hostgametypestaticText = new wxStaticText( m_hostedroompanel, wxID_ANY, wxT("Game Type:"), wxDefaultPosition, wxDefaultSize, 0 );
	hostedroompanelinfo2bSizer->Add( m_hostgametypestaticText, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_hostgametypetextCtrl = new DUKEMATCHERTextCtrl( m_hostedroompanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(96, -1)), wxTE_READONLY );
	hostedroompanelinfo2bSizer->Add( m_hostgametypetextCtrl, 1, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_hostteamstaticText = new wxStaticText( m_hostedroompanel, wxID_ANY, wxT("Choose Team:"), wxDefaultPosition, wxDefaultSize, 0 );
	hostedroompanelinfo2bSizer->Add( m_hostteamstaticText, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_hostteamchoice = new DUKEMATCHERChoice( m_hostedroompanel, ID_HOSTCHANGETEAM, wxDefaultPosition, wxDefaultSize, 0 );
	hostedroompanelinfo2bSizer->Add( m_hostteamchoice, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_hostskillstaticText = new wxStaticText( m_hostedroompanel, wxID_ANY, wxT("Skill:"), wxDefaultPosition, wxDefaultSize, 0 );
	hostedroompanelinfo2bSizer->Add( m_hostskillstaticText, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_hostskilltextCtrl = new DUKEMATCHERTextCtrl( m_hostedroompanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(64, -1)), wxTE_READONLY );
	hostedroompanelinfo2bSizer->Add( m_hostskilltextCtrl, 1, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_hostfraglimitstaticText = new wxStaticText( m_hostedroompanel, wxID_ANY, wxT("Frag Limit:"), wxDefaultPosition, wxDefaultSize, 0 );
	hostedroompanelinfo2bSizer->Add( m_hostfraglimitstaticText, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_hostfraglimittextCtrl = new DUKEMATCHERTextCtrl( m_hostedroompanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY );
	hostedroompanelinfo2bSizer->Add( m_hostfraglimittextCtrl, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	hostedroompanelbSizer->Add( hostedroompanelinfo2bSizer, 0, wxEXPAND, 5 );
	
	wxBoxSizer* hostedroompanelinfo3bSizer = new wxBoxSizer( wxHORIZONTAL );
	
	m_hostrecordstaticText = new wxStaticText( m_hostedroompanel, wxID_ANY, wxT("Record Game:"), wxDefaultPosition, wxDefaultSize, 0 );
	hostedroompanelinfo3bSizer->Add( m_hostrecordstaticText, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_hostrecordtextCtrl = new DUKEMATCHERTextCtrl( m_hostedroompanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY );
	hostedroompanelinfo3bSizer->Add( m_hostrecordtextCtrl, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_hostmodstaticText = new wxStaticText( m_hostedroompanel, wxID_ANY, wxT("TC/MOD:"), wxDefaultPosition, wxDefaultSize, 0 );
	hostedroompanelinfo3bSizer->Add( m_hostmodstaticText, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_hostmodtextCtrl = new DUKEMATCHERTextCtrl( m_hostedroompanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(80, -1)), wxTE_READONLY );
	hostedroompanelinfo3bSizer->Add( m_hostmodtextCtrl, 1, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_hostnumplayersstaticText = new wxStaticText( m_hostedroompanel, wxID_ANY, wxT("Players:"), wxDefaultPosition, wxDefaultSize, 0 );
	hostedroompanelinfo3bSizer->Add( m_hostnumplayersstaticText, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_hostnumplayerstextCtrl = new DUKEMATCHERTextCtrl( m_hostedroompanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY );
	hostedroompanelinfo3bSizer->Add( m_hostnumplayerstextCtrl, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_hostconnectionstaticText = new wxStaticText( m_hostedroompanel, wxID_ANY, wxT("Connection:"), wxDefaultPosition, wxDefaultSize, 0 );
	hostedroompanelinfo3bSizer->Add( m_hostconnectionstaticText, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_hostconnectiontextCtrl = new DUKEMATCHERTextCtrl( m_hostedroompanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(64, -1)), wxTE_READONLY );
	hostedroompanelinfo3bSizer->Add( m_hostconnectiontextCtrl, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	hostedroompanelbSizer->Add( hostedroompanelinfo3bSizer, 0, wxEXPAND, 5 );

m_hostplayerslistCtrl = new DUKEMATCHERListCtrl( m_hostedroompanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLC_REPORT );

	wxListItem itemCol;
	itemCol.SetText(wxT("Nickname"));
	itemCol.SetWidth(150);
//	itemCol.SetAlign(wxLIST_FORMAT_LEFT);
//	itemCol.SetImage(-1);
	m_hostplayerslistCtrl->InsertColumn(0, itemCol);
	itemCol.SetText(wxT("Status"));
	itemCol.SetWidth(100);
	m_hostplayerslistCtrl->InsertColumn(1, itemCol);
	itemCol.SetText(wxT("Location"));
	itemCol.SetWidth(280);
	m_hostplayerslistCtrl->InsertColumn(2, itemCol);
	itemCol.SetText(wxT("Detected IP"));
	itemCol.SetWidth(150);
	m_hostplayerslistCtrl->InsertColumn(3, itemCol);
	itemCol.SetText(wxT("In-game Address"));
	itemCol.SetWidth(180);
	m_hostplayerslistCtrl->InsertColumn(4, itemCol);
	itemCol.SetText(wxT("Request"));
	itemCol.SetWidth(80);
	m_hostplayerslistCtrl->InsertColumn(5, itemCol);
	itemCol.SetText(wxT("File name"));
	itemCol.SetWidth(120);
	m_hostplayerslistCtrl->InsertColumn(6, itemCol);
	itemCol.SetText(wxT("File size"));
	itemCol.SetWidth(80);
	m_hostplayerslistCtrl->InsertColumn(7, itemCol);
//	RefreshListCtrlSize();

	hostedroompanelbSizer->Add( m_hostplayerslistCtrl, 1, wxALL|wxEXPAND, 5 );

	wxBoxSizer* hostedroompaneltransferbSizer = new wxBoxSizer( wxHORIZONTAL );
	
	m_hostreadycheckBox = new DUKEMATCHERCheckBox( m_hostedroompanel, ID_HOSTCHANGEREADY, wxT("Ready"), wxDefaultPosition, wxDefaultSize, 0 );
	hostedroompaneltransferbSizer->Add( m_hostreadycheckBox, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	hostedroompaneltransferbSizer->Add( 290, 0, 1, wxEXPAND, 5 );
	
	m_hosttransferstaticText = new wxStaticText( m_hostedroompanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(96, -1)) );
	hostedroompaneltransferbSizer->Add( m_hosttransferstaticText, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_hosttransfercheckBox = new DUKEMATCHERCheckBox( m_hostedroompanel, ID_HOSTAUTOACCEPTCHECK, wxT("Auto-accept downloads"), wxDefaultPosition, wxDefaultSize, 0 );

	hostedroompaneltransferbSizer->Add( m_hosttransfercheckBox, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_hosttransferrefusebutton = new DUKEMATCHERButton( m_hostedroompanel, ID_HOSTREFUSESELECTIONS, wxT("Refuse selections"), wxDefaultPosition, wxDefaultSize, 0 );
	hostedroompaneltransferbSizer->Add( m_hosttransferrefusebutton, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_hosttransferdynamicbutton = new DUKEMATCHERButton( m_hostedroompanel, ID_HOSTGENERALTRANSFERPURPOSE, wxT("Accept selections"), wxDefaultPosition, wxDefaultSize, 0 );
	hostedroompaneltransferbSizer->Add( m_hosttransferdynamicbutton, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_hosttransfergauge = new wxGauge( m_hostedroompanel, wxID_ANY, 100, wxDefaultPosition, wxDLG_UNIT(this, wxSize(128, -1)), wxGA_HORIZONTAL );
	hostedroompaneltransferbSizer->Add( m_hosttransfergauge, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
		
	hostedroompanelbSizer->Add( hostedroompaneltransferbSizer, 0, wxEXPAND, 5 );

	m_hostoutputtextCtrl = new DUKEMATCHERTextCtrl( m_hostedroompanel, ID_HOSTOUTPUTTEXT, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(160, 110)), wxTE_AUTO_URL|wxTE_MULTILINE|wxTE_READONLY|wxTE_RICH );
	hostedroompanelbSizer->Add( m_hostoutputtextCtrl, 1, wxALL|wxEXPAND, 5 );
	
	wxBoxSizer* hostedroompanelinputbSizer = new wxBoxSizer( wxHORIZONTAL );

	m_hostinputtextCtrl = new DUKEMATCHERTextCtrl( m_hostedroompanel, ID_HOSTINPUTTEXT, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)), wxTE_PROCESS_ENTER );
#if ((defined __WXMAC__) || (defined __WXCOCOA__))
        m_hostinputtextCtrl->MacCheckSpelling(true);
#endif 
	hostedroompanelinputbSizer->Add( m_hostinputtextCtrl, 1, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_hostsendbutton = new DUKEMATCHERButton( m_hostedroompanel, ID_HOSTSENDTEXT, wxT("Send"), wxDefaultPosition, wxDefaultSize, 0 );
	hostedroompanelinputbSizer->Add( m_hostsendbutton, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	hostedroompanelbSizer->Add( hostedroompanelinputbSizer, 0, wxEXPAND, 5 );

/*	m_hostedroomnatfreecheckBox = new wxCheckBox( m_hostedroompanel, ID_HOSTNATFREE, wxT("Use UDP hole-punching in game (NAT free)."), wxDefaultPosition, wxDefaultSize, 0 );

	hostedroompanelbSizer->Add( m_hostedroomnatfreecheckBox, 0, wxALL, 5 );*/

	wxBoxSizer* hostedroompanelbuttonsbSizer = new wxBoxSizer( wxHORIZONTAL );

	m_hostcloseroombutton = new DUKEMATCHERButton( m_hostedroompanel, wxID_EXIT, wxT("Close Room"), wxDefaultPosition, wxDefaultSize, 0 );
	hostedroompanelbuttonsbSizer->Add( m_hostcloseroombutton, 0, wxALL, 5 );
	
	m_hostkicknicknamebutton = new DUKEMATCHERButton( m_hostedroompanel, ID_HOSTKICK, wxT("Kick selected client"), wxDefaultPosition, wxDefaultSize, 0 );
	hostedroompanelbuttonsbSizer->Add( m_hostkicknicknamebutton, 0, wxALL, 5 );
	
//	m_hostsendpmbutton = new DUKEMATCHERButton( m_hostedroompanel, ID_HOSTSENDPM, wxT("Send private message"), wxDefaultPosition, wxDefaultSize, 0 );
//	hostedroompanelbuttonsbSizer->Add( m_hostsendpmbutton, 0, wxALL, 5 );

	m_hostsettingsbutton = new DUKEMATCHERButton( m_hostedroompanel, ID_HOSTROOMSETTINGS, wxT("Room settings"), wxDefaultPosition, wxDefaultSize, 0 );
	hostedroompanelbuttonsbSizer->Add( m_hostsettingsbutton, 0, wxALL, 5 );

	m_hostadvertisebutton = new DUKEMATCHERButton( m_hostedroompanel, ID_HOSTROOMADVERTISE, wxT(" Advertise room "), wxDefaultPosition, wxDefaultSize, 0 );
	hostedroompanelbuttonsbSizer->Add( m_hostadvertisebutton, 0, wxALL, 5 );


	hostedroompanelbuttonsbSizer->Add( 0, 0, 1, wxEXPAND, 5 );

	m_hostlaunchbutton = new DUKEMATCHERButton( m_hostedroompanel, wxID_OK, wxT("Launch game"), wxDefaultPosition, wxDefaultSize, 0 );
	hostedroompanelbuttonsbSizer->Add( m_hostlaunchbutton, 0, wxALL, 5 );

	hostedroompanelbSizer->Add( hostedroompanelbuttonsbSizer, 0, wxEXPAND, 5 );

	m_hostedroompanel->SetSizer( hostedroompanelbSizer );
	m_hostedroompanel->Layout();
//	hostedroompanelbSizer->Fit( m_hostedroompanel );
        hostedroompanelbSizer->SetSizeHints(m_hostedroompanel);
	HostedRoomFramebSizer->Add( m_hostedroompanel, 1, wxEXPAND | wxALL, 0 );

	SetSizer( HostedRoomFramebSizer );
	Layout();
//	HostedRoomFramebSizer->Fit( this );
        HostedRoomFramebSizer->SetSizeHints(this);

  Centre();
  SetIcon(wxIcon(dukematcher_xpm));

  m_hosttransfercheckBox->SetValue(g_configuration->host_autoaccept_downloads);
//m_hostedroomnatfreecheckBox->SetValue(g_configuration->gamelaunch_enable_natfree);

  m_server = NULL;
  m_in_game = false;
  m_allow_sounds = true;
  m_not_first_update = false;
//m_num_players = 0;
//m_max_num_players = MAX_NUM_PLAYERS;
  m_transfer_state = TRANSFERSTATE_NONE;

  m_is_advertised = false;
  m_client = NULL;
  m_serverlist_timer = new wxTimer(this, ID_ADVERTISETIMER);

  if (g_configuration->theme_is_dark)
    l_transfer_listctrl_color = *wxWHITE;
  else
    l_transfer_listctrl_color = *wxBLACK;

  m_hostinputtextCtrl->SetFocus();
  // Add teams to the choice
	m_hostteamchoice->Clear();
	m_hostteamchoice->Append("No Team");
	m_hostteamchoice->Append("Blue Team");
	m_hostteamchoice->Append("Red Team");
	m_hostteamchoice->Append("Green Team");
	m_hostteamchoice->Append("Grey Team");
	m_hostteamchoice->Append("Dark Grey Team");
	m_hostteamchoice->Append("Dark Green Team");
	m_hostteamchoice->Append("Brown Team");
	m_hostteamchoice->Append("Dark Blue Team");
	m_hostteamchoice->Append("Hell Grey Team");
	m_hostteamchoice->SetStringSelection("No Team");
}

HostedRoomFrame::~HostedRoomFrame()
{
  // The MP Dialog also creates its own hidden HostedRoomFrame object
  // for testing; It should NOT touch g_host_frame!
  if (this == g_host_frame)
  {
    g_host_frame = NULL;
    g_main_frame->ShowMainFrame();
  }
}
/*
void HostedRoomFrame::RefreshListCtrlSize()
{
  int l_width;
  if (m_hostplayerslistCtrl->GetItemCount())
    l_width = wxLIST_AUTOSIZE;
  else
    l_width = wxLIST_AUTOSIZE_USEHEADER;
  m_hostplayerslistCtrl->SetColumnWidth(0, l_width);
  m_hostplayerslistCtrl->SetColumnWidth(1, l_width);
  m_hostplayerslistCtrl->SetColumnWidth(2, l_width);
  m_hostplayerslistCtrl->SetColumnWidth(3, l_width);
  m_hostplayerslistCtrl->SetColumnWidth(4, l_width);
  m_hostplayerslistCtrl->SetColumnWidth(5, l_width);
}
*/
void HostedRoomFrame::FocusFrame()
{
  m_hostinputtextCtrl->SetFocus();
}

void HostedRoomFrame::SendRoomInfo(bool is_new_room)
{
  wxArrayString l_str_list;

  if (is_new_room)
  {
    l_str_list.Add(wxT("advertise"));
    l_str_list.Add(DUKEMATCHERMASTER_STR_NET_VERSION);
    l_str_list.Add(DUKEMATCHER_STR_NET_VERSION);
    l_str_list.Add(wxString::Format(wxT("%d"), g_configuration->server_port_number));
  }
  else
    l_str_list.Add(wxT("updateinfo"));
  
  if (m_game == GAME_DN3D)
  {
    l_str_list.Add(wxT("dn3d"));
    l_str_list.Add(m_roomname);
    l_str_list.Add(wxString::Format(wxT("%d"), m_max_num_players));
    if (m_in_game)
      l_str_list.Add(wxT("yes"));
    else
      l_str_list.Add(wxT("no"));

    switch (m_srcport)
    {
      case SOURCEPORT_EDUKE32:   l_str_list.Add(wxT("eduke32")); break;
      case SOURCEPORT_JFDUKE3D:  l_str_list.Add(wxT("jfduke3d")); break;
      case SOURCEPORT_IDUKE3D:   l_str_list.Add(wxT("iduke3d")); break;
      case SOURCEPORT_DUKE3DW32: l_str_list.Add(wxT("duke3dw32")); break;
      case SOURCEPORT_XDUKE:     l_str_list.Add(wxT("xduke")); break;
      default:                   l_str_list.Add(wxT("dosduke"));
    }
    l_str_list.Add(wxString::Format(wxT("%d"), m_skill));

    switch (m_dn3dgametype)
    {
      case DN3DMPGAMETYPE_DMSPAWN:    l_str_list.Add(wxT("dmspawn")); break;
      case DN3DMPGAMETYPE_COOP:       l_str_list.Add(wxT("coop")); break;
      case DN3DMPGAMETYPE_DMNOSPAWN:  l_str_list.Add(wxT("dmnospawn")); break;
      case DN3DMPGAMETYPE_TDMSPAWN:   l_str_list.Add(wxT("tdmspawn")); break;
      default:                        l_str_list.Add(wxT("tdmnospawn"));
    }
  }
  else
  {
    l_str_list.Add(wxT("sw"));
    l_str_list.Add(m_roomname);
    l_str_list.Add(wxString::Format(wxT("%d"), m_max_num_players));
    if (m_in_game)
      l_str_list.Add(wxT("yes"));
    else
      l_str_list.Add(wxT("no"));

    switch (m_srcport)
    {
      case SOURCEPORT_JFSW: l_str_list.Add(wxT("jfsw")); break;
      case SOURCEPORT_SWP:  l_str_list.Add(wxT("swp")); break;
      default:              l_str_list.Add(wxT("dossw"));
    }
    l_str_list.Add(wxString::Format(wxT("%d"), m_skill));
  }

  if (m_launch_usermap)
  {
    l_str_list.Add(wxT("usermap"));
    l_str_list.Add(m_usermap);
  }
  else
  {
    l_str_list.Add(wxT("epimap"));
    l_str_list.Add(wxString::Format(wxT("%d"), m_epimap_num));
  }
  l_str_list.Add(m_hostmodtextCtrl->GetValue());

  g_SendStringListToSocket(m_client, l_str_list);
}

void HostedRoomFrame::UpdateGameSettings(GameType game,
                                         SourcePortType srcport,
                                         const wxString& roomname,
                                         size_t max_num_players,
                                         bool recorddemo,
                                         const wxString& demofilename,
                                         size_t skill,
                                         DN3DMPGameT_Type dn3dgametype,
                                         DN3DSpawnType dn3dspawn,
										 size_t fraglimit,
										 bool noteamchange,
										 bool forcerespawn,
                                         const wxString& args_for_host,
                                         const wxString& args_for_all,
                                         bool launch_usermap,
                                         size_t epimap_num,
                                         const wxString& usermap,
                                         const wxArrayString& modfiles,
                                         const wxString& modname,
                                         bool showmodurl,
                                         const wxString& modurl,
                                         bool advertise,
                                         bool usemasterslave)
{
  size_t l_loop_var, num_of_files;
  wxArrayString l_str_list;
  wxString l_temp_str;
// Set nicknames back to default colour.
if (g_configuration->theme_is_dark)
	l_def_listctrl_color = *wxGREEN;
else
  l_def_listctrl_color = *wxBLACK;
for (l_loop_var = 0; l_loop_var < m_players_table.num_players; l_loop_var++)
	m_hostplayerslistCtrl->SetItemTextColour(l_loop_var, l_def_listctrl_color);
// Is there an on-going transfer? Then we may have to abort a current transfer,
// and we should refuse all transfer requests anyway.
// That would happen if there's a change in the game (e.g. DN3D to SW),
// or when there's a change in the user map,
// or from a user map to an episode map.
// On a change from episode map to user map there's nothing to do,
// as there's no transfer in that case.
		
  if (m_not_first_update &&
      ((m_game != game) || (m_launch_usermap && (m_usermap != usermap)) ||
       (m_launch_usermap && (!launch_usermap))))
  { // In case of no transfer or a pending transfer, we simply refuse all requests.
    if ((m_transfer_state == TRANSFERSTATE_NONE) ||
        (m_transfer_state == TRANSFERSTATE_PENDING))
    {
      m_transfer_state = TRANSFERSTATE_NONE;
      for (l_loop_var = 1; l_loop_var < m_players_table.num_players; l_loop_var++)
        if (m_players_table.requests[l_loop_var].transferrequest != TRANSFERREQUEST_NONE)
        {
          m_players_table.requests[l_loop_var].transferrequest = TRANSFERREQUEST_NONE;
          g_SendCStringToSocket(m_host_clients_table.clients[l_loop_var].sock, "1:requestrefuse:");
		  if (m_players_table.players[l_loop_var].clientversion == DUKEMATCHER_VERSION)
				m_hostplayerslistCtrl->SetItemTextColour(l_loop_var, l_def_listctrl_color);
          m_hostplayerslistCtrl->SetItem(l_loop_var, 5, wxEmptyString);
          m_hostplayerslistCtrl->SetItem(l_loop_var, 6, wxEmptyString);
          m_hostplayerslistCtrl->SetItem(l_loop_var, 7, wxEmptyString);
//        RefreshListCtrlSize();
        }
      m_hosttransferstaticText->SetLabel(wxEmptyString);
      m_hosttransferdynamicbutton->SetLabel(wxT("Accept selections"));
      m_hostlaunchbutton->Enable();
    }
    else // Just don't refuse for the client who's in the middle of a file transfer.
    {
      if (m_transfer_state == TRANSFERSTATE_BUSY) // Abort and wait for thread end.
      {
        m_transfer_state = TRANSFERSTATE_ABORTED;
        g_SendCStringToSocket(m_host_clients_table.clients[m_transfer_client_index].sock, "1:aborttransfer:");
      }
      
      /**************************************************************************
      If transfer isn't busy, then it's aborted and we're waiting for thread end.
      In case it has been aborted and the relevant client has recently been
      deleted from the list, m_transfer_client_index == 0.
      The loops will still "just work", from client 1 upwards.
      **************************************************************************/

      for (l_loop_var = 1; l_loop_var < m_transfer_client_index; l_loop_var++)
        if (m_players_table.requests[l_loop_var].transferrequest != TRANSFERREQUEST_NONE)
        {
          m_players_table.requests[l_loop_var].transferrequest = TRANSFERREQUEST_NONE;
          g_SendCStringToSocket(m_host_clients_table.clients[l_loop_var].sock, "1:requestrefuse:");
		  if (m_players_table.players[l_loop_var].clientversion == DUKEMATCHER_VERSION)
          m_hostplayerslistCtrl->SetItemTextColour(l_loop_var, *wxRED);
          m_hostplayerslistCtrl->SetItem(l_loop_var, 5, wxEmptyString);
          m_hostplayerslistCtrl->SetItem(l_loop_var, 6, wxEmptyString);
          m_hostplayerslistCtrl->SetItem(l_loop_var, 7, wxEmptyString);
//        RefreshListCtrlSize();
        }
      for (l_loop_var = m_transfer_client_index+1; l_loop_var < m_players_table.num_players; l_loop_var++)
        if (m_players_table.requests[l_loop_var].transferrequest != TRANSFERREQUEST_NONE)
        {
          m_players_table.requests[l_loop_var].transferrequest = TRANSFERREQUEST_NONE;
          g_SendCStringToSocket(m_host_clients_table.clients[l_loop_var].sock, "1:requestrefuse:");
		  if (m_players_table.players[l_loop_var].clientversion == DUKEMATCHER_VERSION)
			m_hostplayerslistCtrl->SetItemTextColour(l_loop_var, *wxRED);
          m_hostplayerslistCtrl->SetItem(l_loop_var, 5, wxEmptyString);
          m_hostplayerslistCtrl->SetItem(l_loop_var, 6, wxEmptyString);
          m_hostplayerslistCtrl->SetItem(l_loop_var, 7, wxEmptyString);
//        RefreshListCtrlSize();
        }
    }
  }
  m_not_first_update = true;

  m_game = game;
  m_srcport = srcport;
  m_roomname = roomname;
  if (m_players_table.num_players > max_num_players)
  {
    AddMessage(wxT("* You can't reduce the maximum amount of players to be lower than currently connected. There may be a few more who've just connected, even if not accepted into the room."));
    if (g_configuration->play_snd_error)
      g_PlaySound(g_configuration->snd_error_file);
  }
  else
  {
    m_max_num_players = max_num_players;
    // Send message only if we have enough clients to send to
    // (excluding the last one), and avoid assigning the negative value -1
    // to l_loop_var (which would happen if num_clients == 0).
    if (m_host_clients_table.num_clients > 1)
    {
      // There may still be a few clients connected but not in room.
      // Let's disconnect them, from the latest who's got connected,
      // until we have enough room.
      // We should reserve one more for the case of file transfer.
      // Better keep the last one.
      l_loop_var = m_host_clients_table.num_clients - 1; // Last client - We skip it.
      while (max_num_players < l_loop_var)
      {
        l_loop_var--;
        g_SendCStringToSocket(m_host_clients_table.clients[l_loop_var].sock, "2:error:Server is full.:");
        m_host_clients_table.clients[l_loop_var].sock->Destroy();
        m_host_clients_table.DelByIndex(l_loop_var);
      }
    }
  }
  m_recorddemo = recorddemo;
  m_demofilename = demofilename;
  m_skill = skill;
  m_dn3dgametype = dn3dgametype;
  m_dn3dspawn = dn3dspawn;
  m_fraglimit = fraglimit;
  m_noteamchange = noteamchange;
  m_forcerespawn = forcerespawn;
  m_args_for_host = args_for_host;
  m_args_for_all = args_for_all;
  m_launch_usermap = launch_usermap;
  m_epimap_num = epimap_num;
  m_usermap = usermap;
  m_modfiles = modfiles;
  m_modname = modname;
  m_showmodurl = showmodurl;
  m_modurl = modurl;
  m_usemasterslave = usemasterslave;

  if (game == GAME_DN3D)
  {
    m_hostgamenametextCtrl->SetValue(GAMENAME_DN3D);
    switch (srcport)
    {
      case SOURCEPORT_EDUKE32: m_hostsrcporttextCtrl->SetValue(SRCPORTNAME_EDUKE32); break;
      case SOURCEPORT_JFDUKE3D: m_hostsrcporttextCtrl->SetValue(SRCPORTNAME_JFDUKE3D); break;
      case SOURCEPORT_IDUKE3D: m_hostsrcporttextCtrl->SetValue(SRCPORTNAME_IDUKE3D); break;
      case SOURCEPORT_DUKE3DW32: m_hostsrcporttextCtrl->SetValue(SRCPORTNAME_DUKE3DW32); break;
      case SOURCEPORT_XDUKE: m_hostsrcporttextCtrl->SetValue(SRCPORTNAME_XDUKE); break;
      default: m_hostsrcporttextCtrl->SetValue(SRCPORTNAME_DOSDUKE);
    }
    m_hostskilltextCtrl->SetValue((*g_dn3d_skill_list)[skill]);
    m_hostgametypetextCtrl->SetValue((*g_dn3d_gametype_list)[dn3dgametype-1]);
    m_hostspawntextCtrl->SetValue((*g_dn3d_spawn_list)[dn3dspawn-1]);
	// Disable frag limit text control if source port is not hduke otherwise show the frag limit.
	if (srcport != SOURCEPORT_IDUKE3D) {
		m_hostfraglimittextCtrl->SetValue("0");
		m_hostfraglimittextCtrl->Disable();
		}
	else
	{
		m_hostfraglimittextCtrl->Enable();
		m_hostfraglimittextCtrl->SetValue(wxString::Format(wxT("%d"), fraglimit));
		}
    if (launch_usermap)
      m_hostmaptextCtrl->SetValue(usermap);
    else
      m_hostmaptextCtrl->SetValue((*g_dn3d_level_list)[g_GetDN3DLevelSelectionByNum(epimap_num)]);
  }
  else
  {
    m_hostgamenametextCtrl->SetValue(GAMENAME_SW);
    switch (srcport)
    {
      case SOURCEPORT_JFSW: m_hostsrcporttextCtrl->SetValue(SRCPORTNAME_JFSW); break;
      case SOURCEPORT_SWP: m_hostsrcporttextCtrl->SetValue(SRCPORTNAME_SWP); break;
      default: m_hostsrcporttextCtrl->SetValue(SRCPORTNAME_DOSSW);
    }
    m_hostskilltextCtrl->SetValue((*g_sw_skill_list)[skill]);
    m_hostgametypetextCtrl->Clear();
    m_hostspawntextCtrl->Clear();
    if (launch_usermap)
      m_hostmaptextCtrl->SetValue(usermap);
    else
      m_hostmaptextCtrl->SetValue((*g_sw_level_list)[g_GetSWLevelSelectionByNum(epimap_num)]);
  }
  SetTitle(roomname);
  m_hostnumplayerstextCtrl->SetValue(wxString::Format(wxT("%d/%d"), m_players_table.num_players, m_max_num_players));
  if (recorddemo)
    m_hostrecordtextCtrl->SetValue(wxT("Yes"));
  else
    m_hostrecordtextCtrl->SetValue(wxT("No"));
	
	// get info for the tc/mod box.
	m_hostmodtextCtrl->Enable();
    num_of_files = modfiles.GetCount();
	  for (l_loop_var = 0; l_loop_var < num_of_files; l_loop_var++) {
		l_temp_str += modfiles[l_loop_var];
		  // if not last item, add a comma as there is another file to come.
		 if (l_loop_var != (num_of_files-1))
		  l_temp_str += ", ";
    }
	// if there is a mod name and files add to the text control.
	if ((modname != "") && (l_temp_str != ""))
	   m_hostmodtextCtrl->SetValue(modname + wxT(" - ") + l_temp_str);
	// otherwise if there are files but no mod name, add the files to the text control.
	else if ((modname == "") && (l_temp_str != ""))
		m_hostmodtextCtrl->SetValue(l_temp_str);
	// if there is a mod name but no files, the host may be playing with custom cons so will still need to display just the name.
	else if ((modname != "") && (l_temp_str == ""))
		m_hostmodtextCtrl->SetValue(modname);
	// Finally if there is no mod name and no files, disable the control.
	else {
		m_hostmodtextCtrl->Clear();
		m_hostmodtextCtrl->Disable();
		}
		
  if (((srcport == SOURCEPORT_EDUKE32) || (srcport == SOURCEPORT_JFDUKE3D))
      && usemasterslave)
    m_hostconnectiontextCtrl->SetValue(wxT("Master/Slave"));
  else
    if ((srcport == SOURCEPORT_DOSDUKE) || (srcport == SOURCEPORT_DOSSW))
      m_hostconnectiontextCtrl->SetValue(wxT("Emulated IPX/SPX"));
  else
    m_hostconnectiontextCtrl->SetValue(wxT("Peer-2-Peer"));

  // If there are clients, that means it's just a refresh.
  // For all clients we'll tell the main info (e.g. source port).

  if (m_host_clients_table.num_clients > 1)
  {
    l_str_list = GetServerInfoSockStrList();
    for (l_loop_var = 1; l_loop_var < m_host_clients_table.num_clients; l_loop_var++)
      g_SendStringListToSocket(m_host_clients_table.clients[l_loop_var].sock, l_str_list);
    // For the ones actually in the room, changes to MOD files list as well.
    if (m_players_table.num_players > 1)
    {
      l_str_list = GetServerModFilesSockStrList(0);
      for (l_loop_var = 1; l_loop_var < m_players_table.num_players; l_loop_var++)
        g_SendStringListToSocket(m_host_clients_table.clients[l_loop_var].sock, l_str_list);
    }
  }

  if (m_is_advertised)
    SendRoomInfo(false);
  else
    if ((advertise) && (m_hostadvertisebutton->GetLabel() == wxT(" Advertise room ")))
      Advertise();
// disable team choice if not team dm
if ((m_dn3dgametype != DN3DMPGAMETYPE_DMSPAWN) && (m_dn3dgametype != DN3DMPGAMETYPE_COOP) && (m_dn3dgametype != DN3DMPGAMETYPE_DMNOSPAWN) && (srcport == SOURCEPORT_IDUKE3D))
{
	m_hostteamchoice->Enable();
	for (l_loop_var = 0; l_loop_var < m_players_table.num_players; l_loop_var++){
				m_hostplayerslistCtrl->SetItem(l_loop_var, 0, m_players_table.players[l_loop_var].nickname + wxT(" [") + m_players_table.players[l_loop_var].team + wxT("]"));
	}		
} 
else {
		m_hostteamchoice->Disable();
		for (l_loop_var = 0; l_loop_var < m_players_table.num_players; l_loop_var++)
			m_hostplayerslistCtrl->SetItem(l_loop_var, 0, m_players_table.players[l_loop_var].nickname);
}}

bool HostedRoomFrame::StartServer()
{
  wxIPV4address l_addr;
  l_addr.Service(g_configuration->server_port_number);

  m_server = new wxSocketServer(l_addr);

  if (!m_server->Ok())
  {
    delete m_server;
    m_server = NULL;
    return false;
  }

  m_server->SetEventHandler(*this, ID_HOSTSERVER);
  m_server->SetNotify(wxSOCKET_CONNECTION_FLAG);
  m_server->Notify(true);

  m_players_table.Add(g_configuration->nickname, wxEmptyString,
                      g_configuration->game_port_number);
  m_host_clients_table.Add(NULL, wxEmptyString, NULL);
  m_players_table.players[0].team = "No Team";
  m_hostplayerslistCtrl->InsertItem(0, g_configuration->nickname);
if (g_configuration->theme_is_dark)
	l_def_listctrl_color = *wxGREEN;
else
  l_def_listctrl_color = *wxBLACK;
  m_hostplayerslistCtrl->SetItemTextColour(0, l_def_listctrl_color);
//RefreshListCtrlSize();
	m_hostplayerslistCtrl->SetItem(0, 1, "Ready");
	m_players_table.players[0].status = "Ready";
	m_players_table.players[0].clientversion = DUKEMATCHER_VERSION;
	m_hostreadycheckBox->SetValue(true);
	// start automatic message to keep clients connected.
	m_msg_timer = new wxTimer(this, ID_HOSTMSGTIMER);
    m_msg_timer->Start(180000, wxTIMER_CONTINUOUS);
  return true;
}

void HostedRoomFrame::AddMessage(const wxString& msg)
{
//m_hostoutputtextCtrl->AppendText(wxT('\n') + msg + wxT('\n'));
  (*m_hostoutputtextCtrl) << msg << wxT('\n');
  while (m_hostoutputtextCtrl->GetNumberOfLines() > MAX_NUM_MSG_LINES)
    m_hostoutputtextCtrl->Remove(0, (m_hostoutputtextCtrl->GetLineText(0)).Len()+1);
  // Without that, text scrolling may not work as expected.
#ifdef __WXMSW__
  m_hostoutputtextCtrl->ScrollLines(-1);
#elif ((defined __WXMAC__) || (defined __WXCOCOA__))
  ((wxWindow*)m_hostoutputtextCtrl)->SetScrollPos(wxVERTICAL, ((wxWindow*)m_hostoutputtextCtrl)->GetScrollRange(wxVERTICAL));
#endif
}

void HostedRoomFrame::OnTextURLEvent(wxTextUrlEvent& event)
{
  if (event.GetMouseEvent().GetEventType() == wxEVT_LEFT_DOWN)
    g_OpenURL(m_hostoutputtextCtrl->GetRange(event.GetURLStart(), event.GetURLEnd()));
}

void HostedRoomFrame::OnEnterText(wxCommandEvent& WXUNUSED(event))
{
  // Get current date.
  if (g_configuration->show_timestamp)
  {
  DateTime = wxDateTime::Now();
  time = DateTime.Format(wxT("%X"));
  }
  //
  wxString l_text = m_hostinputtextCtrl->GetValue();
  wxArrayString l_str_list;
  size_t l_loop_var;
  m_in_game = false;
  if (m_is_advertised)
    SendRoomInfo(false);
  m_allow_sounds = true;
  if (l_text.IsEmpty())
  {
    if (g_configuration->play_snd_error)
      g_PlaySound(g_configuration->snd_error_file);
  }
  else
  {
	 // Is user wanting to change their nickname?
	if ((l_text.SubString(0,4) == wxT("/nick")) || (l_text.SubString(0,4) == wxT("/name"))){
	  bool m_player_exists = false;
	// Check that there isn't already a user with that nickname
	for (l_loop_var = 1; l_loop_var < m_players_table.num_players; l_loop_var++){
     if (m_players_table.players[l_loop_var].nickname == l_text.SubString(6,0))
	 m_player_exists = true;}
	if (!m_player_exists)
		{
		AddMessage(wxT("* ") + m_players_table.players[0].nickname + wxT(" is now known as ") + l_text.SubString(6,0) + wxT("."));
	l_str_list.Add(wxT("nickchange"));
	l_str_list.Add(m_players_table.players[0].nickname);
      l_str_list.Add(l_text.SubString(6,0));
      for (l_loop_var = 1; l_loop_var < m_players_table.num_players; l_loop_var++)
      g_SendStringListToSocket(m_host_clients_table.clients[l_loop_var].sock, l_str_list);
	 g_configuration->nickname = l_text.SubString(6,0);
	m_players_table.players[0].nickname = l_text.SubString(6,0);
	if ((m_dn3dgametype != DN3DMPGAMETYPE_DMSPAWN) && (m_dn3dgametype != DN3DMPGAMETYPE_COOP) && (m_dn3dgametype != DN3DMPGAMETYPE_DMNOSPAWN) && (m_srcport == SOURCEPORT_IDUKE3D)) 
		m_hostplayerslistCtrl->SetItem(0, 0, l_text.SubString(6,0) + wxT(" [") + m_players_table.players[0].team + wxT("]"));
	else
	m_hostplayerslistCtrl->SetItem(0, 0, l_text.SubString(6,0));	
	m_hostinputtextCtrl->Clear();
	 // Update master server player list
				if (m_is_advertised)
          {
            SendRoomInfo(false);
          }
	}
	else
		AddMessage(wxT("* Unable to change nickname as there is already a user in the room with that name."));
	}
	// if not changing nick
	else
	{
		// If timestamp
		if (g_configuration->show_timestamp)
		{
			AddMessage(wxT("[") + time + wxT("] ") + m_players_table.players[0].nickname + wxT(": ") + l_text);
		}
		else
		{
			AddMessage(m_players_table.players[0].nickname + wxT(": ") + l_text);
		}
		 l_str_list.Add(wxT("msg"));
		l_str_list.Add(m_players_table.players[0].nickname);
		l_str_list.Add(l_text);
		 for (l_loop_var = 1; l_loop_var < m_players_table.num_players; l_loop_var++)
      g_SendStringListToSocket(m_host_clients_table.clients[l_loop_var].sock, l_str_list);
    m_hostinputtextCtrl->Clear();
	}
    if (g_configuration->play_snd_send)
      g_PlaySound(g_configuration->snd_send_file);
   
   
  }
}

void HostedRoomFrame::OnKickClient(wxCommandEvent& WXUNUSED(event))
{
  size_t l_loop_var;
  wxArrayString l_str_list;
  m_in_game = false;
  if (m_is_advertised)
    SendRoomInfo(false);
  m_allow_sounds = true;

  long l_selection = m_hostplayerslistCtrl->GetNextItem(-1, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED);
  if (!l_selection) // Number 0, or the host.
  {
    AddMessage(wxT("* You can't kick yourself. Please deselect yourself in the list before retrying to kick."));
    if (g_configuration->play_snd_error)
      g_PlaySound(g_configuration->snd_error_file);
  }
  else
    if (l_selection < 0) // -1
    {
      AddMessage(wxT("* You should select at least one client in the list for kicking (apart from yourself)."));
      if (g_configuration->play_snd_error)
        g_PlaySound(g_configuration->snd_error_file);
    }
    else
    {
      do // NOTICE: Following l_selection values would get decreased by 1!
      {  // We'd keep all kinds of lists (UI and internal) IN SYNC while looping.
        l_str_list.Add(wxT("kicked"));
        l_str_list.Add(m_players_table.players[l_selection].nickname);
        for (l_loop_var = 1; l_loop_var < m_players_table.num_players; l_loop_var++)
          g_SendStringListToSocket(m_host_clients_table.clients[l_loop_var].sock, l_str_list);
        AddMessage(wxT("* ") + m_players_table.players[l_selection].nickname + wxT(" has been kicked."));
        m_host_clients_table.clients[l_selection].sock->Destroy();
        RemoveInRoomClient(l_selection);
        m_hostplayerslistCtrl->DeleteItem(l_selection);
//      RefreshListCtrlSize();

        l_selection = m_hostplayerslistCtrl->GetNextItem(-1, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED);
        l_str_list.Empty();
      }
      while (l_selection >= 0); // Not -1
      if (g_configuration->play_snd_leave)
        g_PlaySound(g_configuration->snd_leave_file);
      m_hostnumplayerstextCtrl->SetValue(wxString::Format(wxT("%d/%d"), m_players_table.num_players, m_max_num_players));
      if (m_is_advertised)
      {
        l_str_list.Empty();
        l_str_list.Add(wxT("nicks"));
        for (l_loop_var = 0; l_loop_var < m_players_table.num_players; l_loop_var++)
          l_str_list.Add(m_players_table.players[l_loop_var].nickname);
        g_SendStringListToSocket(m_client, l_str_list);
      }
    }
}

void HostedRoomFrame::OnOpenRoomSettings(wxCommandEvent& WXUNUSED(event))
{
  Disable();
  m_in_game = false;
  if (m_is_advertised)
    SendRoomInfo(false);
  m_allow_sounds = true;
  g_mp_dialog = new MPDialog;
  g_mp_dialog->Show(true);
  g_mp_dialog->SetFocus();
//g_mp_dialog->Raise();
}

void HostedRoomFrame::OnLaunchGame(wxCommandEvent& WXUNUSED(event))
{
size_t l_loop_var;
wxString notready, noteam;
bool error = false;
wxArrayString l_str_list;
	// First of all need to make sure all players are ready to play.
for (l_loop_var = 0; l_loop_var < m_players_table.num_players; l_loop_var++)
{
     if ((m_players_table.players[l_loop_var].status == wxT("Not Ready")) || (m_players_table.players[l_loop_var].status == wxT("Back Soon")))
	 {
		// if first name we need a space before the nickname.
		if (notready == wxT(""))
		  notready += wxT(" ") + m_players_table.players[l_loop_var].nickname;
		// otherwise we need a comma and a space before the nickname
		else
			notready += wxT(", ") + m_players_table.players[l_loop_var].nickname;
	}
}
if (notready != "") {
l_str_list.Empty();
l_str_list.Add(wxT("msg"));
		l_str_list.Add("* WARNING");
		l_str_list.Add(wxT("Cannot launch, players not ready: ") + notready);
		 for (l_loop_var = 1; l_loop_var < m_players_table.num_players; l_loop_var++)
      g_SendStringListToSocket(m_host_clients_table.clients[l_loop_var].sock, l_str_list);
	  AddMessage(wxT("* WARNING: Cannot launch, players not ready: ") + notready);
	  if (g_configuration->play_snd_error)
        g_PlaySound(g_configuration->snd_error_file);
		error = true;
}
	// Next, we need to make sure that if hduke team dm - teams are set.
if ((m_dn3dgametype != DN3DMPGAMETYPE_DMSPAWN) && (m_dn3dgametype != DN3DMPGAMETYPE_COOP) && (m_dn3dgametype != DN3DMPGAMETYPE_DMNOSPAWN) && (m_srcport == SOURCEPORT_IDUKE3D))
{
for (l_loop_var = 0; l_loop_var < m_players_table.num_players; l_loop_var++)
{
     if (m_players_table.players[l_loop_var].team == wxT("No Team"))
	 {
		// if first name we need a space before the nickname.
		if (noteam == wxT(""))
		  noteam += wxT(" ") + m_players_table.players[l_loop_var].nickname;
		// otherwise we need a comma and a space before the nickname
		else
			noteam += wxT(", ") + m_players_table.players[l_loop_var].nickname;
	}
}
if (noteam != "") {
l_str_list.Empty();
l_str_list.Add(wxT("msg"));
		l_str_list.Add("* WARNING");
		l_str_list.Add(wxT("Cannot launch, players not in a team: ") + noteam);
		 for (l_loop_var = 1; l_loop_var < m_players_table.num_players; l_loop_var++)
      g_SendStringListToSocket(m_host_clients_table.clients[l_loop_var].sock, l_str_list);
	  AddMessage(wxT("* WARNING: Cannot launch, players not in a team: ") + noteam);
	  if (g_configuration->play_snd_error)
        g_PlaySound(g_configuration->snd_error_file);
		error = true;
}
}
// If players are ready and in teams (if required) the game can launch.
if (!error)
{	
  bool l_script_is_ready;
#ifdef __WXMSW__
  wxString l_temp_str = wxT("dukematcher.bat");
#elif (defined __WXMAC__) || (defined __WXCOCOA__)
  wxString l_temp_str = wxT("dukematcher.command");
#else
  wxString l_temp_str = wxT("dukematcher.sh");
#endif
m_team = m_players_table.players[0].team;
  if (m_players_table.num_players == 1)
    l_script_is_ready = g_MakeLaunchScript(l_temp_str, NULL, 0, m_game, m_srcport,
                                           false, 0, false, wxEmptyString,
                                           m_recorddemo, m_demofilename, m_skill,
                                           (DN3DMPGameT_Type)0, (DN3DSpawnType)0, false,
                                           m_launch_usermap, m_usermap,
                                           m_epimap_num, m_modfiles, m_team,
                                           m_args_for_host + m_hduke_args + wxT(' ') + m_args_for_all);
  else
  {
    l_script_is_ready = g_MakeLaunchScript(l_temp_str, &m_players_table, 0,
                                           m_game, m_srcport,
                                           false, 0, false, wxEmptyString,
                                           m_recorddemo, m_demofilename, m_skill,
                                           m_dn3dgametype, m_dn3dspawn, m_usemasterslave,
                                           m_launch_usermap, m_usermap,
                                           m_epimap_num, m_modfiles, m_team,
                                           m_args_for_host + m_hduke_args + wxT(' ') + m_args_for_all);
    for (size_t l_loop_var = 1; l_loop_var < m_players_table.num_players; l_loop_var++)
      g_SendCStringToSocket(m_host_clients_table.clients[l_loop_var].sock, "1:launch:");
    m_in_game = true;
    if (m_is_advertised)
      SendRoomInfo(false);
    m_allow_sounds = !(g_configuration->mute_sounds_while_ingame);
  }
  if (l_script_is_ready)
  {
    AddMessage(wxT("* Launching game..."));
#ifdef __WXMSW__
    wxExecute(wxT('"') + l_temp_str + wxT('"'));
#else
    wxExecute(g_configuration->terminal_fullcmd + wxT(" \"") + l_temp_str + wxT('"'));
#endif
  }
}
}

wxArrayString HostedRoomFrame::GetServerInfoSockStrList()
{

// if source port is hduke, we may have extra arguments i.e. frag limit, no team change in game, force respawn.
if (m_srcport == SOURCEPORT_IDUKE3D) {
	if (m_fraglimit > 0)
		m_hduke_args = wxT(" /y") + m_fraglimit;
	if (m_noteamchange)
		m_hduke_args += wxT(" -nochange");
	if (m_forcerespawn)
		m_hduke_args += wxT(" -forcerespawn");
} // if not hduke, there will be no additional hduke arguments.
else
		m_hduke_args = "";	
		
  wxArrayString l_str_list;
  l_str_list.Add(wxT("info"));
  if (m_game == GAME_DN3D)
  {
    l_str_list.Add(wxT("dn3d"));
    l_str_list.Add(m_roomname);
    l_str_list.Add(wxString::Format(wxT("%d"), m_max_num_players));
    if (m_in_game)
      l_str_list.Add(wxT("yes"));
    else
      l_str_list.Add(wxT("no"));
    switch (m_srcport)
    {
      case SOURCEPORT_EDUKE32: l_str_list.Add(wxT("eduke32")); break;
      case SOURCEPORT_JFDUKE3D: l_str_list.Add(wxT("jfduke3d")); break;
      case SOURCEPORT_IDUKE3D: l_str_list.Add(wxT("iduke3d")); break;
      case SOURCEPORT_DUKE3DW32: l_str_list.Add(wxT("duke3dw32")); break;
      case SOURCEPORT_XDUKE: l_str_list.Add(wxT("xduke")); break;
      default: l_str_list.Add(wxT("dosduke"));
    }
    l_str_list.Add(wxString::Format(wxT("%d"), m_skill));
    switch (m_dn3dgametype)
    {
      case DN3DMPGAMETYPE_DMSPAWN: l_str_list.Add(wxT("dmspawn")); break;
      case DN3DMPGAMETYPE_COOP: l_str_list.Add(wxT("coop")); break;
      case DN3DMPGAMETYPE_DMNOSPAWN: l_str_list.Add(wxT("dmnospawn")); break;
      case DN3DMPGAMETYPE_TDMSPAWN: l_str_list.Add(wxT("tdmspawn")); break;
      default: l_str_list.Add(wxT("tdmnospawn"));
    }
    switch (m_dn3dspawn)
    {
      case DN3DSPAWN_MONSTERS: l_str_list.Add(wxT("monsters")); break;
      case DN3DSPAWN_ITEMS: l_str_list.Add(wxT("items")); break;
      case DN3DSPAWN_INVENTORY: l_str_list.Add(wxT("inventory")); break;
      default: l_str_list.Add(wxT("all"));
    }
    l_str_list.Add(m_args_for_all + m_hduke_args);
    if (m_launch_usermap)
    {
      l_str_list.Add(wxT("usermap"));
      l_str_list.Add(m_usermap);
    }
    else
    {
      l_str_list.Add(wxT("epimap"));
      l_str_list.Add(wxString::Format(wxT("%d"), m_epimap_num));
    }
    if (((m_srcport == SOURCEPORT_EDUKE32) || (m_srcport == SOURCEPORT_JFDUKE3D))
         && m_usemasterslave)
      l_str_list.Add(wxT("masterslave"));
  }
  else
  {
    l_str_list.Add(wxT("sw"));
    l_str_list.Add(m_roomname);
    l_str_list.Add(wxString::Format(wxT("%d"), m_max_num_players));
    if (m_in_game)
      l_str_list.Add(wxT("yes"));
    else
      l_str_list.Add(wxT("no"));
    switch (m_srcport)
    {
      case SOURCEPORT_JFSW: l_str_list.Add(wxT("jfsw")); break;
      case SOURCEPORT_SWP: l_str_list.Add(wxT("swp")); break;
      default: l_str_list.Add(wxT("dossw"));
    }
    l_str_list.Add(wxString::Format(wxT("%d"), m_skill));
    l_str_list.Add(m_args_for_all);
    if (m_launch_usermap)
    {
      l_str_list.Add(wxT("usermap"));
      l_str_list.Add(m_usermap);
    }
    else
    {
      l_str_list.Add(wxT("epimap"));
      l_str_list.Add(wxString::Format(wxT("%d"), m_epimap_num));
    }
  }
  return l_str_list;
}

wxArrayString HostedRoomFrame::GetServerModFilesSockStrList(size_t num_of_files)
{
  wxArrayString l_str_list;
  size_t l_loop_var;
  if (num_of_files <= 0)
    num_of_files = m_modfiles.GetCount();
  l_str_list.Add(wxT("mod"));
  l_str_list.Add(m_modname);
  if (m_showmodurl)
  {
    l_str_list.Add(wxT("withurl"));
    l_str_list.Add(m_modurl);
  }
  else
    l_str_list.Add(wxT("nourl"));
  for (l_loop_var = 0; l_loop_var < num_of_files; l_loop_var++)
    l_str_list.Add(m_modfiles[l_loop_var]);
  return l_str_list;
}

void HostedRoomFrame::RemoveInRoomClient(size_t index)
{
//size_t l_loop_var;
  m_players_table.DelByIndex(index);
  m_host_clients_table.DelByIndex(index);

  //  Is there a file transfer? Maybe we need to update its client's index.
  if (m_transfer_state != TRANSFERSTATE_NONE)
  {
    // If m_transfer_client_index is 0, meaning another client has recently
    // been deleted using this very same function,
    // nothing would get changed, as index > 0 anyway (it isn't the host).
    if (m_transfer_client_index > index)
      m_transfer_client_index--;
    else
      if (m_transfer_client_index == index) // Is it the very same client?
      {
        // Mark that the client has been deleted.
        // 0 is the host and can't be a client anyway.
        m_transfer_client_index = 0;
        // In case the state is TRANSFERSTATE_BUSY, abort the transfer
        // and wait for thread end.
        if (m_transfer_state == TRANSFERSTATE_BUSY)
          m_transfer_state = TRANSFERSTATE_ABORTED;
        else // TRANSFERSTATE_PENDING - Set to NONE and check for other download requests.
          if (m_transfer_state == TRANSFERSTATE_PENDING)
          {
            m_transfer_state = TRANSFERSTATE_NONE;
            FindNextDownloadRequest();
          }
      }
  }
/*
  m_num_players--;
  for (l_loop_var = index; l_loop_var < m_num_players; l_loop_var++)
    m_selfdetectedips[l_loop_var] = m_selfdetectedips[l_loop_var+1];
*/
}

void HostedRoomFrame::FindNextDownloadRequest()
{
  size_t l_loop_var = 1;
  while (l_loop_var < m_players_table.num_players)
  {
    if ((m_players_table.requests[l_loop_var].transferrequest == TRANSFERREQUEST_DOWNLOAD)
        && (m_players_table.requests[l_loop_var].awaiting_download))
    {
      AcceptTransferRequest(l_loop_var);
      break;
    }
    l_loop_var++;
  }
  if (l_loop_var == m_players_table.num_players) // Haven't found?
  {
    m_hosttransferstaticText->SetLabel(wxEmptyString);
    m_hosttransferdynamicbutton->SetLabel(wxT("Accept selections"));
    m_hostlaunchbutton->Enable();
  }
}

void HostedRoomFrame::AcceptTransferRequest(size_t player_index)
{
  wxArrayString l_str_list;
  m_transfer_state = TRANSFERSTATE_PENDING;
  m_transfer_client_index = player_index;
  m_transfer_is_download = (m_players_table.requests[player_index].transferrequest == TRANSFERREQUEST_DOWNLOAD);
  if (m_transfer_is_download)
  {
    l_str_list.Add(wxT("downloadaccept"));
    if (m_game == GAME_DN3D)
      l_str_list.Add(wxString::Format(wxT("%d"), (wxFileName::GetSize(g_configuration->dn3d_maps_dir +
                                                                      wxFILE_SEP_PATH + m_usermap)).ToULong()));
    else
      l_str_list.Add(wxString::Format(wxT("%d"), (wxFileName::GetSize(g_configuration->sw_maps_dir +
                                                                      wxFILE_SEP_PATH + m_usermap)).ToULong()));
    g_SendStringListToSocket(m_host_clients_table.clients[player_index].sock, l_str_list);
  }
  else
  {
    g_SendCStringToSocket(m_host_clients_table.clients[player_index].sock, "1:uploadaccept:");
    m_transfer_filename = m_players_table.requests[player_index].filename;
  }
  m_hosttransferstaticText->SetLabel(m_transfer_filename);
  m_players_table.requests[player_index].transferrequest = TRANSFERREQUEST_NONE;
  if (m_players_table.players[player_index].clientversion == DUKEMATCHER_VERSION)
	m_hostplayerslistCtrl->SetItemTextColour(player_index, l_transfer_listctrl_color);
}

void HostedRoomFrame::OnTransferButtonClick(wxCommandEvent& WXUNUSED(event))
{
  long l_item;
  size_t l_client_index;
  TransferRequestType l_request;
  bool l_bad_selection;
  m_in_game = false;
  if (m_is_advertised)
    SendRoomInfo(false);
  if (m_transfer_state == TRANSFERSTATE_NONE) // "Accept" button
  {
    l_request = TRANSFERREQUEST_NONE;
    l_bad_selection = false;
    l_item = m_hostplayerslistCtrl->GetNextItem(-1, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED);
    while (l_item >= 0)
    {
      if (m_players_table.requests[l_item].transferrequest == TRANSFERREQUEST_DOWNLOAD)
      {
        if (l_request == TRANSFERREQUEST_UPLOAD) // Mixing is unallowed.
        {
          l_bad_selection = true;
          break;
        }
        else
          if (l_request == TRANSFERREQUEST_NONE)
          {
            l_request = TRANSFERREQUEST_DOWNLOAD;
            l_client_index = l_item;
//          l_client_index = (unsigned long)l_item;
          }
      }
      else
        if (m_players_table.requests[l_item].transferrequest == TRANSFERREQUEST_UPLOAD)
        {
          if (l_request == TRANSFERREQUEST_NONE)
          {
            l_request = TRANSFERREQUEST_UPLOAD;
            l_client_index = l_item;
//          l_client_index = (unsigned long)l_item;
          }
          else // Only one upload request may be selected,
          {    // and again, mixing is unallowed.
            l_bad_selection = true;
            break;
          }
        }
      l_item = m_hostplayerslistCtrl->GetNextItem(l_item, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED);
    }
    if (l_bad_selection)
    {
      AddMessage(wxT("* You may accept downloads or an upload, but not both. Furthermore, only one upload may be accepted."));
      if (g_configuration->play_snd_error)
        g_PlaySound(g_configuration->snd_error_file);
    }
    else
      if (l_request == TRANSFERREQUEST_UPLOAD)
      {
        m_hosttransferdynamicbutton->SetLabel(wxT("Abort transfer"));
        m_hostlaunchbutton->Disable();
        AcceptTransferRequest(l_client_index);
      }
    else
      if (l_request == TRANSFERREQUEST_DOWNLOAD)
      {
        l_item = m_hostplayerslistCtrl->GetNextItem(l_client_index, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED);
        while (l_item >= 0) // Make all selected downloads occur later,
        {                   // For bit better performance, don't check if everybody has a request.
                            // That doesn't matter for clients with no download requests anyway.
          m_players_table.requests[l_item].awaiting_download = true;
          l_item = m_hostplayerslistCtrl->GetNextItem(l_item, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED);
        }
        m_hosttransferdynamicbutton->SetLabel(wxT("Abort transfer"));
        m_hostlaunchbutton->Disable();
        AcceptTransferRequest(l_client_index);
      }
  }
  else // "Abort" button
  {
    if (m_transfer_state == TRANSFERSTATE_BUSY)
    {
      m_transfer_state = TRANSFERSTATE_ABORTED;
      g_SendCStringToSocket(m_host_clients_table.clients[m_transfer_client_index].sock, "1:aborttransfer:");
    }
    else
      if (m_transfer_state == TRANSFERSTATE_PENDING)
      {
        m_transfer_state = TRANSFERSTATE_NONE;
        m_players_table.requests[m_transfer_client_index].transferrequest = TRANSFERREQUEST_NONE;
        g_SendCStringToSocket(m_host_clients_table.clients[m_transfer_client_index].sock, "1:requestrefuse:");
		if (m_players_table.players[m_transfer_client_index].clientversion == DUKEMATCHER_VERSION)
			m_hostplayerslistCtrl->SetItemTextColour(m_transfer_client_index, l_def_listctrl_color);
        m_hostplayerslistCtrl->SetItem(m_transfer_client_index, 5, wxEmptyString);
        m_hostplayerslistCtrl->SetItem(m_transfer_client_index, 6, wxEmptyString);
        m_hostplayerslistCtrl->SetItem(m_transfer_client_index, 7, wxEmptyString);
//      RefreshListCtrlSize();
        // Let's check if there's a pending download request
        // from another client.
        l_item = 1;
        while ((l_item) && ((unsigned long)l_item < m_transfer_client_index))
          if ((m_players_table.requests[l_item].transferrequest == TRANSFERREQUEST_DOWNLOAD)
              && (m_players_table.requests[l_item].awaiting_download))
          {
            AcceptTransferRequest(l_item);
            l_item = 0; // Mark we've found a client.
          }
          else
            l_item++;
        if (l_item) // Have we not found a client?
        {
          l_item = m_transfer_client_index+1; // We skip the client for which we've just cancelled a request.
          while ((l_item) && ((unsigned long)l_item < m_transfer_client_index))
            if ((m_players_table.requests[l_item].transferrequest == TRANSFERREQUEST_DOWNLOAD)
                && (m_players_table.requests[l_item].awaiting_download))
            {
              AcceptTransferRequest(l_item);
              l_item = 0; // Mark we've found a client.
            }
            else
              l_item++;
        }
        if (l_item) // Still haven't found?
        {               // Let's make a little "clean up".
          m_hosttransferstaticText->SetLabel(wxEmptyString);
          m_hosttransferdynamicbutton->SetLabel(wxT("Accept selections"));
          m_hostlaunchbutton->Enable();
        }
      }
      // In case of TRANSFERSTATE_ABORTED, the relevant socket
      // deletion should end the file transfer thread and
      // therefore trigger an event used to continue.
  }
}

void HostedRoomFrame::OnRefuseButtonClick(wxCommandEvent& WXUNUSED(event))
{
  long l_item = m_hostplayerslistCtrl->GetNextItem(-1, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED);
  m_in_game = false;
  if (m_is_advertised)
    SendRoomInfo(false);
  if (!l_item) // Equals 0? If yes, without that there may be a problem in case m_transfer_client_index == 0.
    l_item = m_hostplayerslistCtrl->GetNextItem(0, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED);
  while (l_item >= 0)
  {
    // We remove requests only for clients which are NOT in a middle of a transfer.
    // The expression (unsigned long) is provided so no warning is produced by g++.
    if ((m_transfer_state == TRANSFERSTATE_NONE) || (m_transfer_client_index != (unsigned long)l_item))
    {
      m_players_table.requests[l_item].transferrequest = TRANSFERREQUEST_NONE;
      g_SendCStringToSocket(m_host_clients_table.clients[l_item].sock, "1:requestrefuse:");
	  if (m_players_table.players[l_item].clientversion == DUKEMATCHER_VERSION)
		m_hostplayerslistCtrl->SetItemTextColour(l_item, l_def_listctrl_color);
      m_hostplayerslistCtrl->SetItem(l_item, 5, wxEmptyString);
      m_hostplayerslistCtrl->SetItem(l_item, 6, wxEmptyString);
      m_hostplayerslistCtrl->SetItem(l_item, 7, wxEmptyString);
//    RefreshListCtrlSize();
    }
    l_item = m_hostplayerslistCtrl->GetNextItem(l_item, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED);
  }
}

void HostedRoomFrame::OnTransferThreadEnd(wxCommandEvent& WXUNUSED(event))
{
  size_t l_loop_var;
  wxString l_filename;
  wxArrayString l_str_list;
  bool check_for_download_request = true;
  m_transfer_timer->Stop();
  delete m_transfer_timer;

  if (m_server != NULL) // Not closing the room?
  {
    m_hosttransfergauge->SetValue(0);

    if (m_transfer_client_index) // If it's 0, it means the client has been deleted from list.
    {
      m_players_table.requests[m_transfer_client_index].transferrequest = TRANSFERREQUEST_NONE;
	  if (m_players_table.players[m_transfer_client_index].clientversion == DUKEMATCHER_VERSION)
		m_hostplayerslistCtrl->SetItemTextColour(m_transfer_client_index, l_def_listctrl_color);
      m_hostplayerslistCtrl->SetItem(m_transfer_client_index, 5, wxEmptyString);
      m_hostplayerslistCtrl->SetItem(m_transfer_client_index, 6, wxEmptyString);
      m_hostplayerslistCtrl->SetItem(m_transfer_client_index, 7, wxEmptyString);
//    RefreshListCtrlSize();
    }
  }
  m_transfer_fp->Close();
  delete m_transfer_fp;
  m_transfer_fp = NULL;

  if (!m_transfer_is_download)
  {
    if (m_game == GAME_DN3D)
      l_filename = g_configuration->dn3d_maps_dir + wxFILE_SEP_PATH + m_transfer_filename;
    else
      l_filename = g_configuration->sw_maps_dir + wxFILE_SEP_PATH + m_transfer_filename;
    // Also make sure it's a client still in list and not deleted,
    // and that the host hasn't closed the room.
    if ((m_transfer_state == TRANSFERSTATE_BUSY) &&
        (m_transfer_client_index) && (m_server != NULL) &&
        (wxFileName::GetSize(l_filename) == m_players_table.requests[m_transfer_client_index].filesize))
    {
      check_for_download_request = false;

      m_usermap = m_transfer_filename;
      m_launch_usermap = true;
      m_hostmaptextCtrl->SetValue(m_usermap);
	if (m_game == GAME_DN3D)
      g_configuration->gamelaunch_dn3dusermap = m_usermap;
	  else
	  g_configuration->gamelaunch_swusermap = m_usermap;
	  
      l_str_list = GetServerInfoSockStrList();
      for (l_loop_var = 1; l_loop_var < m_players_table.num_players; l_loop_var++)
      {
        if (m_players_table.requests[l_loop_var].transferrequest != TRANSFERREQUEST_NONE)
        {
          m_players_table.requests[l_loop_var].transferrequest = TRANSFERREQUEST_NONE;
          g_SendCStringToSocket(m_host_clients_table.clients[l_loop_var].sock, "1:requestrefuse:");
		  if (m_players_table.players[l_loop_var].clientversion == DUKEMATCHER_VERSION)
			m_hostplayerslistCtrl->SetItemTextColour(l_loop_var, l_def_listctrl_color);
          m_hostplayerslistCtrl->SetItem(l_loop_var, 5, wxEmptyString);
          m_hostplayerslistCtrl->SetItem(l_loop_var, 6, wxEmptyString);
          m_hostplayerslistCtrl->SetItem(l_loop_var, 7, wxEmptyString);
//        RefreshListCtrlSize();
        }
        g_SendStringListToSocket(m_host_clients_table.clients[l_loop_var].sock, l_str_list);
      }
    }
    else
      if (wxFileExists(l_filename))
        wxRemoveFile(l_filename);
  }
  m_transfer_state = TRANSFERSTATE_NONE;

  if (m_server == NULL) // Close the room?
  {
    Destroy();
    return;
  }

  if (check_for_download_request)
    FindNextDownloadRequest();
  else
  {
    m_hosttransferstaticText->SetLabel(wxEmptyString);
    m_hosttransferdynamicbutton->SetLabel(wxT("Accept selections"));
    m_hostlaunchbutton->Enable();
  }
}

void HostedRoomFrame::OnTimer(wxTimerEvent& WXUNUSED(event))
{
  delete m_host_clients_table.clients[m_players_table.num_players].timer;
  g_SendCStringToSocket(m_host_clients_table.clients[m_players_table.num_players].sock, "2:error:Timed out while waiting for a join request:");
  m_host_clients_table.clients[m_players_table.num_players].sock->Destroy();
  m_host_clients_table.DelByIndex(m_players_table.num_players);
}

void HostedRoomFrame::OnHostMsgTimer(wxTimerEvent& WXUNUSED(event))
{
/*
wxArrayString l_str_list2;
					l_str_list2.Empty();
					l_str_list2.Add(wxT("msg"));
					//l_str_list2.Add("*NOTICE");
					//l_str_list2.Add(wxT("Welcome to Duke Matcher. Visit the website at www.dukematches.net or on facebook https://www.facebook.com/groups/Duke3DOnline/"));
					l_str_list2.Add("");
					l_str_list2.Add(wxT("#(HeartBeat)@ :"));
					for (size_t l_loop_var2 = 1; l_loop_var2 < m_players_table.num_players; l_loop_var2++)
						g_SendStringListToSocket(m_host_clients_table.clients[l_loop_var2].sock, l_str_list2);*/
}

void HostedRoomFrame::OnFileTransferTimer(wxTimerEvent& WXUNUSED(event))
{
  wxFileOffset l_position;
  if (m_transfer_fp)
  {
    l_position = m_transfer_fp->Tell();
    m_hosttransfergauge->SetValue(l_position);
    if (m_transfer_is_download)
// Temporary wrapper for a possible bug with MinGW?
#if (defined __WXMSW__) && (wxCHECK_VERSION(2, 8, 9)) && (!wxCHECK_VERSION(2, 8, 10))
      m_hosttransferstaticText->SetLabel(m_usermap + wxString::Format(wxT(": %d / "), l_position)
                                                   + wxString::Format(wxT("%d"), m_hosttransfergauge->GetRange()));
    else
      m_hosttransferstaticText->SetLabel(m_transfer_filename + wxString::Format(wxT(": %d / "), l_position)
                                                             + wxString::Format(wxT("%d"), m_hosttransfergauge->GetRange()));
#else
      m_hosttransferstaticText->SetLabel(m_usermap + wxString::Format(wxT(": %d / %d"), l_position, m_hosttransfergauge->GetRange()));
    else
      m_hosttransferstaticText->SetLabel(m_transfer_filename + wxString::Format(wxT(": %d / %d"), l_position, m_hosttransfergauge->GetRange()));
#endif
  }
}

void HostedRoomFrame::OnServerEvent(wxSocketEvent& WXUNUSED(event))
{
  if (!m_server) // Shutdown?
    return;
  wxSocketBase* l_sock = m_server->Accept(false);
  wxArrayString l_str_list;
  wxIPV4address l_sock_addr;
  l_sock->GetPeer(l_sock_addr);
  // While the room may be full, it may just be a file transfer socket
  // from the same address of an already connected client.
  if ((m_host_clients_table.num_clients > m_max_num_players) ||
      ((m_host_clients_table.num_clients == m_max_num_players)
       && ((m_transfer_state != TRANSFERSTATE_PENDING) ||
           (l_sock_addr.IPAddress() != m_host_clients_table.clients[m_transfer_client_index].peerdetectedip))))
  {
    g_SendCStringToSocket(l_sock, "2:error:Server is full.:");
    l_sock->Destroy();
  }
  else
  {
    l_sock->GetPeer(l_sock_addr);
    m_host_clients_table.Add(l_sock, l_sock_addr.IPAddress(), new wxTimer(this, ID_HOSTTIMER));
    m_host_clients_table.clients[m_host_clients_table.num_clients-1].timer->Start(45000, wxTIMER_ONE_SHOT);
/*
    m_players_table.Add(l_sock, wxEmptyString, l_sock_addr.IPAddress(),
                        wxEmptyString, 0, false);
*/
  // Tell the new socket how and where to process its events
    l_sock->SetEventHandler(*this, ID_HOSTSOCKET);
    l_sock->SetFlags(wxSOCKET_NONE|wxSOCKET_BLOCK);
	l_sock->SetTimeout(1);
    l_sock->SetNotify(wxSOCKET_INPUT_FLAG | wxSOCKET_LOST_FLAG);
    l_sock->Notify(true);
  }
}

void HostedRoomFrame::OnSocketEvent(wxSocketEvent& event)
{
  if (!m_server) // Shutdown?
    return;
  wxSocketBase *l_sock = event.GetSocket();
  wxArrayString l_str_list, l_str_list2;
  size_t l_num_of_items,
         l_index = m_host_clients_table.FindIndexBySock(l_sock),
         l_loop_var, l_num_of_read_attempts;
  long l_temp_num;
  char l_short_buffer[MAX_COMM_TEXT_LENGTH];
  wxString l_long_buffer, l_filename;
  wxIPV4address l_sock_addr;
  if (l_index < m_host_clients_table.num_clients)
    switch (event.GetSocketEvent())
    {
      case wxSOCKET_INPUT:
        l_num_of_read_attempts = 0;
        do
        {
          l_sock->Read(l_short_buffer, MAX_COMM_TEXT_LENGTH);
/*        if (l_sock->Error())
          {
            AddMessage(wxString::Format(wxT("* A communication error has occured while trying to fetch data: %d"), l_sock->LastError()));
            if (m_allow_sounds && g_configuration->play_snd_error)
              g_PlaySound(g_configuration->snd_error_file);
            break;
          }*/
          l_temp_num = l_sock->LastCount();
          if (l_temp_num < MAX_COMM_TEXT_LENGTH)
            l_num_of_read_attempts = MAX_COMM_READ_ATTEMPTS;
          else
            l_num_of_read_attempts++;
          l_long_buffer << wxString(l_short_buffer, wxConvUTF8, l_temp_num);
          l_num_of_items = g_ConvertStringBufToStringList(l_long_buffer, l_str_list);
          while (l_num_of_items)
          {
            if ((l_num_of_items == 1) && (l_str_list[0] == wxT("requestroominfo")))
              g_SendStringListToSocket(l_sock, GetServerInfoSockStrList());
            else // Clients not in room only.
              if (m_players_table.num_players <= l_index)
              {
                if ((l_num_of_items == 6) && (l_str_list[0] == wxT("join")))
                {
                  m_host_clients_table.clients[l_index].timer->Stop();
                  delete m_host_clients_table.clients[l_index].timer;
                  if (m_players_table.num_players == m_max_num_players)
                  {
                    m_host_clients_table.DelByIndex(l_index);
                    g_SendCStringToSocket(l_sock, "2:error:Server is full.:");
                    l_sock->Destroy();
                    break;
                  }
                  if (l_str_list[5].Len() > NICK_MAX_LENGTH)
                  {
                    m_host_clients_table.DelByIndex(l_index);
                    l_str_list.Empty();
                    l_str_list.Add(wxT("error"));
                    l_str_list.Add(wxString::Format(wxT("Your configured nickname is too long. It shouldn't exceed %d characters."), NICK_MAX_LENGTH));
                    g_SendStringListToSocket(l_sock, l_str_list);
                    l_sock->Destroy();
                    break;
                  }
                  if (m_players_table.FindIndexByNick(l_str_list[5]) != m_players_table.num_players)
                  {
                    m_host_clients_table.DelByIndex(l_index);
                    g_SendCStringToSocket(l_sock, "2:error:Your configured nickname is already in use.:");
                    l_sock->Destroy();
                    break;
                  }
                  // That way all players in room appear in the beginning of the array.
                  // That's to avoid mixing the orders
                  // while clients are not aware of that.  
                  m_host_clients_table.PutInRoom(l_index, m_players_table.num_players);
                  l_index = m_players_table.num_players;
				  m_players_table.players[l_index].team = "No Team";	
                  m_host_clients_table.clients[l_index].selfdetectedip = l_str_list[3];
                  if ((m_host_clients_table.clients[0].peerdetectedip).IsEmpty())
                  {
                    m_host_clients_table.clients[0].peerdetectedip = l_str_list[2];
                    m_hostplayerslistCtrl->SetItem(0, 3, l_str_list[2]);
                    l_sock->GetLocal(l_sock_addr);
                    m_host_clients_table.clients[0].selfdetectedip = l_sock_addr.IPAddress();
                    m_players_table.players[0].ingameip = m_host_clients_table.clients[0].selfdetectedip;
                    m_hostplayerslistCtrl->SetItem(0, 4, m_players_table.players[0].ingameip +
                                                         wxString::Format(wxT(":%d"), g_configuration->game_port_number));
                  }
                  l_str_list[4].ToLong(&l_temp_num);
                  if (g_configuration->enable_localip_optimization &&
                      (l_str_list[2] == m_host_clients_table.clients[l_index].peerdetectedip))
                    m_players_table.Add(l_str_list[5], m_host_clients_table.clients[l_index].selfdetectedip, l_temp_num);
                  else
                    m_players_table.Add(l_str_list[5], m_host_clients_table.clients[l_index].peerdetectedip, l_temp_num);
                  m_hostplayerslistCtrl->InsertItem(l_index, l_str_list[5]);
                  m_hostplayerslistCtrl->SetItemTextColour(l_index, l_def_listctrl_color);
				  m_hostplayerslistCtrl->SetItem(l_index, 1, "Ready");
				  m_players_table.players[l_index].status = "Ready";
                  m_hostplayerslistCtrl->SetItem(l_index, 3, m_host_clients_table.clients[l_index].peerdetectedip);
                  m_hostplayerslistCtrl->SetItem(l_index, 4, m_players_table.players[l_index].ingameip + wxT(':') + l_str_list[4]);
//                RefreshListCtrlSize();
                  AddMessage(wxT("* ") + m_players_table.players[l_index].nickname + wxT(" has joined the room."));
                  m_hostnumplayerstextCtrl->SetValue(wxString::Format(wxT("%d/%d"), m_players_table.num_players, m_max_num_players));
				  // If team dm, display [No Team] next to nickname, since user has not yet chosen a team.
				  if ((m_dn3dgametype != DN3DMPGAMETYPE_DMSPAWN) && (m_dn3dgametype != DN3DMPGAMETYPE_COOP) && (m_dn3dgametype != DN3DMPGAMETYPE_DMNOSPAWN) && (m_srcport == SOURCEPORT_IDUKE3D)) 
					m_hostplayerslistCtrl->SetItem(l_index, 0, m_players_table.players[l_index].nickname + wxT(" [") + m_players_table.players[l_index].team + wxT("]"));
                  // l_index should now be the latest, or the number of in-room players minus 1.
				  // Check version now before l_str_list is cleared.
				  if (l_str_list[1] != DUKEMATCHER_VERSION)
                  {
					m_hostplayerslistCtrl->SetItemTextColour(l_index, *wxLIGHT_GREY);
					// Note clients version and turn their name to grey if different version.
					m_hostplayerslistCtrl->SetItemTextColour(l_index, *wxLIGHT_GREY);
					// Old version 461 should say 0.48n
					if (l_str_list[1] == wxT("461"))
						l_str_list[1] = "0.48n";
					AddMessage(wxT("* WARNING: ") + m_players_table.players[l_index].nickname + wxT(" is running Duke Matcher version ") + l_str_list[1] + wxT(" but the host is running version ") + DUKEMATCHER_VERSION + wxT(". Some features may be unavailable."));
					// Announce to room that client is on a different version.
					l_str_list2.Empty();
					l_str_list2.Add(wxT("msg"));
					l_str_list2.Add("* WARNING");
					l_str_list2.Add(m_players_table.players[l_index].nickname + wxT(" is running Duke Matcher version ") + l_str_list[1] + wxT(" but the host is running version ") + DUKEMATCHER_VERSION + wxT(". Some features may be unavailable."));
					 for (l_loop_var = 1; l_loop_var < m_players_table.num_players; l_loop_var++)
				  g_SendStringListToSocket(m_host_clients_table.clients[l_loop_var].sock, l_str_list2);
				  if (g_configuration->play_snd_error)
					g_PlaySound(g_configuration->snd_error_file);
					m_players_table.players[l_index].clientversion = l_str_list[1];
                  }
				  else
				  m_players_table.players[l_index].clientversion = DUKEMATCHER_VERSION;
				  // End of checking version but add to the table.
				  m_players_table.players[l_index].clientversion = l_str_list[1];
                  for (l_loop_var = 1; l_loop_var < l_index; l_loop_var++)
                  {
                    l_str_list.Empty();
                    l_str_list.Add(wxT("joined"));
                    l_str_list.Add(m_players_table.players[l_index].nickname);
                    l_str_list.Add(m_host_clients_table.clients[l_index].peerdetectedip);
                    if (g_configuration->enable_localip_optimization &&
                        (m_host_clients_table.clients[l_index].peerdetectedip == m_host_clients_table.clients[l_loop_var].peerdetectedip))
                      l_str_list.Add(m_host_clients_table.clients[l_index].selfdetectedip);
                    else
                      l_str_list.Add(m_host_clients_table.clients[l_index].peerdetectedip);
                    l_str_list.Add(wxString::Format(wxT("%d"), m_players_table.players[l_index].game_port_number));
                    g_SendStringListToSocket(m_host_clients_table.clients[l_loop_var].sock, l_str_list);
                  }
                  if (m_is_advertised)
                  {
                    l_str_list.Empty();
                    l_str_list.Add(wxT("nicks"));
                    for (l_loop_var = 0; l_loop_var <= l_index; l_loop_var++)
                      l_str_list.Add(m_players_table.players[l_loop_var].nickname);
                    g_SendStringListToSocket(m_client, l_str_list);
                  }
                  // Sending the player table.
                  l_str_list.Empty();
                  l_str_list.Add(wxT("playertable"));
                  for (l_loop_var = 0; l_loop_var < l_index; l_loop_var++)
                  {
                    l_str_list.Add(m_players_table.players[l_loop_var].nickname);
                    l_str_list.Add(m_host_clients_table.clients[l_loop_var].peerdetectedip);
                    if (g_configuration->enable_localip_optimization &&
                        (m_host_clients_table.clients[l_index].peerdetectedip == m_host_clients_table.clients[l_loop_var].peerdetectedip))
                      l_str_list.Add(m_host_clients_table.clients[l_loop_var].selfdetectedip);
                    else
                      l_str_list.Add(m_host_clients_table.clients[l_loop_var].peerdetectedip);
                    l_str_list.Add(wxString::Format(wxT("%d"), m_players_table.players[l_loop_var].game_port_number));
                  }
                  l_str_list.Add(m_players_table.players[l_index].nickname);
                  l_str_list.Add(m_host_clients_table.clients[l_index].peerdetectedip);
                  l_str_list.Add(m_host_clients_table.clients[l_index].selfdetectedip);
                  l_str_list.Add(wxString::Format(wxT("%d"), m_players_table.players[l_index].game_port_number));

                  g_SendStringListToSocket(l_sock, l_str_list);
                  l_num_of_items = m_modfiles.GetCount();
                  if (l_num_of_items)
                  {
                    l_str_list = GetServerModFilesSockStrList(l_num_of_items);
                    g_SendStringListToSocket(l_sock, l_str_list);
                  }
				  // get location from freegeoip.net
/*
wxHTTP get;
get.SetHeader(_T("Content-type"), _T("text/html; charset=utf-8"));
get.SetTimeout(3);
get.Connect(wxT("freegeoip.net"),false);

wxInputStream *httpStream = get.GetInputStream(_T("/csv/" + m_host_clients_table.clients[l_index].peerdetectedip));

if (get.GetError() == wxPROTO_NOERR)
{
    wxString res, country, region, city;
    wxStringOutputStream out_stream(&res);
    httpStream->Read(out_stream);
	wxStringTokenizer tokens(res, wxT(","));
	long x = 0;
	while ( tokens.HasMoreTokens() )
{
    wxString token = tokens.GetNextToken();
    if (x == 2){
		token.Remove(0,1);
		token.RemoveLast();
		country = token;
	}
	if (x == 4){
		token.Remove(0,1);
		token.RemoveLast();
		region = token;
	}
	if (x == 5){
							token.Remove(0,1);
							token.RemoveLast();
							city = token;
						}
						x++;
					}
					wxString location = city + ", " + region + ", " + country;
m_hostplayerslistCtrl->SetItem(l_index, 2, location);	
}
else
{
    m_hostplayerslistCtrl->SetItem(l_index, 2, "Unknown");
}

wxDELETE(httpStream);
get.Close();
// end of getting location.	
*/
                  if (m_allow_sounds && g_configuration->play_snd_join)
                    g_PlaySound(g_configuration->snd_join_file);
				  }
                else
                  if ((l_num_of_items == 1) && (l_str_list[0] == wxT("readyfortransfer")) &&
                      (m_transfer_state == TRANSFERSTATE_PENDING) &&
                      (m_host_clients_table.clients[l_index].peerdetectedip == m_host_clients_table.clients[m_transfer_client_index].peerdetectedip))
                  {
                    m_host_clients_table.clients[l_index].timer->Stop();
                    delete m_host_clients_table.clients[l_index].timer;
                    m_host_clients_table.DelByIndex(l_index);
                    l_sock->Notify(false);
                    if (m_transfer_is_download)
                    {
                      if (m_game == GAME_DN3D)
                        l_filename = g_configuration->dn3d_maps_dir + wxFILE_SEP_PATH + m_usermap;
                      else
                        l_filename = g_configuration->sw_maps_dir + wxFILE_SEP_PATH + m_usermap;
                      m_transfer_fp = new wxFile(l_filename, wxFile::read);
                      if (!m_transfer_fp->IsOpened())
                      {
                        wxLogError(wxT("Can't open file for reading!"));
                        delete m_transfer_fp;    
                        m_transfer_fp = NULL;
                        l_sock->Destroy();
                      }
                      else
                      {
                        m_transfer_thread = new FileSendThread(l_sock, this, m_transfer_fp, &m_transfer_state);
                        if ( m_transfer_thread->Create() != wxTHREAD_NO_ERROR )
                        {
                          wxLogError(wxT("Can't create file-transfer thread!"));
                          m_transfer_thread->Delete();
                          m_transfer_fp->Close();
                          delete m_transfer_fp;
                          m_transfer_fp = NULL;
                          l_sock->Destroy();
                        }
                        else
                        {
                          m_transfer_state = TRANSFERSTATE_BUSY;
                          m_transfer_thread->Run();
                          m_hosttransfergauge->SetRange((wxFileName::GetSize(l_filename)).ToULong());
                          m_transfer_timer = new wxTimer(this, ID_HOSTFILETRANSFERTIMER);
                          m_transfer_timer->Start(500, wxTIMER_CONTINUOUS);
                        }
                      }
                    }
                    else
                    {
                      g_SendCStringToSocket(l_sock, "1:readyforupload:");
                      if (m_game == GAME_DN3D)
                      {
                        if (!wxDirExists(g_configuration->dn3d_maps_dir))
                          wxFileName::Mkdir(g_configuration->dn3d_maps_dir, 0777, wxPATH_MKDIR_FULL);
                        l_filename = g_configuration->dn3d_maps_dir + wxFILE_SEP_PATH + m_transfer_filename;
                      }
                      else
                      {
                        if (!wxDirExists(g_configuration->sw_maps_dir))
                          wxFileName::Mkdir(g_configuration->sw_maps_dir, 0777, wxPATH_MKDIR_FULL);
                        l_filename = g_configuration->sw_maps_dir + wxFILE_SEP_PATH + m_transfer_filename;
                      }

                      // If file already exists, delete, as...
                      if (wxFileExists(l_filename))
                        wxRemoveFile(l_filename);
                      // ...if current map selected by the host is the very same,
                      // deselect and update all clients!
                      // As long as the newly uploaded map isn't ready, don't assume it is!
                      // The file transfer may also get aborted at some point,
                      // so for now, no map should be selected.

                      // Finally, the reason we don't just compare m_transfer_filename to m_usermap
                      // is that, on certain operating systems, we'd have to make a case-insensitive
                      // string comparison, while on others we'd have to make a case-sensitive one!

                      // The following method should work as expected on all platforms,
                      // at least in theory.
                      if (m_launch_usermap &&
                          (((m_game == GAME_DN3D) && (!wxFileExists(g_configuration->dn3d_maps_dir + wxFILE_SEP_PATH + m_usermap)))
                           || ((m_game == GAME_SW) && (!wxFileExists(g_configuration->sw_maps_dir + wxFILE_SEP_PATH + m_usermap)))))
                      {
                        m_launch_usermap = false;
                        m_epimap_num = 0;
                        // For any game this should be none.
                        m_hostmaptextCtrl->SetValue((*g_dn3d_level_list)[g_GetDN3DLevelSelectionByNum(0)]);
						g_configuration->gamelaunch_dn3dusermap = (*g_dn3d_level_list)[g_GetDN3DLevelSelectionByNum(0)];
                        l_str_list = GetServerInfoSockStrList();
                        for (l_loop_var = 1; l_loop_var < m_host_clients_table.num_clients; l_loop_var++)
                          g_SendStringListToSocket(m_host_clients_table.clients[l_loop_var].sock, l_str_list);
                      }

                      m_transfer_fp = new wxFile(l_filename, wxFile::write);
                      if (!m_transfer_fp->IsOpened())
                      {
                        wxLogError(wxT("Can't open file for writing!"));
                        delete m_transfer_fp;
                        m_transfer_fp = NULL;
                        l_sock->Destroy();
                      }
                      else
                      {
                        m_transfer_thread = new FileReceiveThread(l_sock, this, m_transfer_fp,
                                                                  m_players_table.requests[m_transfer_client_index].filesize,
                                                                  &m_transfer_state);
                        if ( m_transfer_thread->Create() != wxTHREAD_NO_ERROR )
                        {
                          wxLogError(wxT("Can't create file-transfer thread!"));
                          m_transfer_thread->Delete();
                          m_transfer_fp->Close();
                          delete m_transfer_fp;
                          m_transfer_fp = NULL;
                          l_sock->Destroy();
                        }
                        else
                        {
                          m_transfer_state = TRANSFERSTATE_BUSY;
                          m_transfer_thread->Run();
                          m_hosttransfergauge->SetRange(m_players_table.requests[m_transfer_client_index].filesize);
                          m_transfer_timer = new wxTimer(this, ID_HOSTFILETRANSFERTIMER);
                          m_transfer_timer->Start(500, wxTIMER_CONTINUOUS);
                        }
                      }
                    }
                  }
				  }
            else // Send additional info (teams + ready status) to clients when they ask for it.
              if ((l_str_list[0] == wxT("GetAdditionalPlayerInfo")))
              {
					// Send additional info about all players
				l_index = m_players_table.num_players;
                  l_str_list.Empty();
                  l_str_list.Add(wxT("AdditionalPlayerInfo"));
                  for (l_loop_var = 0; l_loop_var < l_index; l_loop_var++)
                  {
                    l_str_list.Add(m_players_table.players[l_loop_var].nickname);
                    l_str_list.Add(m_players_table.players[l_loop_var].team);
					l_str_list.Add(m_players_table.players[l_loop_var].status);
					l_str_list.Add(m_players_table.players[l_loop_var].clientversion);
                  }
                  l_str_list.Add(m_players_table.players[l_index].nickname);
                  l_str_list.Add(m_players_table.players[l_index].team);
                  l_str_list.Add(m_players_table.players[l_index].status);
				  l_str_list.Add(m_players_table.players[l_index].clientversion);
                  g_SendStringListToSocket(l_sock, l_str_list);
			  }				  
            else // In-room clients only.
            {
              if ((l_num_of_items == 1) && (l_str_list[0] == wxT("leave")))
              {
                l_str_list.Empty();
                l_str_list.Add(wxT("left"));
                l_str_list.Add(m_players_table.players[l_index].nickname);
                AddMessage(wxT("* ") + m_players_table.players[l_index].nickname + wxT(" has left the room."));
                if (m_allow_sounds && g_configuration->play_snd_leave)
                  g_PlaySound(g_configuration->snd_leave_file);
                RemoveInRoomClient(l_index);
                m_hostnumplayerstextCtrl->SetValue(wxString::Format(wxT("%d/%d"), m_players_table.num_players, m_max_num_players));
                m_hostplayerslistCtrl->DeleteItem(l_index);
//              RefreshListCtrlSize();
                l_sock->Destroy();
                for (l_loop_var = 1; l_loop_var < m_players_table.num_players; l_loop_var++)
                  g_SendStringListToSocket(m_host_clients_table.clients[l_loop_var].sock, l_str_list);
                if (m_is_advertised)
                {
                  l_str_list.Empty();
                  l_str_list.Add(wxT("nicks"));
                  for (l_loop_var = 0; l_loop_var < m_players_table.num_players; l_loop_var++)
                    l_str_list.Add(m_players_table.players[l_loop_var].nickname);
                  g_SendStringListToSocket(m_client, l_str_list);
                }
              }
              else
                if ((l_num_of_items == 2) && (l_str_list[0] == wxT("msg")))
                {
				  //Get current date.
				  if (g_configuration->show_timestamp)
				{
				  DateTime = wxDateTime::Now();
	              time = DateTime.Format(wxT("%X"));
				  }
				  // Output message and sound if not automated message (heartbeat) from host.
					if (l_str_list[1] != wxT("#(HeartBeat)@ :"))
					{
				  // 
				  // If timestamp
				  if (g_configuration->show_timestamp)
				{
                  AddMessage(wxT("[") + time + wxT("] ") + m_players_table.players[l_index].nickname + wxT(": ") + l_str_list[1]);
				  }
				 else {
				 AddMessage(m_players_table.players[l_index].nickname + wxT(": ") + l_str_list[1]);
				  }
                  if (m_allow_sounds && g_configuration->play_snd_receive)
                    g_PlaySound(g_configuration->snd_receive_file);
                  l_str_list.Insert(m_players_table.players[l_index].nickname, 1);
                  for (l_loop_var = 1; l_loop_var < l_index; l_loop_var++)
                    g_SendStringListToSocket(m_host_clients_table.clients[l_loop_var].sock, l_str_list);
                  for (l_loop_var = l_index+1; l_loop_var < m_players_table.num_players; l_loop_var++)
                    g_SendStringListToSocket(m_host_clients_table.clients[l_loop_var].sock, l_str_list);
                }
				}
				  else
			   if (l_str_list[0] == wxT("nickchange"))
                {
				// Broadcast that user has changed name
				AddMessage(wxT("* " + m_players_table.players[l_index].nickname + wxT(" is now known as ") + l_str_list[2] + wxT(".")));
				// Prepare str list for clients
				newnick = l_str_list[2];
				l_str_list.Empty();
                l_str_list.Add(wxT("nickchange"));
                l_str_list.Add(m_players_table.players[l_index].nickname);
				l_str_list.Add(newnick);
				// update host's player list.
				if ((m_dn3dgametype != DN3DMPGAMETYPE_DMSPAWN) && (m_dn3dgametype != DN3DMPGAMETYPE_COOP) && (m_dn3dgametype != DN3DMPGAMETYPE_DMNOSPAWN) && (m_srcport == SOURCEPORT_IDUKE3D)) 
		m_hostplayerslistCtrl->SetItem(l_index, 0, newnick + wxT(" [") + m_players_table.players[l_index].team + wxT("]"));
	else
		m_hostplayerslistCtrl->SetItem(l_index, 0, newnick);
				m_players_table.players[l_index].nickname = newnick;	
				// Broadcast to clients
					for (l_loop_var = 1; l_loop_var < l_index; l_loop_var++)
                    g_SendStringListToSocket(m_host_clients_table.clients[l_loop_var].sock, l_str_list);
                  for (l_loop_var = l_index+1; l_loop_var < m_players_table.num_players; l_loop_var++)
                    g_SendStringListToSocket(m_host_clients_table.clients[l_loop_var].sock, l_str_list);					
                // Update master server player list
				if (m_is_advertised)
          {
            l_str_list.Empty();
            l_str_list.Add(wxT("nicks"));
            for (l_loop_var = 0; l_loop_var < m_players_table.num_players; l_loop_var++)
              l_str_list.Add(m_players_table.players[l_loop_var].nickname);
            g_SendStringListToSocket(m_client, l_str_list);
          }
                  
				}
				else
			   if (l_str_list[0] == wxT("changeteam"))
                {
				wxString l_temp_str;
				l_temp_str = l_str_list[1];
				m_players_table.players[l_index].team = l_temp_str;
				m_hostplayerslistCtrl->SetItem(l_index, 0, m_players_table.players[l_index].nickname + wxT(" [") + l_temp_str + wxT("]"));	
				// Prepare str list for clients
				l_str_list.Empty();
                l_str_list.Add(wxT("changeteam"));
                l_str_list.Add(m_players_table.players[l_index].nickname);
				l_str_list.Add(l_temp_str);
				// Broadcast to clients
					for (l_loop_var = 1; l_loop_var < l_index; l_loop_var++)
                    g_SendStringListToSocket(m_host_clients_table.clients[l_loop_var].sock, l_str_list);
                  for (l_loop_var = l_index+1; l_loop_var < m_players_table.num_players; l_loop_var++)
                    g_SendStringListToSocket(m_host_clients_table.clients[l_loop_var].sock, l_str_list);					
				}
				else
			   if (l_str_list[0] == wxT("changeready"))
                {
				m_players_table.players[l_index].status = l_str_list[1];
				m_hostplayerslistCtrl->SetItem(l_index, 1, l_str_list[1]);	
				// Prepare str list for clients
				l_str_list.Empty();
                l_str_list.Add(wxT("changeready"));
                l_str_list.Add(m_players_table.players[l_index].nickname);
				l_str_list.Add(m_players_table.players[l_index].status);
				// Broadcast to clients
					for (l_loop_var = 1; l_loop_var < l_index; l_loop_var++)
                    g_SendStringListToSocket(m_host_clients_table.clients[l_loop_var].sock, l_str_list);
                  for (l_loop_var = l_index+1; l_loop_var < m_players_table.num_players; l_loop_var++)
                    g_SendStringListToSocket(m_host_clients_table.clients[l_loop_var].sock, l_str_list);					
				}			
				
				else
			// Acknowledge heartbeat if received
			   if (l_str_list[0] == wxT("Heartbeat"))
                {
					l_str_list.Empty();
					l_str_list.Add(wxT("Heartbeat"));
					l_str_list.Add(l_str_list[1]);
					l_str_list.Add(wxT("Acknowledge"));
					for (l_loop_var = l_index+1; l_loop_var < m_players_table.num_players; l_loop_var++)
                    g_SendStringListToSocket(m_host_clients_table.clients[l_loop_var].sock, l_str_list);	
				}
				
				else
			// If clients are asking for client version for a specific nickname.
			   if (l_str_list[0] == wxT("GetClientVersion"))
                {
					l_index = m_players_table.FindIndexByNick(l_str_list[1]);
					l_str_list.Empty();
					l_str_list.Add(wxT("GetClientVersion"));
					l_str_list.Add(m_players_table.players[l_index].nickname);
					l_str_list.Add(m_players_table.players[l_index].clientversion);
					g_SendStringListToSocket(l_sock, l_str_list);
				}
              else
                if ((l_num_of_items == 1) && (l_str_list[0] == wxT("requestdownload")))
                {
                  // We can do 4 things: Refuse, add to list of requests,
                  //                     add to list and waiting (while someone is downloading),
                  //                     or accept immediately.

                  // Refuse download when no user map is currently selected.
                  // We could also refuse in case there's an upload, but it's
                  // better to let the host decide in that case.
                  // However, refuse if the very same client is in the middle
                  // of a transfer.
                  if ((!m_launch_usermap) ||
                      ((m_transfer_state != TRANSFERSTATE_NONE) && (m_transfer_client_index == l_index)))
                    g_SendCStringToSocket(l_sock, "1:requestrefuse:");
                  else
                    // We may auto-accept a download if there's no transfer,
                    // we aren't in a game, and auto-acception is enabled.
                    if ((g_configuration->host_autoaccept_downloads) && (!m_in_game)
                        && (m_transfer_state == TRANSFERSTATE_NONE))
                    {
                      // The following lines are needed for some preparation.
                      m_players_table.requests[l_index].transferrequest = TRANSFERREQUEST_DOWNLOAD;
                      m_hostplayerslistCtrl->SetItem(l_index, 5, wxT("Download"));
//                    RefreshListCtrlSize();
                      m_hosttransferdynamicbutton->SetLabel(wxT("Abort transfer"));
                      m_hostlaunchbutton->Disable();
                      AcceptTransferRequest(l_index);
                    }
                    else // Add to list of requests.
                    {
                      m_players_table.requests[l_index].transferrequest = TRANSFERREQUEST_DOWNLOAD;
                      // If downloads are automatically accepted, it should be
                      // auto-accepted sometime if another download is completed.
                      m_players_table.requests[l_index].awaiting_download = g_configuration->host_autoaccept_downloads;
					if (m_players_table.players[l_index].clientversion == DUKEMATCHER_VERSION)
                      m_hostplayerslistCtrl->SetItemTextColour(l_index, *wxBLUE);
                      // Empty strings are set in case the client has recently requested an upload.
                      // The host should already be notified about a cancel
                      // of the upload request though.
                      m_hostplayerslistCtrl->SetItem(l_index, 5, wxT("Download"));
                      m_hostplayerslistCtrl->SetItem(l_index, 6, wxEmptyString);
                      m_hostplayerslistCtrl->SetItem(l_index, 7, wxEmptyString);
//                    RefreshListCtrlSize();
                    }
                }
              else
                if ((l_num_of_items == 3) && (l_str_list[0] == wxT("requestupload")))
                {
                  // We can do 2 things: Refuse, or add to list of requests.

                  // Refuse upload when the very same client is in the middle of a transfer.
                  if ((m_transfer_state != TRANSFERSTATE_NONE) && (m_transfer_client_index == l_index))
                    g_SendCStringToSocket(l_sock, "1:requestrefuse:");
                  else // Valid file size?
                    if (!l_str_list[2].ToLong(&l_temp_num))
                      g_SendCStringToSocket(l_sock, "1:requestrefuse:");
                    else
                      if (l_temp_num < 0)
                        g_SendCStringToSocket(l_sock, "1:requestrefuse:");
                      else
                      {
                        // The following should also work if the client has
                        // requested something else before.
                        // The host should already be notified about a cancel
                        // of the other request, though.

                        m_players_table.requests[l_index].transferrequest = TRANSFERREQUEST_UPLOAD;
                        m_players_table.requests[l_index].filename = l_str_list[1];
                        m_players_table.requests[l_index].filesize = l_temp_num;
                        // Use red color to warn the host about an existing file with the same name?
                        if (((m_game == GAME_DN3D) && (wxFileExists(g_configuration->dn3d_maps_dir + wxFILE_SEP_PATH + l_str_list[1])))
                            || ((m_game == GAME_SW) && (wxFileExists(g_configuration->sw_maps_dir + wxFILE_SEP_PATH + l_str_list[1]))))
							if (m_players_table.players[l_index].clientversion == DUKEMATCHER_VERSION)
                          m_hostplayerslistCtrl->SetItemTextColour(l_index, *wxRED);
                        else
						if (m_players_table.players[l_index].clientversion == DUKEMATCHER_VERSION)
                          m_hostplayerslistCtrl->SetItemTextColour(l_index, *wxBLUE);
                        m_hostplayerslistCtrl->SetItem(l_index, 5, wxT("Upload"));
                        m_hostplayerslistCtrl->SetItem(l_index, 6, l_str_list[1]);
                        m_hostplayerslistCtrl->SetItem(l_index, 7, l_str_list[2]);
//                      RefreshListCtrlSize();
                      }
                }
              else
                if ((l_num_of_items == 1) && (l_str_list[0] == wxT("requestcancel")))
                {
                  // Is the client in the middle of a transfer, or pending for one?
                  // In case it has already been aborted,
                  // that should be quite recent, so the request is simply ignored.
                  if ((m_transfer_state != TRANSFERSTATE_NONE) && (m_transfer_client_index == l_index))
                  {
                    if (m_transfer_state == TRANSFERSTATE_BUSY)
                    {
                      m_transfer_state = TRANSFERSTATE_ABORTED;
                      g_SendCStringToSocket(l_sock, "1:aborttransfer:");
                    }
                    else
                      if (m_transfer_state == TRANSFERSTATE_PENDING)
                      {
                        m_transfer_state = TRANSFERSTATE_NONE;
                        m_players_table.requests[l_index].transferrequest = TRANSFERREQUEST_NONE;
                        m_hostplayerslistCtrl->SetItemTextColour(l_index, *wxRED);
                        m_hostplayerslistCtrl->SetItem(l_index, 5, wxEmptyString);
                        m_hostplayerslistCtrl->SetItem(l_index, 6, wxEmptyString);
                        m_hostplayerslistCtrl->SetItem(l_index, 7, wxEmptyString);
//                      RefreshListCtrlSize();
                        // Let's check if there's a pending download request
                        // from another client.
                        FindNextDownloadRequest();
                      }
                      // In case of TRANSFERSTATE_ABORTED, the relevant socket
                      // deletion should end the file transfer thread and
                      // therefore trigger an event used to continue.
                  }
                  else // A simple cancel of a request instead.
                  {
                    m_players_table.requests[l_index].transferrequest = TRANSFERREQUEST_NONE;
					if (m_players_table.players[l_index].clientversion == DUKEMATCHER_VERSION)
						m_hostplayerslistCtrl->SetItemTextColour(l_index, *wxRED);
                    m_hostplayerslistCtrl->SetItem(l_index, 5, wxEmptyString);
                    m_hostplayerslistCtrl->SetItem(l_index, 6, wxEmptyString);
                    m_hostplayerslistCtrl->SetItem(l_index, 7, wxEmptyString);
//                  RefreshListCtrlSize();
                  }
                }
              else
                if ((l_num_of_items == 1) && (l_str_list[0] == wxT("aborttransfer"))
                    && (m_transfer_state == TRANSFERSTATE_BUSY))
                  m_transfer_state = TRANSFERSTATE_ABORTED;
            }
            l_num_of_items = g_ConvertStringBufToStringList(l_long_buffer, l_str_list);
          }
        }
        while (l_num_of_read_attempts < MAX_COMM_READ_ATTEMPTS);
        break;
      case wxSOCKET_LOST:
        if (l_index < m_players_table.num_players)
        {
          l_str_list.Add(wxT("disconnect"));
          l_str_list.Add(m_players_table.players[l_index].nickname);
          AddMessage(wxT("* ") + m_players_table.players[l_index].nickname + wxT(" has got disconnected from the room."));
          if (m_allow_sounds && g_configuration->play_snd_leave)
            g_PlaySound(g_configuration->snd_leave_file);
          RemoveInRoomClient(l_index);
          m_hostnumplayerstextCtrl->SetValue(wxString::Format(wxT("%d/%d"), m_players_table.num_players, m_max_num_players));
          m_hostplayerslistCtrl->DeleteItem(l_index);
//        RefreshListCtrlSize();
          for (l_loop_var = 1; l_loop_var < m_players_table.num_players; l_loop_var++)
            g_SendStringListToSocket(m_host_clients_table.clients[l_loop_var].sock, l_str_list);
          if (m_is_advertised)
          {
            l_str_list.Empty();
            l_str_list.Add(wxT("nicks"));
            for (l_loop_var = 0; l_loop_var < m_players_table.num_players; l_loop_var++)
              l_str_list.Add(m_players_table.players[l_loop_var].nickname);
            g_SendStringListToSocket(m_client, l_str_list);
          }
        }
        else
        {
          m_host_clients_table.clients[l_index].timer->Stop();
          delete m_host_clients_table.clients[l_index].timer;
          m_host_clients_table.DelByIndex(l_index);
        }
        l_sock->Destroy();
        break;
      default: ;
    }
}

void HostedRoomFrame::OnServerListSocketEvent(wxSocketEvent& event)
{
  size_t l_num_of_items;
  size_t l_loop_var;
  wxArrayString l_str_list;
  char l_short_buffer[MAX_COMM_TEXT_LENGTH];
  wxString l_long_buffer;
  wxIPV4address l_addr;
  wxSocketBase* l_sock = event.GetSocket();
  if (l_sock == m_client)
    switch (event.GetSocketEvent())
    {
      case wxSOCKET_CONNECTION:
	  m_client->SetTimeout(3);
        SendRoomInfo(true);
        m_serverlist_timer->Stop();
        l_str_list.Add(wxT("nicks"));
        for (l_loop_var = 0; l_loop_var < m_players_table.num_players; l_loop_var++)
          l_str_list.Add(m_players_table.players[l_loop_var].nickname);
        g_SendStringListToSocket(m_client, l_str_list);
        m_serverlist_timer->Start(25000, wxTIMER_ONE_SHOT);
        break;
      case wxSOCKET_INPUT:
        m_client->Read(l_short_buffer, MAX_COMM_TEXT_LENGTH);
        l_long_buffer << wxString(l_short_buffer, wxConvUTF8, m_client->LastCount());
        l_num_of_items = g_ConvertStringBufToStringList(l_long_buffer, l_str_list);
        if (l_num_of_items >= 2)
        {
          if (l_str_list[0] == wxT("error"))
          {
            AddMessage(wxT("* Error from master server: ") + l_str_list[1]);
            m_is_advertised = false;
            g_SendCStringToSocket(m_client, "1:disconnect:");
            m_client->Destroy();
            g_configuration->last_serverlist_in_cycle++;
            g_configuration->Save();
            ConnectToServerList();
          }
          else
            if ((l_str_list[0] == wxT("advertised")) && (!m_is_advertised))
            { // Send heartbeats to the master server.
              m_serverlist_timer->Start(60000, wxTIMER_CONTINUOUS);
              m_is_advertised = true;
              AddMessage(wxT("* Server is now advertised."));
              if (l_num_of_items == 3)
                AddMessage(wxT("\n* Message from the master server:\n") + l_str_list[2]);
              if (l_str_list[1] != (m_host_clients_table.clients[0].peerdetectedip))
              {
                m_host_clients_table.clients[0].peerdetectedip = l_str_list[1];
                m_hostplayerslistCtrl->SetItem(0, 3, l_str_list[1]);
                l_sock->GetLocal(l_addr);
                m_host_clients_table.clients[0].selfdetectedip = l_addr.IPAddress();
                m_players_table.players[0].ingameip = m_host_clients_table.clients[0].selfdetectedip;
                m_hostplayerslistCtrl->SetItem(0, 4, m_players_table.players[0].ingameip +
                                                     wxString::Format(wxT(":%d"), g_configuration->game_port_number));  								 
													 
//              RefreshListCtrlSize();
                l_str_list.Empty();
                l_str_list.Add(wxT("hostaddrupdate"));
                l_str_list.Add(m_host_clients_table.clients[0].peerdetectedip);

                for (l_loop_var = 1; l_loop_var < m_players_table.num_players; l_loop_var++)
                {
                  if (g_configuration->enable_localip_optimization &&
                      (m_host_clients_table.clients[0].peerdetectedip == m_host_clients_table.clients[l_loop_var].peerdetectedip))
                    l_str_list.Add(m_host_clients_table.clients[0].selfdetectedip);
                  else
                    l_str_list.Add(m_host_clients_table.clients[0].peerdetectedip);
                  g_SendStringListToSocket(m_host_clients_table.clients[l_loop_var].sock, l_str_list);
                  l_str_list.RemoveAt(3);
                }
              }
			  					// get location from freegeoip.net
wxHTTP get;
get.SetHeader(_T("Content-type"), _T("text/html; charset=utf-8"));
get.SetTimeout(3);
get.Connect(wxT("freegeoip.net"),false);

wxInputStream *httpStream = get.GetInputStream(_T("/csv/" + m_host_clients_table.clients[0].peerdetectedip));

if (get.GetError() == wxPROTO_NOERR)
{
    wxString res, country, region, city;
    wxStringOutputStream out_stream(&res);
    httpStream->Read(out_stream);
	wxStringTokenizer tokens(res, wxT(","));
	long x = 0;
	while ( tokens.HasMoreTokens() )
{
    wxString token = tokens.GetNextToken();
    if (x == 2){
		token.Remove(0,1);
		token.RemoveLast();
		country = token;
	}
	if (x == 4){
		token.Remove(0,1);
		token.RemoveLast();
		region = token;
	}
	if (x == 5){
							token.Remove(0,1);
							token.RemoveLast();
							city = token;
						}
						x++;
					}
					wxString location = city + ", " + region + ", " + country;
m_hostplayerslistCtrl->SetItem(0, 2, location);	
}
else
{
    m_hostplayerslistCtrl->SetItem(0, 2, "Unknown");
}

wxDELETE(httpStream);
get.Close();
// end of getting location.	
            }
//        l_num_of_items = g_ConvertStringBufToStringList(l_long_buffer, l_str_list);
        }
        break;
      case wxSOCKET_LOST:
        AddMessage(wxT("* Disconnected from master server."));
        m_is_advertised = false;
        m_client->Destroy();
        g_configuration->last_serverlist_in_cycle++;
        g_configuration->Save();
        ConnectToServerList();
        break;
      default: ;
    }
}

void HostedRoomFrame::ConnectToServerList()
{
  size_t l_num_of_lists = g_serverlists_portnums->GetCount(),
         l_num_of_extra_lists = g_configuration->extraservers_portnums.GetCount(),
         l_curr_server = g_configuration->last_serverlist_in_cycle;
  wxIPV4address l_addr;
  m_serverlist_timer->Stop();
  if (l_curr_server >= l_num_of_lists+l_num_of_extra_lists)
  {
    g_configuration->last_serverlist_in_cycle = l_curr_server = 0;
    g_configuration->Save();
  }
  AddMessage(wxString::Format(wxT("* Trying master server %d, please wait..."), l_curr_server+1));
  if (l_curr_server < l_num_of_extra_lists)
  {
    l_addr.Hostname(g_configuration->extraservers_addrs[l_curr_server]);
    l_addr.Service(g_configuration->extraservers_addrs[l_curr_server]);
  }
  else
  {
    l_curr_server -= l_num_of_extra_lists;
    l_addr.Hostname((*g_serverlists_addrs)[l_curr_server]);
    l_addr.Service((*g_serverlists_portnums)[l_curr_server]);
  }
  m_client = new wxSocketClient;
  m_client->SetTimeout(3);
  m_client->SetEventHandler(*this, ID_MASTERSOCKET);
  m_client->SetFlags(wxSOCKET_NONE|wxSOCKET_BLOCK);
  m_client->SetNotify(wxSOCKET_CONNECTION_FLAG |
                      wxSOCKET_INPUT_FLAG | wxSOCKET_LOST_FLAG);
  m_client->Notify(true);

  m_client->Connect(l_addr, false);
  m_serverlist_timer->Start(15000, wxTIMER_ONE_SHOT);
}

void HostedRoomFrame::Advertise()
{
  if (!(g_serverlists_portnums->GetCount() | g_configuration->extraservers_portnums.GetCount()))
  {
//  m_hostadvertisebutton->SetLabel(wxT(" Advertise room "));
    m_hostadvertisebutton->Disable();
    AddMessage(wxString::Format(wxT("ERROR: The list of master servers seems to be empty.")));
    if (g_configuration->play_snd_error)
      g_PlaySound(g_configuration->snd_error_file);
  }
  else
  {
    m_hostadvertisebutton->SetLabel(wxT("Stop advertising"));
    ConnectToServerList();
  }
}

void HostedRoomFrame::OnAdvertiseButtonClick(wxCommandEvent& WXUNUSED(event))
{
  m_in_game = false;
  if (m_hostadvertisebutton->GetLabel() == wxT(" Advertise room "))
    Advertise();
  else
  {
    m_is_advertised = false;
    m_hostadvertisebutton->SetLabel(wxT(" Advertise room "));
    if (m_client)
    {
      g_SendCStringToSocket(m_client, "1:disconnect:");
      m_client->Destroy();
      m_client = NULL;
    }
    m_serverlist_timer->Stop();
    AddMessage(wxString::Format(wxT("Server is now not advertised.")));
  }
}
/*
void HostedRoomFrame::OnNatFreeCheck(wxCommandEvent& WXUNUSED(event))
{
  m_in_game = false;
  if (m_is_advertised)
    SendRoomInfo(false);
  g_configuration->gamelaunch_enable_natfree = m_hostedroomnatfreecheckBox->GetValue();
  g_configuration->Save();
}
*/
void HostedRoomFrame::OnServerListTimer(wxTimerEvent& WXUNUSED(event))
{
  if (m_client != NULL)
  {
    if (m_is_advertised)
      g_SendCStringToSocket(m_client, "1:heartbeat:");
    else
    {
      g_SendCStringToSocket(m_client, "1:disconnect:");
      m_client->Destroy();
      g_configuration->last_serverlist_in_cycle++;
      g_configuration->Save();
      ConnectToServerList();
    }
  }
}

void HostedRoomFrame::OnCloseWindow(wxCloseEvent& WXUNUSED(event))
{
#ifdef ENABLE_UPNP
  UPNPUnForwardThread* l_thread = new UPNPUnForwardThread(g_configuration->game_port_number, false);
  if (l_thread->Create() == wxTHREAD_NO_ERROR )
    l_thread->Run();
  else
    l_thread->Delete();
  l_thread = new UPNPUnForwardThread(g_configuration->server_port_number, true);
  if (l_thread->Create() == wxTHREAD_NO_ERROR )
    l_thread->Run();
  else
    l_thread->Delete();
#endif
  bool l_do_shutdown = true;
  m_in_game = false;
  if (m_is_advertised)
    SendRoomInfo(false);
  m_allow_sounds = true;
  if (m_server)
  {
    if (m_players_table.num_players > 1)
      l_do_shutdown = (wxMessageBox(wxT("There are clients in your room. Are you sure you want to close it?"),
                                    wxT("Confirmation"), wxYES_NO|wxICON_EXCLAMATION) == wxYES);
    if (l_do_shutdown)
    {
      for (size_t l_loop_var = 1; l_loop_var < m_players_table.num_players; l_loop_var++)
        g_SendCStringToSocket(m_host_clients_table.clients[l_loop_var].sock, "1:shutdown:"); 
      for (size_t l_loop_var = 1; l_loop_var < m_host_clients_table.num_clients; l_loop_var++)
        m_host_clients_table.clients[l_loop_var].sock->Destroy();
      delete m_server;
      m_is_advertised = false;
      if (m_client)
      {
        g_SendCStringToSocket(m_client, "1:disconnect:");
        m_client->Destroy();
        m_client = NULL;
      }
      m_serverlist_timer->Stop();
	  if (m_msg_timer)
            {
              m_msg_timer->Stop();
              delete m_msg_timer;
              m_msg_timer = NULL;
            }
      m_server = NULL; // In case there's a pending event.
      // Is there an ongoing transfer?
      // - If pending, there's nothing to do.
      // - If busy, we should abort and wait for the transfer thread end.
      // - If already aborted, we should just wait, so there's nothing to do.
      if (m_transfer_state == TRANSFERSTATE_BUSY)
        m_transfer_state = TRANSFERSTATE_ABORTED;
      // Don't destroy the window if we need to wait for file transfer abortion.
      if (m_transfer_state == TRANSFERSTATE_ABORTED)
        Hide();
      else
        Destroy();
    }
  }
  else
    Destroy();
}

void HostedRoomFrame::OnChangeTeam(wxCommandEvent& WXUNUSED(event))
{
size_t l_loop_var;
wxString l_temp_str;
wxArrayString l_str_list;
l_temp_str = m_hostteamchoice->GetStringSelection();
m_players_table.players[0].team = l_temp_str;
m_hostplayerslistCtrl->SetItem(0, 0, m_players_table.players[0].nickname + wxT(" [") + m_players_table.players[0].team + wxT("]"));	
// Send team info to clients.
l_str_list.Empty();
l_str_list.Add(wxT("changeteam"));
l_str_list.Add(m_players_table.players[0].nickname);
l_str_list.Add(l_temp_str);
for (l_loop_var = 1; l_loop_var < m_players_table.num_players; l_loop_var++)
  g_SendStringListToSocket(m_host_clients_table.clients[l_loop_var].sock, l_str_list);
}

void HostedRoomFrame::OnChangeReady(wxCommandEvent& WXUNUSED(event))
{
size_t l_loop_var;
wxString l_temp_str;
wxArrayString l_str_list;
if (m_hostreadycheckBox->GetValue())
l_temp_str = "Ready";
else
l_temp_str = "Back Soon";
m_hostplayerslistCtrl->SetItem(0, 1, l_temp_str);
m_players_table.players[0].status = l_temp_str;
// Send info to clients.
l_str_list.Empty();
l_str_list.Add(wxT("changeready"));
l_str_list.Add(m_players_table.players[0].nickname);
l_str_list.Add(l_temp_str);
for (l_loop_var = 1; l_loop_var < m_players_table.num_players; l_loop_var++)
  g_SendStringListToSocket(m_host_clients_table.clients[l_loop_var].sock, l_str_list);
}

void HostedRoomFrame::OnCloseRoom(wxCommandEvent& WXUNUSED(event))
{
  Close();
}

void HostedRoomFrame::OnAutoAcceptCheck(wxCommandEvent& WXUNUSED(event))
{
  m_in_game = false;
  if (m_is_advertised)
    SendRoomInfo(false);
  g_configuration->host_autoaccept_downloads = m_hosttransfercheckBox->GetValue();
  g_configuration->Save();
}
