/*************************************************************************

Copyright 2008-2009 - NY00123

This file is part of DUKEMATCHER.

DUKEMATCHER is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

DUKEMATCHER is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DUKEMATCHER; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*************************************************************************/

#ifndef _DUKEMATCHER_HOSTFRAME_H_
#define _DUKEMATCHER_HOSTFRAME_H_

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/wx.h>
#include <wx/listctrl.h>
#include <wx/socket.h>
#include <wx/wfstream.h>
#include <wx/datetime.h>
#include <wx/timer.h>
#endif

#include "config.h"
#include "mp_common.h"
#include "file_transfer.h"
#include "theme.h"

#define ID_HOSTAUTOACCEPTCHECK 1000
#define ID_HOSTREFUSESELECTIONS 1001
#define ID_HOSTGENERALTRANSFERPURPOSE 1002
#define ID_HOSTOUTPUTTEXT 1003
#define ID_HOSTINPUTTEXT 1004
#define ID_HOSTSENDTEXT 1005
//#define ID_HOSTNATFREE 1006
#define ID_HOSTKICK 1007
//#define ID_HOSTSENDPM 1008
#define ID_HOSTROOMSETTINGS 1009
#define ID_HOSTROOMADVERTISE 1010
#define ID_ADVERTISETIMER 1011
#define ID_HOSTTIMESTAMPCHECK 1012

enum { ID_HOSTSERVER = wxID_HIGHEST+1, ID_HOSTSOCKET,
       ID_HOSTTIMER, ID_HOSTMSGTIMER, ID_HOSTCHANGETEAM, ID_HOSTCHANGEREADY, ID_HOSTFILETRANSFERTIMER, ID_MASTERSOCKET };

class HostedRoomFrame : public DUKEMATCHERFrame 
{
public:
  HostedRoomFrame();
  ~HostedRoomFrame();
//void RefreshListCtrlSize();
  void FocusFrame();

  void UpdateGameSettings(GameType game,
                          SourcePortType srcport,
                          const wxString& roomname,
                          size_t max_num_players,
                          bool recorddemo,
                          const wxString& demofilename,
                          size_t skill,
                          DN3DMPGameT_Type dn3dgametype,
                          DN3DSpawnType dn3dspawn,
						  size_t fraglimit,
						  bool noteamchange,
						  bool forcerespawn,
                          const wxString& args_for_host,
                          const wxString& args_for_all,
                          bool launch_usermap,
                          size_t epimap_num,
                          const wxString& usermap,
                          const wxArrayString& modfiles,
                          const wxString& modname,
                          bool showmodurl,
                          const wxString& modurl,
                          bool advertise,
                          bool usemasterslave);

  bool StartServer();
  void OnTextURLEvent(wxTextUrlEvent& event);
  void OnEnterText(wxCommandEvent& event);
  void OnKickClient(wxCommandEvent& event);
  void OnOpenRoomSettings(wxCommandEvent& event);
  void OnLaunchGame(wxCommandEvent& event);
  void FindNextDownloadRequest();
  void AcceptTransferRequest(size_t player_index);
  void OnTransferButtonClick(wxCommandEvent& event);
  void OnRefuseButtonClick(wxCommandEvent& event);
  void OnTransferThreadEnd(wxCommandEvent& event);
  void OnTimer(wxTimerEvent& event);
  void OnHostMsgTimer(wxTimerEvent& event);
  void OnFileTransferTimer(wxTimerEvent& event);
  void OnServerEvent(wxSocketEvent& event);
  void OnSocketEvent(wxSocketEvent& event);
  void SendRoomInfo(bool is_new_room);
  void OnServerListSocketEvent(wxSocketEvent& event);
  void ConnectToServerList();
  void Advertise();
  void OnAdvertiseButtonClick(wxCommandEvent& event);
//void OnNatFreeCheck(wxCommandEvent& event);
  void OnServerListTimer(wxTimerEvent& event);
  void OnClientVersionTimer(wxTimerEvent& event);
  void OnCloseRoom(wxCommandEvent& event);
  void OnCloseWindow(wxCloseEvent& event);
  void OnChangeTeam(wxCommandEvent& event);
  void OnChangeReady(wxCommandEvent& event);
  void OnAutoAcceptCheck(wxCommandEvent& event);
  
  wxArrayString GetServerInfoSockStrList();
  wxArrayString GetServerModFilesSockStrList(size_t num_of_files);
  wxDateTime DateTime;
  wxString time;
  wxString newnick;
private:
  DUKEMATCHERPanel* m_hostedroompanel;
  wxStaticText* m_hostgamenamestaticText;
  wxStaticText* m_hostskillstaticText;
  DUKEMATCHERTextCtrl* m_hostskilltextCtrl;
  DUKEMATCHERTextCtrl* m_hostgamenametextCtrl;
  wxStaticText* m_hostsrcportstaticText;
  wxStaticText* m_hostmapstaticText;
  DUKEMATCHERTextCtrl* m_hostmaptextCtrl;
  wxStaticText* m_hostspawnstaticText;
  DUKEMATCHERTextCtrl* m_hostspawntextCtrl;
  wxStaticText* m_hostmodstaticText;
  DUKEMATCHERTextCtrl* m_hostmodtextCtrl;
   wxStaticText* m_hostfraglimitstaticText;
  DUKEMATCHERTextCtrl* m_hostfraglimittextCtrl;
  DUKEMATCHERTextCtrl* m_hostsrcporttextCtrl;
  wxStaticText* m_hostgametypestaticText;
  DUKEMATCHERTextCtrl* m_hostgametypetextCtrl;
  DUKEMATCHERListCtrl* m_hostplayerslistCtrl;
  wxGauge* m_hosttransfergauge;
  wxStaticText* m_hosttransferstaticText;
DUKEMATCHERCheckBox* m_hostreadycheckBox;
wxStaticText* m_hostteamstaticText;
DUKEMATCHERChoice* m_hostteamchoice;

  DUKEMATCHERCheckBox* m_hosttransfercheckBox;
  DUKEMATCHERButton* m_hosttransferrefusebutton;
  DUKEMATCHERButton* m_hosttransferdynamicbutton;
  DUKEMATCHERTextCtrl* m_hostoutputtextCtrl;
  wxStaticText* m_hostnumplayersstaticText;
  DUKEMATCHERTextCtrl* m_hostnumplayerstextCtrl;
  wxStaticText* m_hostconnectionstaticText;
  DUKEMATCHERTextCtrl* m_hostconnectiontextCtrl;
  wxStaticText* m_hostrecordstaticText;
  DUKEMATCHERTextCtrl* m_hostrecordtextCtrl;
  DUKEMATCHERTextCtrl* m_hostinputtextCtrl;
  DUKEMATCHERButton* m_hostsendbutton;
//wxCheckBox* m_hostedroomnatfreecheckBox;
  DUKEMATCHERButton* m_hostcloseroombutton;
//DUKEMATCHERButton* m_hostsendpmbutton;
  DUKEMATCHERButton* m_hostkicknicknamebutton;
  DUKEMATCHERButton* m_hostsettingsbutton;
  DUKEMATCHERButton* m_hostadvertisebutton;
  DUKEMATCHERButton* m_hostlaunchbutton;
	
  void AddMessage(const wxString& msg);
  void RemoveInRoomClient(size_t index);

  wxSocketServer* m_server;

  // Avoids references to uninitialized variables on settings update when we don't want to!
  // This is related to file transfer.
  bool m_not_first_update;
  TransferStateType m_transfer_state;
  wxSocketServer* m_transfer_server;
//wxSocketBase* m_transfer_sock;
  size_t m_transfer_client_index;
  wxString m_transfer_filename;
  wxThread* m_transfer_thread;
  wxFile* m_transfer_fp;
  bool m_transfer_is_download;
  wxTimer* m_transfer_timer;
  wxTimer* m_msg_timer;
  
  ExtendedRoomPlayersTable m_players_table;
  HostRoomClientsTable m_host_clients_table;
//wxString m_selfdetectedips[MAX_NUM_PLAYERS];
//size_t m_num_players;

  bool m_allow_sounds;

  GameType m_game;
  SourcePortType m_srcport;
  wxString m_roomname;
  size_t m_max_num_players;
  bool m_recorddemo;
  wxString m_demofilename;
  size_t m_skill;
  DN3DMPGameT_Type m_dn3dgametype;
  DN3DSpawnType m_dn3dspawn;
  size_t m_fraglimit;
  bool m_noteamchange, m_forcerespawn;
  wxString m_args_for_host, m_args_for_all, m_hduke_args, m_team;
  bool m_launch_usermap;
  size_t m_epimap_num;
  wxString m_usermap;
  wxArrayString m_modfiles;
  wxString m_modname;
  bool m_showmodurl;
  wxString m_modurl;
  bool m_usemasterslave;
  bool m_in_game;

  bool m_is_advertised;
  wxSocketClient* m_client;
  wxTimer* m_serverlist_timer;

  wxColour l_def_listctrl_color;
  wxColour l_transfer_listctrl_color;

  DECLARE_EVENT_TABLE()
};

#endif

