/*************************************************************************

Copyright 2008-2009 - NY00123

This file is part of DUKEMATCHER.

DUKEMATCHER is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

DUKEMATCHER is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DUKEMATCHER; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*************************************************************************/

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/wx.h>
#include <wx/listctrl.h>
#include <wx/gbsizer.h>
#endif

#include "manualjoin_dialog.h"
#include "dukematcher.h"
#include "theme.h"
#include "dukematcher.xpm"

BEGIN_EVENT_TABLE(ManualJoinDialog, DUKEMATCHERDialog)

EVT_BUTTON(wxID_OK, ManualJoinDialog::OnOk)
EVT_BUTTON(wxID_CANCEL, ManualJoinDialog::OnCancel)
EVT_BUTTON(ID_MANUALJOINADD, ManualJoinDialog::OnAddHost)
EVT_BUTTON(ID_MANUALJOINREM, ManualJoinDialog::OnRemHost)
EVT_LIST_ITEM_SELECTED(ID_MANUALJOINLIST, ManualJoinDialog::OnHostSelect)
EVT_LIST_ITEM_ACTIVATED(ID_MANUALJOINLIST, ManualJoinDialog::OnHostActivate)
EVT_TEXT_ENTER(ID_MANUALJOINENTER, ManualJoinDialog::OnOk)

END_EVENT_TABLE()

ManualJoinDialog::ManualJoinDialog() : DUKEMATCHERDialog(NULL, wxID_ANY, wxT("Enter full host address"), wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE|wxDIALOG_NO_PARENT, wxEmptyString)
{
	wxBoxSizer* ManualJoinbSizer = new wxBoxSizer( wxVERTICAL );
	
	m_ManualJoinpanel = new DUKEMATCHERPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* ManualJoinpanelbSizer = new wxBoxSizer( wxVERTICAL );
	
	wxGridBagSizer* ManualJoinpanelgbSizer = new wxGridBagSizer( 0, 0 );
	ManualJoinpanelgbSizer->SetFlexibleDirection( wxBOTH );
	ManualJoinpanelgbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	m_manualjoinnamestaticText = new wxStaticText( m_ManualJoinpanel, wxID_ANY, wxT("Name"), wxDefaultPosition, wxDefaultSize, 0 );
	ManualJoinpanelgbSizer->Add( m_manualjoinnamestaticText, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_HORIZONTAL, 5 );
	
	m_manualjoinhostaddrstaticText = new wxStaticText( m_ManualJoinpanel, wxID_ANY, wxT("Host Address (IP)"), wxDefaultPosition, wxDefaultSize, 0 );
	ManualJoinpanelgbSizer->Add( m_manualjoinhostaddrstaticText, wxGBPosition( 0, 1 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_HORIZONTAL, 5 );
	
	m_manualjoinportnumstaticText = new wxStaticText( m_ManualJoinpanel, wxID_ANY, wxT("Port Number"), wxDefaultPosition, wxDefaultSize, 0 );
	ManualJoinpanelgbSizer->Add( m_manualjoinportnumstaticText, wxGBPosition( 0, 3 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_HORIZONTAL, 5 );
	
	m_manualjoinnametextCtrl = new DUKEMATCHERTextCtrl( m_ManualJoinpanel, ID_MANUALJOINENTER, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(64, -1)), wxTE_PROCESS_ENTER );
	ManualJoinpanelgbSizer->Add( m_manualjoinnametextCtrl, wxGBPosition( 1, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );
	
	m_manualjoinhostaddrtextCtrl = new DUKEMATCHERTextCtrl( m_ManualJoinpanel, ID_MANUALJOINENTER, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(80, -1)), wxTE_PROCESS_ENTER );
	ManualJoinpanelgbSizer->Add( m_manualjoinhostaddrtextCtrl, wxGBPosition( 1, 1 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );
	
	m_manualjoinseparatorstaticText = new wxStaticText( m_ManualJoinpanel, wxID_ANY, wxT(":"), wxDefaultPosition, wxDefaultSize, 0 );
	ManualJoinpanelgbSizer->Add( m_manualjoinseparatorstaticText, wxGBPosition( 1, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_manualjoinportnumtextCtrl = new DUKEMATCHERTextCtrl( m_ManualJoinpanel, ID_MANUALJOINENTER, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER );
	ManualJoinpanelgbSizer->Add( m_manualjoinportnumtextCtrl, wxGBPosition( 1, 3 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );
	
	m_manualjoinaddbutton = new DUKEMATCHERButton( m_ManualJoinpanel, ID_MANUALJOINADD, wxT("Add and save"), wxDefaultPosition, wxDefaultSize, 0 );
	ManualJoinpanelgbSizer->Add( m_manualjoinaddbutton, wxGBPosition( 1, 4 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL|wxALIGN_CENTER_HORIZONTAL, 5 );

	ManualJoinpanelbSizer->Add( ManualJoinpanelgbSizer, 0, wxEXPAND, 5 );

	wxBoxSizer* ManualJoinlistbSizer = new wxBoxSizer( wxHORIZONTAL );

//	m_manualjoinlistCtrl = new DUKEMATCHERListCtrl( m_ManualJoinpanel, ID_MANUALJOINLIST, wxDefaultPosition, wxDefaultSize, wxLC_REPORT );
	m_manualjoinlistCtrl = new DUKEMATCHERListCtrl( m_ManualJoinpanel, ID_MANUALJOINLIST, wxDefaultPosition, wxDLG_UNIT(this, wxSize(200, 80)), wxLC_REPORT );

	wxListItem itemCol;
	itemCol.SetText(wxT("Name"));
//	itemCol.SetAlign(wxLIST_FORMAT_LEFT);
//	itemCol.SetImage(-1);
	m_manualjoinlistCtrl->InsertColumn(0, itemCol);
	m_manualjoinlistCtrl->SetColumnWidth(0, (wxDLG_UNIT(this, wxSize(64, -1))).GetWidth() );
	itemCol.SetText(wxT("Host Address (IP)"));
	m_manualjoinlistCtrl->InsertColumn(1, itemCol);
	m_manualjoinlistCtrl->SetColumnWidth(1, (wxDLG_UNIT(this, wxSize(80, -1))).GetWidth() );
	itemCol.SetText(wxT("Port Number"));
	m_manualjoinlistCtrl->InsertColumn(2, itemCol);
	m_manualjoinlistCtrl->SetColumnWidth(2, (wxDLG_UNIT(this, wxSize(56, -1))).GetWidth() );

	ManualJoinlistbSizer->Add( m_manualjoinlistCtrl, 1, wxALL, 5 );

	m_manualjoinrembutton = new DUKEMATCHERButton( m_ManualJoinpanel, ID_MANUALJOINREM, wxT("Remove and save"), wxDefaultPosition, wxDefaultSize, 0 );
	ManualJoinlistbSizer->Add( m_manualjoinrembutton, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	ManualJoinpanelbSizer->Add( ManualJoinlistbSizer, 1, wxEXPAND, 5 );


	m_ManualJoinsdbSizer = new wxStdDialogButtonSizer();
	m_ManualJoinsdbSizerOK = new DUKEMATCHERButton( m_ManualJoinpanel, wxID_OK );
	m_ManualJoinsdbSizer->AddButton( m_ManualJoinsdbSizerOK );
	m_ManualJoinsdbSizerCancel = new DUKEMATCHERButton( m_ManualJoinpanel, wxID_CANCEL );
	m_ManualJoinsdbSizer->AddButton( m_ManualJoinsdbSizerCancel );
	m_ManualJoinsdbSizer->Realize();
	ManualJoinpanelbSizer->Add( m_ManualJoinsdbSizer, 0, wxALIGN_CENTER_HORIZONTAL, 5 );
	
	m_ManualJoinpanel->SetSizer( ManualJoinpanelbSizer );
	m_ManualJoinpanel->Layout();
	ManualJoinpanelbSizer->Fit( m_ManualJoinpanel );
	ManualJoinbSizer->Add( m_ManualJoinpanel, 1, wxEXPAND | wxALL, 5 );
	
	SetSizer( ManualJoinbSizer );
	Layout();
	ManualJoinbSizer->Fit( this );

  Centre();
  SetIcon(wxIcon(dukematcher_xpm));

  m_manualjoinportnumtextCtrl->SetValue(wxString::Format(wxT("%d"), g_configuration->server_port_number));

  size_t l_num_of_items = g_configuration->manualjoin_portnums.GetCount();
  for (size_t l_loop_var = 0; l_loop_var < l_num_of_items; l_loop_var++)
  {
    m_manualjoinlistCtrl->InsertItem(l_loop_var, g_configuration->manualjoin_names[l_loop_var]);
    m_manualjoinlistCtrl->SetItemTextColour(l_loop_var, *wxGREEN);
    m_manualjoinlistCtrl->SetItem(l_loop_var, 1, g_configuration->manualjoin_addrs[l_loop_var]);
    m_manualjoinlistCtrl->SetItem(l_loop_var, 2, wxString::Format(wxT("%d"), g_configuration->manualjoin_portnums[l_loop_var]));
  }
//RefreshListCtrlSize();
}
/*
void ManualJoinDialog::RefreshListCtrlSize()
{
  int l_width;
  if (m_manualjoinlistCtrl->GetItemCount())
    l_width = wxLIST_AUTOSIZE;
  else
    l_width = wxLIST_AUTOSIZE_USEHEADER;
  m_manualjoinlistCtrl->SetColumnWidth(0, l_width);
  m_manualjoinlistCtrl->SetColumnWidth(1, l_width);
  m_manualjoinlistCtrl->SetColumnWidth(2, l_width);
}
*/
void ManualJoinDialog::OnAddHost(wxCommandEvent& WXUNUSED(event))
{
  long l_temp_num;
  size_t l_num_of_items = g_configuration->manualjoin_names.GetCount();
  g_configuration->manualjoin_names.Add(m_manualjoinnametextCtrl->GetValue());
  g_configuration->manualjoin_addrs.Add(m_manualjoinhostaddrtextCtrl->GetValue());
  if (!((m_manualjoinportnumtextCtrl->GetValue()).ToLong(&l_temp_num)))
    l_temp_num = g_configuration->server_port_number;
  g_configuration->manualjoin_portnums.Add(l_temp_num);
  m_manualjoinlistCtrl->InsertItem(l_num_of_items, m_manualjoinnametextCtrl->GetValue());
  m_manualjoinlistCtrl->SetItemTextColour(l_num_of_items, *wxGREEN);
  m_manualjoinlistCtrl->SetItem(l_num_of_items, 1, m_manualjoinhostaddrtextCtrl->GetValue());
  m_manualjoinlistCtrl->SetItem(l_num_of_items, 2, wxString::Format(wxT("%d"), l_temp_num));
//RefreshListCtrlSize();
  g_configuration->Save();
}

void ManualJoinDialog::OnRemHost(wxCommandEvent& WXUNUSED(event))
{
  long l_item = m_manualjoinlistCtrl->GetNextItem(-1, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED);
  while (l_item >= 0) // Not -1
  {
    g_configuration->manualjoin_names.RemoveAt(l_item);
    g_configuration->manualjoin_addrs.RemoveAt(l_item);
    g_configuration->manualjoin_portnums.RemoveAt(l_item);
    m_manualjoinlistCtrl->DeleteItem(l_item);
    l_item = m_manualjoinlistCtrl->GetNextItem(-1, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED);
  }
//RefreshListCtrlSize();
  g_configuration->Save();
}

void ManualJoinDialog::OnHostSelect(wxListEvent& event)
{
// Thanks to the help of the following Wiki entry:
// http://wiki.wxwidgets.org/WxListCtrl

  wxListItem l_item;
  l_item.m_itemId = event.GetIndex();
  l_item.m_mask = wxLIST_MASK_TEXT;

  l_item.m_col = 0;
  m_manualjoinlistCtrl->GetItem(l_item);
  m_manualjoinnametextCtrl->SetValue(l_item.m_text);

  l_item.m_col = 1;
  m_manualjoinlistCtrl->GetItem(l_item);
  m_manualjoinhostaddrtextCtrl->SetValue(l_item.m_text);

  l_item.m_col = 2;
  m_manualjoinlistCtrl->GetItem(l_item);
  m_manualjoinportnumtextCtrl->SetValue(l_item.m_text);
}

void ManualJoinDialog::Connect()
{
  long l_port_num;
  wxString l_addr = m_manualjoinhostaddrtextCtrl->GetValue();
  if (l_addr.IsEmpty())
    return;
  if ((m_manualjoinportnumtextCtrl->GetValue()).ToLong(&l_port_num))
  {
    g_client_frame = new ClientRoomFrame;
    Disable();
    Update();
//  g_client_frame->ConnectToServer(l_addr, l_port_num);
    if (g_client_frame->ConnectToServer(l_addr, l_port_num))
    {
      g_client_frame->Show(true);
      g_client_frame->FocusFrame();
//    g_client_frame->SetFocus();
//    g_client_frame->Raise();
      Destroy();
    }
    else
    {
      Enable();
      wxMessageBox(wxT("Connection establishment has failed."), wxEmptyString, wxOK|wxICON_INFORMATION);
      g_client_frame->Destroy();
      SetFocus();
    }
  }
  else
    wxMessageBox(wxT("The given port number seems to be invalid.\nIs it really a number?"), wxT("Invalid port number"), wxOK|wxICON_INFORMATION);
}

void ManualJoinDialog::OnHostActivate(wxListEvent& WXUNUSED(event))
{
  Connect();
}

/*
ManualJoinDialog::~ManualJoinDialog()
{
  g_manualjoin_dialog = NULL;
}
*/
void ManualJoinDialog::OnOk(wxCommandEvent& WXUNUSED(event))
{
  Connect();
}

void ManualJoinDialog::OnCancel(wxCommandEvent& WXUNUSED(event))
{
  g_main_frame->ShowMainFrame();
  Destroy();
}

