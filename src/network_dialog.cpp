/*************************************************************************

Copyright 2008-2009 - NY00123

This file is part of DUKEMATCHER.

DUKEMATCHER is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

DUKEMATCHER is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DUKEMATCHER; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*************************************************************************/

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/wx.h>
#include <wx/notebook.h>
//#include <wx/listbook.h>
#include <wx/gbsizer.h>
#endif

#include "network_dialog.h"
#include "adv_dialog.h"
#include "main_frame.h"
#include "config.h"
#include "dukematcher.h"
#include "theme.h"
#include "dukematcher.xpm"

BEGIN_EVENT_TABLE(NetworkDialog, DUKEMATCHERDialog)

EVT_BUTTON(wxID_OK, NetworkDialog::OnOk)
//EVT_BUTTON(wxID_APPLY, NetworkDialog::OnApply)
EVT_BUTTON(wxID_CANCEL, NetworkDialog::OnCancel)

EVT_BUTTON(ID_SNDJOINTEST, NetworkDialog::OnTestSound)
EVT_BUTTON(ID_SNDLEAVETEST, NetworkDialog::OnTestSound)
EVT_BUTTON(ID_SNDSENDTEST, NetworkDialog::OnTestSound)
EVT_BUTTON(ID_SNDRECEIVETEST, NetworkDialog::OnTestSound)
EVT_BUTTON(ID_SNDERRORTEST, NetworkDialog::OnTestSound)

EVT_BUTTON(ID_SNDJOINLOCATE, NetworkDialog::OnLocateSndFile)
EVT_BUTTON(ID_SNDLEAVELOCATE, NetworkDialog::OnLocateSndFile)
EVT_BUTTON(ID_SNDSENDLOCATE, NetworkDialog::OnLocateSndFile)
EVT_BUTTON(ID_SNDRECEIVELOCATE, NetworkDialog::OnLocateSndFile)
EVT_BUTTON(ID_SNDERRORLOCATE, NetworkDialog::OnLocateSndFile)
EVT_BUTTON(ID_SNDRESTORE, NetworkDialog::OnSndRestore)

EVT_CHECKBOX(ID_SNDJOINCHECKBOX, NetworkDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_SNDLEAVECHECKBOX, NetworkDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_SNDSENDCHECKBOX, NetworkDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_SNDRECEIVECHECKBOX, NetworkDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_SNDERRORCHECKBOX, NetworkDialog::OnCheckBoxClick)

EVT_TEXT(ID_GAMENICKNAMETEXT, NetworkDialog::OnUpdateGameNickName)

END_EVENT_TABLE()

NetworkDialog::NetworkDialog() : DUKEMATCHERDialog(NULL, wxID_ANY, wxT("Multiplayer and networking"), wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE|wxDIALOG_NO_PARENT, wxEmptyString)
{
	wxBoxSizer* NetworkDialogbSizer = new wxBoxSizer( wxVERTICAL );
	
        m_networknotebook = new DUKEMATCHERNotebook( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
//	m_networklistbook = new wxListbook( this, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(88, -1)), wxLB_DEFAULT|wxTAB_TRAVERSAL );
	m_networkprofilepanel = new DUKEMATCHERPanel( m_networknotebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* profilepanelbSizer = new wxBoxSizer( wxVERTICAL );
	
	wxGridBagSizer* profilegbSizer = new wxGridBagSizer( 0, 0 );
	profilegbSizer->AddGrowableCol( 1 );
	profilegbSizer->SetFlexibleDirection( wxBOTH );
	profilegbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	m_nicknamestaticText = new wxStaticText( m_networkprofilepanel, wxID_ANY, wxT("Nickname:"), wxDefaultPosition, wxDefaultSize, 0 );
	profilegbSizer->Add( m_nicknamestaticText, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_nicknametextCtrl = new DUKEMATCHERTextCtrl( m_networkprofilepanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(128, -1)) );
	profilegbSizer->Add( m_nicknametextCtrl, wxGBPosition( 0, 1 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );
	
	m_gamenicknamestaticText = new wxStaticText( m_networkprofilepanel, wxID_ANY, wxT("In-game nickname*:"), wxDefaultPosition, wxDefaultSize, 0 );
	profilegbSizer->Add( m_gamenicknamestaticText, wxGBPosition( 1, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_nicknamenotestaticText = new wxStaticText( m_networkprofilepanel, wxID_ANY, wxT("* For the in-game nickname,\nonly the following characters are allowed:\n\n# @ _ - +\nand all digits and English letters."), wxDefaultPosition, wxDefaultSize, 0 );
	profilegbSizer->Add( m_nicknamenotestaticText, wxGBPosition( 2, 0 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_gamenicknametextCtrl = new DUKEMATCHERTextCtrl( m_networkprofilepanel, ID_GAMENICKNAMETEXT, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(48, -1)) );
	profilegbSizer->Add( m_gamenicknametextCtrl, wxGBPosition( 1, 1 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	profilepanelbSizer->Add( profilegbSizer, 1, wxEXPAND, 0 );
	
	m_networkprofilepanel->SetSizer( profilepanelbSizer );
	m_networkprofilepanel->Layout();
	profilepanelbSizer->Fit( m_networkprofilepanel );
	m_networknotebook->AddPage( m_networkprofilepanel, wxT("Player profile"), true );
	m_networksndnotipanel = new DUKEMATCHERPanel( m_networknotebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* sndnotibSizer = new wxBoxSizer( wxVERTICAL );
	
	wxGridBagSizer* sndnotigbSizer = new wxGridBagSizer( 0, 0 );
	sndnotigbSizer->SetFlexibleDirection( wxBOTH );
	sndnotigbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	m_soundjoincheckBox = new DUKEMATCHERCheckBox( m_networksndnotipanel, ID_SNDJOINCHECKBOX, wxT("Play a sound when a player joins a room."), wxDefaultPosition, wxDefaultSize, 0 );
	
	sndnotigbSizer->Add( m_soundjoincheckBox, wxGBPosition( 0, 0 ), wxGBSpan( 1, 3 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_soundjointestbutton = new DUKEMATCHERButton( m_networksndnotipanel, ID_SNDJOINTEST, wxT("Test"), wxDefaultPosition, wxDefaultSize, 0 );
	sndnotigbSizer->Add( m_soundjointestbutton, wxGBPosition( 0, 3 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT, 5 );
	
	m_soundjoinlocatebutton = new DUKEMATCHERButton( m_networksndnotipanel, ID_SNDJOINLOCATE, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	sndnotigbSizer->Add( m_soundjoinlocatebutton, wxGBPosition( 1, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_soundjointextCtrl = new DUKEMATCHERTextCtrl( m_networksndnotipanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	sndnotigbSizer->Add( m_soundjointextCtrl, wxGBPosition( 1, 1 ), wxGBSpan( 1, 3 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_soundleavecheckBox = new DUKEMATCHERCheckBox( m_networksndnotipanel, ID_SNDLEAVECHECKBOX, wxT("Play a sound when a player leaves a room."), wxDefaultPosition, wxDefaultSize, 0 );
	
	sndnotigbSizer->Add( m_soundleavecheckBox, wxGBPosition( 2, 0 ), wxGBSpan( 1, 3 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_soundleavetestbutton = new DUKEMATCHERButton( m_networksndnotipanel, ID_SNDLEAVETEST, wxT("Test"), wxDefaultPosition, wxDefaultSize, 0 );
	sndnotigbSizer->Add( m_soundleavetestbutton, wxGBPosition( 2, 3 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT, 5 );
	
	m_soundleavelocatebutton = new DUKEMATCHERButton( m_networksndnotipanel, ID_SNDLEAVELOCATE, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	sndnotigbSizer->Add( m_soundleavelocatebutton, wxGBPosition( 3, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_soundleavetextCtrl = new DUKEMATCHERTextCtrl( m_networksndnotipanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	sndnotigbSizer->Add( m_soundleavetextCtrl, wxGBPosition( 3, 1 ), wxGBSpan( 1, 3 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_soundsendcheckBox = new DUKEMATCHERCheckBox( m_networksndnotipanel, ID_SNDSENDCHECKBOX, wxT("Play a sound when you send a message."), wxDefaultPosition, wxDefaultSize, 0 );
	
	sndnotigbSizer->Add( m_soundsendcheckBox, wxGBPosition( 4, 0 ), wxGBSpan( 1, 3 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_soundsendtestbutton = new DUKEMATCHERButton( m_networksndnotipanel, ID_SNDSENDTEST, wxT("Test"), wxDefaultPosition, wxDefaultSize, 0 );
	sndnotigbSizer->Add( m_soundsendtestbutton, wxGBPosition( 4, 3 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT, 5 );
	
	m_soundsendlocatebutton = new DUKEMATCHERButton( m_networksndnotipanel, ID_SNDSENDLOCATE, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	sndnotigbSizer->Add( m_soundsendlocatebutton, wxGBPosition( 5, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_soundsendtextCtrl = new DUKEMATCHERTextCtrl( m_networksndnotipanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	sndnotigbSizer->Add( m_soundsendtextCtrl, wxGBPosition( 5, 1 ), wxGBSpan( 1, 3 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_soundreceivecheckBox = new DUKEMATCHERCheckBox( m_networksndnotipanel, ID_SNDRECEIVECHECKBOX, wxT("Play a sound when you receive a message."), wxDefaultPosition, wxDefaultSize, 0 );
	
	sndnotigbSizer->Add( m_soundreceivecheckBox, wxGBPosition( 6, 0 ), wxGBSpan( 1, 3 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_soundreceivetestbutton = new DUKEMATCHERButton( m_networksndnotipanel, ID_SNDRECEIVETEST, wxT("Test"), wxDefaultPosition, wxDefaultSize, 0 );
	sndnotigbSizer->Add( m_soundreceivetestbutton, wxGBPosition( 6, 3 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT, 5 );
	
	m_soundreceivelocatebutton = new DUKEMATCHERButton( m_networksndnotipanel, ID_SNDRECEIVELOCATE, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	sndnotigbSizer->Add( m_soundreceivelocatebutton, wxGBPosition( 7, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_soundreceivetextCtrl = new DUKEMATCHERTextCtrl( m_networksndnotipanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	sndnotigbSizer->Add( m_soundreceivetextCtrl, wxGBPosition( 7, 1 ), wxGBSpan( 1, 3 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_sounderrorcheckBox = new DUKEMATCHERCheckBox( m_networksndnotipanel, ID_SNDERRORCHECKBOX, wxT("Play a sound when something wrong occurs."), wxDefaultPosition, wxDefaultSize, 0 );
	
	sndnotigbSizer->Add( m_sounderrorcheckBox, wxGBPosition( 8, 0 ), wxGBSpan( 1, 3 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_sounderrortestbutton = new DUKEMATCHERButton( m_networksndnotipanel, ID_SNDERRORTEST, wxT("Test"), wxDefaultPosition, wxDefaultSize, 0 );
	sndnotigbSizer->Add( m_sounderrortestbutton, wxGBPosition( 8, 3 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT, 5 );
	
	m_sounderrorlocatebutton = new DUKEMATCHERButton( m_networksndnotipanel, ID_SNDERRORLOCATE, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	sndnotigbSizer->Add( m_sounderrorlocatebutton, wxGBPosition( 9, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_sounderrortextCtrl = new DUKEMATCHERTextCtrl( m_networksndnotipanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	sndnotigbSizer->Add( m_sounderrortextCtrl, wxGBPosition( 9, 1 ), wxGBSpan( 1, 3 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_soundmutewhileingamecheckBox = new DUKEMATCHERCheckBox( m_networksndnotipanel, wxID_ANY, wxT("Mute all sounds while in game and until you do some action."), wxDefaultPosition, wxDefaultSize, 0 );

	sndnotigbSizer->Add( m_soundmutewhileingamecheckBox, wxGBPosition( 10, 0 ), wxGBSpan( 1, 4 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_soundrestorebutton = new DUKEMATCHERButton( m_networksndnotipanel, ID_SNDRESTORE, wxT("Restore defaults"), wxDefaultPosition, wxDefaultSize, 0 );
	sndnotigbSizer->Add( m_soundrestorebutton, wxGBPosition( 11, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	sndnotibSizer->Add( sndnotigbSizer, 1, wxEXPAND, 5 );

	m_networksndnotipanel->SetSizer( sndnotibSizer );
	m_networksndnotipanel->Layout();
	sndnotibSizer->Fit( m_networksndnotipanel );
	m_networknotebook->AddPage( m_networksndnotipanel, wxT("Sound notifications"), false );
	m_networknetpanel = new DUKEMATCHERPanel( m_networknotebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* netbSizer = new wxBoxSizer( wxVERTICAL );
	
	wxGridBagSizer* netgbSizer = new wxGridBagSizer( 0, 0 );
	netgbSizer->SetFlexibleDirection( wxBOTH );
	netgbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	m_gameportstaticText = new wxStaticText( m_networknetpanel, wxID_ANY, wxT("Game port number (UDP):"), wxDefaultPosition, wxDefaultSize, 0 );
	netgbSizer->Add( m_gameportstaticText, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_gameporttextCtrl = new DUKEMATCHERTextCtrl( m_networknetpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(32, -1)) );
	netgbSizer->Add( m_gameporttextCtrl, wxGBPosition( 0, 1 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_serverportstaticText = new wxStaticText( m_networknetpanel, wxID_ANY, wxT("DUKEMATCHER server port number (TCP):"), wxDefaultPosition, wxDefaultSize, 0 );
	netgbSizer->Add( m_serverportstaticText, wxGBPosition( 1, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_serverporttextCtrl = new DUKEMATCHERTextCtrl( m_networknetpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(32, -1)) );
	netgbSizer->Add( m_serverporttextCtrl, wxGBPosition( 1, 1 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	

	m_netnotesstaticText = new wxStaticText( m_networknetpanel, wxID_ANY, wxT("PLEASE READ THE FOLLOWING:\n\n* If you want two or more players on the same\nlocal network to join INTERNET rooms, you should\nset DIFFERENT game port numbers. Otherwise\nthere won't be a way for the outside (Internet) players\nto tell the difference between you two or more.\n\n* If you want to host two or more game rooms\non the same local network, apart from stealing a\nnot so-tiny amount of bandwidth, that'd also require\nyou to use DIFFERENT server port numbers\nfor the rooms."), wxDefaultPosition, wxDefaultSize, 0 );
	netgbSizer->Add( m_netnotesstaticText, wxGBPosition( 3, 0 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	netbSizer->Add( netgbSizer, 1, wxEXPAND, 0 );
	
	m_networknetpanel->SetSizer( netbSizer );
	m_networknetpanel->Layout();
	netbSizer->Fit( m_networknetpanel );
	m_networknotebook->AddPage( m_networknetpanel, wxT("Networking"), false );
	// Chat config
	m_chatpanel = new DUKEMATCHERPanel( m_networknotebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* chatpanelbSizer = new wxBoxSizer( wxVERTICAL );
	
	wxGridBagSizer* chatgbSizer = new wxGridBagSizer( 0, 0 );
	chatgbSizer->AddGrowableCol( 1 );
	chatgbSizer->SetFlexibleDirection( wxBOTH );
	chatgbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	m_chattimestampcheckBox = new DUKEMATCHERCheckBox( m_chatpanel, wxID_ANY, wxT("Show timestamp in chat."), wxDefaultPosition, wxDefaultSize, 0 );
	
	chatgbSizer->Add( m_chattimestampcheckBox, wxGBPosition( 1, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	chatpanelbSizer->Add( chatgbSizer, 1, wxEXPAND, 0 );
	
	m_chatpanel->SetSizer( chatpanelbSizer );
	m_chatpanel->Layout();
	chatpanelbSizer->Fit( m_chatpanel );
	m_networknotebook->AddPage( m_chatpanel, wxT("Chat"), false );	
	//
	NetworkDialogbSizer->Add( m_networknotebook, 1, wxEXPAND | wxALL, 5 );
	
	m_networksdbSizer = new wxStdDialogButtonSizer();
	m_networksdbSizerOK = new DUKEMATCHERButton( this, wxID_OK );
	m_networksdbSizer->AddButton( m_networksdbSizerOK );
	m_networksdbSizerCancel = new DUKEMATCHERButton( this, wxID_CANCEL );
	m_networksdbSizer->AddButton( m_networksdbSizerCancel );
	m_networksdbSizer->Realize();
	NetworkDialogbSizer->Add( m_networksdbSizer, 0, wxALIGN_CENTER_HORIZONTAL, 5 );
	
//	m_networknotebook->Layout();
	SetSizer( NetworkDialogbSizer );
	Layout();
	NetworkDialogbSizer->Fit( this );

  Centre();
  SetIcon(wxIcon(dukematcher_xpm));

  m_nicknametextCtrl->SetMaxLength(NICK_MAX_LENGTH);
  m_gamenicknametextCtrl->SetMaxLength(GAMENICK_MAX_LENGTH);

  m_nicknametextCtrl->SetValue(g_configuration->nickname);
  m_gamenicknametextCtrl->SetValue(g_configuration->game_nickname);

  m_soundjoincheckBox->SetValue(g_configuration->play_snd_join);
  m_soundjointextCtrl->SetValue(g_configuration->snd_join_file);
  m_soundjointestbutton->Enable(g_configuration->play_snd_join);
  m_soundjoinlocatebutton->Enable(g_configuration->play_snd_join);
  m_soundjointextCtrl->Enable(g_configuration->play_snd_join);

  m_soundleavecheckBox->SetValue(g_configuration->play_snd_leave);
  m_soundleavetextCtrl->SetValue(g_configuration->snd_leave_file);
  m_soundleavetestbutton->Enable(g_configuration->play_snd_leave);
  m_soundleavelocatebutton->Enable(g_configuration->play_snd_leave);
  m_soundleavetextCtrl->Enable(g_configuration->play_snd_leave);

  m_soundsendcheckBox->SetValue(g_configuration->play_snd_send);
  m_soundsendtextCtrl->SetValue(g_configuration->snd_send_file);
  m_soundsendtestbutton->Enable(g_configuration->play_snd_send);
  m_soundsendlocatebutton->Enable(g_configuration->play_snd_send);
  m_soundsendtextCtrl->Enable(g_configuration->play_snd_send);

  m_soundreceivecheckBox->SetValue(g_configuration->play_snd_receive);
  m_soundreceivetextCtrl->SetValue(g_configuration->snd_receive_file);
  m_soundreceivetestbutton->Enable(g_configuration->play_snd_receive);
  m_soundreceivelocatebutton->Enable(g_configuration->play_snd_receive);
  m_soundreceivetextCtrl->Enable(g_configuration->play_snd_receive);

  m_sounderrorcheckBox->SetValue(g_configuration->play_snd_error);
  m_sounderrortextCtrl->SetValue(g_configuration->snd_error_file);
  m_sounderrortestbutton->Enable(g_configuration->play_snd_error);
  m_sounderrorlocatebutton->Enable(g_configuration->play_snd_error);
  m_sounderrortextCtrl->Enable(g_configuration->play_snd_error);

  m_soundmutewhileingamecheckBox->SetValue(g_configuration->mute_sounds_while_ingame);

  m_gameporttextCtrl->SetValue(wxString::Format(wxT("%d"), g_configuration->game_port_number));
  m_serverporttextCtrl->SetValue(wxString::Format(wxT("%d"), g_configuration->server_port_number));
  
   m_chattimestampcheckBox->SetValue(g_configuration->show_timestamp);
}

NetworkDialog::~NetworkDialog()
{
#if ((defined __WXMSW__) || (defined __WXMAC__) || (defined __WXCOCOA__))
  if (!g_main_frame)
    g_main_frame = new MainFrame;
  g_main_frame->ShowMainFrame();
#else
  if (g_main_frame)
    g_main_frame->ShowMainFrame();
  else
  {
    g_configuration->DetectTerminal();
    if (g_configuration->terminal_fullcmd.IsEmpty())
    {
      wxMessageBox(wxT("WARNING: No graphical terminal emulator has been detected.\nIt's HIGHLY RECOMMENDED (and sometimes, even a must!) to execute a game from within a terminal emulator.\nAs an example, an EDuke32 multiplayer game has got stuck at the GTK startup because of that."), wxT("No terminal emulator detected"), wxOK|wxICON_EXCLAMATION);
      g_adv_dialog = new AdvDialog;
      g_adv_dialog->Show(true);
      g_adv_dialog->SetFocus();
    }
    else
    {
      g_configuration->Save();
      g_main_frame = new MainFrame;
      g_main_frame->ShowMainFrame();
    }
  }
#endif
//g_network_dialog = NULL;
}

void NetworkDialog::OnLocateSndFile(wxCommandEvent& event)
{
  wxFileDialog l_dialog(NULL, wxT("Select a sound file"), wxEmptyString, wxEmptyString,
                        wxT("RIFF wave files (*.wav)|*.wav|All files|*"), wxFD_OPEN|wxFD_FILE_MUST_EXIST);
  if (l_dialog.ShowModal() == wxID_OK)
  {
    wxString l_fullpath = l_dialog.GetPath();
    switch (event.GetId())
    {
      case ID_SNDJOINLOCATE: m_soundjointextCtrl->SetValue(l_fullpath); break;
      case ID_SNDLEAVELOCATE: m_soundleavetextCtrl->SetValue(l_fullpath); break;
      case ID_SNDSENDLOCATE: m_soundsendtextCtrl->SetValue(l_fullpath); break;
      case ID_SNDRECEIVELOCATE: m_soundreceivetextCtrl->SetValue(l_fullpath); break;
      default: /*ID_SNDERRORLOCATE*/ m_sounderrortextCtrl->SetValue(l_fullpath);
    }
  }
}

void NetworkDialog::OnTestSound(wxCommandEvent& event)
{
  switch (event.GetId())
  {
      case ID_SNDJOINTEST: g_PlaySound(m_soundjointextCtrl->GetValue()); break;
      case ID_SNDLEAVETEST: g_PlaySound(m_soundleavetextCtrl->GetValue()); break;
      case ID_SNDSENDTEST: g_PlaySound(m_soundsendtextCtrl->GetValue()); break;
      case ID_SNDRECEIVETEST: g_PlaySound(m_soundreceivetextCtrl->GetValue()); break;
      default: /*ID_SNDERRORTEST*/ g_PlaySound(m_sounderrortextCtrl->GetValue());
  }
}

void NetworkDialog::OnSndRestore(wxCommandEvent& WXUNUSED(event))
{
  wxString l_snd_join, l_snd_leave, l_snd_send, l_snd_receive, l_snd_error;
  RestoreDefaultSounds(l_snd_join, l_snd_leave, l_snd_send, l_snd_receive, l_snd_error);
  m_soundjointextCtrl->SetValue(l_snd_join);
  m_soundleavetextCtrl->SetValue(l_snd_leave);
  m_soundsendtextCtrl->SetValue(l_snd_send);
  m_soundreceivetextCtrl->SetValue(l_snd_receive);
  m_sounderrortextCtrl->SetValue(l_snd_error);
  m_soundjointextCtrl->Enable();
  m_soundleavetextCtrl->Enable();
  m_soundsendtextCtrl->Enable();
  m_soundreceivetextCtrl->Enable();
  m_sounderrortextCtrl->Enable();
  m_soundjoincheckBox->SetValue(true);
  m_soundleavecheckBox->SetValue(true);
  m_soundsendcheckBox->SetValue(true);
  m_soundreceivecheckBox->SetValue(true);
  m_sounderrorcheckBox->SetValue(true);
  m_soundjointestbutton->Enable();
  m_soundleavetestbutton->Enable();
  m_soundsendtestbutton->Enable();
  m_soundreceivetestbutton->Enable();
  m_sounderrortestbutton->Enable();
  m_soundjoinlocatebutton->Enable();
  m_soundleavelocatebutton->Enable();
  m_soundsendlocatebutton->Enable();
  m_soundreceivelocatebutton->Enable();
  m_sounderrorlocatebutton->Enable();
/*m_soundjointextCtrl->Enable(l_snd_join);
  m_soundleavetextCtrl->Enable(l_snd_leave);
  m_soundsendtextCtrl->Enable(l_snd_send);
  m_soundreceivetextCtrl->Enable(l_snd_receive);
  m_sounderrortextCtrl->Enable(l_snd_error);
  m_soundjoincheckBox->SetValue(l_snd_join);
  m_soundleavecheckBox->SetValue(l_snd_leave);
  m_soundsendcheckBox->SetValue(l_snd_send);
  m_soundreceivecheckBox->SetValue(l_snd_receive);
  m_sounderrorcheckBox->SetValue(l_snd_error);
  m_soundjointestbutton->Enable(l_snd_join);
  m_soundleavetestbutton->Enable(l_snd_leave);
  m_soundsendtestbutton->Enable(l_snd_send);
  m_soundreceivetestbutton->Enable(l_snd_receive);
  m_sounderrortestbutton->Enable(l_snd_error);
  m_soundjoinlocatebutton->Enable(l_snd_join);
  m_soundleavelocatebutton->Enable(l_snd_leave);
  m_soundsendlocatebutton->Enable(l_snd_send);
  m_soundreceivelocatebutton->Enable(l_snd_receive);
  m_sounderrorlocatebutton->Enable(l_snd_error);*/
  m_soundmutewhileingamecheckBox->SetValue(false);
}

void NetworkDialog::OnCheckBoxClick(wxCommandEvent& event)
{
  bool l_selection = event.GetInt();
  switch (event.GetId())
  {
    case ID_SNDJOINCHECKBOX:         m_soundjointestbutton->Enable(l_selection);
                                     m_soundjoinlocatebutton->Enable(l_selection);
                                     m_soundjointextCtrl->Enable(l_selection);
                                     break;
    case ID_SNDLEAVECHECKBOX:        m_soundleavetestbutton->Enable(l_selection);
                                     m_soundleavelocatebutton->Enable(l_selection);
                                     m_soundleavetextCtrl->Enable(l_selection);
                                     break;
    case ID_SNDSENDCHECKBOX:         m_soundsendtestbutton->Enable(l_selection);
                                     m_soundsendlocatebutton->Enable(l_selection);
                                     m_soundsendtextCtrl->Enable(l_selection);
                                     break;
    case ID_SNDRECEIVECHECKBOX:      m_soundreceivetestbutton->Enable(l_selection);
                                     m_soundreceivelocatebutton->Enable(l_selection);
                                     m_soundreceivetextCtrl->Enable(l_selection);
                                     break;
    default: /*ID_SNDERRORCHECKBOX*/ m_sounderrortestbutton->Enable(l_selection);
                                     m_sounderrorlocatebutton->Enable(l_selection);
                                     m_sounderrortextCtrl->Enable(l_selection);
  }
}

void NetworkDialog::OnUpdateGameNickName(wxCommandEvent& WXUNUSED(event))
{
  wxString l_nickname = m_gamenicknametextCtrl->GetValue(),
           l_allowedchars = wxT("#+-0123456789@ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz");
  int l_loop_var = 0, l_str_length = l_nickname.Len();
  while (l_loop_var < l_str_length)
    if (l_allowedchars.Find(l_nickname[l_loop_var]) >= 0)
      l_loop_var++;
    else
    {
      l_nickname.Remove(l_loop_var, 1);
      m_gamenicknametextCtrl->SetValue(l_nickname);
      l_str_length--;
    }
}
/*
void NetworkDialog::OnApply(wxCommandEvent& WXUNUSED(event))
{
  ApplySettings();
}
*/
void NetworkDialog::OnOk(wxCommandEvent& WXUNUSED(event))
{
  ApplySettings();
  Destroy();
}

void NetworkDialog::OnCancel(wxCommandEvent& WXUNUSED(event))
{
  Destroy();
}

void NetworkDialog::ApplySettings()
{
  g_configuration->nickname = m_nicknametextCtrl->GetValue();
  g_configuration->game_nickname = m_gamenicknametextCtrl->GetValue();

  g_configuration->play_snd_join = m_soundjoincheckBox->GetValue();
  g_configuration->snd_join_file = m_soundjointextCtrl->GetValue();

  g_configuration->play_snd_leave = m_soundleavecheckBox->GetValue();
  g_configuration->snd_leave_file = m_soundleavetextCtrl->GetValue();

  g_configuration->play_snd_send = m_soundsendcheckBox->GetValue();
  g_configuration->snd_send_file = m_soundsendtextCtrl->GetValue();

  g_configuration->play_snd_receive = m_soundreceivecheckBox->GetValue();
  g_configuration->snd_receive_file = m_soundreceivetextCtrl->GetValue();

  g_configuration->play_snd_error = m_sounderrorcheckBox->GetValue();
  g_configuration->snd_error_file = m_sounderrortextCtrl->GetValue();

  g_configuration->mute_sounds_while_ingame = m_soundmutewhileingamecheckBox->GetValue();
  g_configuration->show_timestamp = m_chattimestampcheckBox->GetValue();
  
  if (!((m_gameporttextCtrl->GetValue()).ToLong(&(g_configuration->game_port_number))))
    g_configuration->game_port_number = DEFAULT_GAME_PORT_NUM;
  if (!((m_serverporttextCtrl->GetValue()).ToLong(&(g_configuration->server_port_number))))
    g_configuration->server_port_number = DEFAULT_SERVER_PORT_NUM;
	
	

  g_configuration->Save();
}
