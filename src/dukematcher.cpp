/*************************************************************************

Copyright 2008-2009 - NY00123

This file is part of DUKEMATCHER.

DUKEMATCHER is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

DUKEMATCHER is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DUKEMATCHER; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*************************************************************************/

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/wx.h>
//#include <wx/socket.h>
#include <wx/socket.h>
#include <wx/stdpaths.h>
#include <wx/dir.h>
#include <wx/filename.h>
#include <wx/textfile.h>
#include <wx/sound.h>
#endif

#include "dukematcher.h"
#include "common.h"
#include "config.h"
#include "main_frame.h"
#include "host_frame.h"
#include "client_frame.h"
#include "sp_dialog.h"
#include "mp_dialog.h"
//#include "manualjoin_dialog.h"
#include "srcports_dialog.h"
#include "network_dialog.h"
#include "adv_dialog.h"



wxArrayString *g_dn3d_skill_list, *g_sw_skill_list, *g_dn3d_level_list, *g_sw_level_list,
              *g_dn3d_gametype_list, *g_dn3d_spawn_list, *g_serverlists_addrs;
wxArrayLong *g_serverlists_portnums;
LauncherConfig *g_configuration;
MainFrame *g_main_frame;
HostedRoomFrame *g_host_frame;
ClientRoomFrame *g_client_frame;
SRCPortsDialog *g_srcports_dialog;
NetworkDialog *g_network_dialog;
AdvDialog *g_adv_dialog;
SPDialog *g_sp_dialog;
MPDialog *g_mp_dialog;
//ManualJoinDialog *g_manualjoin_dialog;
//wxString *g_launcher_user_profile_path, *g_launcher_resources_path;
#if (defined __WXMAC__) || (defined __WXCOCOA__)
DUKEMATCHERMenuBar *g_empty_menubar;
#endif
#ifdef ENABLE_UPNP
UPNPData *g_upnpdata;
#endif

// Give wxWidgets the means to create a DUKEMATCHERApp object
IMPLEMENT_APP(DUKEMATCHERApp)

void g_CheckForUpdates()
{
/*
  int l_pos1, l_pos2;
  wxSocketClient* m_client = new wxSocketClient();
  wxFrame* m_frame = new wxFrame(NULL, wxID_ANY, wxT("Checking for updates..."));
  m_frame->Centre();
  m_frame->Show(true);
  wxIPV4address l_addr;
  char l_buffer[2048];
  wxString l_temp_str;
  l_addr.Hostname(WEBSITE_HOSTNAME);
  l_addr.Service(80);
  m_client->SetNotify(false);
  m_client->SetTimeout(3);
  if (m_client->Connect(l_addr, true))
  {
    g_SendCStringToSocket(m_client, (wxString(wxT("GET /files/dukematcher/dukematcherver.txt HTTP/1.1\nHost: ")) + WEBSITE_HOSTNAME + wxT("\n\n")).mb_str(wxConvUTF8));
    m_client->Read(l_buffer, 2048);
    if (m_client->LastCount())
    {
      l_temp_str = wxString(l_buffer, wxConvUTF8, m_client->LastCount());
      m_client->Destroy();
      l_pos1 = l_temp_str.Find(wxT("DUKEMATCHERVer:"));
      if (l_pos1 != wxNOT_FOUND)
      {
        l_pos1 += 8;
        if ((l_temp_str.Len()) > (unsigned)l_pos1)
        {
          l_pos2 = l_temp_str.Mid(l_pos1).Find(wxT(':'));
          if ((l_pos2 != wxNOT_FOUND) && (l_pos2 > 0))
          {
            l_temp_str = l_temp_str.Mid(l_pos1, l_pos2);
            if (l_temp_str != DUKEMATCHER_STR_VERSION)
            {
              m_frame->Hide();
              if (wxMessageBox(wxT("A new version of DUKEMATCHER is available. Would you like to go to the website?"),
                  wxT("An update is available"), wxYES_NO|wxICON_INFORMATION) == wxYES)
                g_OpenURL(WEBSITE_ADDRESS);
            }
          }
        }
      }
    }
    else
      m_client->Destroy();
  }
  m_frame->Destroy();*/
}

size_t g_GetDN3DLevelSelectionByNum(long dn3d_level)
{
  if ((dn3d_level > 100) && (dn3d_level <= 107)) // Episode 1 level?
    return (dn3d_level % 100 + 1);
  if ((dn3d_level > 200) && (dn3d_level <= 411)
      && (dn3d_level % 100 > 0) && (dn3d_level % 100 <= 11))
    return ((dn3d_level / 100) * 12 + dn3d_level % 100 - 15);
  return 0;
}

size_t g_GetSWLevelSelectionByNum(long sw_level)
{
  if ((sw_level > 0) && (sw_level <= 4))
    return (sw_level+1);
  if ((sw_level > 4) && (sw_level <= 22))
    return (sw_level+2);
  return 0;
}

size_t g_GetDN3DLevelNumBySelection(long dn3d_selection)
{
  if ((dn3d_selection >= 2) && (dn3d_selection <= 8))
    return (dn3d_selection+99); // So E1L4 becomes 104, for instance.
  if ((dn3d_selection >= 10) && (dn3d_selection != 21) && (dn3d_selection != 33))
    return ((dn3d_selection - 8) % 12 + ((dn3d_selection + 16) / 12) * 100 - 1);
  return 0;
}

size_t g_GetSWLevelNumBySelection(long sw_selection)
{
  if ((sw_selection >= 2) && (sw_selection <= 5))
    return (sw_selection-1);
  if (sw_selection >= 7)
    return (sw_selection-2);
  return 0;
}

void l_ShrinkFilesToNamesOnly(wxArrayString *file_list)
{
  size_t i, l_temp_num_strings = file_list->GetCount();
  for (i = 0; i < l_temp_num_strings; i++)
    (*file_list)[i] = wxFileName((*file_list)[i]).GetFullName();
}

void g_AddAllDN3DMapFiles(wxArrayString *file_list)
{
// Although we could provide the file extension to GetAllFiles, we don't do this
// because some operating systems are case-sensitive, and some aren't!
  size_t l_loop_var;
  if (wxDir::Exists(g_configuration->dn3d_maps_dir))
  {
    wxDir::GetAllFiles(g_configuration->dn3d_maps_dir, file_list, wxEmptyString, wxDIR_FILES);
    l_loop_var = 0;
    while (l_loop_var < file_list->GetCount())
      if (((*file_list)[l_loop_var].Right(4)).IsSameAs(wxT(".map"), false))
        l_loop_var++;
      else
        file_list->RemoveAt(l_loop_var);
    l_ShrinkFilesToNamesOnly(file_list);
    file_list->Sort();
  }
}

void g_AddAllSWMapFiles(wxArrayString *file_list)
{
  size_t l_loop_var;
  if (wxDir::Exists(g_configuration->sw_maps_dir))
  {
    wxDir::GetAllFiles(g_configuration->sw_maps_dir, file_list, wxEmptyString, wxDIR_FILES);
    l_loop_var = 0;
    while (l_loop_var < file_list->GetCount())
      if (((*file_list)[l_loop_var].Right(4)).IsSameAs(wxT(".map"), false))
        l_loop_var++;
      else
        file_list->RemoveAt(l_loop_var);
    l_ShrinkFilesToNamesOnly(file_list);
    file_list->Sort();
  }
}

void g_AddAllDN3DModFiles(wxArrayString *file_list)
{
  size_t l_loop_var;
  if (wxDir::Exists(g_configuration->dn3d_mods_dir))
  {
    wxDir::GetAllFiles(g_configuration->dn3d_mods_dir, file_list, wxEmptyString, wxDIR_FILES);
    l_loop_var = 0;
    while (l_loop_var < file_list->GetCount())
      if ((((*file_list)[l_loop_var].Right(4)).IsSameAs(wxT(".con"), false))
          || (((*file_list)[l_loop_var].Right(4)).IsSameAs(wxT(".grp"), false))
          || (((*file_list)[l_loop_var].Right(4)).IsSameAs(wxT(".zip"), false))
          || (((*file_list)[l_loop_var].Right(4)).IsSameAs(wxT(".def"), false)))
        l_loop_var++;
      else
        file_list->RemoveAt(l_loop_var);
    l_ShrinkFilesToNamesOnly(file_list);
    file_list->Sort();
  }
}

void g_AddAllSWModFiles(wxArrayString *file_list)
{
  size_t l_loop_var;
  if (wxDir::Exists(g_configuration->sw_mods_dir))
  {
    wxDir::GetAllFiles(g_configuration->sw_mods_dir, file_list, wxEmptyString, wxDIR_FILES);
    l_loop_var = 0;
    while (l_loop_var < file_list->GetCount())
      if ((((*file_list)[l_loop_var].Right(4)).IsSameAs(wxT(".grp"), false))
          || (((*file_list)[l_loop_var].Right(4)).IsSameAs(wxT(".zip"), false)))
        l_loop_var++;
      else
        file_list->RemoveAt(l_loop_var);
    l_ShrinkFilesToNamesOnly(file_list);
    file_list->Sort();
  }
}

void g_AddAllDemoFiles(wxArrayString *file_list, const wxString& demo_dir)
{
  size_t l_loop_var;
  if (wxDir::Exists(demo_dir))
  {
    wxDir::GetAllFiles(demo_dir, file_list, wxEmptyString, wxDIR_FILES);
    l_loop_var = 0;
    while (l_loop_var < file_list->GetCount())
      if (((*file_list)[l_loop_var].Right(4)).IsSameAs(wxT(".dmo"), false))
        l_loop_var++;
      else
        file_list->RemoveAt(l_loop_var);
    l_ShrinkFilesToNamesOnly(file_list);
    file_list->Sort();
  }
}

void g_OpenURL(const wxString& url)
{
  if (g_configuration->override_browser)
    wxExecute(wxT('"') + g_configuration->browser_exec + wxT("\" \"") + url + wxT('"'));
  else
    wxLaunchDefaultBrowser(url);
}

void g_PlaySound(const wxString& filename)
{
  if (g_configuration->override_soundcmd)
    wxExecute(wxT('"') + g_configuration->playsound_cmd + wxT("\" \"") + filename + wxT('"'));
  else
    wxSound::Play(filename);
}

#ifdef __WXMSW__
#define l_ConvToLocalExec(fullfilename) (((wxFileName)fullfilename).GetFullName())
#else
#define l_ConvToLocalExec(fullfilename) (wxT("./")+((wxFileName)fullfilename).GetFullName())
#endif

bool g_MakeLaunchScript(const wxString& script_filename,
                        RoomPlayersTable* players_table, size_t player_index,
                        GameType game, SourcePortType src_port,
                        bool have_bots, size_t num_bots,
                        bool play_demo, const wxString& playdemo_filename,
                        bool record_demo, const wxString& recorddemo_filename,
                        size_t skill_num,
                        DN3DMPGameT_Type gametype, DN3DSpawnType spawn,
                        bool usemasterslave, bool launch_usermap,
                        const wxString& user_map, size_t episode_map,
                        const wxArrayString& mod_files, const wxString& team, const wxString& extra_args)
{
  size_t l_temp_num, l_loop_var;
  wxString l_game_fullcmd, l_game_userpath, l_game_execpath, l_temp_str, teamnumber;
  bool l_dos_game = false;
  wxTextFile l_settings_file;

  // First of all, a room for some per-srcport settings...
  // Including the actual game execution, and setting
  // the path for temporary files.
  switch (src_port)
  {
    case SOURCEPORT_EDUKE32:      l_game_fullcmd = g_configuration->eduke32_exec;
                                  l_game_execpath = ((wxFileName)l_game_fullcmd).GetPath();
                                  if (g_configuration->eduke32_userpath_use)
                                    l_game_userpath = g_configuration->eduke32_userpath;
                                  else
                                    l_game_userpath = l_game_execpath;
                                  l_game_fullcmd = wxT('"') + l_ConvToLocalExec(l_game_fullcmd) + wxT('"');
//                                l_game_fullcmd = wxT('"') + l_game_fullcmd + wxT('"');
                                  break;
    case SOURCEPORT_JFDUKE3D:     l_game_fullcmd = g_configuration->jfduke3d_exec;
                                  l_game_execpath = ((wxFileName)l_game_fullcmd).GetPath();
                                  if (g_configuration->jfduke3d_userpath_use)
                                    l_game_userpath = g_configuration->jfduke3d_userpath;
                                  else
                                    l_game_userpath = l_game_execpath;
                                  l_game_fullcmd = wxT('"') + l_ConvToLocalExec(l_game_fullcmd) + wxT('"');
                                  break;
    case SOURCEPORT_IDUKE3D:      l_game_fullcmd = g_configuration->iduke3d_exec;
                                  l_game_execpath = ((wxFileName)l_game_fullcmd).GetPath();
                                  if (g_configuration->iduke3d_userpath_use)
                                    l_game_userpath = g_configuration->iduke3d_userpath;
                                  else
                                    l_game_userpath = l_game_execpath;
                                  l_game_fullcmd = wxT('"') + l_ConvToLocalExec(l_game_fullcmd) + wxT('"');
                                  break;
    case SOURCEPORT_DUKE3DW32:    l_game_fullcmd = g_configuration->duke3dw32_exec;
                                  l_game_execpath = ((wxFileName)l_game_fullcmd).GetPath();
                                  l_game_userpath = l_game_execpath;
                                  l_game_fullcmd = wxT('"') + l_ConvToLocalExec(l_game_fullcmd) + wxT('"');
                                  break;
    case SOURCEPORT_XDUKE:        l_game_fullcmd = g_configuration->xduke_exec;
                                  l_game_execpath = ((wxFileName)l_game_fullcmd).GetPath();
                                  l_game_userpath = l_game_execpath;
                                  l_game_fullcmd = wxT('"') + l_ConvToLocalExec(l_game_fullcmd) + wxT('"');
                                  break;
    case SOURCEPORT_DOSDUKE:      if (players_table)
                                    l_game_fullcmd = wxT("COMMIT.EXE");
                                  else
                                    l_game_fullcmd = ((wxFileName)g_configuration->dosduke_exec).GetFullName();
                                  l_game_userpath = ((wxFileName)g_configuration->dosduke_exec).GetPath();
                                  l_dos_game = true;
                                  break;
    case SOURCEPORT_JFSW:         l_game_fullcmd = g_configuration->jfsw_exec;
                                  l_game_execpath = ((wxFileName)l_game_fullcmd).GetPath();
                                  if (g_configuration->jfsw_userpath_use)
                                    l_game_userpath = g_configuration->jfsw_userpath;
                                  else
                                    l_game_userpath = l_game_execpath;
                                  l_game_fullcmd = wxT('"') + l_ConvToLocalExec(l_game_fullcmd) + wxT('"');
                                  break;
    case SOURCEPORT_SWP:          l_game_fullcmd = g_configuration->swp_exec;
                                  l_game_execpath = ((wxFileName)l_game_fullcmd).GetPath();
                                  l_game_userpath = l_game_execpath;
                                  l_game_fullcmd = wxT('"') + l_ConvToLocalExec(l_game_fullcmd) + wxT('"');
                                  break;
    default: /*SOURCEPORT_DOSSW*/ if (players_table)
                                    l_game_fullcmd = wxT("COMMIT.EXE");
                                  else
                                    l_game_fullcmd = ((wxFileName)g_configuration->dossw_exec).GetFullName();
                                  l_game_userpath = ((wxFileName)g_configuration->dossw_exec).GetPath();
                                  l_dos_game = true;
  }
#ifndef __WXMSW__
// If the command ends with an EXE suffix and it's not a DOS game,
// we should use Wine if it's set to be used.
// It should then end with .exe" (including double-quotes);
// DOS apps should not have the path and not need quotes anyway,
// so we would "catch" them anyway.
  if ((l_game_fullcmd.Right(5).IsSameAs(wxT(".exe\""), false))
      && (g_configuration->have_wine))
    l_game_fullcmd = g_configuration->wine_exec + wxT(' ') + l_game_fullcmd;
#endif
  // Next step: Command line arguments.
  if (players_table){
  // if team dm, need to add the -team argument.
  
  // get teams
  if (team == "Blue Team") teamnumber = "1#";
  else if (team == "Red Team") teamnumber = "2#";
  else if (team == "Green Team") teamnumber = "3#";
  else if (team == "Grey Team") teamnumber = "4#";
  else if (team == "Dark Grey Team") teamnumber = "5#";
  else if (team == "Dark Green Team") teamnumber = "6#";
  else if (team == "Brown Team") teamnumber = "7#";
  else if (team == "Dark Blue Team") teamnumber = "8#";
  else if (team == "Hell Grey Team") teamnumber = "9#";
  else teamnumber = "1#";
	if ((gametype != DN3DMPGAMETYPE_DMSPAWN) && (gametype != DN3DMPGAMETYPE_COOP) && (gametype != DN3DMPGAMETYPE_DMNOSPAWN) && (src_port == SOURCEPORT_IDUKE3D)){
	l_game_fullcmd << wxT(" -team");
    l_game_fullcmd << wxT(" -name ") << teamnumber << g_configuration->game_nickname;
	}
	else
	{
	l_game_fullcmd << wxT(" -name ") << g_configuration->game_nickname;
	}
	}
  // Game-specific.
  if (game == GAME_DN3D)
  {
    // Play a demo?
    if (play_demo)
      l_game_fullcmd << wxT(" /d") << playdemo_filename;
    else // Play some game.
    {
      // The first or zero-index selection is 1.
      // And the total amount of players is the number of bots plus 1.
      if (have_bots)
        l_game_fullcmd << wxT(" -name ") << g_configuration->game_nickname << wxT(" /a /c1 /q") << (num_bots+1);
      if (record_demo)
        l_game_fullcmd << wxT(" /r");
      if (!skill_num)
        l_game_fullcmd << wxT(" /m");
      else // Respawn enemies on highest skill? In Single-Player mode only.
        if ((skill_num == DN3D_MAX_SKILLNUM) && (!players_table))
          l_game_fullcmd << wxT(" /s") << DN3D_MAX_SKILLNUM << wxT(" /t1");
        else
          l_game_fullcmd << wxT(" /s") << skill_num;
      if (!launch_usermap)
        if (episode_map)
          l_game_fullcmd << wxT(" /l") << (episode_map % 100) << wxT(" /v") << (episode_map / 100);
    }
    // May be required for both a real game and a demo.
    if (launch_usermap)
      l_game_fullcmd << wxT(" -map ") << user_map;
    if (players_table) // Multiplayer?
    {
      l_game_fullcmd << wxT(" /c") << gametype;
      l_game_fullcmd << wxT(" /t") << spawn;
    }
    l_temp_num = mod_files.GetCount();
    // CON and DEF files at first. DEF - In EDuke32 and JFDuke3D only.
    for (l_loop_var = 0; l_loop_var < l_temp_num; l_loop_var++)
    {
      l_temp_str = mod_files[l_loop_var];
      if ((l_temp_str.Right(4)).IsSameAs(wxT(".con"), false))
        l_game_fullcmd << wxT(" /x") << l_temp_str;
      else
        if (((src_port == SOURCEPORT_EDUKE32) || (src_port == SOURCEPORT_JFDUKE3D))
            && ((l_temp_str.Right(4)).IsSameAs(wxT(".def"), false)))
          l_game_fullcmd << wxT(" /h") << l_temp_str;
    }
    // Afterwards, GRP And ZIP files.
    for (l_loop_var = 0; l_loop_var < l_temp_num; l_loop_var++)
    {
      l_temp_str = mod_files[l_loop_var];
      if (((l_temp_str.Right(4)).IsSameAs(wxT(".grp"), false))
          || ((l_temp_str.Right(4)).IsSameAs(wxT(".zip"), false)))
        l_game_fullcmd << wxT(" /g") << l_temp_str;
    }
    // Finally...
    l_game_fullcmd << wxT(' ') << extra_args;
  }
  else // Shadow Warrior
  {
    // Play a demo?
    if (play_demo)
      l_game_fullcmd << wxT(" -dp") << playdemo_filename;
    else // Play some game.
    {
      if (record_demo)
        l_game_fullcmd << wxT(" -dr") << recorddemo_filename;
      if (!skill_num)
        l_game_fullcmd << wxT(" -monst");
      else
        l_game_fullcmd << wxT(" -s") << skill_num;
      if (!launch_usermap)
        if (episode_map)
          l_game_fullcmd << wxT(" -level") << episode_map;
    }
    // May be required for both a real game and a demo.
    if (launch_usermap)
      l_game_fullcmd << wxT(" -map ") << user_map;
    l_temp_num = mod_files.GetCount();
    // GRP and ZIP files only.
    for (l_loop_var = 0; l_loop_var < l_temp_num; l_loop_var++)
    {
      l_temp_str = mod_files[l_loop_var];
      if (((l_temp_str.Right(4)).IsSameAs(wxT(".grp"), false))
          || ((l_temp_str.Right(4)).IsSameAs(wxT(".zip"), false)))
        l_game_fullcmd << wxT(" -g") << l_temp_str;
    }
    // Finally...
    l_game_fullcmd << wxT(' ') << extra_args;
  }
  // Source Port specific parameters for multiplayer.
  if (players_table)
  {
/*  if (((src_port == SOURCEPORT_EDUKE32) ||
         (src_port == SOURCEPORT_DUKE3DW32) || (src_port == SOURCEPORT_XDUKE))
        && (g_configuration->gamelaunch_enable_natfree))
      l_game_fullcmd << wxT(" -stun");
*/
    if (src_port == SOURCEPORT_EDUKE32)
    {
      if (usemasterslave)
      {
        if (player_index) // A client.
          l_game_fullcmd << wxT(" /net /n0 ") << players_table->players[0].ingameip
                         << wxT(':') << players_table->players[0].game_port_number;
        else // A server.
          l_game_fullcmd << wxT(" /net /n0:") << players_table->num_players
                         << wxT(" /p") << players_table->players[0].game_port_number;
      }
      else
      {
        l_game_fullcmd << wxT(" /net /p") << players_table->players[player_index].game_port_number;
        for (l_loop_var = 0; l_loop_var < player_index; l_loop_var++)
          l_game_fullcmd << wxT(' ') << players_table->players[l_loop_var].ingameip
                         << wxT(':') << players_table->players[l_loop_var].game_port_number;
        l_game_fullcmd << wxT(" /n1");
        for (l_loop_var = player_index+1; l_loop_var < players_table->num_players; l_loop_var++)
          l_game_fullcmd << wxT(' ') << players_table->players[l_loop_var].ingameip
                         << wxT(':') << players_table->players[l_loop_var].game_port_number;
      }
    }
    else
      if (src_port == SOURCEPORT_JFDUKE3D)
      {
        if (usemasterslave)
        {
          if (player_index) // A client.
          {
            l_game_fullcmd << wxT(" /net /n0 ") << players_table->players[0].ingameip;
            if (players_table->players[0].game_port_number != DEFAULT_GAME_PORT_NUM)
              l_game_fullcmd << wxT(':') << players_table->players[0].game_port_number;
          }
          else // A server.
          {
            l_game_fullcmd << wxT(" /net /n0:") << players_table->num_players;
            if (players_table->players[player_index].game_port_number != DEFAULT_GAME_PORT_NUM)
              l_game_fullcmd << wxT(" /p") << players_table->players[player_index].game_port_number;
          }
        }
        else
        {
          l_game_fullcmd << wxT(" /net");
          if (players_table->players[player_index].game_port_number != DEFAULT_GAME_PORT_NUM)
            l_game_fullcmd << wxT(" /p") << players_table->players[player_index].game_port_number;
          for (l_loop_var = 0; l_loop_var < player_index; l_loop_var++)
          {
            l_game_fullcmd << wxT(' ') << players_table->players[l_loop_var].ingameip;
            if (players_table->players[l_loop_var].game_port_number != DEFAULT_GAME_PORT_NUM)
              l_game_fullcmd << wxT(':') << players_table->players[l_loop_var].game_port_number;
          }
          l_game_fullcmd << wxT(" /n1");
          for (l_loop_var = player_index+1; l_loop_var < players_table->num_players; l_loop_var++)
          {
            l_game_fullcmd << wxT(' ') << players_table->players[l_loop_var].ingameip;
            if (players_table->players[l_loop_var].game_port_number != DEFAULT_GAME_PORT_NUM)
              l_game_fullcmd << wxT(':') << players_table->players[l_loop_var].game_port_number;
          }
        }
      }
    else
      if ((src_port == SOURCEPORT_IDUKE3D) || (src_port == SOURCEPORT_DUKE3DW32) || (src_port == SOURCEPORT_XDUKE))
        l_game_fullcmd << wxT(" -net netlist.txt");
    else
      if ((src_port == SOURCEPORT_JFSW) || (src_port == SOURCEPORT_SWP))
      {
        l_game_fullcmd << wxT(" /net");
        if (players_table->players[player_index].game_port_number != DEFAULT_GAME_PORT_NUM)
          l_game_fullcmd << wxT(" /p") << players_table->players[player_index].game_port_number;
        for (l_loop_var = 0; l_loop_var < player_index; l_loop_var++)
        {
          l_game_fullcmd << wxT(' ') << players_table->players[l_loop_var].ingameip;
          if (players_table->players[l_loop_var].game_port_number != DEFAULT_GAME_PORT_NUM)
            l_game_fullcmd << wxT(':') << players_table->players[l_loop_var].game_port_number;
        }
        l_game_fullcmd << wxT(" /n1");
        for (l_loop_var = player_index+1; l_loop_var < players_table->num_players; l_loop_var++)
        {
          l_game_fullcmd << wxT(' ') << players_table->players[l_loop_var].ingameip;
          if (players_table->players[l_loop_var].game_port_number != DEFAULT_GAME_PORT_NUM)
            l_game_fullcmd << wxT(':') << players_table->players[l_loop_var].game_port_number;
        }
      }
  }
  // Let's try creating the game user folder if it doesn't exist.
  // It's at least required for COMMIT.DAT and netlist.txt.
  if (!wxDirExists(l_game_userpath))
    wxFileName::Mkdir(l_game_userpath, 0777, wxPATH_MKDIR_FULL);

  // Now, for the case we'd want to use DOSBox, we'd later change l_game_fullcmd.
  if (l_dos_game)
  {
    if (players_table) // Multiplayer?
    {
      // Let's create a COMMIT.DAT file.
#ifdef __WXMSW__
      l_temp_str = l_game_userpath + wxT("\\COMMIT.DAT");
#else
      l_temp_str = l_game_userpath + wxT("/COMMIT.DAT");
#endif
      if (((!wxFileExists(l_temp_str)) || (wxRemoveFile(l_temp_str)))
          && (l_settings_file.Create(l_temp_str)))
      {
        l_settings_file.AddLine(wxT("[Commit]"));
        l_settings_file.AddLine(wxT(";COMMIT data file"));
        l_settings_file.AddLine(wxT("; "));
        l_settings_file.AddLine(wxT(";This data file defines the configurable options of COMMIT.  Presumably,"));
        l_settings_file.AddLine(wxT(";the user never sees this file, but rather it is setup by another"));
        l_settings_file.AddLine(wxT(";program which launches COMMIT."));
        l_settings_file.AddLine(wxT("; "));
        l_settings_file.AddLine(wxT("; "));
        l_settings_file.AddLine(wxT(";COMMTYPE"));
        l_settings_file.AddLine(wxT("; - SERIAL - 1"));
        l_settings_file.AddLine(wxT("; - MODEM - 2"));
        l_settings_file.AddLine(wxT("; - NETWORK - 3"));
        l_settings_file.AddLine(wxT("; "));
        l_settings_file.AddLine(wxT("; NUMPLAYERS"));
        l_settings_file.AddLine(wxT("; - 0..MAXPLAYERS"));
        l_settings_file.AddLine(wxT("; "));
        l_settings_file.AddLine(wxT("; VECTOR"));
        l_settings_file.AddLine(wxT("; - ~ find an empty one"));
        l_settings_file.AddLine(wxT("; - 0x60..0x69 specify one"));
        l_settings_file.AddLine(wxT("; "));
        l_settings_file.AddLine(wxT("; CONNECTTYPE"));
        l_settings_file.AddLine(wxT("; - DIALMODE - 0"));
        l_settings_file.AddLine(wxT("; - ANSWERMODE - 1"));
        l_settings_file.AddLine(wxT("; - ALREADYCONNECTED - 2"));
        l_settings_file.AddLine(wxT("; "));
        l_settings_file.AddLine(wxT("COMMTYPE = 3"));
        l_settings_file.AddLine(wxString::Format(wxT("NUMPLAYERS = %d"), players_table->num_players));
        l_settings_file.AddLine(wxT("PAUSE = 0"));
        l_settings_file.AddLine(wxT("VECTOR = ~"));
        l_settings_file.AddLine(wxT("SOCKETNUMBER = 34889"));
        l_settings_file.AddLine(wxT("INITSTRING = \"ATZ\""));
        l_settings_file.AddLine(wxT("HANGUPSTRING = \"ATH0=0\""));
        l_settings_file.AddLine(wxT("PHONENUMBER = \"\""));
        if (src_port == SOURCEPORT_DOSDUKE)
          l_settings_file.AddLine(wxT("LAUNCHNAME = \"") +
                              ((wxFileName)g_configuration->dosduke_exec).GetFullName()
                              + wxT('"'));
        else // DOS SW
          l_settings_file.AddLine(wxT("LAUNCHNAME = \"") +
                              ((wxFileName)g_configuration->dossw_exec).GetFullName()
                              + wxT('"'));
        l_settings_file.AddLine(wxT("CONNECTTYPE = 0"));
        l_settings_file.AddLine(wxT("USETONE = 1"));
        l_settings_file.AddLine(wxT("COMPORT = 2"));
        l_settings_file.AddLine(wxT("IRQNUMBER = 0"));
        l_settings_file.AddLine(wxT("UARTADDRESS = ~"));
        l_settings_file.AddLine(wxT("PORTSPEED = 9600"));
        l_settings_file.AddLine(wxT("SHOWSTATS = 0"));
        // COMMIT.DAT should be read in an emulated DOS environment.
        // Therefore it's important to use the CR-LF kind of newlines!
        l_settings_file.Write(wxTextFileType_Dos);
        l_settings_file.Close();
      }
    }
    l_temp_str = wxT("dosdukematcher.conf");
    if (((!wxFileExists(l_temp_str)) || (wxRemoveFile(l_temp_str)))
        && (l_settings_file.Create(l_temp_str)))
    {
      if (players_table)
      {
        l_settings_file.AddLine(wxT("[ipx]"));
        l_settings_file.AddLine(wxT("ipx=true"));
      }
      l_settings_file.AddLine(wxT("[autoexec]"));
// Now let's write the actual commands.
      if (players_table)
      {
        if (player_index) // A client.
          l_settings_file.AddLine(wxT("ipxnet connect ") + players_table->players[0].ingameip +
                                  wxString::Format(wxT(" %d"), players_table->players[0].game_port_number));
        else // A server.
          l_settings_file.AddLine(wxString::Format(wxT("ipxnet startserver %d"), players_table->players[0].game_port_number));
      }
      if (g_configuration->dosbox_cdmount == CDROMMOUNT_DIR)
// On Windows, the CD-ROM letter should already have a trailing slash
// (and it is required by DOSBox, at least with v0.72).
// On Linux there seems to be no need for that anyway.
        l_settings_file.AddLine(wxT("mount d ") + g_configuration->dosbox_cdrom_location + wxT(" -t cdrom"));
      else
        if (g_configuration->dosbox_cdmount == CDROMMOUNT_IMG)
// At least on Linux, and with the real CD-ROM device /dev/scd0,
// it seems like the mount command has to be executed twice.
        {
          l_settings_file.AddLine(wxT("imgmount d ") + g_configuration->dosbox_cdrom_image + wxT(" -t cdrom"));
          l_settings_file.AddLine(wxT("imgmount d ") + g_configuration->dosbox_cdrom_image + wxT(" -t cdrom"));
        }
// Now let's mount the actual game dir.
// No need to worry about trailing any slash here.
      l_settings_file.AddLine(wxT("mount o ") + l_game_userpath);
      l_settings_file.AddLine(wxT("o:"));
// Launch the game!
      l_settings_file.AddLine(l_game_fullcmd);
// Don't forget to exit!
      l_settings_file.AddLine(wxT("exit"));
      l_settings_file.Write();
      l_settings_file.Close();
    }
// The real command would rather be... one of two:
    if (g_configuration->dosbox_use_conf)
      l_game_fullcmd = wxT('"') + g_configuration->dosbox_exec + wxT("\" -conf \"")
                       + g_configuration->dosbox_conf + wxT("\" -conf \"") + l_temp_str + wxT('"');
    else
      l_game_fullcmd = wxT('"') + g_configuration->dosbox_exec + wxT("\" -conf \"") + l_temp_str + wxT('"');
  }
  else // In the case of multiplayer, we should maybe create a netlist.txt file otherwise.
    if (players_table &&
        ((src_port == SOURCEPORT_IDUKE3D) || (src_port == SOURCEPORT_DUKE3DW32) || (src_port == SOURCEPORT_XDUKE)))
    {
#ifdef __WXMSW__
      l_temp_str = l_game_userpath + wxT("\\netlist.txt");
#else
      l_temp_str = l_game_userpath + wxT("/netlist.txt");
#endif
      if (((!wxFileExists(l_temp_str)) || (wxRemoveFile(l_temp_str)))
          && (l_settings_file.Create(l_temp_str)))
      {
        l_settings_file.AddLine(wxT("interface ") +
                                players_table->players[player_index].ingameip +
                                wxString::Format(wxT(":%d"), players_table->players[player_index].game_port_number));
        l_settings_file.AddLine(wxT("mode peer"));
        for (l_loop_var = 0; l_loop_var < player_index; l_loop_var++)
          l_settings_file.AddLine(wxT("allow ") +
                                  players_table->players[l_loop_var].ingameip +
                                  wxString::Format(wxT(":%d"), players_table->players[l_loop_var].game_port_number));
        for (l_loop_var = player_index+1; l_loop_var < players_table->num_players; l_loop_var++)
          l_settings_file.AddLine(wxT("allow ") +
                                  players_table->players[l_loop_var].ingameip +
                                  wxString::Format(wxT(":%d"), players_table->players[l_loop_var].game_port_number));
        l_settings_file.Write();
        l_settings_file.Close();
      }
    }
  // Now let's try creating the script!
  if (((!wxFileExists(script_filename)) || (wxRemoveFile(script_filename)))
      && (l_settings_file.Create(script_filename)))
  {
#ifdef __WXMSW__
    l_settings_file.AddLine(wxT("md \"") + l_game_userpath + wxT('"'));
#else
    l_settings_file.AddLine(wxT("#!/bin/sh"));
    l_settings_file.AddLine(wxT("mkdir -p \"") + l_game_userpath + wxT('"'));
#endif
// Copy or symlink map file, if used.
    if ((launch_usermap) && (!wxFileExists(l_game_userpath + wxFILE_SEP_PATH + user_map)))
    {
      if (game == GAME_DN3D)
#ifdef __WXMSW__
        l_settings_file.AddLine(wxT("copy \"") + g_configuration->dn3d_maps_dir + wxT('\\') +
#else
        l_settings_file.AddLine(wxT("ln -s \"") + g_configuration->dn3d_maps_dir + wxT('/') +
#endif
                              user_map + wxT("\" \"")
                              + l_game_userpath + wxT('"'));
      else
#ifdef __WXMSW__
        l_settings_file.AddLine(wxT("copy \"") + g_configuration->sw_maps_dir + wxT('\\') +
#else
        l_settings_file.AddLine(wxT("ln -s \"") + g_configuration->sw_maps_dir + wxT('/') +
#endif
                              user_map + wxT("\" \"")
                              + l_game_userpath + wxT('"'));
    }
    l_temp_num = mod_files.GetCount();
    if (l_temp_num)
    {
#ifdef __WXMSW__
      if (game == GAME_DN3D)
        for (l_loop_var = 0; l_loop_var < l_temp_num; l_loop_var++)
          if (!wxFileExists(l_game_userpath + wxT('\\') + mod_files[l_loop_var]))
            l_settings_file.AddLine(wxT("copy \"") + g_configuration->dn3d_mods_dir + wxT('\\')
                                    + mod_files[l_loop_var] + wxT("\" \"")
                                    + l_game_userpath + wxT('"'));
      else
        for (l_loop_var = 0; l_loop_var < l_temp_num; l_loop_var++)
          if (!wxFileExists(l_game_userpath + wxT('\\') + mod_files[l_loop_var]))
            l_settings_file.AddLine(wxT("copy \"") + g_configuration->sw_mods_dir + wxT('\\')
                                    + mod_files[l_loop_var] + wxT("\" \"")
                                    + l_game_userpath + wxT('"'));
#else
      l_temp_str = wxEmptyString;
      if (game == GAME_DN3D)
        for (l_loop_var = 0; l_loop_var < l_temp_num; l_loop_var++)
          if (!wxFileExists(l_game_userpath + wxT('/') + mod_files[l_loop_var]))
            l_temp_str << wxT('"') << g_configuration->dn3d_mods_dir << wxT('/')
                       << mod_files[l_loop_var] << wxT("\" ");
      else
        for (l_loop_var = 0; l_loop_var < l_temp_num; l_loop_var++)
          if (!wxFileExists(l_game_userpath + wxT('/') + mod_files[l_loop_var]))
            l_temp_str << wxT('"') << g_configuration->sw_mods_dir << wxT('/')
                       << mod_files[l_loop_var] << wxT("\" ");
      if (l_temp_str != wxEmptyString)
        l_settings_file.AddLine(wxT("ln -s ") + l_temp_str + wxT('"') + l_game_userpath + wxT('"'));
#endif
    }
// Now changing to the directory of the game, if not a DOS game.
    if (!l_dos_game)
#ifdef __WXMSW__
      l_settings_file.AddLine(((wxFileName)l_game_execpath).GetVolume() + wxT(':'));
#endif
      l_settings_file.AddLine(wxT("cd \"") + l_game_execpath + wxT('"'));
// Finally, let's play!
    l_settings_file.AddLine(l_game_fullcmd);
// But don't forget to remove temporary files!
    if ((launch_usermap) && (!wxFileExists(l_game_userpath + wxFILE_SEP_PATH + user_map)))
#ifdef __WXMSW__
      l_settings_file.AddLine(wxT("del \"") + l_game_userpath + wxT('\\') + user_map + wxT('"'));
#else
      l_settings_file.AddLine(wxT("rm -f \"") + l_game_userpath + wxT('/') + user_map + wxT('"'));
#endif
    l_temp_num = mod_files.GetCount();
    if (l_temp_num)
    {
      l_temp_str = wxEmptyString;
      for (l_loop_var = 0; l_loop_var < l_temp_num; l_loop_var++)
        if (!wxFileExists(l_game_userpath + wxFILE_SEP_PATH + mod_files[l_loop_var]))
          l_temp_str << wxT(" \"") << l_game_userpath << wxFILE_SEP_PATH
                     << mod_files[l_loop_var] << wxT('"');
      if (l_temp_str != wxEmptyString)
#ifdef __WXMSW__
        l_settings_file.AddLine(wxT("del") + l_temp_str);
#else
        l_settings_file.AddLine(wxT("rm -f") + l_temp_str);
#endif
    }
    l_settings_file.Write();
    l_settings_file.Close();
#ifndef __WXMSW__
    wxExecute(wxT("chmod +x \"") + script_filename + wxT('"'));
#endif
    return true;
  }
  return false;
}

/* 
Function removed because deflists.txt is no longer used.

void l_AddServerLists(wxString filename)
{
  wxTextFile l_settingsfile;
  wxString l_curr_text;
  size_t l_loop_var, l_num_of_lines;
  int l_sep_pos;
  long l_portnum;
  if (wxFile::Access(filename, wxFile::read))
  {
    l_settingsfile.Open(filename);
    l_num_of_lines = l_settingsfile.GetLineCount();
    for (l_loop_var = 0; l_loop_var < l_num_of_lines; l_loop_var++)
    {
      l_curr_text = l_settingsfile[l_loop_var];
      l_sep_pos = l_curr_text.Find(wxT(':'), true);
      if (l_sep_pos != wxNOT_FOUND)
      {
        g_serverlists_addrs->Add(l_curr_text.Left(l_sep_pos));
        if (!(l_curr_text.Mid(l_sep_pos+1)).ToLong(&l_portnum))
          l_portnum = 0;
        g_serverlists_portnums->Add(l_portnum);
      }
    }
    l_settingsfile.Close();
  }
}
*/

void l_global_lists_init()
{
  g_dn3d_skill_list = new wxArrayString;
  g_dn3d_skill_list->Add(wxT("No Monsters"));
  g_dn3d_skill_list->Add(wxT("Piece Of Cake"));
  g_dn3d_skill_list->Add(wxT("Let's Rock"));
  g_dn3d_skill_list->Add(wxT("Come Get Some"));
  g_dn3d_skill_list->Add(wxT("Damn I'm Good"));

  g_sw_skill_list = new wxArrayString;
  g_sw_skill_list->Add(wxT("No Monsters"));
  g_sw_skill_list->Add(wxT("Tiny Grasshopper"));
  g_sw_skill_list->Add(wxT("I Have No Fear"));
  g_sw_skill_list->Add(wxT("Who Wants Wang"));
  g_sw_skill_list->Add(wxT("No Pain, No Gain"));

  g_dn3d_level_list = new wxArrayString;
  g_dn3d_level_list->Add(wxT("None"));
  g_dn3d_level_list->Add(wxT("--- Episode 1: L.A. Meltdown ---"));
  g_dn3d_level_list->Add(wxT("Episode 1 Level 1: Hollywood holocaust"));
  g_dn3d_level_list->Add(wxT("Episode 1 Level 2: Red Light District"));
  g_dn3d_level_list->Add(wxT("Episode 1 Level 3: Death Row"));
  g_dn3d_level_list->Add(wxT("Episode 1 Level 4: Toxic Dump"));
  g_dn3d_level_list->Add(wxT("Episode 1 Level 5: The Abyss"));
  g_dn3d_level_list->Add(wxT("Episode 1 Level 6: Launch Facility"));
  g_dn3d_level_list->Add(wxT("Episode 1 Level 7: Faces Of Death"));
  g_dn3d_level_list->Add(wxT("--- Episode 2: Lunar Apocalypse ---"));
  g_dn3d_level_list->Add(wxT("Episode 2 Level 1: Spaceport"));
  g_dn3d_level_list->Add(wxT("Episode 2 Level 2: Incubator"));
  g_dn3d_level_list->Add(wxT("Episode 2 Level 3: Warp Factor"));
  g_dn3d_level_list->Add(wxT("Episode 2 Level 4: Fusion Station"));
  g_dn3d_level_list->Add(wxT("Episode 2 Level 5: Occupied Territory"));
  g_dn3d_level_list->Add(wxT("Episode 2 Level 6: Tiberius Station"));
  g_dn3d_level_list->Add(wxT("Episode 2 Level 7: Lunar Reactor"));
  g_dn3d_level_list->Add(wxT("Episode 2 Level 8: Dark Side"));
  g_dn3d_level_list->Add(wxT("Episode 2 Level 9: Overlord"));
  g_dn3d_level_list->Add(wxT("Episode 2 Level 10: Spin Cycle"));
  g_dn3d_level_list->Add(wxT("Episode 2 Level 11: Lunatic Fringe"));
  g_dn3d_level_list->Add(wxT("--- Episode 3: Shrapnel City ---"));
  g_dn3d_level_list->Add(wxT("Episode 3 Level 1: Raw Meat"));
  g_dn3d_level_list->Add(wxT("Episode 3 Level 2: Bank Roll"));
  g_dn3d_level_list->Add(wxT("Episode 3 Level 3: Flood Zone"));
  g_dn3d_level_list->Add(wxT("Episode 3 Level 4: L.A. Rumble"));
  g_dn3d_level_list->Add(wxT("Episode 3 Level 5: Movie Set"));
  g_dn3d_level_list->Add(wxT("Episode 3 Level 6: Rabid Transit"));
  g_dn3d_level_list->Add(wxT("Episode 3 Level 7: Fahrenheit"));
  g_dn3d_level_list->Add(wxT("Episode 3 Level 8: Hotel Hell"));
  g_dn3d_level_list->Add(wxT("Episode 3 Level 9: Stadium"));
  g_dn3d_level_list->Add(wxT("Episode 3 Level 10: Tier Drops"));
  g_dn3d_level_list->Add(wxT("Episode 3 Level 11: Freeway"));
  g_dn3d_level_list->Add(wxT("--- Episode 4: The Birth ---"));
  g_dn3d_level_list->Add(wxT("Episode 4 Level 1: It's Impossible"));
  g_dn3d_level_list->Add(wxT("Episode 4 Level 2: Duke-Burger"));
  g_dn3d_level_list->Add(wxT("Episode 4 Level 3: Shop-N-Bag"));
  g_dn3d_level_list->Add(wxT("Episode 4 Level 4: Babe Land"));
  g_dn3d_level_list->Add(wxT("Episode 4 Level 5: Pigsty"));
  g_dn3d_level_list->Add(wxT("Episode 4 Level 6: Going Postal"));
  g_dn3d_level_list->Add(wxT("Episode 4 Level 7: XXX-Stacy"));
  g_dn3d_level_list->Add(wxT("Episode 4 Level 8: Critical Mass"));
  g_dn3d_level_list->Add(wxT("Episode 4 Level 9: Derelict"));
  g_dn3d_level_list->Add(wxT("Episode 4 Level 10: The Queen"));
  g_dn3d_level_list->Add(wxT("Episode 4 Level 11: Area 51"));

  g_sw_level_list = new wxArrayString;
  g_sw_level_list->Add(wxT("None"));
  g_sw_level_list->Add(wxT("--- Enter The Wang ---"));
  g_sw_level_list->Add(wxT("Level 1: Seppuku Station"));
  g_sw_level_list->Add(wxT("Level 2: Zilla Construction"));
  g_sw_level_list->Add(wxT("Level 3: Master Leep's Temple"));
  g_sw_level_list->Add(wxT("Level 4: Dark Woods of the Serpent"));
  g_sw_level_list->Add(wxT("--- Code Of Honor ---"));
  g_sw_level_list->Add(wxT("Level 5: Rising Son"));
  g_sw_level_list->Add(wxT("Level 6: Killing Fields"));
  g_sw_level_list->Add(wxT("Level 7: Hara-Kiri Harbor"));
  g_sw_level_list->Add(wxT("Level 8: Zilla's Villa"));
  g_sw_level_list->Add(wxT("Level 9: Monastery"));
  g_sw_level_list->Add(wxT("Level 10: Raider of the Lost Wang"));
  g_sw_level_list->Add(wxT("Level 11: Sumo Sky Palace"));
  g_sw_level_list->Add(wxT("Level 12: Bath House"));
  g_sw_level_list->Add(wxT("Level 13: Unfriendly Skies"));
  g_sw_level_list->Add(wxT("Level 14: Crude Oil"));
  g_sw_level_list->Add(wxT("Level 15: Coolie Mines"));
  g_sw_level_list->Add(wxT("Level 16: Subpen 7"));
  g_sw_level_list->Add(wxT("Level 17: The Great Escape"));
  g_sw_level_list->Add(wxT("Level 18: Floating Fortress"));
  g_sw_level_list->Add(wxT("Level 19: Water Torture"));
  g_sw_level_list->Add(wxT("Level 20: Stone Rain"));
  g_sw_level_list->Add(wxT("Level 21: Shanghai Shipwreck - Secret Level"));
  g_sw_level_list->Add(wxT("Level 22: Auto Maul - Secret Level"));

  g_dn3d_gametype_list = new wxArrayString;
  g_dn3d_gametype_list->Add(wxT("Dukematch (Spawn)"));
  g_dn3d_gametype_list->Add(wxT("Cooperative"));
  g_dn3d_gametype_list->Add(wxT("Dukematch (No Spawn)"));
  g_dn3d_gametype_list->Add(wxT("Team Dukematch (Spawn)"));
  g_dn3d_gametype_list->Add(wxT("Team Dukematch (No Spawn)"));

  g_dn3d_spawn_list = new wxArrayString;
  g_dn3d_spawn_list->Add(wxT("Monsters"));
  g_dn3d_spawn_list->Add(wxT("Items"));
  g_dn3d_spawn_list->Add(wxT("Inventory"));
  g_dn3d_spawn_list->Add(wxT("All"));

  g_serverlists_addrs = new wxArrayString;
  g_serverlists_portnums = new wxArrayLong;
  g_serverlists_addrs->Add(wxT("dukematch.servegame.com"));
  g_serverlists_portnums->Add(35211);
    g_serverlists_addrs->Add(wxT("dukematch2.servegame.com"));
  g_serverlists_portnums->Add(35212);
    g_serverlists_addrs->Add(wxT("dukematch3.servegame.com"));
  g_serverlists_portnums->Add(35213);
    g_serverlists_addrs->Add(wxT("dukematch4.servegame.com"));
  g_serverlists_portnums->Add(35214);
  //l_AddServerLists(wxT("deflists.cfg"));
#ifndef __WXMSW__
  //l_AddServerLists(*g_launcher_resources_path + wxT("deflists.cfg"));
#endif
}

bool l_configuration_initandload()
{
  g_configuration = new LauncherConfig;
  return g_configuration->Load();
}

#ifdef ENABLE_UPNP
void* UPNPDetectThread::Entry()
{
  g_upnpdata->Detect();
  return NULL;
};

UPNPForwardThread::UPNPForwardThread(long port_num, bool is_tcp)
{
  m_port_num = port_num;
  m_is_tcp = is_tcp;
}

void* UPNPForwardThread::Entry()
{
  g_upnpdata->Forward(m_port_num, m_is_tcp);
  return NULL;
}

UPNPUnForwardThread::UPNPUnForwardThread(long port_num, bool is_tcp)
{
  m_port_num = port_num;
  m_is_tcp = is_tcp;
}

void* UPNPUnForwardThread::Entry()
{
  g_upnpdata->UnForward(m_port_num, m_is_tcp);
  return NULL;
}

void l_Init_UPNP()
{
  g_upnpdata = new UPNPData;
  UPNPDetectThread* l_thread = new UPNPDetectThread();
  if ( l_thread->Create() == wxTHREAD_NO_ERROR )
    l_thread->Run();
  else
    l_thread->Delete();
}
#endif

bool DUKEMATCHERApp::OnInit()
{
#if 0
  wxStandardPaths l_standard_paths;
#if ((defined __UNIX__) || (defined __UNIX_LIKE__) || (defined __LINUX__)) && (defined UNIX_INSTALL_PREFIX)
  l_standard_paths.SetInstallPrefix(wxT(UNIX_INSTALL_PREFIX));
#endif
  // Used for storing user-profile.
  SetAppName(wxT("dukematcher"));

  g_launcher_user_profile_path = new wxString(l_standard_paths.GetUserDataDir());
  *g_launcher_user_profile_path << wxFILE_SEP_PATH;
  g_launcher_resources_path = new wxString(l_standard_paths.GetResourcesDir());
  *g_launcher_resources_path << wxFILE_SEP_PATH;
#if (defined __WXMSW__) || (defined __WXMAC__) || (defined __WXCOCOA__)
  // Remove "\dukematcher" or "/dukematcher"
  g_user_global_profile_path = new wxString(g_launcher_user_profile_path->Mid(0, g_launcher_user_profile_path->Len()-5));
#else
  // Remove "/.dukematcher"
  g_user_global_profile_path = new wxString(g_launcher_user_profile_path->Mid(0, g_launcher_user_profile_path->Len()-6));
#endif
#endif
  l_global_lists_init();
  g_host_frame = NULL;
#if (defined __WXMAC__) || (defined __WXCOCOA__)
  g_empty_menubar = new DUKEMATCHERMenuBar();
  DUKEMATCHERMenuBar::MacSetCommonMenuBar(g_empty_menubar);
#endif
  wxSocketBase::Initialize();
#ifdef ENABLE_UPNP
  l_Init_UPNP();
#endif
  if (!l_configuration_initandload())
  {
    wxMessageBox(wxT("Welcome to DUKE MATCHER!\n\nThis launcher lets you create and join Duke Nukem 3D and Shadow Warrior game rooms so you can play online multiplayer.\n\nSingle player games are supported as well.\n\nBefore DUKE MATCHER can be used, a few games or their source ports have to be specified for DUKE MATCHER.\n\nNow you're going to get two dialogs for initial setup."), wxT("Welcome to DUKEMATCHER!"), wxOK|wxICON_INFORMATION);
    g_main_frame = NULL;
    g_srcports_dialog = new SRCPortsDialog;
    g_srcports_dialog->Show(true);
    g_srcports_dialog->SetFocus();
//  g_srcports_dialog->Raise();
  }
  else
  {
    if (g_configuration->check_for_updates_on_startup)
      g_CheckForUpdates();
    g_main_frame = new MainFrame;
    g_main_frame->ShowMainFrame();
  }
  return true;
}

