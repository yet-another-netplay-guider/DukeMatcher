/*************************************************************************

Copyright 2008-2009 - NY00123

This file is part of DUKEMATCHER.

DUKEMATCHER is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

DUKEMATCHER is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DUKEMATCHER; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*************************************************************************/

#ifndef _DUKEMATCHER_FILETRANSFER_H_
#define _DUKEMATCHER_FILETRANSFER_H_

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/wx.h>
#include <wx/socket.h>
#include <wx/wfstream.h>
#endif

#define ID_TRANSFERTHREADEND (wxID_LOWEST-1)

enum TransferRequestType { TRANSFERREQUEST_NONE,
                           TRANSFERREQUEST_DOWNLOAD, TRANSFERREQUEST_UPLOAD };
enum TransferStateType { TRANSFERSTATE_NONE, TRANSFERSTATE_PENDING,
                         TRANSFERSTATE_BUSY, TRANSFERSTATE_ABORTED };

class FileSendThread : public wxThread
{
public:
    FileSendThread(wxSocketBase* sock, wxWindow* window,
                   wxFile* fp, TransferStateType* transfer_state_ptr);
    virtual void *Entry();
private:
    wxSocketBase *m_sock;
    wxWindow* m_window;
    wxFile* m_fp;
    TransferStateType* m_transfer_state_ptr;
};

class FileReceiveThread : public wxThread
{
public:
    FileReceiveThread(wxSocketBase* sock, wxWindow* window,
                      wxFile* fp, wxULongLong filesize,
                      TransferStateType* transfer_state_ptr);
    virtual void *Entry();
private:
    wxSocketBase *m_sock;
    wxWindow* m_window;
    wxFile* m_fp;
    wxULongLong m_filesize;
    TransferStateType* m_transfer_state_ptr;
};

#endif
