/*************************************************************************

Copyright 2008-2009 - NY00123

This file is part of DUKEMATCHER.

DUKEMATCHER is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

DUKEMATCHER is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DUKEMATCHER; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*************************************************************************/

#ifndef _DUKEMATCHER_CONFIG_H_
#define _DUKEMATCHER_CONFIG_H_

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
//#include <wx/wx.h> // CAUSES ERRORS ON COMPILATION OF THE MASTER SERVER! (wxBase only)
#endif

#define DUKEMATCHER_CONFIG_FULLFILEPATH (wxT("dukematcher.cfg"))

enum GameType        { GAME_DN3D, GAME_SW };
enum SourcePortType  { SOURCEPORT_EDUKE32, SOURCEPORT_JFDUKE3D, SOURCEPORT_IDUKE3D,
                       SOURCEPORT_DUKE3DW32, SOURCEPORT_XDUKE, SOURCEPORT_DOSDUKE,
                       SOURCEPORT_JFSW, SOURCEPORT_SWP, SOURCEPORT_DOSSW };
enum CDRomMountType  { CDROMMOUNT_NONE, CDROMMOUNT_DIR, CDROMMOUNT_IMG };
//enum LevelChoiceType { LEVELCHOICE_EPIMAP, LEVELCHOICE_USERMAP, LEVELCHOICE_MODFILES };
enum DN3DMPGameT_Type  { DN3DMPGAMETYPE_DMSPAWN = 1, DN3DMPGAMETYPE_COOP, DN3DMPGAMETYPE_DMNOSPAWN,
                       DN3DMPGAMETYPE_TDMSPAWN, DN3DMPGAMETYPE_TDMNOSPAWN };
enum DN3DSpawnType   { DN3DSPAWN_MONSTERS = 1 , DN3DSPAWN_ITEMS, DN3DSPAWN_INVENTORY, DN3DSPAWN_ALL };

#define SOURCEPORT_FIRSTDUKEPORT SOURCEPORT_EDUKE32
#define SOURCEPORT_FIRSTSWPORT SOURCEPORT_JFSW
#define SOURCEPORT_LASTDUKEPORT SOURCEPORT_DOSDUKE

void RestoreDefaultSounds(wxString& snd_join, wxString& snd_leave,
                          wxString& snd_send, wxString& snd_receive, wxString& snd_error);

class LauncherConfig
{
  public:
    LauncherConfig();
    bool Load();
    bool Save();
#if (!((defined __WXMSW__) || (defined __WXMAC__) || (defined __WXCOCOA__)))
    void DetectTerminal();
#endif
    // Theme
    bool theme_is_dark, check_for_updates_on_startup;
    // Room Host / Single Player settings
    enum SourcePortType gamelaunch_source_port;
    long gamelaunch_dn3d_skill, gamelaunch_sw_skill;
    wxString gamelaunch_args_in_sp;
    long gamelaunch_dn3dlvl, gamelaunch_swlvl;
    wxString gamelaunch_dn3dusermap, gamelaunch_swusermap;
    wxArrayString gamelaunch_dn3dmodfiles, gamelaunch_swmodfiles;
    wxString gamelaunch_roomname;
    long gamelaunch_numofplayers;
    DN3DMPGameT_Type gamelaunch_dn3dgametype;
    DN3DSpawnType gamelaunch_dn3dspawn;
    long gamelaunch_fraglimit;
	bool gamelaunch_noteamchange, gamelaunch_forcerespawn;
	wxString gamelaunch_args_for_host, gamelaunch_args_for_all,
             gamelaunch_modname, gamelaunch_modurl;
    bool gamelaunch_show_modurl, gamelaunch_use_masterslave,
         gamelaunch_advertise, gamelaunch_skip_settings;
    unsigned long last_serverlist_in_cycle;
//  bool gamelaunch_enable_natfree;
    // Duke Nukem 3D Ports Setup
    bool have_eduke32, have_jfduke3d, have_iduke3d, have_duke3dw32, have_xduke, have_dosduke;
    wxString eduke32_exec, jfduke3d_exec, iduke3d_exec, duke3dw32_exec, xduke_exec, dosduke_exec;
    wxString dn3d_maps_dir, dn3d_mods_dir;
    // Shadow Warrior Ports Setup
    bool have_jfsw, have_swp, have_dossw;
    wxString jfsw_exec, swp_exec, dossw_exec;
    wxString sw_maps_dir, sw_mods_dir;
    // DOSBox Setup
    bool have_dosbox, dosbox_use_conf;
    CDRomMountType dosbox_cdmount;
    wxString dosbox_exec, dosbox_conf, dosbox_cdrom_location, dosbox_cdrom_image;
#ifndef __WXMSW__
    // Windows compatibility (e.g. Wine) Setup, and a terminal.
    bool have_wine;
    wxString wine_exec, terminal_fullcmd;
#endif
    // Multiplayer and network settings
    bool play_snd_join, play_snd_leave, play_snd_send, play_snd_receive, play_snd_error,
         mute_sounds_while_ingame;
    wxString nickname, game_nickname,
             snd_join_file, snd_leave_file, snd_send_file, snd_receive_file, snd_error_file;
    long game_port_number, server_port_number;
    // Advanced options
    bool eduke32_userpath_use, jfduke3d_userpath_use, iduke3d_userpath_use, jfsw_userpath_use,
         override_browser, override_soundcmd, enable_localip_optimization;
    wxString eduke32_userpath, jfduke3d_userpath, iduke3d_userpath, jfsw_userpath,
             playsound_cmd, browser_exec;
    wxArrayString extraservers_addrs;
    wxArrayLong extraservers_portnums;
	// Timestamp in chat
	bool show_timestamp;
    // A host in-room option
    bool host_autoaccept_downloads, advertise;
    // Main window: Retrieve rooms on startup, and stuff for manual joining.
    bool lookup_on_startup;
    wxArrayString manualjoin_names, manualjoin_addrs;
    wxArrayLong manualjoin_portnums;
};

#endif
