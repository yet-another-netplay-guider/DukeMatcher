/*************************************************************************

Copyright 2008-2009 - NY00123

This file is part of DUKEMATCHER.

DUKEMATCHER is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

DUKEMATCHER is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DUKEMATCHER; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*************************************************************************/

#ifndef _DUKEMATCHER_ADVANCEDDIALOG_H_
#define _DUKEMATCHER_ADVANCEDDIALOG_H_

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/wx.h>
#include <wx/notebook.h>
#endif

#include "theme.h"

#define ID_EDUKE32USERPATHCHECKBOX 1000
#define ID_SELECTEDUKE32USERPATH 1001
#define ID_IDUKE3DUSERPATHCHECKBOX 1002
#define ID_JFDUKE3DUSERPATHCHECKBOX 1003
#define ID_SELECTJFDUKE3DUSERPATH 1004
#define ID_SELECTIDUKE3DUSERPATH 1005
#define ID_JFSWUSERPATHCHECKBOX 1006
#define ID_SELECTJFSWUSERPATH 1007
#define ID_BROWSERCHECKBOX 1008
#define ID_LOCATEBROWSER 1009
#define ID_SOUNDCMDCHECKBOX 1010
#define ID_SERVERLISTADD 1011
#define ID_SERVERLISTREM 1012

class AdvDialog : public DUKEMATCHERDialog
{
public:
  AdvDialog();
  ~AdvDialog();
  void OnSelectDir(wxCommandEvent& event);
  void OnLocateExec(wxCommandEvent& event);
  void OnCheckBoxClick(wxCommandEvent& event);

  void OnServerListAdd(wxCommandEvent& event);
  void OnServerListRem(wxCommandEvent& event);

//void OnApply(wxCommandEvent& event);
  void OnCancel(wxCommandEvent& event);
  void OnOk(wxCommandEvent& event);
  void ApplySettings();
private:
  DUKEMATCHERNotebook* m_advancednotebook;
  DUKEMATCHERPanel* m_advpanel;
  wxStaticText* m_userpathstaticText;
  DUKEMATCHERCheckBox* m_eduke32userpathcheckBox;
  DUKEMATCHERTextCtrl* m_eduke32userpathtextCtrl;
  DUKEMATCHERButton* m_eduke32userpathbutton;
  DUKEMATCHERCheckBox* m_jfduke3duserpathcheckBox;
  DUKEMATCHERTextCtrl* m_jfduke3duserpathtextCtrl;
  DUKEMATCHERButton* m_jfduke3duserpathbutton;
  DUKEMATCHERCheckBox* m_iduke3duserpathcheckBox;
  DUKEMATCHERTextCtrl* m_iduke3duserpathtextCtrl;
  DUKEMATCHERButton* m_iduke3duserpathbutton;
  DUKEMATCHERCheckBox* m_jfswuserpathcheckBox;
  DUKEMATCHERTextCtrl* m_jfswuserpathtextCtrl;
  DUKEMATCHERButton* m_jfswuserpathbutton;
  DUKEMATCHERCheckBox* m_overridebrowsercheckBox;
  DUKEMATCHERTextCtrl* m_browsertextCtrl;
  DUKEMATCHERButton* m_browserlocatebutton;
  DUKEMATCHERCheckBox* m_overridesoundcheckBox;
  DUKEMATCHERTextCtrl* m_soundcmdtextCtrl;
  DUKEMATCHERCheckBox* m_localipoptimizecheckBox;
#ifndef __WXMSW__
  wxStaticText* m_terminalstaticText;
  DUKEMATCHERTextCtrl* m_terminaltextCtrl;
#endif
  DUKEMATCHERPanel* m_extraserverspanel;
  wxStaticText* m_extraserveraddrstaticText;
  wxStaticText* m_extraserverportnumstaticText;
  DUKEMATCHERTextCtrl* m_extraserverhostaddrtextCtrl;
  wxStaticText* m_extraserverseparatorstaticText;
  DUKEMATCHERTextCtrl* m_extraserverportnumtextCtrl;
  DUKEMATCHERButton* m_extraserveraddbutton;
  DUKEMATCHERListBox* m_extraserverlistBox;
  DUKEMATCHERButton* m_extraserverrembutton;

  wxStdDialogButtonSizer* m_advsdbSizer;
  DUKEMATCHERButton* m_advsdbSizerOK;
  DUKEMATCHERButton* m_advsdbSizerCancel;

  wxArrayString m_extra_addrs;
  wxArrayLong m_extra_portnums;

  DECLARE_EVENT_TABLE()
};

#endif

