/*************************************************************************

Copyright 2008-2009 - NY00123

This file is part of DUKEMATCHER.

DUKEMATCHER is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

DUKEMATCHER is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DUKEMATCHER; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*************************************************************************/

#ifndef _DUKEMATCHER_MPCOMMON_H_
#define _DUKEMATCHER_MPCOMMON_H_

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
//#include <wx/wx.h> // CAUSES ERRORS ON COMPILATION OF THE MASTER SERVER! (wxBase only)
#include <wx/socket.h>
#endif

#include "common.h"
#include "config.h"
#include "file_transfer.h"

#define MAX_NUM_MSG_LINES 512

typedef struct
{
  wxString nickname, peerdetectedip, ingameip, team, status, clientversion;
  long game_port_number;
} RoomPlayerData;

class RoomPlayersTable
{
public:
  RoomPlayerData players[MAX_NUM_PLAYERS];
  size_t num_players;

  RoomPlayersTable();
  bool Add(const wxString& nickname,
           const wxString& ingameip, long game_port_number);
  bool DelByIndex(size_t index);
  size_t FindIndexByNick(const wxString& nickname);
};

typedef struct
{
  TransferRequestType transferrequest;
  wxString filename;
  long filesize;
  bool awaiting_download;
} TransferRequestData;

class ExtendedRoomPlayersTable : public RoomPlayersTable
{
public:
  TransferRequestData requests[MAX_NUM_PLAYERS];

  ExtendedRoomPlayersTable();
  bool Add(const wxString& nickname,
           const wxString& ingameip, long game_port_number);
  bool DelByIndex(size_t index);
};

/*
typedef struct
{
  wxString nickname, ingameip, filename;
  long game_port_number, filesize;
  TransferRequestType transferrequest;
} ExtendedRoomPlayerData;

class ExtendedRoomPlayersTable
{
public:
  ExtendedRoomPlayerData players[MAX_NUM_PLAYERS];
  size_t num_players;

  ExtendedRoomPlayersTable();
  bool Add(const wxString& nickname,
           const wxString& ingameip, long game_port_number);
  bool DelByIndex(size_t index);
  size_t FindIndexByNick(const wxString& nickname);
};
*/
typedef struct
{
  wxSocketBase* sock;
  wxString peerdetectedip, selfdetectedip, team, status, clientversion;
  wxTimer* timer;
} HostRoomClientData;

class HostRoomClientsTable
{
public:
  HostRoomClientData clients[MAX_NUM_PLAYERS+1]; // One more for file-transfer.
  size_t num_clients;

  HostRoomClientsTable();
  bool Add(wxSocketBase* sock,
           const wxString& peerdetectedip,//const wxString& selfdetectedip
           wxTimer* timer);
  bool PutInRoom(size_t index, size_t inroom_num_clients);
  bool DelBySock(wxSocketBase* sock);
  bool DelByIndex(size_t index);
  size_t FindIndexBySock(wxSocketBase* sock);
};

#endif
