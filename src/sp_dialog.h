/*************************************************************************

Copyright 2008-2009 - NY00123

This file is part of DUKEMATCHER.

DUKEMATCHER is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

DUKEMATCHER is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DUKEMATCHER; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*************************************************************************/

#ifndef _DUKEMATCHER_SPDIALOG_H_
#define _DUKEMATCHER_SPDIALOG_H_

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "theme.h"

#define ID_SPSELECTGAME 1000
#define ID_SPSELECTSRCPORT 1001
#define ID_SPHAVEBOTS 1002
#define ID_SPPLAYDEMO 1003
#define ID_SPRECORDDEMO 1004
#define ID_SPUSEEPIMAP 1005
#define ID_SPUSEUSERMAP 1006
#define ID_SPUSEMODFILE 1007
#define ID_SPREMMODFILE 1008

class SPDialog : public DUKEMATCHERDialog
{
public:
  SPDialog();
  ~SPDialog();

  void OnGameSelect(wxCommandEvent& event);
  void OnSrcPortSelect(wxCommandEvent& event);
  void OnCheckBoxClick(wxCommandEvent& event);
  void OnRadioBtnClick(wxCommandEvent& event);

  void OnAddModFile(wxCommandEvent& event);
  void OnRemModFile(wxCommandEvent& event);

//void OnApply(wxCommandEvent& event);
  void OnCancel(wxCommandEvent& event);
  void OnOk(wxCommandEvent& event);
  void ApplySettings();

private:
  void PartialDialogControlsReset();
  void PrepareDemoFileControls();

  DUKEMATCHERPanel* m_sppanel;
  wxStaticText* m_spgamestaticText;
  DUKEMATCHERChoice* m_spgamechoice;
  wxStaticText* m_spsourceportstaticText;
  DUKEMATCHERChoice* m_spsourceportchoice;
  DUKEMATCHERCheckBox* m_spbotscheckBox;
  DUKEMATCHERChoice* m_spbotschoice;
  wxStaticText* m_spskillstaticText;
  DUKEMATCHERChoice* m_spskillchoice;
  DUKEMATCHERCheckBox* m_spplaydemocheckBox;
  DUKEMATCHERChoice* m_spplaydemochoice;
  DUKEMATCHERCheckBox* m_sprecorddemocheckBox;
  DUKEMATCHERTextCtrl* m_sprecorddemotextCtrl;
  DUKEMATCHERRadioButton* m_spepimapradioBtn;
  DUKEMATCHERChoice* m_spepimapchoice;
  DUKEMATCHERRadioButton* m_spusermapradioBtn;
  DUKEMATCHERChoice* m_spusermapchoice;
  wxStaticText* m_spmodstaticText;
  DUKEMATCHERListBox* m_spavailmodfileslistBox;
  DUKEMATCHERListBox* m_spusedmodfileslistBox;
  DUKEMATCHERButton* m_spusemodfilebutton;
  DUKEMATCHERButton* m_spremmodfilebutton;
  wxStaticText* m_spextraargsstaticText;
  DUKEMATCHERTextCtrl* m_spextraargstextCtrl;
  wxStdDialogButtonSizer* m_spsdbSizer;
  DUKEMATCHERButton* m_spsdbSizerOK;
  DUKEMATCHERButton* m_spsdbSizerCancel;

  DECLARE_EVENT_TABLE()
};

#endif

