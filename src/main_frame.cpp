/*************************************************************************

Copyright 2008-2009 - NY00123

This file is part of DUKEMATCHER.

DUKEMATCHER is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

DUKEMATCHER is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DUKEMATCHER; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*************************************************************************/

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/wx.h>
#include <wx/gbsizer.h>
//#include <wx/socket.h>
#endif

#include "main_frame.h"
#include "sp_dialog.h"
#include "mp_dialog.h"
//#include "manualjoin_dialog.h"
#include "srcports_dialog.h"
#include "comm_txt.h"
#include "dukematcher.h"
#include "theme.h"
#include "dukematcher.xpm"
#include "common.h"

#include <wx/arrimpl.cpp>

WX_DEFINE_OBJARRAY(ArrayOfStringArrays);

BEGIN_EVENT_TABLE(MainFrame, DUKEMATCHERFrame)
  EVT_MENU(ID_OPENSPDIALOG, MainFrame::OnLoadSPDialog)
  EVT_MENU(ID_OPENMPDIALOG, MainFrame::OnLoadMPDialog)
//EVT_MENU(ID_OPENMANUALJOINDIALOG, MainFrame::OnLoadManualJoinDialog)
  EVT_MENU(ID_CHANGETHEME, MainFrame::OnChangeTheme)
  EVT_MENU(ID_OPENSRCPORTSETTINGS, MainFrame::OnLoadSRCPortsDialog)
  EVT_MENU(ID_OPENNETWORKSETTINGS, MainFrame::OnLoadNetworkDialog)
  EVT_MENU(ID_OPENADVANCEDSETTINGS, MainFrame::OnLoadAdvancedDialog)
  EVT_MENU(wxID_ABOUT, MainFrame::OnAbout)
  EVT_MENU(ID_CHECKFORUPDATES, MainFrame::OnCheckForUpdates)
  EVT_MENU(ID_ALWAYSCHECKFORUPDATES, MainFrame::OnAutoCheckForUpdates)
  EVT_MENU(ID_VISITWEBSITE, MainFrame::OnVisitWebsite)
  EVT_MENU(wxID_EXIT, MainFrame::OnQuit)
  EVT_CHECKBOX(ID_SERVERLISTAUTOCHECK, MainFrame::OnServerListAutoCheck)
  EVT_BUTTON(wxID_REFRESH, MainFrame::OnGetList)
  EVT_BUTTON(wxID_OK, MainFrame::OnJoin)
  EVT_LIST_ITEM_SELECTED(ID_SERVERLIST, MainFrame::OnHostSelect)
  EVT_LIST_ITEM_ACTIVATED(ID_SERVERLIST, MainFrame::OnHostActivate)
  EVT_BUTTON(ID_MANUALJOINADD, MainFrame::OnAddHost)
  EVT_BUTTON(ID_MANUALJOINREM, MainFrame::OnRemHost)
  EVT_CHOICE(ID_MANUALJOINLIST, MainFrame::OnManualJoinHostSelect)
  EVT_TEXT_ENTER(wxID_OK, MainFrame::OnJoin)
  EVT_SOCKET(ID_GETLISTSOCKET, MainFrame::OnSocketEvent)
  EVT_TIMER(ID_GETLISTTIMER, MainFrame::OnTimer)
  EVT_CLOSE(MainFrame::OnClose)
END_EVENT_TABLE()

MainFrame::MainFrame() : DUKEMATCHERFrame(NULL, wxID_ANY,
#if (defined __WXMAC__) || (defined __WXCOCOA__)
                                 wxT("DUKE MATCHER"), wxDefaultPosition, wxDefaultSize, wxDEFAULT_FRAME_STYLE|wxFRAME_NO_TASKBAR)
#else
                                 wxT("DUKE MATCHER"))
#endif
{
	m_Mainmenubar = new wxMenuBar();
	m_Actionmenu = new wxMenu();

	wxMenuItem* m_SPmenuItem = new wxMenuItem( m_Actionmenu, ID_OPENSPDIALOG, wxString( wxT("&Single Player") ) , wxEmptyString, wxITEM_NORMAL );
	m_Actionmenu->Append( m_SPmenuItem );

	wxMenuItem* m_HostmenuItem = new wxMenuItem( m_Actionmenu, ID_OPENMPDIALOG, wxString( wxT("&Create a room") ) , wxEmptyString, wxITEM_NORMAL );
	m_Actionmenu->Append( m_HostmenuItem );

//	wxMenuItem* m_ManualjoinmenuItem = new wxMenuItem( m_Actionmenu, ID_OPENMANUALJOINDIALOG, wxString( wxT("&Join a room manually") ) , wxEmptyString, wxITEM_NORMAL );
//	m_Actionmenu->Append( m_ManualjoinmenuItem );

	wxMenuItem* m_ThememenuItem = new wxMenuItem( m_Actionmenu, ID_CHANGETHEME, wxString( wxT("Change theme") ) , wxEmptyString, wxITEM_NORMAL );
	m_Actionmenu->Append( m_ThememenuItem );

	wxMenuItem* m_QuitmenuItem = new wxMenuItem( m_Actionmenu, wxID_EXIT, wxString( wxT("&Quit") ) + wxT('\t') + wxT("Ctrl-Q"), wxEmptyString, wxITEM_NORMAL );
	m_Actionmenu->Append( m_QuitmenuItem );
	
	m_Mainmenubar->Append( m_Actionmenu, wxT("&Action") );
	
	m_Settingsmenu = new wxMenu();

	wxMenuItem* m_SRCPortsmenuItem = new wxMenuItem( m_Settingsmenu, ID_OPENSRCPORTSETTINGS, wxString( wxT("&Source ports") ) , wxEmptyString, wxITEM_NORMAL );
	m_Settingsmenu->Append( m_SRCPortsmenuItem );
	
	wxMenuItem* m_NetworkingmenuItem = new wxMenuItem( m_Settingsmenu, ID_OPENNETWORKSETTINGS, wxString( wxT("&Multiplayer and networking") ) , wxEmptyString, wxITEM_NORMAL );
	m_Settingsmenu->Append( m_NetworkingmenuItem );
	
	wxMenuItem* m_AdvancedmenuItem = new wxMenuItem( m_Settingsmenu, ID_OPENADVANCEDSETTINGS, wxString( wxT("&Advanced") ) , wxEmptyString, wxITEM_NORMAL );
	m_Settingsmenu->Append( m_AdvancedmenuItem );
	
	m_Mainmenubar->Append( m_Settingsmenu, wxT("&Settings") );
	
	m_Helpmenu = new wxMenu();

	wxMenuItem* m_AboutmenuItem = new wxMenuItem( m_Helpmenu, wxID_ABOUT, wxString( wxT("&About") ) , wxEmptyString, wxITEM_NORMAL );
	m_Helpmenu->Append( m_AboutmenuItem );

	wxMenuItem* m_checkforupdatesmenuItem = new wxMenuItem( m_Helpmenu, ID_CHECKFORUPDATES, wxString( wxT("Check for updates") ) , wxEmptyString, wxITEM_NORMAL );
	//m_Helpmenu->Append( m_checkforupdatesmenuItem );
	
	m_checkforupdatesonstartupmenuItem = new wxMenuItem( m_Helpmenu, ID_ALWAYSCHECKFORUPDATES, wxString( wxT("Check for updates on startup") ) , wxEmptyString, wxITEM_CHECK );
	//m_Helpmenu->Append( m_checkforupdatesonstartupmenuItem );
	//m_checkforupdatesonstartupmenuItem->Check(g_configuration->check_for_updates_on_startup);
	
	wxMenuItem* m_visitwebsitemenuItem = new wxMenuItem( m_Helpmenu, ID_VISITWEBSITE, wxString( wxT("&Visit NY00123's website") ) , wxEmptyString, wxITEM_NORMAL );
	//m_Helpmenu->Append( m_visitwebsitemenuItem );
	
	m_Mainmenubar->Append( m_Helpmenu, wxT("&Help") );
#if (!((defined __WXMAC__) || (defined __WXCOCOA__)))
	SetMenuBar( m_Mainmenubar );
#endif
	wxBoxSizer* MainFramebSizer = new wxBoxSizer( wxVERTICAL );
	
	m_mainpanel = new DUKEMATCHERPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* mainpanelbSizer = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* serverlistbSizer;
	serverlistbSizer = new wxBoxSizer( wxHORIZONTAL );
	
	m_serverlistCtrl = new DUKEMATCHERListCtrl( m_mainpanel, ID_SERVERLIST, wxDefaultPosition, wxDLG_UNIT(this, wxSize(600, 80)), wxLC_REPORT|wxLC_SINGLE_SEL );
//      wxImageList* imageList = new wxImageList(16, 16);
//      m_serverlistCtrl->AssignImageList(imageList, wxIMAGE_LIST_SMALL);

	wxListItem itemCol;
	itemCol.SetText(wxT("Title"));
//	itemCol.SetAlign(wxLIST_FORMAT_LEFT);
//	itemCol.SetImage(-1);
	itemCol.SetWidth(230);
	m_serverlistCtrl->InsertColumn(0, itemCol);
	itemCol.SetWidth(219);
	//m_serverlistCtrl->SetColumnWidth(0, (wxDLG_UNIT(this, wxSize(70, -1))).GetWidth());
	itemCol.SetText(wxT("Game"));
	itemCol.SetWidth(120);
	m_serverlistCtrl->InsertColumn(1, itemCol);
	//m_serverlistCtrl->SetColumnWidth(1, (wxDLG_UNIT(this, wxSize(60, -1))).GetWidth());
	itemCol.SetText(wxT("Source Port"));
	itemCol.SetWidth(100);
	m_serverlistCtrl->InsertColumn(2, itemCol);
	//m_serverlistCtrl->SetColumnWidth(2, (wxDLG_UNIT(this, wxSize(60, -1))).GetWidth());
	itemCol.SetText(wxT("Map"));
	itemCol.SetWidth(160);
	m_serverlistCtrl->InsertColumn(3, itemCol);
	//m_serverlistCtrl->SetColumnWidth(3, (wxDLG_UNIT(this, wxSize(130, -1))).GetWidth());
	itemCol.SetText(wxT("In Game"));
	itemCol.SetWidth(56);
	m_serverlistCtrl->InsertColumn(4, itemCol);
	//m_serverlistCtrl->SetColumnWidth(4, (wxDLG_UNIT(this, wxSize(60, -1))).GetWidth());
	itemCol.SetText(wxT("Players"));
	itemCol.SetWidth(50);
	m_serverlistCtrl->InsertColumn(5, itemCol);
	//m_serverlistCtrl->SetColumnWidth(5, (wxDLG_UNIT(this, wxSize(75, -1))).GetWidth());
	itemCol.SetText(wxT("Game Type"));
	itemCol.SetWidth(180);
	m_serverlistCtrl->InsertColumn(6, itemCol);
	//m_serverlistCtrl->SetColumnWidth(6, (wxDLG_UNIT(this, wxSize(60, -1))).GetWidth());
	itemCol.SetText(wxT("Skill"));
	itemCol.SetWidth(120);
	m_serverlistCtrl->InsertColumn(7, itemCol);
	//m_serverlistCtrl->SetColumnWidth(7, (wxDLG_UNIT(this, wxSize(40, -1))).GetWidth());
	itemCol.SetText(wxT("TC/MOD"));
	itemCol.SetWidth(150);
	m_serverlistCtrl->InsertColumn(8, itemCol);
	//m_serverlistCtrl->SetColumnWidth(8, (wxDLG_UNIT(this, wxSize(50, -1))).GetWidth());
//	RefreshListCtrlSize();

	serverlistbSizer->Add( m_serverlistCtrl, 8, wxALL|wxEXPAND, 5 );
	
	m_playerlistBox = new DUKEMATCHERListBox( m_mainpanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(56, 80)), 0, NULL, 0 ); 
	serverlistbSizer->Add( m_playerlistBox, 2, wxALL|wxEXPAND, 5 );
	
	mainpanelbSizer->Add( serverlistbSizer, 1, wxEXPAND, 5 );
	
	wxBoxSizer* serverlistcontrolbSizer;
	serverlistcontrolbSizer = new wxBoxSizer( wxHORIZONTAL );
	
	m_getlistbutton = new DUKEMATCHERButton( m_mainpanel, wxID_REFRESH, wxT("Get list of rooms"), wxDefaultPosition, wxDefaultSize, 0 );
	serverlistcontrolbSizer->Add( m_getlistbutton, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_serverliststaticText = new wxStaticText( m_mainpanel, wxID_ANY, wxT("Double-click a selection to join."), wxDefaultPosition, wxDefaultSize, 0 );
	serverlistcontrolbSizer->Add( m_serverliststaticText, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

//	m_joinbutton = new DUKEMATCHERButton( m_mainpanel, wxID_OK, wxT("Join"), wxDefaultPosition, wxDefaultSize, 0 );
//	serverlistcontrolbSizer->Add( m_joinbutton, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5 );
	
	
	serverlistcontrolbSizer->Add( 0, 0, 1, wxEXPAND, 5 );
	
	m_autolookupcheckBox = new DUKEMATCHERCheckBox( m_mainpanel, ID_SERVERLISTAUTOCHECK, wxT("Look for rooms on startup."), wxDefaultPosition, wxDefaultSize, 0 );
	
	serverlistcontrolbSizer->Add( m_autolookupcheckBox, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	mainpanelbSizer->Add( serverlistcontrolbSizer, 0, wxEXPAND, 5 );
	
	/****** WORKAROUND: Why should the button be created here, so with dark theme it isn't fully grey? ******/
	m_joinbutton = new DUKEMATCHERButton( m_mainpanel, wxID_OK, wxT("Join"), wxDefaultPosition, wxDefaultSize, 0 );

	wxStaticBoxSizer* ManualJoinsbSizer = new wxStaticBoxSizer( new wxStaticBox( m_mainpanel, wxID_ANY, wxT("") ), wxVERTICAL );

	wxFlexGridSizer* ManualJoinfgSizer = new wxFlexGridSizer( 2, 5, 0, 0 );
	ManualJoinfgSizer->AddGrowableCol( 0 );
	ManualJoinfgSizer->AddGrowableCol( 1 );
	ManualJoinfgSizer->AddGrowableCol( 4 );
	ManualJoinfgSizer->SetFlexibleDirection( wxBOTH );
	ManualJoinfgSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	m_manualjoinnamestaticText = new wxStaticText( m_mainpanel, wxID_ANY, wxT("Name"), wxDefaultPosition, wxDefaultSize, 0 );
	ManualJoinfgSizer->Add( m_manualjoinnamestaticText, 0, wxALL|wxALIGN_CENTER_HORIZONTAL, 5 );
	
	
	m_manualjoinhostaddrstaticText = new wxStaticText( m_mainpanel, wxID_ANY, wxT("Hostname or IP"), wxDefaultPosition, wxDefaultSize, 0 );
	ManualJoinfgSizer->Add( m_manualjoinhostaddrstaticText, 0, wxALL|wxALIGN_CENTER_HORIZONTAL, 5 );
	
	
	ManualJoinfgSizer->Add( 0, 0, 1, wxEXPAND, 5 );
	
	m_manualjoinportnumstaticText = new wxStaticText( m_mainpanel, wxID_ANY, wxT("Port Number"), wxDefaultPosition, wxDefaultSize, 0 );
	ManualJoinfgSizer->Add( m_manualjoinportnumstaticText, 0, wxALL|wxALIGN_CENTER_HORIZONTAL, 5 );
	
	
	ManualJoinfgSizer->Add( 0, 0, 1, wxEXPAND, 5 );
	
	m_manualjoinnametextCtrl = new DUKEMATCHERTextCtrl( m_mainpanel, wxID_OK, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER );
	ManualJoinfgSizer->Add( m_manualjoinnametextCtrl, 0, wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL|wxALIGN_CENTER_HORIZONTAL, 5 );
	
	m_manualjoinhostaddrtextCtrl = new DUKEMATCHERTextCtrl( m_mainpanel, wxID_OK, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER );
	ManualJoinfgSizer->Add( m_manualjoinhostaddrtextCtrl, 0, wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL|wxALIGN_CENTER_HORIZONTAL, 5 );
	
	m_manualjoinseparatorstaticText = new wxStaticText( m_mainpanel, wxID_ANY, wxT(":"), wxDefaultPosition, wxDefaultSize, 0 );
	m_manualjoinseparatorstaticText->Wrap( -1 );
	ManualJoinfgSizer->Add( m_manualjoinseparatorstaticText, 0, wxALL|wxALIGN_CENTER_VERTICAL|wxALIGN_CENTER_HORIZONTAL, 5 );
	
	m_manualjoinportnumtextCtrl = new DUKEMATCHERTextCtrl( m_mainpanel, wxID_OK, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER );
	ManualJoinfgSizer->Add( m_manualjoinportnumtextCtrl, 0, wxALL|wxALIGN_CENTER_VERTICAL|wxALIGN_CENTER_HORIZONTAL, 5 );
	
	m_manualjoinaddbutton = new DUKEMATCHERButton( m_mainpanel, ID_MANUALJOINADD, wxT("Add and save"), wxDefaultPosition, wxDefaultSize, 0 );
	ManualJoinfgSizer->Add( m_manualjoinaddbutton, 0, wxALL|wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT, 5 );
	
	ManualJoinsbSizer->Add( ManualJoinfgSizer, 0, wxEXPAND, 5 );
//	mainpanelbSizer->Add( ManualJoinfgSizer, 0, wxEXPAND, 5 );

		ManualJoinsbSizer->Hide(ManualJoinfgSizer, true);
	
	wxBoxSizer* ManualJoinlistbSizer = new wxBoxSizer( wxHORIZONTAL );
	
//	wxArrayString m_manualjoinchoiceChoices;
	m_manualjoinchoice = new DUKEMATCHERChoice( m_mainpanel, ID_MANUALJOINLIST, wxDefaultPosition, wxDefaultSize/*, m_manualjoinchoiceChoices, 0 */);
//	m_manualjoinchoice->SetSelection( 0 );
	ManualJoinlistbSizer->Add( m_manualjoinchoice, 1, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_manualjoinrembutton = new DUKEMATCHERButton( m_mainpanel, ID_MANUALJOINREM, wxT("Remove permanently"), wxDefaultPosition, wxDefaultSize, 0 );
	ManualJoinlistbSizer->Add( m_manualjoinrembutton, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	ManualJoinsbSizer->Add( ManualJoinlistbSizer, 0, wxEXPAND, 5 );
//	mainpanelbSizer->Add( ManualJoinlistbSizer, 0, wxEXPAND, 5 );

	/****** WORKAROUND: Why should the button be created before, so with dark theme it isn't fully grey? ******/
//	m_joinbutton = new DUKEMATCHERButton( m_mainpanel, wxID_OK, wxT("Join"), wxDefaultPosition, wxDefaultSize, 0 );
	ManualJoinsbSizer->Add( m_joinbutton, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5 );

	ManualJoinsbSizer->Hide(1);

	mainpanelbSizer->Add( ManualJoinsbSizer, 0, wxEXPAND, 5 );
	
	m_mainpanel->SetSizer( mainpanelbSizer );
	m_mainpanel->Layout();
	mainpanelbSizer->Fit( m_mainpanel );
	MainFramebSizer->Add( m_mainpanel, 1, wxEXPAND | wxALL, 5 );
	
	SetSizer( MainFramebSizer );
	Layout();
	m_MainstatusBar = CreateStatusBar( 1, wxST_SIZEGRIP, wxID_ANY );
//	MainFramebSizer->Fit( this );
        MainFramebSizer->SetSizeHints(this);

  Centre();
  SetIcon(wxIcon(dukematcher_xpm));

  m_autolookupcheckBox->SetValue(g_configuration->lookup_on_startup);
  size_t l_num_of_items = g_configuration->manualjoin_portnums.GetCount();
  for (size_t l_loop_var = 0; l_loop_var < l_num_of_items; l_loop_var++)
    m_manualjoinchoice->Append(g_configuration->manualjoin_names[l_loop_var] +
                               wxT(' ') + g_configuration->manualjoin_addrs[l_loop_var] +
                               wxString::Format(wxT(":%d"), g_configuration->manualjoin_portnums[l_loop_var]));
  m_client = NULL;
  m_serverlist_timer = new wxTimer(this, ID_GETLISTTIMER);
}
/*
void MainFrame::RefreshListCtrlSize()
{
  int l_width;
  if (m_serverlistCtrl->GetItemCount())
    l_width = wxLIST_AUTOSIZE;
  else
    l_width = wxLIST_AUTOSIZE_USEHEADER;
  m_serverlistCtrl->SetColumnWidth(0, l_width);
  m_serverlistCtrl->SetColumnWidth(1, l_width);
  m_serverlistCtrl->SetColumnWidth(2, l_width);
  m_serverlistCtrl->SetColumnWidth(3, l_width);
  m_serverlistCtrl->SetColumnWidth(4, l_width);
  m_serverlistCtrl->SetColumnWidth(5, l_width);
  m_serverlistCtrl->SetColumnWidth(6, l_width);
  m_serverlistCtrl->SetColumnWidth(7, l_width);
  m_serverlistCtrl->SetColumnWidth(8, l_width);
  m_serverlistCtrl->Refresh();
}
*/
void MainFrame::OnChangeTheme(wxCommandEvent& WXUNUSED(event))
{
//int l_item_count = m_serverlistCtrl->GetItemCount(), l_loop_var;
//wxArrayLong l_array_items;
//wxColour l_colour;
  g_configuration->theme_is_dark = !g_configuration->theme_is_dark;
  g_configuration->Save();

  g_main_frame = new MainFrame;
  HideMainFrame();
  g_main_frame->ShowMainFrame();
  Close();

  // According to the experience, wxStaticText objects are updated automatically.
/*m_Mainmenubar->SetThemeColour();
  m_mainpanel->SetThemeColour();

  for (l_loop_var = 0; l_loop_var < l_item_count; l_loop_var++)
  {
    l_colour = m_serverlistCtrl->GetItemTextColour(l_loop_var);
    if (l_colour == *wxRED)
      l_array_items.Add(l_loop_var);
  }
  m_serverlistCtrl->SetThemeColour();
  l_loop_var = 0;
  while ((l_loop_var < l_item_count) && (l_array_items.GetCount()))
  {
    if (l_loop_var == l_array_items[0])
    {
      l_array_items.RemoveAt(0);
      m_serverlistCtrl->SetItemTextColour(l_loop_var, *wxRED);
    }
    else
      if (g_configuration->theme_is_dark)
        m_serverlistCtrl->SetItemTextColour(l_loop_var, *wxGREEN);
    l_loop_var++;
  } // Even if there's nothing marked with red colour, we're probably not done.
  while (l_loop_var < l_item_count)
  {
    if (g_configuration->theme_is_dark)
      m_serverlistCtrl->SetItemTextColour(l_loop_var, *wxGREEN);
    l_loop_var++;
  }

  m_playerlistBox->SetThemeColour();
  m_getlistbutton->SetThemeColour();
  m_joinbutton->SetThemeColour();
  m_autolookupcheckBox->SetThemeColour();
  m_manualjoinnametextCtrl->SetThemeColour();
  m_manualjoinhostaddrtextCtrl->SetThemeColour();
  m_manualjoinportnumtextCtrl->SetThemeColour();
  m_manualjoinaddbutton->SetThemeColour();
  m_manualjoinchoice->SetThemeColour();
  m_manualjoinrembutton->SetThemeColour();
  SetStatusBarThemeColour(m_MainstatusBar);*/
}

void MainFrame::OnLoadSPDialog(wxCommandEvent& WXUNUSED(event))
{
  if (g_configuration->have_eduke32 || g_configuration->have_jfduke3d || g_configuration->have_iduke3d
      || g_configuration->have_duke3dw32 || g_configuration->have_xduke || g_configuration->have_dosduke
      || g_configuration->have_jfsw || g_configuration->have_swp || g_configuration->have_dossw)
  {
//  if (!g_sp_dialog)
//  {
    HideMainFrame();
    g_sp_dialog = new SPDialog;
    g_sp_dialog->Show(true);
    g_sp_dialog->SetFocus();
//  g_sp_dialog->Raise();
//  }
//  else
//    g_sp_dialog->SetFocus();
  }
  else
    wxMessageBox(wxT("You should configure a source port\nso you can use one to launch the game,\naccessing Settings --> Source ports."),
                 wxT("There are no configured source ports."), wxOK|wxICON_INFORMATION);
}

void MainFrame::OnLoadMPDialog(wxCommandEvent& WXUNUSED(event))
{
  GameType l_game;
  size_t l_skill;
  bool l_launch_usermap;
  size_t l_epimap;
  wxString l_usermap;
  wxArrayString l_modfiles;
  bool l_start_server = (g_configuration->gamelaunch_skip_settings);
#ifdef ENABLE_UPNP
  UPNPForwardThread* l_thread;
#endif
  if (l_start_server)
  {
    if (((g_configuration->gamelaunch_source_port == SOURCEPORT_EDUKE32) && g_configuration->have_eduke32) ||
        ((g_configuration->gamelaunch_source_port == SOURCEPORT_JFDUKE3D) && g_configuration->have_jfduke3d) ||
        ((g_configuration->gamelaunch_source_port == SOURCEPORT_IDUKE3D) && g_configuration->have_iduke3d) ||
        ((g_configuration->gamelaunch_source_port == SOURCEPORT_DUKE3DW32) && g_configuration->have_duke3dw32) ||
        ((g_configuration->gamelaunch_source_port == SOURCEPORT_XDUKE) && g_configuration->have_xduke) ||
        ((g_configuration->gamelaunch_source_port == SOURCEPORT_DOSDUKE) && g_configuration->have_dosduke && g_configuration->have_dosbox))
    {
      l_game = GAME_DN3D;
      l_skill = g_configuration->gamelaunch_dn3d_skill;
      l_epimap = g_configuration->gamelaunch_dn3dlvl;
      l_usermap = g_configuration->gamelaunch_dn3dusermap;
    }
    else
      if (((g_configuration->gamelaunch_source_port == SOURCEPORT_JFSW) && g_configuration->have_jfsw) ||
          ((g_configuration->gamelaunch_source_port == SOURCEPORT_SWP) && g_configuration->have_swp) ||
          ((g_configuration->gamelaunch_source_port == SOURCEPORT_DOSSW) && g_configuration->have_dossw && g_configuration->have_dosbox))
      {
        l_game = GAME_SW;
        l_skill = g_configuration->gamelaunch_sw_skill;
        l_epimap = g_configuration->gamelaunch_swlvl;
        l_usermap = g_configuration->gamelaunch_swusermap;
      }
    else
      l_start_server = false;
    if (l_start_server)
    {
      l_launch_usermap = (!l_usermap.IsEmpty());

      g_host_frame = new HostedRoomFrame;
      if (g_host_frame->StartServer())
      {
        //HideMainFrame();
#ifdef ENABLE_UPNP
        l_thread = new UPNPForwardThread(g_configuration->game_port_number, false);
        if (l_thread->Create() == wxTHREAD_NO_ERROR )
          l_thread->Run();
        else
          l_thread->Delete();
        l_thread = new UPNPForwardThread(g_configuration->server_port_number, true);
        if (l_thread->Create() == wxTHREAD_NO_ERROR )
          l_thread->Run();
        else
          l_thread->Delete();
#endif
        g_host_frame->Show(true);
        g_host_frame->UpdateGameSettings(l_game,
                                         g_configuration->gamelaunch_source_port,
                                         g_configuration->gamelaunch_roomname,
                                         g_configuration->gamelaunch_numofplayers,
                                         false, wxEmptyString, l_skill,
                                         g_configuration->gamelaunch_dn3dgametype,
                                         g_configuration->gamelaunch_dn3dspawn,
										 g_configuration->gamelaunch_fraglimit,
										 g_configuration->gamelaunch_noteamchange,
										 g_configuration->gamelaunch_forcerespawn,
                                         g_configuration->gamelaunch_args_for_host,
                                         g_configuration->gamelaunch_args_for_all,
                                         l_launch_usermap, l_epimap,
                                         l_usermap, l_modfiles,
                                         g_configuration->gamelaunch_modname,
                                         g_configuration->gamelaunch_show_modurl,
                                         g_configuration->gamelaunch_modurl,
                                         g_configuration->gamelaunch_advertise,
                                         g_configuration->gamelaunch_use_masterslave);
        g_host_frame->FocusFrame();
//      g_host_frame->Raise();
      }
      else
      {
        wxMessageBox(wxString::Format(wxT("ERROR: Couldn't start server on port %d for an unknown reason.\nPossible reasons:\n* The configured server port number is already in use by another listener.\n* The server has disconnected a client by itself. At least in that case you might have to wait for about a minute before you can rehost on the same port.\n* Somehow a disconnection hasn't been done probably."), g_configuration->server_port_number),
        wxT("Couldn't start listening"), wxOK|wxICON_EXCLAMATION);
        g_host_frame->Destroy();
        g_host_frame = NULL;
      }
    }
  }
  if (!l_start_server)
  {
    if ((g_configuration->have_eduke32 || g_configuration->have_jfduke3d || g_configuration->have_iduke3d
        || g_configuration->have_duke3dw32 || g_configuration->have_xduke || g_configuration->have_dosduke
        || g_configuration->have_jfsw || g_configuration->have_swp || g_configuration->have_dossw))
    {
//    if (!g_mp_dialog)
//    {
      HideMainFrame();
      g_mp_dialog = new MPDialog;
      g_mp_dialog->Show(true);
      g_mp_dialog->SetFocus();
//    g_mp_dialog->Raise();
//    }
//    else
  //    g_mp_dialog->SetFocus();
    }
    else
      wxMessageBox(wxT("You should configure a source port\nso you can use one to launch the game,\naccessing Settings --> Source ports."),
                   wxT("There are no configured source ports."), wxOK|wxICON_INFORMATION);
  }
}
/*
void MainFrame::OnLoadManualJoinDialog(wxCommandEvent& WXUNUSED(event))
{
  if (g_configuration->have_eduke32 || g_configuration->have_jfduke3d || g_configuration->have_iduke3d
      || g_configuration->have_duke3dw32 || g_configuration->have_xduke || g_configuration->have_dosduke
      || g_configuration->have_jfsw || g_configuration->have_swp || g_configuration->have_dossw)
  {
//  if (g_manualjoin_dialog) // Shouldn't happen but for the case it does...
//    g_manualjoin_dialog->SetFocus();
//  else
//  {
//    g_manualjoin_dialog = new ManualJoinDialog;
//    g_manualjoin_dialog->Show(true);
//  }
    HideMainFrame();
    g_manualjoin_dialog = new ManualJoinDialog;
    g_manualjoin_dialog->Show(true);
    g_manualjoin_dialog->SetFocus();
//  g_manualjoin_dialog->Raise();
  }
  else
    wxMessageBox(wxT("You should configure a source port\nso you can use one to launch the game,\naccessing Settings --> Source ports."),
                 wxT("There are no configured source ports."), wxOK|wxICON_INFORMATION);
}
*/
void MainFrame::OnLoadSRCPortsDialog(wxCommandEvent& WXUNUSED(event))
{
//if (!g_srcports_dialog)
//{
  HideMainFrame();
  g_srcports_dialog = new SRCPortsDialog;
  g_srcports_dialog->Show(true);
  g_srcports_dialog->SetFocus();
//g_srcports_dialog->Raise();
//}
//else
//  g_srcports_dialog->SetFocus();
}

void MainFrame::OnLoadNetworkDialog(wxCommandEvent& WXUNUSED(event))
{
//if (!g_network_dialog)
//{
  HideMainFrame();
  g_network_dialog = new NetworkDialog;
  g_network_dialog->Show(true);
  g_network_dialog->SetFocus();
//g_network_dialog->Raise();
//}
//else
//  g_network_dialog->SetFocus();
}

void MainFrame::OnLoadAdvancedDialog(wxCommandEvent& WXUNUSED(event))
{
//if (!g_adv_dialog)
//{
  HideMainFrame();
  g_adv_dialog = new AdvDialog;
  g_adv_dialog->Show(true);
  g_adv_dialog->SetFocus();
//g_adv_dialog->Raise();
//}
//else
//  g_adv_dialog->SetFocus();
}

void MainFrame::OnAbout(wxCommandEvent& WXUNUSED(event))
{
  wxMessageBox((wxString)wxT("DUKE MATCHER ") + DUKEMATCHER_VERSION + wxT(" mod of 0.47a by NY00123. This product includes GeoLite2 data created by MaxMind, available from http://www.maxmind.com."), wxT("About DUKEMATCHER"), wxOK|wxICON_INFORMATION);
 //wxMessageBox((wxString)wxT("DUKE MATCHER ") + DUKEMATCHER_VERSION + wxT(" revision ") + DUKEMATCHER_REVISION + wxT(" mod of 0.47a by NY00123. This product includes GeoLite2 data created by MaxMind, available from http://www.maxmind.com."), wxT("About DUKEMATCHER"), wxOK|wxICON_INFORMATION);
}

void MainFrame::OnCheckForUpdates(wxCommandEvent& WXUNUSED(event))
{
  Disable();
  g_CheckForUpdates();
  Enable();
  Raise();
}

void MainFrame::OnAutoCheckForUpdates(wxCommandEvent& WXUNUSED(event))
{
  g_configuration->check_for_updates_on_startup = !g_configuration->check_for_updates_on_startup;
  m_checkforupdatesonstartupmenuItem->Check(g_configuration->check_for_updates_on_startup);
  g_configuration->Save();
  /****** WORKAROUND: Why do we need to do this? ******/
  SetStatusText(wxEmptyString);
}

void MainFrame::OnVisitWebsite(wxCommandEvent& WXUNUSED(event))
{
  g_OpenURL(WEBSITE_ADDRESS);
}

void MainFrame::OnQuit(wxCommandEvent& WXUNUSED(event))
{
  if (m_client)
  {
    m_client->Destroy();
    m_client = NULL;
  }
  if ((g_client_frame) || (g_host_frame))
  HideMainFrame();
  else
  Close();
}

void MainFrame::OnClose(wxCloseEvent& event)
{
  if (m_client)
  {
    m_client->Destroy();
    m_client = NULL;
  }
  if ((g_client_frame) || (g_host_frame))
  HideMainFrame();
  else
  Destroy();
}

  
void MainFrame::OnServerListAutoCheck(wxCommandEvent& WXUNUSED(event))
{
  g_configuration->lookup_on_startup = m_autolookupcheckBox->GetValue();
  g_configuration->Save();
}

void MainFrame::OnHostSelect(wxListEvent& event)
{
// Thanks to the help of the following Wiki entry:
// http://wiki.wxwidgets.org/WxListCtrl

  size_t l_index = event.GetIndex();

  m_playerlistBox->Clear();
  m_playerlistBox->Append(m_nicknames[l_index]);

  wxListItem l_item;
  l_item.m_itemId = l_index;
  l_item.m_mask = wxLIST_MASK_TEXT;
  l_item.m_col = 0;
  m_serverlistCtrl->GetItem(l_item);
  m_manualjoinnametextCtrl->SetValue(l_item.m_text);

  m_manualjoinhostaddrtextCtrl->SetValue(m_addrs[l_index]);
  m_manualjoinportnumtextCtrl->SetValue(wxString::Format(wxT("%d"), m_portnums[l_index]));
}

void MainFrame::OnHostActivate(wxListEvent& WXUNUSED(event))
{
  Join();
}

void MainFrame::OnJoin(wxCommandEvent& WXUNUSED(event))
{
  Join();
}

void MainFrame::Join()
{
  long l_port_num;
#ifdef ENABLE_UPNP
  UPNPForwardThread* l_thread;
#endif
  wxString l_addr = m_manualjoinhostaddrtextCtrl->GetValue();
  if (l_addr.IsEmpty())
    return;
  if ((m_manualjoinportnumtextCtrl->GetValue()).ToLong(&l_port_num))
  {
    g_client_frame = new ClientRoomFrame;
    Disable();
    Update();
//  g_client_frame->ConnectToServer(l_addr, l_port_num);
    if (g_client_frame->ConnectToServer(l_addr, l_port_num))
    {
#ifdef ENABLE_UPNP
      l_thread = new UPNPForwardThread(g_configuration->game_port_number, false);
      if (l_thread->Create() == wxTHREAD_NO_ERROR )
        l_thread->Run();
      else
        l_thread->Delete();
#endif
      g_client_frame->Show(true);
      g_client_frame->FocusFrame();
//    g_client_frame->SetFocus();
//    g_client_frame->Raise();
      //HideMainFrame();
      Enable();
    }
    else
    {
      Enable();
      wxMessageBox(wxT("Connection establishment has failed."), wxEmptyString, wxOK|wxICON_INFORMATION);
      g_client_frame->Destroy();
      SetFocus();
    }
  }
  else
    wxMessageBox(wxT("The given port number seems to be invalid.\nIs it really a number?"), wxT("Invalid port number"), wxOK|wxICON_INFORMATION);
}
void MainFrame::OnAddHost(wxCommandEvent& WXUNUSED(event))
{
  long l_temp_num;
  g_configuration->manualjoin_names.Add(m_manualjoinnametextCtrl->GetValue());
  g_configuration->manualjoin_addrs.Add(m_manualjoinhostaddrtextCtrl->GetValue());
  if (!((m_manualjoinportnumtextCtrl->GetValue()).ToLong(&l_temp_num)))
    l_temp_num = g_configuration->server_port_number;
  g_configuration->manualjoin_portnums.Add(l_temp_num);
  {
    m_manualjoinchoice->Append(m_manualjoinnametextCtrl->GetValue() +
                               wxT(' ') + m_manualjoinhostaddrtextCtrl->GetValue() +
                               wxString::Format(wxT(":%d"), l_temp_num));
    m_manualjoinchoice->SetSelection(m_manualjoinchoice->GetCount()-1);
  }
  g_configuration->Save();
}

void MainFrame::OnRemHost(wxCommandEvent& WXUNUSED(event))
{
  int l_selection = m_manualjoinchoice->GetSelection();
  if (l_selection >= 0)
  {
    g_configuration->manualjoin_names.RemoveAt(l_selection);
    g_configuration->manualjoin_addrs.RemoveAt(l_selection);
    g_configuration->manualjoin_portnums.RemoveAt(l_selection);
    m_manualjoinchoice->Delete(l_selection);
    m_manualjoinchoice->SetSelection(l_selection);
    g_configuration->Save();
  }
}

void MainFrame::OnManualJoinHostSelect(wxCommandEvent& event)
{
  int l_selection = event.GetSelection();
  m_manualjoinnametextCtrl->SetValue(g_configuration->manualjoin_names[l_selection]);
  m_manualjoinhostaddrtextCtrl->SetValue(g_configuration->manualjoin_addrs[l_selection]);
  m_manualjoinportnumtextCtrl->SetValue(wxString::Format(wxT("%d"),g_configuration->manualjoin_portnums[l_selection]));
}

void MainFrame::OnSocketEvent(wxSocketEvent& event)
{
  size_t l_num_of_items;
  unsigned long l_temp_num;
  wxArrayString l_str_list;
  wxSocketBase* l_sock = event.GetSocket();
//wxListItem l_list_item;
  if (l_sock == m_client)
    switch (event.GetSocketEvent())
    {
      case wxSOCKET_CONNECTION:
        m_client->SetTimeout(1);
        SetStatusText(wxT("Getting list of rooms..."));
        g_SendCStringToSocket(m_client, "1:getroomlist:");
        m_serverlist_timer->Start(2000, true);
        m_wait_for_nicks = false;
        m_long_buffer = wxEmptyString;
        break;
      case wxSOCKET_INPUT:
        m_client->Read(m_short_buffer, MAX_COMM_TEXT_LENGTH);
        m_long_buffer << wxString(m_short_buffer, wxConvUTF8, m_client->LastCount());
        l_num_of_items = g_ConvertStringBufToStringList(m_long_buffer, l_str_list);
        while (l_num_of_items)
        {
          if ((l_num_of_items == 2) && (l_str_list[0] == wxT("error")))
          {
            m_serverlist_timer->Stop();
            m_client->Destroy();
            m_client = NULL;
            if (wxMessageBox(wxT("Error from server list:\n") + l_str_list[1]
                             + wxT("\n\nWould you like to check the rest of the server lists (if any)?"),
                             wxT("Error from master server"), wxYES_NO|wxICON_EXCLAMATION) == wxYES)
              TryNextServer();            
            else
              SetStatusText(wxEmptyString);
          }
          else
            if (m_wait_for_nicks)
            {
              if ((l_num_of_items) && (l_str_list[0] == wxT("nicks")))
              {
                m_curr_room--;
                l_str_list.RemoveAt(0);
                m_nicknames.RemoveAt(m_curr_room);
                m_nicknames.Add(l_str_list);
                m_serverlistCtrl->SetItem(m_curr_room, 5,
                                          wxString::Format(wxT("%d/"), l_str_list.GetCount())
                                        + m_numplayers_limits[m_curr_room]);
//              RefreshListCtrlSize();
                m_curr_room++;
                m_wait_for_nicks = false;
              }
            }
            else
              if ((l_num_of_items >= 13) && (l_num_of_items <= 14)
                  && (l_str_list[0] == wxT("info")))
              {
                m_addrs.Add(l_str_list[1]);
                l_str_list[2].ToULong(&l_temp_num);
                m_portnums.Add(l_temp_num);
                m_nicknames.Add(wxArrayString());
//              l_list_item.SetColumn(0);
//              l_list_item.SetTextColour(*wxGREEN);
//              l_list_item.SetText(l_str_list[5]);
//              m_serverlistCtrl->InsertItem(l_list_item);
                m_serverlistCtrl->InsertItem(m_curr_room, l_str_list[5]);
                if (l_str_list[3] != DUKEMATCHER_STR_NET_VERSION)
                  m_serverlistCtrl->SetItemTextColour(m_curr_room, *wxRED);
                else
                  if (g_configuration->theme_is_dark)
                    m_serverlistCtrl->SetItemTextColour(m_curr_room, *wxGREEN);
                m_serverlistCtrl->SetItem(m_curr_room, 5, wxT("?/") + l_str_list[6]);
                m_numplayers_limits.Add(l_str_list[6]);
                if (l_str_list[7] == wxT("yes"))
                  m_serverlistCtrl->SetItem(m_curr_room, 4, wxT("Yes"));
                else
                  m_serverlistCtrl->SetItem(m_curr_room, 4, wxT("No"));
                if (l_str_list[4] == wxT("dn3d"))
                {
                  m_serverlistCtrl->SetItem(m_curr_room, 1, GAMENAME_DN3D);
                  if (l_str_list[8] == wxT("eduke32"))
                    m_serverlistCtrl->SetItem(m_curr_room, 2, SRCPORTNAME_EDUKE32);
                  else
                    if (l_str_list[8] == wxT("jfduke3d"))
                      m_serverlistCtrl->SetItem(m_curr_room, 2, SRCPORTNAME_JFDUKE3D);
                  else
                    if (l_str_list[8] == wxT("iduke3d"))
                      m_serverlistCtrl->SetItem(m_curr_room, 2, SRCPORTNAME_IDUKE3D);
                  else
                    if (l_str_list[8] == wxT("duke3dw32"))
                      m_serverlistCtrl->SetItem(m_curr_room, 2, SRCPORTNAME_DUKE3DW32);
                  else
                    if (l_str_list[8] == wxT("xduke"))
                      m_serverlistCtrl->SetItem(m_curr_room, 2, SRCPORTNAME_XDUKE);
                  else
                    m_serverlistCtrl->SetItem(m_curr_room, 2, SRCPORTNAME_DOSDUKE);
                  l_str_list[9].ToULong(&l_temp_num);
                  if (l_temp_num > DN3D_MAX_SKILLNUM)
                    l_temp_num = 0;
                  m_serverlistCtrl->SetItem(m_curr_room, 7, (*g_dn3d_skill_list)[l_temp_num]);
                  if (l_str_list[10] == wxT("dmspawn"))
                    m_serverlistCtrl->SetItem(m_curr_room, 6, (*g_dn3d_gametype_list)[DN3DMPGAMETYPE_DMSPAWN-1]);
                  else
                    if (l_str_list[10] == wxT("coop"))
                      m_serverlistCtrl->SetItem(m_curr_room, 6, (*g_dn3d_gametype_list)[DN3DMPGAMETYPE_COOP-1]);
                  else
                    if (l_str_list[10] == wxT("dmnospawn"))
                      m_serverlistCtrl->SetItem(m_curr_room, 6, (*g_dn3d_gametype_list)[DN3DMPGAMETYPE_DMNOSPAWN-1]);
                  else
                    if (l_str_list[10] == wxT("tdmspawn"))
                      m_serverlistCtrl->SetItem(m_curr_room, 6, (*g_dn3d_gametype_list)[DN3DMPGAMETYPE_TDMSPAWN-1]);
                  else
                    m_serverlistCtrl->SetItem(m_curr_room, 6, (*g_dn3d_gametype_list)[DN3DMPGAMETYPE_TDMNOSPAWN-1]);
                  if (l_str_list[11] == wxT("usermap"))
                    m_serverlistCtrl->SetItem(m_curr_room, 3, l_str_list[12]);
                  else
                  {
                    l_str_list[12].ToULong(&l_temp_num);
                    m_serverlistCtrl->SetItem(m_curr_room, 3, (*g_dn3d_level_list)[g_GetDN3DLevelSelectionByNum(l_temp_num)]);
                  }
                  m_serverlistCtrl->SetItem(m_curr_room, 8, l_str_list[13]);
                }
                else
                {
                  m_serverlistCtrl->SetItem(m_curr_room, 1, GAMENAME_SW);
                  if (l_str_list[8] == wxT("jfsw"))
                    m_serverlistCtrl->SetItem(m_curr_room, 2, SRCPORTNAME_JFSW);
                  else
                    if (l_str_list[8] == wxT("swp"))
                      m_serverlistCtrl->SetItem(m_curr_room, 2, SRCPORTNAME_SWP);
                  else
                    m_serverlistCtrl->SetItem(m_curr_room, 2, SRCPORTNAME_DOSSW);
                  l_str_list[9].ToULong(&l_temp_num);
                  if (l_temp_num > SW_MAX_SKILLNUM)
                    l_temp_num = 0;
                  m_serverlistCtrl->SetItem(m_curr_room, 7, (*g_sw_skill_list)[l_temp_num]);
                  if (l_str_list[10] == wxT("usermap"))
                    m_serverlistCtrl->SetItem(m_curr_room, 3, l_str_list[11]);
                  else
                  {
                    l_str_list[11].ToULong(&l_temp_num);
                    m_serverlistCtrl->SetItem(m_curr_room, 3, (*g_sw_level_list)[g_GetSWLevelSelectionByNum(l_temp_num)]);
                  }
                  m_serverlistCtrl->SetItem(m_curr_room, 8, l_str_list[12]);
                }
//              RefreshListCtrlSize();
                m_curr_room++;
                m_wait_for_nicks = true;
                m_serverlist_timer->Start(5000, true);
              }
          l_num_of_items = g_ConvertStringBufToStringList(m_long_buffer, l_str_list);
        }
        break;
      case wxSOCKET_LOST:
        m_serverlist_timer->Stop();
        TryNextServer();
      default: ;
    }
}

void MainFrame::OnTimer(wxTimerEvent& WXUNUSED(event))
{
  if ((m_client != NULL) && (m_servers_to_count)) // A few optimizations, and
    TryNextServer(); // possible race conditions (although not catching all cases).
}

void MainFrame::Connect()
{
  wxIPV4address l_addr;
  m_client = new wxSocketClient;
  if (m_check_default_lists)
  {
    l_addr.Hostname((*g_serverlists_addrs)[m_server_in_curr_list]);
    l_addr.Service((*g_serverlists_portnums)[m_server_in_curr_list]);
    SetStatusText(wxString::Format(wxT("Connecting to master server %d..."),
                                   m_server_in_curr_list+1+g_configuration->extraservers_portnums.GetCount()));
  }
  else
  {
    l_addr.Hostname(g_configuration->extraservers_addrs[m_server_in_curr_list]);
    l_addr.Service(g_configuration->extraservers_portnums[m_server_in_curr_list]);
    SetStatusText(wxString::Format(wxT("Connecting to master server %d..."), m_server_in_curr_list+1));
  }
  m_client->SetTimeout(2);
  m_client->SetEventHandler(*this, ID_GETLISTSOCKET);
  m_client->SetFlags(wxSOCKET_NONE|wxSOCKET_BLOCK);
  m_client->SetNotify(wxSOCKET_CONNECTION_FLAG |
                      wxSOCKET_INPUT_FLAG | wxSOCKET_LOST_FLAG);
  m_client->Notify(true);

  m_client->Connect(l_addr, false);
  m_serverlist_timer->Start(2000, true);
}

void MainFrame::TryNextServer()
{
  m_client->Destroy();
  m_serverlist_timer->Stop();
  if (m_check_default_lists)
  {
    m_server_in_curr_list++;
    if (m_server_in_curr_list < m_servers_to_count)
      Connect();
    else
    {
      SetStatusText(wxEmptyString);
      m_client = NULL;
    }
  }
  else
  {
    m_server_in_curr_list++;
    if (m_server_in_curr_list < m_servers_to_count)
      Connect();
    else
    {
      m_servers_to_count = g_serverlists_portnums->GetCount();
      if (m_servers_to_count)
      {
        m_server_in_curr_list = 0;
        m_check_default_lists = true;
        Connect();
      }
      else
      {
        SetStatusText(wxEmptyString);
        m_client = NULL;
      }
    }
  }
}

void MainFrame::RefreshList()
{
  m_serverlistCtrl->DeleteAllItems();
//RefreshListCtrlSize();
  m_playerlistBox->Clear();
  m_nicknames.Empty();
  m_addrs.Empty();
  m_numplayers_limits.Empty();
  m_portnums.Empty();
  if (m_client)
    m_client->Destroy();
  m_server_in_curr_list = m_curr_room/* = m_last_server*/ = 0;
  m_servers_to_count = g_configuration->extraservers_portnums.GetCount();
  m_check_default_lists = (!m_servers_to_count);
  if (m_check_default_lists)
  {
    m_servers_to_count = g_serverlists_portnums->GetCount();
    if (m_servers_to_count)
      Connect();
    else
    {
      wxLogError(wxT("ERROR: The list of master servers seems to be empty."));
      SetStatusText(wxEmptyString);
    }
  }
  else
    Connect();
}

void MainFrame::OnGetList(wxCommandEvent& WXUNUSED(event))
{
  RefreshList();
}

void MainFrame::ShowMainFrame()
{
#if (defined __WXMAC__) || (defined __WXCOCOA__)
  SetMenuBar(m_Mainmenubar);
#endif
  m_manualjoinportnumtextCtrl->SetValue(wxString::Format(wxT("%d"), g_configuration->server_port_number));
  m_manualjoinchoice->SetSelection(0);
  Show(true);
//SetFocus();
  Raise();
  if (g_configuration->lookup_on_startup)
    RefreshList();
}

void MainFrame::HideMainFrame()
{
#if (defined __WXMAC__) || (defined __WXCOCOA__)
  SetMenuBar(g_empty_menubar);
#endif
  Hide();
  if (m_client)
    m_client->Destroy();
  m_client = NULL;
  m_manualjoinnametextCtrl->Clear();
  m_manualjoinhostaddrtextCtrl->Clear();
  m_serverlistCtrl->DeleteAllItems();
//RefreshListCtrlSize();
  m_playerlistBox->Clear();
  SetStatusText(wxEmptyString);
  m_serverlist_timer->Stop();
}
