/*************************************************************************

Copyright 2008-2009 - NY00123

This file is part of DUKEMATCHER.

DUKEMATCHER is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

DUKEMATCHER is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DUKEMATCHER; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*************************************************************************/

#ifndef _DUKEMATCHER_COLOREDCLASSES_H_
#define _DUKEMATCHER_COLOREDCLASSES_H_

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/frame.h>
#include <wx/dialog.h>
#include <wx/panel.h>
#include <wx/notebook.h>
#include <wx/menu.h>	
#include <wx/statusbr.h>
#include <wx/button.h>
#include <wx/checkbox.h>
#include <wx/radiobut.h>
#include <wx/textctrl.h>
#include <wx/listbox.h>
#include <wx/choice.h>
#include <wx/listctrl.h>
#endif

//void SetStatusBarThemeColour(wxStatusBar* status_bar);

class DUKEMATCHERFrame: public wxFrame
{
public:
  DUKEMATCHERFrame();
  DUKEMATCHERFrame(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_FRAME_STYLE, const wxString& name = wxT("frame"));
//wxStatusBar* CreateStatusBar(int number = 1, long style = wxST_SIZEGRIP | wxFULL_REPAINT_ON_RESIZE, wxWindowID id = 0, const wxString& name = wxT("statusBar"));
//wxStatusBar* OnCreateStatusBar(int number, long style, wxWindowID id, const wxString& name);
  void SetThemeColour();
};

class DUKEMATCHERDialog: public wxDialog
{
public:
  DUKEMATCHERDialog();
  DUKEMATCHERDialog(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE, const wxString& name = wxT("dialogBox"));
  void SetThemeColour();
};

class DUKEMATCHERPanel: public wxPanel
{
public:
  DUKEMATCHERPanel();
  DUKEMATCHERPanel(wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxTAB_TRAVERSAL, const wxString& name = wxT("panel"));
  void SetThemeColour();
};

class DUKEMATCHERNotebook: public wxNotebook
{
public:
  DUKEMATCHERNotebook();
  DUKEMATCHERNotebook(wxWindow* parent, wxWindowID id, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = 0, const wxString& name = wxNotebookNameStr);
  void SetThemeColour();
};
/*
class DUKEMATCHERMenuBar: public wxMenuBar
{
public:
  DUKEMATCHERMenuBar(long style = 0);
  DUKEMATCHERMenuBar(size_t n, wxMenu* menus[], const wxString titles[], long style = 0);
  void SetThemeColour();
};
*/
class DUKEMATCHERButton: public wxButton
{
public:
  DUKEMATCHERButton();
  DUKEMATCHERButton(wxWindow *parent, wxWindowID id,
             const wxString& label = wxEmptyString,
             const wxPoint& pos = wxDefaultPosition,
             const wxSize& size = wxDefaultSize, long style = 0,
             const wxValidator& validator = wxDefaultValidator,
             const wxString& name = wxButtonNameStr);
  void SetThemeColour();
};

class DUKEMATCHERCheckBox: public wxCheckBox
{
public:
  DUKEMATCHERCheckBox();
  DUKEMATCHERCheckBox(wxWindow* parent, wxWindowID id, const wxString& label, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = 0, const wxValidator& val = wxDefaultValidator, const wxString& name = wxT("checkBox"));
  void SetThemeColour();
};

class DUKEMATCHERRadioButton: public wxRadioButton
{
public:
  DUKEMATCHERRadioButton();
  DUKEMATCHERRadioButton(wxWindow* parent, wxWindowID id, const wxString& label, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = 0, const wxValidator& validator = wxDefaultValidator, const wxString& name = wxT("radioButton"));
  void SetThemeColour();
};

class DUKEMATCHERTextCtrl: public wxTextCtrl
{
public:
  DUKEMATCHERTextCtrl();
  DUKEMATCHERTextCtrl(wxWindow* parent, wxWindowID id, const wxString& value = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = 0, const wxValidator& validator = wxDefaultValidator, const wxString& name = wxTextCtrlNameStr);
  void SetThemeColour();
};

class DUKEMATCHERListBox: public wxListBox
{
public:
  DUKEMATCHERListBox();
  DUKEMATCHERListBox(wxWindow* parent, wxWindowID id, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, int n = 0, const wxString choices[] = NULL, long style = 0, const wxValidator& validator = wxDefaultValidator, const wxString& name = wxT("listBox"));
  DUKEMATCHERListBox(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, const wxArrayString& choices, long style = 0, const wxValidator& validator = wxDefaultValidator, const wxString& name = wxT("listBox"));
  void SetThemeColour();
};

class DUKEMATCHERChoice: public wxChoice
{
public:
  DUKEMATCHERChoice();
  DUKEMATCHERChoice(wxWindow* parent, wxWindowID id, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, int n = 0, const wxString choices[] = NULL, long style = 0, const wxValidator& validator = wxDefaultValidator, const wxString& name = wxT("choice"));
  DUKEMATCHERChoice(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, const wxArrayString& choices, long style = 0, const wxValidator& validator = wxDefaultValidator, const wxString& name = wxT("choice"));
  void SetThemeColour();
};

class DUKEMATCHERListCtrl: public wxListCtrl
{
public:
  DUKEMATCHERListCtrl();
  DUKEMATCHERListCtrl(wxWindow* parent, wxWindowID id, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxLC_ICON, const wxValidator& validator = wxDefaultValidator, const wxString& name = wxListCtrlNameStr);
  void SetThemeColour();
};
#endif
