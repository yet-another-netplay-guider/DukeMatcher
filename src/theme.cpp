/*************************************************************************

Copyright 2008-2009 - NY00123

This file is part of DUKEMATCHER.

DUKEMATCHER is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

DUKEMATCHER is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DUKEMATCHER; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*************************************************************************/

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/frame.h>
#include <wx/dialog.h>
#include <wx/panel.h>
#include <wx/notebook.h>
#include <wx/menu.h>
#include <wx/statusbr.h>
#include <wx/button.h>
#include <wx/checkbox.h>
#include <wx/radiobut.h>
#include <wx/textctrl.h>
#include <wx/listbox.h>
#include <wx/choice.h>
#include <wx/listctrl.h>
#endif

#include "theme.h"
#include "dukematcher.h"
/*
void SetDefaultThemeColour(wxWindow* window)
{
  wxVisualAttributes l_attr = window->GetDefaultAttributes();
  window->SetForegroundColour(l_attr.colFg);
  window->SetBackgroundColour(l_attr.colBg);
}

void SetStatusBarThemeColour(wxStatusBar* status_bar)
{
  if (g_configuration->theme_is_dark)
    status_bar->SetBackgroundColour(wxColour(191, 191, 191));
//l_status_bar->SetForegroundColour(wxColour(255, 255, 0));
//else
//  SetDefaultThemeColour(status_bar);
}
*/
DUKEMATCHERFrame::DUKEMATCHERFrame(): wxFrame()
{
  SetThemeColour();
}
DUKEMATCHERFrame::DUKEMATCHERFrame(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style, const wxString& name):
wxFrame(parent, id, title, pos, size, style, name)
{
  SetThemeColour();
}
/*
//wxStatusBar* DUKEMATCHERFrame::OnCreateStatusBar(int number, long style, wxWindowID id, const wxString& name)
wxStatusBar* DUKEMATCHERFrame::CreateStatusBar(int number, long style, wxWindowID id, const wxString& name)
{
  wxStatusBar* l_status_bar = new wxStatusBar(this, id, style, name);
  l_status_bar->SetFieldsCount(number);
//SetStatusBarThemeColour(l_status_bar);
  SetStatusBar(l_status_bar);
  return l_status_bar;
}
*/
void DUKEMATCHERFrame::SetThemeColour()
{
  if (g_configuration->theme_is_dark)
  {
    SetForegroundColour(wxColour(255, 255, 0));
    SetBackgroundColour(wxColour(127, 127, 127));
  }
//else
//  SetDefaultThemeColour(this);
}

DUKEMATCHERDialog::DUKEMATCHERDialog(): wxDialog()
{
  SetThemeColour();

}
DUKEMATCHERDialog::DUKEMATCHERDialog(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style, const wxString& name):
wxDialog(parent, id, title, pos, size, style, name)
{
  SetThemeColour();
}

void DUKEMATCHERDialog::SetThemeColour()
{
  if (g_configuration->theme_is_dark)
  {
    SetForegroundColour(wxColour(255, 255, 0));
    SetBackgroundColour(wxColour(127, 127, 127));
  }
//else
//  SetDefaultThemeColour(this);
}

DUKEMATCHERPanel::DUKEMATCHERPanel(): wxPanel()
{
  SetThemeColour();
}

DUKEMATCHERPanel::DUKEMATCHERPanel(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name):
wxPanel(parent, id, pos, size, style, name)
{
  SetThemeColour();
}

void DUKEMATCHERPanel::SetThemeColour()
{
  if (g_configuration->theme_is_dark)
  {
    SetForegroundColour(wxColour(255, 255, 0));
    SetBackgroundColour(wxColour(127, 127, 127));
  }
//else
//  SetDefaultThemeColour(this);
}

DUKEMATCHERNotebook::DUKEMATCHERNotebook(): wxNotebook()
{
  SetThemeColour();
}

DUKEMATCHERNotebook::DUKEMATCHERNotebook(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name):
wxNotebook(parent, id, pos, size, style|wxNB_NOPAGETHEME, name)
{
  SetThemeColour();
}

void DUKEMATCHERNotebook::SetThemeColour()
{
  if (g_configuration->theme_is_dark)
  {
    SetForegroundColour(wxColour(255, 255, 0));
    SetBackgroundColour(wxColour(127, 127, 127));
  }
//else
//  SetDefaultThemeColour(this);
}
/*
DUKEMATCHERMenuBar::DUKEMATCHERMenuBar(long style): wxMenuBar(style)
{
  SetThemeColour();
}

DUKEMATCHERMenuBar::DUKEMATCHERMenuBar(size_t n, wxMenu* menus[], const wxString titles[], long style): wxMenuBar(n, menus, titles, style)
{
  SetThemeColour();
}

void DUKEMATCHERMenuBar::SetThemeColour()
{
  if (g_configuration->theme_is_dark)
    SetBackgroundColour(wxColour(191, 191, 191));
//else
//  SetDefaultThemeColour(this);
}
*/
DUKEMATCHERButton::DUKEMATCHERButton(): wxButton()
{
  SetThemeColour();
}

DUKEMATCHERButton::DUKEMATCHERButton(wxWindow *parent, wxWindowID id,
                       const wxString& label,
                       const wxPoint& pos,
                       const wxSize& size, long style,
                       const wxValidator& validator,
                       const wxString& name):
wxButton(parent, id, label, pos, size, style, validator, name)
{
  SetThemeColour();
}

void DUKEMATCHERButton::SetThemeColour()
{
  if (g_configuration->theme_is_dark)
  {
    SetForegroundColour(wxColour(0, 0, 127));
    SetBackgroundColour(wxColour(191, 191, 191));
  }
//else
//  SetDefaultThemeColour(this);
}

DUKEMATCHERCheckBox::DUKEMATCHERCheckBox(): wxCheckBox()
{
  SetThemeColour();
}

DUKEMATCHERCheckBox::DUKEMATCHERCheckBox(wxWindow* parent, wxWindowID id, const wxString& label, const wxPoint& pos, const wxSize& size, long style, const wxValidator& val, const wxString& name):
wxCheckBox(parent, id, label, pos, size, style, val, name)
{
  SetThemeColour();
}

void DUKEMATCHERCheckBox::SetThemeColour()
{
  if (g_configuration->theme_is_dark)
  {
    SetForegroundColour(wxColour(255, 255, 0));
    SetBackgroundColour(wxColour(127, 127, 127));
  }
//else
//  SetDefaultThemeColour(this);
}

DUKEMATCHERRadioButton::DUKEMATCHERRadioButton(): wxRadioButton()
{
  SetThemeColour();
}

DUKEMATCHERRadioButton::DUKEMATCHERRadioButton(wxWindow* parent, wxWindowID id, const wxString& label, const wxPoint& pos, const wxSize& size, long style, const wxValidator& validator, const wxString& name):
wxRadioButton(parent, id, label, pos, size, style, validator, name)
{
  SetThemeColour();
}

void DUKEMATCHERRadioButton::SetThemeColour()
{
  if (g_configuration->theme_is_dark)
  {
    SetForegroundColour(wxColour(255, 255, 0));
    SetBackgroundColour(wxColour(127, 127, 127));
  }
//else
//  SetDefaultThemeColour(this);
}

DUKEMATCHERTextCtrl::DUKEMATCHERTextCtrl(): wxTextCtrl()
{
  SetThemeColour();
}

DUKEMATCHERTextCtrl::DUKEMATCHERTextCtrl(wxWindow* parent, wxWindowID id, const wxString& value, const wxPoint& pos, const wxSize& size, long style, const wxValidator& validator, const wxString& name):
wxTextCtrl(parent, id, value, pos, size, style, validator, name)
{
  SetThemeColour();
}

void DUKEMATCHERTextCtrl::SetThemeColour()
{
  if (g_configuration->theme_is_dark)
  {
    SetForegroundColour(*wxGREEN);
    SetBackgroundColour(*wxBLACK);
  }
//else
//  SetDefaultThemeColour(this);
}

DUKEMATCHERListBox::DUKEMATCHERListBox(): wxListBox()
{
  SetThemeColour();
}

DUKEMATCHERListBox::DUKEMATCHERListBox(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, int n, const wxString choices[], long style, const wxValidator& validator, const wxString& name):
wxListBox(parent, id, pos, size, n, choices, style, validator, name)
{
  SetThemeColour();
}

DUKEMATCHERListBox::DUKEMATCHERListBox(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, const wxArrayString& choices, long style, const wxValidator& validator, const wxString& name):
wxListBox(parent, id, pos, size, choices, style, validator, name)
{
  SetThemeColour();
}

void DUKEMATCHERListBox::SetThemeColour()
{
  if (g_configuration->theme_is_dark)
  {
    SetForegroundColour(*wxGREEN);
    SetBackgroundColour(*wxBLACK);
  }
//else
//  SetDefaultThemeColour(this);
}

DUKEMATCHERChoice::DUKEMATCHERChoice(): wxChoice()
{
  SetThemeColour();
}

DUKEMATCHERChoice::DUKEMATCHERChoice(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, int n, const wxString choices[], long style, const wxValidator& validator, const wxString& name):
wxChoice(parent, id, pos, size, n, choices, style, validator, name)
{
  SetThemeColour();
}

DUKEMATCHERChoice::DUKEMATCHERChoice(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, const wxArrayString& choices, long style, const wxValidator& validator, const wxString& name):
wxChoice(parent, id, pos, size, choices, style, validator, name)
{
  SetThemeColour();
}

void DUKEMATCHERChoice::SetThemeColour()
{
  if (g_configuration->theme_is_dark)
  {
    SetForegroundColour(wxColour(0, 0, 127));
    SetBackgroundColour(wxColour(191, 191, 191));
  }
//else
//  SetDefaultThemeColour(this);
}

DUKEMATCHERListCtrl::DUKEMATCHERListCtrl(): wxListCtrl()
{
  SetThemeColour();
}

DUKEMATCHERListCtrl::DUKEMATCHERListCtrl(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxValidator& validator, const wxString& name):
wxListCtrl(parent, id, pos, size, style, validator, name)
{
  SetThemeColour();
}

void DUKEMATCHERListCtrl::SetThemeColour()
{
  if (g_configuration->theme_is_dark)
  {
    SetForegroundColour(wxColour(127, 0, 0));
    SetBackgroundColour(*wxBLACK);
  }
//else
//  SetDefaultThemeColour(this);
}
