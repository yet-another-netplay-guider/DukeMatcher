/*************************************************************************

Copyright 2008-2009 - NY00123

This file is part of DUKEMATCHER.

DUKEMATCHER is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

DUKEMATCHER is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DUKEMATCHER; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*************************************************************************/

#ifndef _DUKEMATCHER_MANUALJOINDIALOG_H_
#define _DUKEMATCHER_MANUALJOINDIALOG_H_

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/wx.h>
#include <wx/listctrl.h>
#include <wx/gbsizer.h>
#endif

#include "theme.h"

#define ID_MANUALJOINENTER 1000
#define ID_MANUALJOINADD 1001
//#define ID_MANUALJOINNAMELIST 1002
//#define ID_MANUALJOINHOSTADDRLIST 1003
//#define ID_MANUALJOINPORTNUMLIST 1004
//#define ID_MANUALJOINREM 1005
#define ID_MANUALJOINLIST 1002
#define ID_MANUALJOINREM 1003

class ManualJoinDialog : public DUKEMATCHERDialog 
{
public:
  ManualJoinDialog();
//~ManualJoinDialog();
//void RefreshListCtrlSize();
  void OnOk(wxCommandEvent& event);
  void OnCancel(wxCommandEvent& event);
  void OnAddHost(wxCommandEvent& event);
  void OnRemHost(wxCommandEvent& event);
//void OnHostSelect(wxCommandEvent& event);
  void OnHostSelect(wxListEvent& event);
  void OnHostActivate(wxListEvent& event);
private:
  void Connect();
  DUKEMATCHERPanel* m_ManualJoinpanel;
  wxStaticText* m_manualjoinnamestaticText;
  wxStaticText* m_manualjoinhostaddrstaticText;
  wxStaticText* m_manualjoinportnumstaticText;
  DUKEMATCHERTextCtrl* m_manualjoinnametextCtrl;
  DUKEMATCHERTextCtrl* m_manualjoinhostaddrtextCtrl;
  wxStaticText* m_manualjoinseparatorstaticText;
  DUKEMATCHERTextCtrl* m_manualjoinportnumtextCtrl;
  DUKEMATCHERButton* m_manualjoinaddbutton;
  DUKEMATCHERListCtrl* m_manualjoinlistCtrl;
  DUKEMATCHERButton* m_manualjoinrembutton;
  wxStdDialogButtonSizer* m_ManualJoinsdbSizer;
  DUKEMATCHERButton* m_ManualJoinsdbSizerOK;
  DUKEMATCHERButton* m_ManualJoinsdbSizerCancel;

  DECLARE_EVENT_TABLE()
};

#endif

