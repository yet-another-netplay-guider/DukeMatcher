/*************************************************************************

Copyright 2008-2009 - NY00123

This file is part of DUKEMATCHER.

DUKEMATCHER is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

DUKEMATCHER is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DUKEMATCHER; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*************************************************************************/

#ifndef _DUKEMATCHER_MAPSELECTIONDIALOG_H_
#define _DUKEMATCHER_MAPSELECTIONDIALOG_H_

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "config.h"
#include "theme.h"

#define ID_ADDNEWMAP 1000
#define ID_MAPSELECTIONCLOSE (wxID_LOWEST-2)

class MapSelectionDialog : public DUKEMATCHERDialog 
{
public:
  MapSelectionDialog( wxWindow* parent, GameType game);
  void ShowDialog();
  void RefreshMapsList();
  bool GetMapSelection(wxString& map_name);
  void OnAddNewMap(wxCommandEvent& event);
  void OnOKOrCancel(wxCommandEvent& event);
//~MapSelectionDialog();

  bool m_response;
  wxString m_response_filename;
  GameType m_game;

private:
  DUKEMATCHERPanel* m_mapselectionpanel;
  DUKEMATCHERListBox* m_mapselectionlistBox;
  DUKEMATCHERButton* m_mapselectionaddbutton;
  wxStdDialogButtonSizer* m_mapselectionsdbSizer;
  DUKEMATCHERButton* m_mapselectionsdbSizerOK;
  DUKEMATCHERButton* m_mapselectionsdbSizerCancel;

  wxWindow* m_parent;

  DECLARE_EVENT_TABLE()
};

#endif
