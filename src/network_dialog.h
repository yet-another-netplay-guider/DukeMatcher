/*************************************************************************

Copyright 2008-2009 - NY00123

This file is part of DUKEMATCHER.

DUKEMATCHER is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

DUKEMATCHER is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DUKEMATCHER; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*************************************************************************/

#ifndef _DUKEMATCHER_NETWORKDIALOG_H_
#define _DUKEMATCHER_NETWORKDIALOG_H_

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/wx.h>
#include <wx/notebook.h>
//#include <wx/listbook.h>
#endif

#include "theme.h"

#define ID_GAMENICKNAMETEXT 1000
#define ID_SNDJOINCHECKBOX 1001
#define ID_SNDJOINTEST 1002
#define ID_SNDJOINLOCATE 1003
#define ID_SNDLEAVECHECKBOX 1004
#define ID_SNDLEAVETEST 1005
#define ID_SNDLEAVELOCATE 1006
#define ID_SNDSENDCHECKBOX 1007
#define ID_SNDSENDTEST 1008
#define ID_SNDSENDLOCATE 1009
#define ID_SNDRECEIVECHECKBOX 1010
#define ID_SNDRECEIVETEST 1011
#define ID_SNDRECEIVELOCATE 1012
#define ID_SNDERRORCHECKBOX 1013
#define ID_SNDERRORTEST 1014
#define ID_SNDERRORLOCATE 1015
#define ID_SNDRESTORE 1016

class NetworkDialog : public DUKEMATCHERDialog
{
public:
  NetworkDialog();
  ~NetworkDialog();
  void OnLocateSndFile(wxCommandEvent& event);
  void OnTestSound(wxCommandEvent& event);
  void OnSndRestore(wxCommandEvent& event);
  void OnCheckBoxClick(wxCommandEvent& event);
  void OnUpdateGameNickName(wxCommandEvent& event);

//void OnApply(wxCommandEvent& event);
  void OnCancel(wxCommandEvent& event);
  void OnOk(wxCommandEvent& event);
  void ApplySettings();

private:
  DUKEMATCHERNotebook* m_networknotebook;
  DUKEMATCHERPanel* m_networkprofilepanel;
  wxStaticText* m_nicknamestaticText;
  DUKEMATCHERTextCtrl* m_nicknametextCtrl;
  wxStaticText* m_gamenicknamestaticText;
  wxStaticText* m_nicknamenotestaticText;
  DUKEMATCHERTextCtrl* m_gamenicknametextCtrl;
  DUKEMATCHERPanel* m_networksndnotipanel;
  DUKEMATCHERCheckBox* m_soundjoincheckBox;
  DUKEMATCHERButton* m_soundjointestbutton;
  DUKEMATCHERButton* m_soundjoinlocatebutton;
  DUKEMATCHERTextCtrl* m_soundjointextCtrl;
  DUKEMATCHERCheckBox* m_soundleavecheckBox;
  DUKEMATCHERButton* m_soundleavetestbutton;
  DUKEMATCHERButton* m_soundleavelocatebutton;
  DUKEMATCHERTextCtrl* m_soundleavetextCtrl;
  DUKEMATCHERCheckBox* m_soundsendcheckBox;
  DUKEMATCHERButton* m_soundsendtestbutton;
  DUKEMATCHERButton* m_soundsendlocatebutton;
  DUKEMATCHERTextCtrl* m_soundsendtextCtrl;
  DUKEMATCHERCheckBox* m_soundreceivecheckBox;
  DUKEMATCHERButton* m_soundreceivetestbutton;
  DUKEMATCHERButton* m_soundreceivelocatebutton;
  DUKEMATCHERTextCtrl* m_soundreceivetextCtrl;
  DUKEMATCHERCheckBox* m_sounderrorcheckBox;
  DUKEMATCHERButton* m_sounderrortestbutton;
  DUKEMATCHERButton* m_sounderrorlocatebutton;
  DUKEMATCHERTextCtrl* m_sounderrortextCtrl;
  DUKEMATCHERCheckBox* m_soundmutewhileingamecheckBox;
  DUKEMATCHERButton* m_soundrestorebutton;
  DUKEMATCHERPanel* m_networknetpanel;
  wxStaticText* m_gameportstaticText;
  DUKEMATCHERTextCtrl* m_gameporttextCtrl;
  wxStaticText* m_serverportstaticText;
  DUKEMATCHERTextCtrl* m_serverporttextCtrl;
  wxStaticText* m_netnotesstaticText;
  wxStdDialogButtonSizer* m_networksdbSizer;
  DUKEMATCHERButton* m_networksdbSizerOK;
  DUKEMATCHERButton* m_networksdbSizerCancel;
  DUKEMATCHERPanel* m_chatpanel;
DUKEMATCHERCheckBox* m_chattimestampcheckBox;
  DECLARE_EVENT_TABLE()
};

#endif
