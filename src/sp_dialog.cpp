/*************************************************************************

Copyright 2008-2009 - NY00123

This file is part of DUKEMATCHER.

DUKEMATCHER is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

DUKEMATCHER is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DUKEMATCHER; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*************************************************************************/

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/wx.h>
#include <wx/filename.h>
#include <wx/textfile.h>
#include <wx/gbsizer.h>
#endif

#include "sp_dialog.h"
#include "config.h"
#include "mp_common.h"
#include "dukematcher.h"
#include "theme.h"
#include "dukematcher.xpm"

BEGIN_EVENT_TABLE(SPDialog, DUKEMATCHERDialog)

EVT_BUTTON(wxID_OK, SPDialog::OnOk)
//EVT_BUTTON(wxID_APPLY, SPDialog::OnApply)
EVT_BUTTON(wxID_CANCEL, SPDialog::OnCancel)

EVT_BUTTON(ID_SPUSEMODFILE, SPDialog::OnAddModFile)
EVT_BUTTON(ID_SPREMMODFILE, SPDialog::OnRemModFile)

EVT_CHOICE(ID_SPSELECTGAME, SPDialog::OnGameSelect)
EVT_CHOICE(ID_SPSELECTSRCPORT, SPDialog::OnSrcPortSelect)

EVT_CHECKBOX(ID_SPHAVEBOTS, SPDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_SPPLAYDEMO, SPDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_SPRECORDDEMO, SPDialog::OnCheckBoxClick)

EVT_RADIOBUTTON(ID_SPUSEEPIMAP, SPDialog::OnRadioBtnClick)
EVT_RADIOBUTTON(ID_SPUSEUSERMAP, SPDialog::OnRadioBtnClick)

END_EVENT_TABLE()

SPDialog::SPDialog() : DUKEMATCHERDialog(NULL, wxID_ANY, wxT("Launch a Single Player game"), wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE|wxDIALOG_NO_PARENT, wxEmptyString)
{
	wxBoxSizer* SPDialogbSizer = new wxBoxSizer( wxVERTICAL );
	
	m_sppanel = new DUKEMATCHERPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* sppanelbSizer = new wxBoxSizer( wxVERTICAL );
	
	wxGridBagSizer* sppanelgbSizer = new wxGridBagSizer( 0, 0 );
	sppanelgbSizer->SetFlexibleDirection( wxBOTH );
	sppanelgbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	m_spgamestaticText = new wxStaticText( m_sppanel, wxID_ANY, wxT("Game:"), wxDefaultPosition, wxDefaultSize, 0 );
	sppanelgbSizer->Add( m_spgamestaticText, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_spgamechoice = new DUKEMATCHERChoice( m_sppanel, ID_SPSELECTGAME, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
	sppanelgbSizer->Add( m_spgamechoice, wxGBPosition( 0, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );
	
	m_spsourceportstaticText = new wxStaticText( m_sppanel, wxID_ANY, wxT("Source port:"), wxDefaultPosition, wxDefaultSize, 0 );
	sppanelgbSizer->Add( m_spsourceportstaticText, wxGBPosition( 1, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_spsourceportchoice = new DUKEMATCHERChoice( m_sppanel, ID_SPSELECTSRCPORT, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
	sppanelgbSizer->Add( m_spsourceportchoice, wxGBPosition( 1, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );
	
	m_spbotscheckBox = new DUKEMATCHERCheckBox( m_sppanel, ID_SPHAVEBOTS, wxT("Have bots (number):"), wxDefaultPosition, wxDefaultSize, 0 );
	
	sppanelgbSizer->Add( m_spbotscheckBox, wxGBPosition( 2, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_spbotschoice = new DUKEMATCHERChoice( m_sppanel, wxID_ANY, wxDefaultPosition, wxDefaultSize );
	sppanelgbSizer->Add( m_spbotschoice, wxGBPosition( 2, 1 ), wxGBSpan( 1, 1 ), wxALL, 5 );
		
	m_spskillstaticText = new wxStaticText( m_sppanel, wxID_ANY, wxT("Skill:"), wxDefaultPosition, wxDefaultSize, 0 );
	sppanelgbSizer->Add( m_spskillstaticText, wxGBPosition( 5, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_spskillchoice = new DUKEMATCHERChoice( m_sppanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
	sppanelgbSizer->Add( m_spskillchoice, wxGBPosition( 5, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );
	
	m_spplaydemocheckBox = new DUKEMATCHERCheckBox( m_sppanel, ID_SPPLAYDEMO, wxT("Play demo"), wxDefaultPosition, wxDefaultSize, 0 );
	
	sppanelgbSizer->Add( m_spplaydemocheckBox, wxGBPosition( 3, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_spplaydemochoice = new DUKEMATCHERChoice( m_sppanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
	sppanelgbSizer->Add( m_spplaydemochoice, wxGBPosition( 3, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_sprecorddemocheckBox = new DUKEMATCHERCheckBox( m_sppanel, ID_SPRECORDDEMO, wxT("Record demo"), wxDefaultPosition, wxDefaultSize, 0 );
	
	sppanelgbSizer->Add( m_sprecorddemocheckBox, wxGBPosition( 4, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_sprecorddemotextCtrl = new DUKEMATCHERTextCtrl( m_sppanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
	sppanelgbSizer->Add( m_sprecorddemotextCtrl, wxGBPosition( 4, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_spepimapradioBtn = new DUKEMATCHERRadioButton( m_sppanel, ID_SPUSEEPIMAP, wxT("Episode Map:"), wxDefaultPosition, wxDefaultSize, 0 );
	sppanelgbSizer->Add( m_spepimapradioBtn, wxGBPosition( 6, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_spepimapchoice = new DUKEMATCHERChoice( m_sppanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
	sppanelgbSizer->Add( m_spepimapchoice, wxGBPosition( 6, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );
	
	m_spusermapradioBtn = new DUKEMATCHERRadioButton( m_sppanel, ID_SPUSEUSERMAP, wxT("User Map:"), wxDefaultPosition, wxDefaultSize, 0 );
	sppanelgbSizer->Add( m_spusermapradioBtn, wxGBPosition( 7, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_spusermapchoice = new DUKEMATCHERChoice( m_sppanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)), 0, NULL, wxCB_SORT);
	sppanelgbSizer->Add( m_spusermapchoice, wxGBPosition( 7, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_spmodstaticText = new wxStaticText( m_sppanel, wxID_ANY, wxT("TC/MOD files:"), wxDefaultPosition, wxDefaultSize, 0 );
	sppanelgbSizer->Add( m_spmodstaticText, wxGBPosition( 8, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_spavailmodfileslistBox = new DUKEMATCHERListBox( m_sppanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(64, 80)), 0, NULL, wxLB_EXTENDED ); 
	sppanelgbSizer->Add( m_spavailmodfileslistBox, wxGBPosition( 8, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_spusedmodfileslistBox = new DUKEMATCHERListBox( m_sppanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(64, 80)), 0, NULL, wxLB_EXTENDED ); 
	sppanelgbSizer->Add( m_spusedmodfileslistBox, wxGBPosition( 8, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );
	
	m_spusemodfilebutton = new DUKEMATCHERButton( m_sppanel, ID_SPUSEMODFILE, wxT("Use"), wxDefaultPosition, wxDefaultSize, 0 );
	sppanelgbSizer->Add( m_spusemodfilebutton, wxGBPosition( 9, 1 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL|wxALIGN_CENTER_HORIZONTAL, 5 );
	
	m_spremmodfilebutton = new DUKEMATCHERButton( m_sppanel, ID_SPREMMODFILE, wxT("Remove"), wxDefaultPosition, wxDefaultSize, 0 );
	sppanelgbSizer->Add( m_spremmodfilebutton, wxGBPosition( 9, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_spextraargsstaticText = new wxStaticText( m_sppanel, wxID_ANY, wxT("Extra arguments at the end:"), wxDefaultPosition, wxDefaultSize, 0 );
	sppanelgbSizer->Add( m_spextraargsstaticText, wxGBPosition( 10, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_spextraargstextCtrl = new DUKEMATCHERTextCtrl( m_sppanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
	sppanelgbSizer->Add( m_spextraargstextCtrl, wxGBPosition( 10, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
	
	sppanelbSizer->Add( sppanelgbSizer, 1, wxEXPAND, 0 );
	
	m_spsdbSizer = new wxStdDialogButtonSizer();
	m_spsdbSizerOK = new DUKEMATCHERButton( m_sppanel, wxID_OK );
	m_spsdbSizer->AddButton( m_spsdbSizerOK );
	m_spsdbSizerCancel = new DUKEMATCHERButton( m_sppanel, wxID_CANCEL );
	m_spsdbSizer->AddButton( m_spsdbSizerCancel );
	m_spsdbSizer->Realize();
	sppanelbSizer->Add( m_spsdbSizer, 0, wxALIGN_CENTER_HORIZONTAL, 5 );
	
	m_sppanel->SetSizer( sppanelbSizer );
	m_sppanel->Layout();
	sppanelbSizer->Fit( m_sppanel );
	SPDialogbSizer->Add( m_sppanel, 1, wxEXPAND | wxALL, 0 );
	
	SetSizer( SPDialogbSizer );
	Layout();
	SPDialogbSizer->Fit( this );

  Centre();
  SetIcon(wxIcon(dukematcher_xpm));

  for (size_t l_loop_var = 1; l_loop_var < MAX_NUM_PLAYERS; l_loop_var++)
    m_spbotschoice->Append(wxString::Format(wxT("%d"), l_loop_var));
  m_spbotschoice->SetSelection(0);

  if (g_configuration->have_eduke32 || g_configuration->have_jfduke3d || g_configuration->have_iduke3d
      || g_configuration->have_duke3dw32 || g_configuration->have_xduke || g_configuration->have_dosduke)
    m_spgamechoice->Append(GAMENAME_DN3D);
  if (g_configuration->have_jfsw || g_configuration->have_swp || g_configuration->have_dossw)
    m_spgamechoice->Append(GAMENAME_SW);
  if (((g_configuration->gamelaunch_source_port == SOURCEPORT_EDUKE32) && g_configuration->have_eduke32) ||
      ((g_configuration->gamelaunch_source_port == SOURCEPORT_JFDUKE3D) && g_configuration->have_jfduke3d) ||
      ((g_configuration->gamelaunch_source_port == SOURCEPORT_IDUKE3D) && g_configuration->have_iduke3d) ||
      ((g_configuration->gamelaunch_source_port == SOURCEPORT_DUKE3DW32) && g_configuration->have_duke3dw32) ||
      ((g_configuration->gamelaunch_source_port == SOURCEPORT_XDUKE) && g_configuration->have_xduke) ||
      ((g_configuration->gamelaunch_source_port == SOURCEPORT_DOSDUKE) && g_configuration->have_dosduke && g_configuration->have_dosbox))
  {
    m_spgamechoice->SetStringSelection(GAMENAME_DN3D);
    if (g_configuration->have_eduke32)
      m_spsourceportchoice->Append(SRCPORTNAME_EDUKE32);
    if (g_configuration->have_jfduke3d)
      m_spsourceportchoice->Append(SRCPORTNAME_JFDUKE3D);
    if (g_configuration->have_iduke3d)
      m_spsourceportchoice->Append(SRCPORTNAME_IDUKE3D);
    if (g_configuration->have_duke3dw32)
      m_spsourceportchoice->Append(SRCPORTNAME_DUKE3DW32);
    if (g_configuration->have_xduke)
      m_spsourceportchoice->Append(SRCPORTNAME_XDUKE);
    if (g_configuration->have_dosduke && g_configuration->have_dosbox)
      m_spsourceportchoice->Append(SRCPORTNAME_DOSDUKE);
    switch (g_configuration->gamelaunch_source_port)
    {
      case SOURCEPORT_EDUKE32: m_spsourceportchoice->SetStringSelection(SRCPORTNAME_EDUKE32); break;
      case SOURCEPORT_JFDUKE3D: m_spsourceportchoice->SetStringSelection(SRCPORTNAME_JFDUKE3D); break;
      case SOURCEPORT_IDUKE3D: m_spsourceportchoice->SetStringSelection(SRCPORTNAME_IDUKE3D); break;
      case SOURCEPORT_DUKE3DW32: m_spsourceportchoice->SetStringSelection(SRCPORTNAME_DUKE3DW32); break;
      case SOURCEPORT_XDUKE: m_spsourceportchoice->SetStringSelection(SRCPORTNAME_XDUKE); break;
      default: /*SOURCEPORT_DOSDUKE*/ m_spsourceportchoice->SetStringSelection(SRCPORTNAME_DOSDUKE);
    }
  }
  else
    if (((g_configuration->gamelaunch_source_port == SOURCEPORT_JFSW) && g_configuration->have_jfsw) ||
        ((g_configuration->gamelaunch_source_port == SOURCEPORT_SWP) && g_configuration->have_swp) ||
        ((g_configuration->gamelaunch_source_port == SOURCEPORT_DOSSW) && g_configuration->have_dossw && g_configuration->have_dosbox))
    {
      m_spgamechoice->SetStringSelection(GAMENAME_SW);
      if (g_configuration->have_jfsw)
        m_spsourceportchoice->Append(SRCPORTNAME_JFSW);
      if (g_configuration->have_swp)
        m_spsourceportchoice->Append(SRCPORTNAME_SWP);
      if (g_configuration->have_dossw && g_configuration->have_dosbox)
        m_spsourceportchoice->Append(SRCPORTNAME_DOSSW);
      switch (g_configuration->gamelaunch_source_port)
      {
        case SOURCEPORT_JFSW: m_spsourceportchoice->SetStringSelection(SRCPORTNAME_JFSW); break;
        case SOURCEPORT_SWP: m_spsourceportchoice->SetStringSelection(SRCPORTNAME_SWP); break;
        default: /*SOURCEPORT_DOSSW*/ m_spsourceportchoice->SetStringSelection(SRCPORTNAME_DOSSW);
      }
    }
    else
    {
      if (g_configuration->have_eduke32)
        m_spsourceportchoice->Append(SRCPORTNAME_EDUKE32);
      if (g_configuration->have_jfduke3d)
        m_spsourceportchoice->Append(SRCPORTNAME_JFDUKE3D);
      if (g_configuration->have_iduke3d)
        m_spsourceportchoice->Append(SRCPORTNAME_IDUKE3D);
      if (g_configuration->have_duke3dw32)
        m_spsourceportchoice->Append(SRCPORTNAME_DUKE3DW32);
      if (g_configuration->have_xduke)
        m_spsourceportchoice->Append(SRCPORTNAME_XDUKE);
      if (g_configuration->have_dosduke && g_configuration->have_dosbox)
        m_spsourceportchoice->Append(SRCPORTNAME_DOSDUKE);
      if (m_spsourceportchoice->GetCount())
        m_spgamechoice->SetStringSelection(GAMENAME_DN3D);
      else
      {
        m_spgamechoice->SetStringSelection(GAMENAME_SW);
        if (g_configuration->have_jfsw)
          m_spsourceportchoice->Append(SRCPORTNAME_JFSW);
        if (g_configuration->have_swp)
          m_spsourceportchoice->Append(SRCPORTNAME_SWP);
        if (g_configuration->have_dossw && g_configuration->have_dosbox)
          m_spsourceportchoice->Append(SRCPORTNAME_DOSSW);
      }
      m_spsourceportchoice->SetSelection(0);
    }
  m_spextraargstextCtrl->SetValue(g_configuration->gamelaunch_args_in_sp);
  PartialDialogControlsReset();
}

SPDialog::~SPDialog()
{
  g_main_frame->ShowMainFrame();
//g_sp_dialog = NULL;
}

void SPDialog::PartialDialogControlsReset()
{
  wxArrayString l_file_list;
  int l_loop_var, l_num_of_files, l_temp_index;
//m_spdemobutton->Enable(m_spplaydemocheckBox->GetValue());
  if (m_spgamechoice->GetStringSelection() == GAMENAME_DN3D)
  {
    m_spbotscheckBox->Enable();
    m_spbotschoice->Enable(m_spbotscheckBox->GetValue());
//  m_spskillchoice->Clear();
    m_spskillchoice->Append(*g_dn3d_skill_list);
    m_spskillchoice->SetSelection(g_configuration->gamelaunch_dn3d_skill);
//  m_spepimapchoice->Clear();
    m_spepimapchoice->Append(*g_dn3d_level_list);
    m_spepimapchoice->SetSelection(g_GetDN3DLevelSelectionByNum(g_configuration->gamelaunch_dn3dlvl));
    g_AddAllDN3DMapFiles(&l_file_list);
//  m_spusermapchoice->Clear();
    m_spusermapchoice->Append(l_file_list);
    m_spusermapchoice->SetStringSelection(g_configuration->gamelaunch_dn3dusermap);
    l_file_list.Empty();
    g_AddAllDN3DModFiles(&l_file_list);
//  m_spavailmodfileslistBox->Clear();
    m_spavailmodfileslistBox->Append(l_file_list);
//  m_spusedmodfileslistBox->Clear();
    m_spusedmodfileslistBox->Append(g_configuration->gamelaunch_dn3dmodfiles);
  }
  else
  {
    m_spbotscheckBox->Disable();
    m_spbotschoice->Disable();
//  m_spskillchoice->Clear();
    m_spskillchoice->Append(*g_sw_skill_list);
    m_spskillchoice->SetSelection(g_configuration->gamelaunch_sw_skill);
//  m_spepimapchoice->Clear();
    m_spepimapchoice->Append(*g_sw_level_list);
    m_spepimapchoice->SetSelection(g_GetSWLevelSelectionByNum(g_configuration->gamelaunch_swlvl));
    g_AddAllSWMapFiles(&l_file_list);
//  m_spusermapchoice->Clear();
    m_spusermapchoice->Append(l_file_list);
    m_spusermapchoice->SetStringSelection(g_configuration->gamelaunch_swusermap);
    l_file_list.Empty();
    g_AddAllSWModFiles(&l_file_list);
//  m_spavailmodfileslistBox->Clear();
    m_spavailmodfileslistBox->Append(l_file_list);
//  m_spusedmodfileslistBox->Clear();
    m_spusedmodfileslistBox->Append(g_configuration->gamelaunch_swmodfiles);
  }
// Do selected MOD files exist? No? Maybe partially?
  l_num_of_files = m_spusedmodfileslistBox->GetCount();
  l_loop_var = 0;
  while (l_loop_var < l_num_of_files)
  {
    l_temp_index = m_spavailmodfileslistBox->FindString(m_spusedmodfileslistBox->GetString(l_loop_var));
    if (l_temp_index == wxNOT_FOUND)
    {
      m_spusedmodfileslistBox->Delete(l_loop_var);
      // Number of items has decreased by 1, and now l_loop_var points to the
      // following item if there is one!
      l_num_of_files--;
    }
    else
    {
      m_spavailmodfileslistBox->Delete(l_temp_index);
      l_loop_var++;
    }
  }
  if (m_spusermapchoice->IsEmpty())
  {
    m_spepimapradioBtn->SetValue(true);
    m_spepimapchoice->Enable();
    m_spusermapradioBtn->Disable();
    m_spusermapchoice->Disable();
  }
  else
  {
    m_spusermapradioBtn->Enable();
    if (m_spusermapchoice->GetSelection() == wxNOT_FOUND)
    {
      m_spepimapradioBtn->SetValue(true);
      m_spepimapchoice->Enable();
      m_spusermapchoice->SetSelection(0);
      m_spusermapchoice->Disable();
    }
    else
    {
      m_spusermapradioBtn->SetValue(true);
      m_spepimapchoice->Disable();
      m_spusermapchoice->Enable();
    }
  }
  PrepareDemoFileControls();
}

void SPDialog::PrepareDemoFileControls()
{
  wxArrayString l_file_list;
  wxString l_srcport_name = (m_spsourceportchoice->GetStringSelection());
  if (l_srcport_name == SRCPORTNAME_EDUKE32)
  {
    if (g_configuration->eduke32_userpath_use)
      g_AddAllDemoFiles(&l_file_list, g_configuration->eduke32_userpath);
    else
      g_AddAllDemoFiles(&l_file_list, ((wxFileName)g_configuration->eduke32_exec).GetPath());
  }
  else
    if (l_srcport_name == SRCPORTNAME_JFDUKE3D)
    {
      if (g_configuration->jfduke3d_userpath_use)
        g_AddAllDemoFiles(&l_file_list, g_configuration->jfduke3d_userpath);
      else
        g_AddAllDemoFiles(&l_file_list, ((wxFileName)g_configuration->jfduke3d_exec).GetPath());
    }
  else
    if (l_srcport_name == SRCPORTNAME_IDUKE3D)
    {
      if (g_configuration->iduke3d_userpath_use)
        g_AddAllDemoFiles(&l_file_list, g_configuration->iduke3d_userpath);
      else
        g_AddAllDemoFiles(&l_file_list, ((wxFileName)g_configuration->iduke3d_exec).GetPath());
    }
  else
    if (l_srcport_name == SRCPORTNAME_DUKE3DW32)
      g_AddAllDemoFiles(&l_file_list, ((wxFileName)g_configuration->duke3dw32_exec).GetPath());
  else
    if (l_srcport_name == SRCPORTNAME_XDUKE)
      g_AddAllDemoFiles(&l_file_list, ((wxFileName)g_configuration->xduke_exec).GetPath());
  else
    if (l_srcport_name == SRCPORTNAME_DOSDUKE)
      g_AddAllDemoFiles(&l_file_list, ((wxFileName)g_configuration->dosduke_exec).GetPath());
  else
    if (l_srcport_name == SRCPORTNAME_JFSW)
    {
      if (g_configuration->jfsw_userpath_use)
        g_AddAllDemoFiles(&l_file_list, g_configuration->jfsw_userpath);
      else
        g_AddAllDemoFiles(&l_file_list, ((wxFileName)g_configuration->jfsw_exec).GetPath());
    }
  else
    if (l_srcport_name == SRCPORTNAME_SWP)
      g_AddAllDemoFiles(&l_file_list, ((wxFileName)g_configuration->swp_exec).GetPath());
  else // SRCPORTNAME_DOSSW
    g_AddAllDemoFiles(&l_file_list, ((wxFileName)g_configuration->dossw_exec).GetPath());
  m_spplaydemochoice->Clear();
  m_spplaydemochoice->Append(l_file_list);
  m_spplaydemochoice->Disable();
  if (m_spplaydemochoice->GetCount())
  {
    m_spplaydemocheckBox->Enable();
    m_spplaydemochoice->SetSelection(0);
  }
  else
    m_spplaydemocheckBox->Disable();
  m_spplaydemocheckBox->SetValue(false);
  m_sprecorddemotextCtrl->Disable();
  m_sprecorddemocheckBox->SetValue(false);
}

void SPDialog::OnGameSelect(wxCommandEvent& WXUNUSED(event))
{
  wxString l_game_name = m_spgamechoice->GetStringSelection();
  wxString l_srcport_name = m_spsourceportchoice->GetStringSelection();
  if (((l_game_name == GAMENAME_DN3D) &&
       ((l_srcport_name == SRCPORTNAME_JFSW) || (l_srcport_name == SRCPORTNAME_SWP)
        || (l_srcport_name == SRCPORTNAME_DOSSW))
      )
      ||
      ((l_game_name == GAMENAME_SW) &&
       ((l_srcport_name == SRCPORTNAME_EDUKE32) || (l_srcport_name == SRCPORTNAME_JFDUKE3D)
        || (l_srcport_name == SRCPORTNAME_IDUKE3D) || (l_srcport_name == SRCPORTNAME_DUKE3DW32)
        || (l_srcport_name == SRCPORTNAME_XDUKE) || (l_srcport_name == SRCPORTNAME_DOSDUKE))
      )
     )
  {
    m_spskillchoice->Clear();
    m_spepimapchoice->Clear();
    m_spusermapchoice->Clear();
    m_spavailmodfileslistBox->Clear();
    m_spusedmodfileslistBox->Clear();
    m_spsourceportchoice->Clear();
    if (l_game_name == GAMENAME_DN3D)
    {
      if (g_configuration->have_eduke32)
        m_spsourceportchoice->Append(SRCPORTNAME_EDUKE32);
      if (g_configuration->have_jfduke3d)
        m_spsourceportchoice->Append(SRCPORTNAME_JFDUKE3D);
      if (g_configuration->have_iduke3d)
        m_spsourceportchoice->Append(SRCPORTNAME_IDUKE3D);
      if (g_configuration->have_duke3dw32)
        m_spsourceportchoice->Append(SRCPORTNAME_DUKE3DW32);
      if (g_configuration->have_xduke)
        m_spsourceportchoice->Append(SRCPORTNAME_XDUKE);
      if (g_configuration->have_dosduke && g_configuration->have_dosbox)
        m_spsourceportchoice->Append(SRCPORTNAME_DOSDUKE);
    }
    else
    {
      if (g_configuration->have_jfsw)
        m_spsourceportchoice->Append(SRCPORTNAME_JFSW);
      if (g_configuration->have_swp)
        m_spsourceportchoice->Append(SRCPORTNAME_SWP);
      if (g_configuration->have_dossw && g_configuration->have_dosbox)
        m_spsourceportchoice->Append(SRCPORTNAME_DOSSW);
    }
    m_spsourceportchoice->SetSelection(0);
    PartialDialogControlsReset();
  }
}

void SPDialog::OnSrcPortSelect(wxCommandEvent& WXUNUSED(event))
{
  PrepareDemoFileControls();
}

void SPDialog::OnCheckBoxClick(wxCommandEvent& event)
{
  switch (event.GetId())
  {
    case ID_SPHAVEBOTS:          m_spbotschoice->Enable(m_spbotscheckBox->GetValue());
                                 break;
    case ID_SPPLAYDEMO:          m_spplaydemochoice->Enable(m_spplaydemocheckBox->GetValue());
                                 m_sprecorddemocheckBox->SetValue(false);
                                 m_sprecorddemotextCtrl->Disable();
                                 break;
    default: /*ID_SPRECORDDEMO*/ m_sprecorddemotextCtrl->Enable((m_sprecorddemocheckBox->GetValue()) &&
                                                                (m_spgamechoice->GetStringSelection() == GAMENAME_SW));
                                 m_spplaydemocheckBox->SetValue(false);
                                 m_spplaydemochoice->Disable();
  }
}

void SPDialog::OnRadioBtnClick(wxCommandEvent& WXUNUSED(event))
{
  m_spepimapchoice->Enable(m_spepimapradioBtn->GetValue());
  m_spusermapchoice->Enable(m_spusermapradioBtn->GetValue());
}

void SPDialog::OnAddModFile(wxCommandEvent& WXUNUSED(event))
{
  wxArrayInt l_file_index_list;
  // We'd get a list of the INDEX NUMBERS of the selected files;
  // Therefore, we should remove them in REVERSED order.
  // That's because on removal of an item, the index numbers of
  // the following items get DECREASED by 1;
  // And if we try removing from the beginning, then from the second item
  // we'd try removing using the WRONG index numbers.
  size_t i = 0, l_num_of_items = m_spavailmodfileslistBox->GetCount();
  bool no_item_found = true;
  while ((i < l_num_of_items) && (no_item_found))
  {
    if (m_spavailmodfileslistBox->IsSelected(i))
    {
      l_file_index_list.Add(i);
      m_spusedmodfileslistBox->Append(m_spavailmodfileslistBox->GetString(i));
      no_item_found = false;
    }
    i++;
  }
// Now, if we've found one item, the array l_file_index_list should contain
// its index number.
// We should insert the rest of the selected items' index numbers
// into the array in REVERSED order.
// If i = l_num_of_items we have nothing to do
// (nothing is selected, or only the latest item is selected).
// But if i < l_num_of_items, now that l_file_index_list contains an item,
// we can simply use the Insert function/procedure to insert right
// in the beginning of the array.
  while (i < l_num_of_items)
  {
    if (m_spavailmodfileslistBox->IsSelected(i))
    {
      l_file_index_list.Insert(i, 0);
      m_spusedmodfileslistBox->Append(m_spavailmodfileslistBox->GetString(i));
    }
    i++;
  }
  l_num_of_items = l_file_index_list.GetCount();
  for (i = 0; i < l_num_of_items; i++)
    m_spavailmodfileslistBox->Delete(l_file_index_list[i]);
}

void SPDialog::OnRemModFile(wxCommandEvent& WXUNUSED(event))
{
// Same story as with OnAddModFile.
  wxArrayInt l_file_index_list;
  size_t i = 0, l_num_of_items = m_spusedmodfileslistBox->GetCount();
  bool no_item_found = true;
  while ((i < l_num_of_items) && (no_item_found))
  {
    if (m_spusedmodfileslistBox->IsSelected(i))
    {
      l_file_index_list.Add(i);
      m_spavailmodfileslistBox->Append(m_spusedmodfileslistBox->GetString(i));
      no_item_found = false;
    }
    i++;
  }
  while (i < l_num_of_items)
  {
    if (m_spusedmodfileslistBox->IsSelected(i))
    {
      l_file_index_list.Insert(i, 0);
      m_spavailmodfileslistBox->Append(m_spusedmodfileslistBox->GetString(i));
    }
    i++;
  }
  l_num_of_items = l_file_index_list.GetCount();
  for (i = 0; i < l_num_of_items; i++)
    m_spusedmodfileslistBox->Delete(l_file_index_list[i]);
}
/*
void SPDialog::OnApply(wxCommandEvent& WXUNUSED(event))
{
  ApplySettings();
}
*/
void SPDialog::OnOk(wxCommandEvent& WXUNUSED(event))
{
  ApplySettings();

  GameType l_game;
  SourcePortType l_srcport;
  wxString l_temp_str;
//bool have_bots = m_spbotscheckBox->GetValue();
//size_t num_bots = m_spbotschoice->GetSelection()+1;
//bool play_demo = m_spplaydemocheckBox->GetValue();
//wxString playdemo_filename = m_spplaydemochoice->GetStringSelection();
//bool record_demo = m_sprecorddemocheckBox->GetValue();
//wxString recorddemo_filename = m_sprecorddemotextCtrl->GetValue();
//size_t skill_num = m_spskillchoice->GetSelection();
//LevelChoiceType l_level_choice;
  bool l_launch_usermap;
  size_t l_episode_map = 0;
  wxString l_user_map;
  wxArrayString l_mod_files;
  if (m_spgamechoice->GetStringSelection() == GAMENAME_DN3D)
  {
    l_game = GAME_DN3D;
    l_temp_str = m_spsourceportchoice->GetStringSelection();
    if (l_temp_str == SRCPORTNAME_EDUKE32)
      l_srcport = SOURCEPORT_EDUKE32;
    else
      if (l_temp_str == SRCPORTNAME_JFDUKE3D)
        l_srcport = SOURCEPORT_JFDUKE3D;
    else
      if (l_temp_str == SRCPORTNAME_IDUKE3D)
        l_srcport = SOURCEPORT_IDUKE3D;
    else
      if (l_temp_str == SRCPORTNAME_DUKE3DW32)
        l_srcport = SOURCEPORT_DUKE3DW32;
    else
      if (l_temp_str == SRCPORTNAME_XDUKE)
        l_srcport = SOURCEPORT_XDUKE;
    else
      l_srcport = SOURCEPORT_DOSDUKE;
  }
  else
  {
    l_game = GAME_SW;
    l_temp_str = m_spsourceportchoice->GetStringSelection();
    if (l_temp_str == SRCPORTNAME_JFSW)
      l_srcport = SOURCEPORT_JFSW;
    else
      if (l_temp_str == SRCPORTNAME_SWP)
        l_srcport = SOURCEPORT_SWP;
    else
      l_srcport = SOURCEPORT_DOSSW;
  }
  if (m_spepimapradioBtn->GetValue())
  {
    l_launch_usermap = false;
    if (l_game == GAME_DN3D)
      l_episode_map = g_GetDN3DLevelNumBySelection(m_spepimapchoice->GetSelection());
    else
      l_episode_map = g_GetSWLevelNumBySelection(m_spepimapchoice->GetSelection());
  }
  else
  {
    l_launch_usermap = true;
    l_user_map = m_spusermapchoice->GetStringSelection();
  }
  l_mod_files = m_spusedmodfileslistBox->GetStrings();
#ifdef __WXMSW__
  l_temp_str = wxT("dukematcher.bat");
#elif (defined __WXMAC__) || (defined __WXCOCOA__)
  l_temp_str = wxT("dukematcher.command");
#else
  l_temp_str = wxT("dukematcher.sh");
#endif
  if (g_MakeLaunchScript(l_temp_str, NULL, 0, l_game, l_srcport,
                         m_spbotscheckBox->GetValue(),
                         m_spbotschoice->GetSelection()+1,
                         m_spplaydemocheckBox->GetValue(),
                         m_spplaydemochoice->GetStringSelection(),
                         m_sprecorddemocheckBox->GetValue(),
                         m_sprecorddemotextCtrl->GetValue(),
                         m_spskillchoice->GetSelection(),
                         (DN3DMPGameT_Type)0, (DN3DSpawnType)0, false,
                         l_launch_usermap, l_user_map, l_episode_map,
                         l_mod_files, m_spextraargstextCtrl->GetValue(), ""))
#ifdef __WXMSW__
    wxExecute(wxT('"') + l_temp_str + wxT('"'));
#else
    wxExecute(g_configuration->terminal_fullcmd + wxT(" \"") + l_temp_str + wxT('"'));
#endif
  Destroy();
}

void SPDialog::OnCancel(wxCommandEvent& WXUNUSED(event))
{
  Destroy();
}

void SPDialog::ApplySettings()
{
  wxString l_selected_port = m_spsourceportchoice->GetStringSelection();
  if (l_selected_port == SRCPORTNAME_EDUKE32)
    g_configuration->gamelaunch_source_port = SOURCEPORT_EDUKE32;
  else
    if (l_selected_port == SRCPORTNAME_JFDUKE3D)
      g_configuration->gamelaunch_source_port = SOURCEPORT_JFDUKE3D;
  else
    if (l_selected_port == SRCPORTNAME_IDUKE3D)
      g_configuration->gamelaunch_source_port = SOURCEPORT_IDUKE3D;
  else
    if (l_selected_port == SRCPORTNAME_DUKE3DW32)
      g_configuration->gamelaunch_source_port = SOURCEPORT_DUKE3DW32;
  else
    if (l_selected_port == SRCPORTNAME_XDUKE)
      g_configuration->gamelaunch_source_port = SOURCEPORT_XDUKE;
  else
    if (l_selected_port == SRCPORTNAME_DOSDUKE)
      g_configuration->gamelaunch_source_port = SOURCEPORT_DOSDUKE;
  else
    if (l_selected_port == SRCPORTNAME_JFSW)
      g_configuration->gamelaunch_source_port = SOURCEPORT_JFSW;
  else
    if (l_selected_port == SRCPORTNAME_SWP)
      g_configuration->gamelaunch_source_port = SOURCEPORT_SWP;
  else // SRCPORTNAME_DOSSW
    g_configuration->gamelaunch_source_port = SOURCEPORT_DOSSW;

  g_configuration->gamelaunch_args_in_sp = m_spextraargstextCtrl->GetValue();

  if (m_spgamechoice->GetStringSelection() == GAMENAME_DN3D)
  {
    g_configuration->gamelaunch_dn3d_skill = m_spskillchoice->GetSelection();
    g_configuration->gamelaunch_dn3dlvl = g_GetDN3DLevelNumBySelection(m_spepimapchoice->GetSelection());
//  l_level_num = g_GetDN3DLevelNumBySelection(m_spepimapchoice->GetSelection());
//  if (l_level_num)
//  g_configuration->gamelaunch_dn3dlvl = l_level_num;
    if (m_spepimapradioBtn->GetValue())
      g_configuration->gamelaunch_dn3dusermap = wxEmptyString;
    else
      g_configuration->gamelaunch_dn3dusermap = m_spusermapchoice->GetStringSelection();
//  l_map_filename = m_spusermapchoice->GetStringSelection();
//  if (!l_map_filename.IsEmpty())
//    g_configuration->gamelaunch_dn3dusermap = l_map_filename;
    g_configuration->gamelaunch_dn3dmodfiles = m_spusedmodfileslistBox->GetStrings();
  }
  else
  {
    g_configuration->gamelaunch_sw_skill = m_spskillchoice->GetSelection();
    g_configuration->gamelaunch_swlvl = g_GetSWLevelNumBySelection(m_spepimapchoice->GetSelection());
//  l_level_num = g_GetSWLevelNumBySelection(m_spepimapchoice->GetSelection());
//  if (l_level_num)
//  g_configuration->gamelaunch_swlvl = l_level_num;
    if (m_spepimapradioBtn->GetValue())
      g_configuration->gamelaunch_swusermap = wxEmptyString;
    else
      g_configuration->gamelaunch_swusermap = m_spusermapchoice->GetStringSelection();
//  l_map_filename = m_spusermapchoice->GetStringSelection();
//  if (!l_map_filename.IsEmpty())
//    g_configuration->gamelaunch_swusermap = l_map_filename;
    g_configuration->gamelaunch_swmodfiles = m_spusedmodfileslistBox->GetStrings();
  }
  g_configuration->Save();
}
