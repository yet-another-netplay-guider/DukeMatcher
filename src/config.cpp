/*************************************************************************

Copyright 2008-2009 - NY00123

This file is part of DUKEMATCHER.

DUKEMATCHER is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

DUKEMATCHER is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DUKEMATCHER; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*************************************************************************/

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include <wx/filename.h>
#include <wx/textfile.h>

#include "config.h"
#include "mp_common.h"
#include "dukematcher.h"


void RestoreDefaultSounds(wxString& snd_join, wxString& snd_leave,
                          wxString& snd_send, wxString& snd_receive, wxString& snd_error)
{
#if 0
#ifndef __WXMSW__
  if (wxFileExists(*g_launcher_resources_path + wxT("snddata/join.wav")))
    snd_join = *g_launcher_resources_path + wxT("snddata/join.wav");
  if (wxFileExists(*g_launcher_resources_path + wxT("snddata/leave.wav")))
    snd_leave = *g_launcher_resources_path + wxT("snddata/leave.wav");
  if (wxFileExists(*g_launcher_resources_path + wxT("snddata/send.wav")))
    snd_send = *g_launcher_resources_path + wxT("snddata/send.wav");
  if (wxFileExists(*g_launcher_resources_path + wxT("snddata/receive.wav")))
    snd_receive = *g_launcher_resources_path + wxT("snddata/receive.wav");
  if (wxFileExists(*g_launcher_resources_path + wxT("snddata/error.wav")))
    snd_error = *g_launcher_resources_path + wxT("snddata/error.wav");
#endif
#endif
  if (wxFileExists(wxT("snddata/join.wav")))
    snd_join = wxT("snddata/join.wav");
  if (wxFileExists(wxT("snddata/leave.wav")))
    snd_leave = wxT("snddata/leave.wav");
  if (wxFileExists(wxT("snddata/send.wav")))
    snd_send = wxT("snddata/send.wav");
  if (wxFileExists(wxT("snddata/receive.wav")))
    snd_receive = wxT("snddata/receive.wav");
  if (wxFileExists(wxT("snddata/error.wav")))
    snd_error = wxT("snddata/error.wav");
}

LauncherConfig::LauncherConfig()
{
  wxString l_temp_str;
  theme_is_dark = false;
  check_for_updates_on_startup = true;
  // May change depending on which ports are configured to be available.
  gamelaunch_source_port = SOURCEPORT_EDUKE32;
  gamelaunch_dn3d_skill = DN3D_DEFAULT_SKILL;
  gamelaunch_sw_skill = SW_DEFAULT_SKILL;
  // All gamelaunch-related strings should be initialized as an empty string by default.
  gamelaunch_dn3dlvl = DN3D_DEFAULT_LEVEL;
  gamelaunch_swlvl = SW_DEFAULT_LEVEL;
  gamelaunch_numofplayers = DEFAULT_NUM_PLAYERS;
  gamelaunch_dn3dgametype = DN3DMPGAMETYPE_DMSPAWN;
  gamelaunch_dn3dspawn = DN3DSPAWN_ALL;
  gamelaunch_show_modurl = gamelaunch_use_masterslave = gamelaunch_skip_settings = false;
  gamelaunch_advertise = true;
  last_serverlist_in_cycle = 0;
  // Many other strings should be empty by default as well.
  // But only if we want.
//gamelaunch_enable_natfree =
  have_eduke32 = have_jfduke3d = have_iduke3d
  = have_duke3dw32 = have_xduke = have_dosduke
  = have_jfsw = have_swp = have_dossw = have_dosbox = dosbox_use_conf
#ifndef __WXMSW__
  = have_wine
#endif
  = false;
  dosbox_cdmount = CDROMMOUNT_NONE;
  dn3d_maps_dir = wxT("dn3dmaps");
  dn3d_mods_dir = wxT("dn3dmods");
  sw_maps_dir = wxT("swmaps");
  sw_mods_dir = wxT("swmods");
#ifdef __WXMSW__
  eduke32_userpath_use = jfduke3d_userpath_use = iduke3d_userpath_use = jfsw_userpath_use = false;
  l_temp_str = wxGetenv(wxT("APPDATA"));
  eduke32_userpath = l_temp_str + wxT("\\EDuke32 Settings");
  jfduke3d_userpath = l_temp_str + wxT("\\JFDuke3D");
  jfsw_userpath = l_temp_str + wxT("\\JFShadowWarrior");
#else
  if (wxFileExists(wxT("/usr/bin/wine")))
    wine_exec = wxT("/usr/bin/wine");
  else
    if (wxFileExists(wxT("/usr/local/bin/wine")))
      wine_exec = wxT("/usr/local/bin/wine");
  l_temp_str = wxGetenv(wxT("HOME"));
  eduke32_userpath_use = jfduke3d_userpath_use = jfsw_userpath_use = true;
  iduke3d_userpath_use = false;
#if (defined __WXMAC__) || (defined __WXCOCOA__)
  if (wxFileExists(wxT("/Applications/DOSBox.app/Contents/MacOS/DOSBox")))
    dosbox_exec = wxT("/Applications/DOSBox.app/Contents/MacOS/DOSBox");
  dosbox_conf = l_temp_str + wxT("/Library/Preferences/DOSBox Preferences");
  eduke32_userpath = l_temp_str + wxT("/Library/Application Support/EDuke32");
  jfduke3d_userpath = l_temp_str + wxT("/Library/Preferences/JFDuke3D");
  iduke3d_userpath = l_temp_str + wxT("/Library/Application Support/Duke Nukem 3D");
  jfsw_userpath = l_temp_str + wxT("/Library/Preferences/JFShadowWarrior");
  terminal_fullcmd = wxT("open");
#else
  if (wxFileExists(wxT("/usr/bin/eduke32")))
    eduke32_exec = wxT("/usr/bin/eduke32");
  else
    if (wxFileExists(wxT("/usr/games/eduke32")))
      eduke32_exec = wxT("/usr/games/eduke32");
  else
    if (wxFileExists(wxT("/usr/local/bin/eduke32")))
      eduke32_exec = wxT("/usr/local/bin/eduke32");
  if (wxFileExists(wxT("/usr/bin/jfsw")))
    jfsw_exec = wxT("/usr/bin/jfsw");
  else
    if (wxFileExists(wxT("/usr/games/jfsw")))
      jfsw_exec = wxT("/usr/games/jfsw");
  else
    if (wxFileExists(wxT("/usr/local/bin/jfsw")))
      jfsw_exec = wxT("/usr/local/bin/jfsw");
  if (wxFileExists(wxT("/usr/bin/dosbox")))
    dosbox_exec = wxT("/usr/bin/dosbox");
  else
    if (wxFileExists(wxT("/usr/games/dosbox")))
      dosbox_exec = wxT("/usr/games/dosbox");
  else
    if (wxFileExists(wxT("/usr/local/bin/dosbox")))
      dosbox_exec = wxT("/usr/local/bin/dosbox");
  if (wxDirExists(wxT("/media/cdrom")))
    dosbox_cdrom_location = wxT("/media/cdrom");
  else
    if (wxDirExists(wxT("/media/cdrom0")))
      dosbox_cdrom_location = wxT("/media/cdrom0");
  else
    if (wxDirExists(wxT("/mnt/cdrom")))
      dosbox_cdrom_location = wxT("/mnt/cdrom");
  else
    if (wxDirExists(wxT("/mnt/cdrom0")))
      dosbox_cdrom_location = wxT("/mnt/cdrom0");
/*if (wxFileExists(wxT("/dev/cdrom")))
    dosbox_cdrom_image = wxT("/dev/cdrom");
  else
    if (wxFileExists(wxT("/dev/cdrw")))
      dosbox_cdrom_image = wxT("/dev/cdrw");
  else
    if (wxFileExists(wxT("/dev/cdrecorder")))
      dosbox_cdrom_image = wxT("/dev/cdrecorder");
  else
    if (wxFileExists(wxT("/dev/dvd")))
      dosbox_cdrom_image = wxT("/dev/dvd");
  else
    if (wxFileExists(wxT("/dev/dvdrom")))
      dosbox_cdrom_image = wxT("/dev/dvdrom");
  else
    if (wxFileExists(wxT("/dev/dvdrw")))
      dosbox_cdrom_image = wxT("/dev/dvdrw");
  else
    if (wxFileExists(wxT("/dev/dvdrecorder")))
      dosbox_cdrom_image = wxT("/dev/dvdrecorder");
  else
    if (wxFileExists(wxT("/dev/dvdram")))
      dosbox_cdrom_image = wxT("/dev/dvdram");*/
  eduke32_userpath = l_temp_str + wxT("/.eduke32");
  jfduke3d_userpath = l_temp_str + wxT("/.jfduke3d");
  iduke3d_userpath = l_temp_str + wxT("/.duke3d");
  jfsw_userpath = l_temp_str + wxT("/.jfsw");
  // Terminal - Empty as well. Well, at least for now.
#endif
#endif
  // Overriden browser string - Ditto.
  override_browser = false;
  enable_localip_optimization = true;
#if ((defined __WXMSW__) || (defined __WXMAC__) || (defined __WXCOCOA__))
  override_soundcmd = false;
#elif (wxUSE_LIBSDL == 1)
  override_soundcmd = false;
#else
  // PulseAudio?
  if (wxFileExists(wxT("/usr/bin/paplay")))
  {
    playsound_cmd = wxT("/usr/bin/paplay");
    override_soundcmd = true;
  }
  else
    if (wxFileExists(wxT("/usr/local/bin/paplay")))
    {
      playsound_cmd = wxT("/usr/local/bin/paplay");
      override_soundcmd = true;
    }
  else // ALSA?
    if (wxFileExists(wxT("/usr/bin/aplay")))
    {
      playsound_cmd = wxT("/usr/bin/aplay");
      override_soundcmd = true;
    }
  else
    if (wxFileExists(wxT("/usr/local/bin/aplay")))
    {
      playsound_cmd = wxT("/usr/local/bin/aplay");
      override_soundcmd = true;
    }
  else // ESD?
    if (wxFileExists(wxT("/usr/bin/esdplay")))
    {
      playsound_cmd = wxT("/usr/bin/esdplay");
      override_soundcmd = true;
    }
  else
    if (wxFileExists(wxT("/usr/local/bin/esdplay")))
    {
      playsound_cmd = wxT("/usr/local/bin/esdplay");
      override_soundcmd = true;
    }
  else
    override_soundcmd = false;
#endif
  nickname = wxT("Duke Match Player");
  game_nickname = wxT("DM-Player");

  mute_sounds_while_ingame = false;
  RestoreDefaultSounds(snd_join_file, snd_leave_file,
                       snd_send_file, snd_receive_file, snd_error_file);
  play_snd_join = play_snd_leave = play_snd_send = play_snd_receive = play_snd_error = true;
/*play_snd_join = (snd_join_file);
  play_snd_leave = (snd_leave_file);
  play_snd_send = (snd_send_file);
  play_snd_receive = (snd_receive_file);
  play_snd_error = (snd_error_file);*/

  game_port_number = DEFAULT_GAME_PORT_NUM;
  server_port_number = DEFAULT_SERVER_PORT_NUM;

  host_autoaccept_downloads = true;
  lookup_on_startup = true;
  show_timestamp = false;
  gamelaunch_forcerespawn = false;
  gamelaunch_noteamchange = true;
}

bool LauncherConfig::Load()
{
  wxTextFile l_settingsfile;
  wxString temp_string, l_nickname, l_hostaddr;
  int l_loop_var, l_num_of_lines, l_temp_int1, l_temp_int2;
  long l_temp_long1, l_temp_long2, l_temp_length;
  wxString l_temp_str1, l_temp_str2;
  if (wxFile::Access(DUKEMATCHER_CONFIG_FULLFILEPATH, wxFile::read))
  {
    l_settingsfile.Open(DUKEMATCHER_CONFIG_FULLFILEPATH);
    l_loop_var = 0;
    l_num_of_lines = l_settingsfile.GetLineCount();
    while (l_loop_var < l_num_of_lines)
    {
      temp_string = l_settingsfile[l_loop_var];
      // Theme
      if (!temp_string.Find(wxT("theme_is_dark = 1")))
        theme_is_dark = true;
      else
        if (!temp_string.Find(wxT("theme_is_dark = 0")))
          theme_is_dark = false;
      // Check for updates on startup
      if (!temp_string.Find(wxT("check_for_updates_on_startup = 1")))
        check_for_updates_on_startup = true;
      else
        if (!temp_string.Find(wxT("check_for_updates_on_startup = 0")))
          check_for_updates_on_startup = false;
      // Single Player / Host settings
      else
        if (!temp_string.Find(wxT("source_port = eduke32")))
          gamelaunch_source_port = SOURCEPORT_EDUKE32;
      else
        if (!temp_string.Find(wxT("source_port = jfduke3d")))
          gamelaunch_source_port = SOURCEPORT_JFDUKE3D;
      else
        if (!temp_string.Find(wxT("source_port = iduke3d")))
          gamelaunch_source_port = SOURCEPORT_IDUKE3D;
      else
        if (!temp_string.Find(wxT("source_port = duke3dw32")))
          gamelaunch_source_port = SOURCEPORT_DUKE3DW32;
      else
        if (!temp_string.Find(wxT("source_port = xduke")))
          gamelaunch_source_port = SOURCEPORT_XDUKE;
      else
        if (!temp_string.Find(wxT("source_port = dosduke")))
          gamelaunch_source_port = SOURCEPORT_DOSDUKE;
      else
        if (!temp_string.Find(wxT("source_port = jfsw")))
          gamelaunch_source_port = SOURCEPORT_JFSW;
      else
        if (!temp_string.Find(wxT("source_port = swp")))
          gamelaunch_source_port = SOURCEPORT_SWP;
      else
        if (!temp_string.Find(wxT("source_port = dossw")))
          gamelaunch_source_port = SOURCEPORT_DOSSW;
      else
        if (!temp_string.Find(wxT("dn3d_skill = ")))
        {
          if (!temp_string.Mid(13).ToLong(&gamelaunch_dn3d_skill))
            gamelaunch_dn3d_skill = DN3D_DEFAULT_SKILL;
        }
      else
        if (!temp_string.Find(wxT("sw_skill = ")))
        {
          if (!temp_string.Mid(11).ToLong(&gamelaunch_sw_skill))
            gamelaunch_sw_skill = SW_DEFAULT_SKILL;
        }
      else
        if (!temp_string.Find(wxT("args_in_sp = ")))
          gamelaunch_args_in_sp = temp_string.Mid(13);
      else
        if (!temp_string.Find(wxT("dn3d_level = ")))
        {
          if (!temp_string.Mid(13).ToLong(&gamelaunch_dn3dlvl))
            gamelaunch_dn3dlvl = DN3D_DEFAULT_LEVEL;
        }
      else
        if (!temp_string.Find(wxT("sw_level = ")))
        {
          if (!temp_string.Mid(11).ToLong(&gamelaunch_swlvl))
            gamelaunch_swlvl = SW_DEFAULT_LEVEL;
        }
      else
        if (!temp_string.Find(wxT("dn3d_usermap = ")))
          gamelaunch_dn3dusermap = temp_string.Mid(15);
      else
        if (!temp_string.Find(wxT("sw_usermap = ")))
          gamelaunch_swusermap = temp_string.Mid(13);
      else
        if (!temp_string.Find(wxT("room_name = ")))
          gamelaunch_roomname = temp_string.Mid(12);
      else
        if (!temp_string.Find(wxT("num_of_players = ")))
        {
          if (!temp_string.Mid(17).ToLong(&gamelaunch_numofplayers))
            gamelaunch_numofplayers = DEFAULT_NUM_PLAYERS;
        }
      else
        if (!temp_string.Find(wxT("dn3d_mpgametype = dm_spawn")))
          gamelaunch_dn3dgametype = DN3DMPGAMETYPE_DMSPAWN;
      else
        if (!temp_string.Find(wxT("dn3d_mpgametype = coop")))
          gamelaunch_dn3dgametype = DN3DMPGAMETYPE_COOP;
      else
        if (!temp_string.Find(wxT("dn3d_mpgametype = dm_nospawn")))
          gamelaunch_dn3dgametype = DN3DMPGAMETYPE_DMNOSPAWN;
      else
        if (!temp_string.Find(wxT("dn3d_mpgametype = tdm_spawn")))
          gamelaunch_dn3dgametype = DN3DMPGAMETYPE_TDMSPAWN;
      else
        if (!temp_string.Find(wxT("dn3d_mpgametype = tdm_nospawn")))
          gamelaunch_dn3dgametype = DN3DMPGAMETYPE_TDMNOSPAWN;
      else
        if (!temp_string.Find(wxT("dn3d_spawntype = monsters")))
          gamelaunch_dn3dspawn = DN3DSPAWN_MONSTERS;
      else
        if (!temp_string.Find(wxT("dn3d_spawntype = items")))
          gamelaunch_dn3dspawn = DN3DSPAWN_ITEMS;
      else
        if (!temp_string.Find(wxT("dn3d_spawntype = inventory")))
          gamelaunch_dn3dspawn = DN3DSPAWN_INVENTORY;
      else
        if (!temp_string.Find(wxT("dn3d_spawntype = all")))
          gamelaunch_dn3dspawn = DN3DSPAWN_ALL;
		else
        if (!temp_string.Find(wxT("frag_limit = ")))
        {
          if (!temp_string.Mid(13).ToLong(&gamelaunch_fraglimit))
            gamelaunch_fraglimit = 0;
        }
		else
        if (!temp_string.Find(wxT("no_team_change = 1")))
          gamelaunch_noteamchange = true;
		else
        if (!temp_string.Find(wxT("no_team_change = 0")))
          gamelaunch_noteamchange = false;
		  
		  else
        if (!temp_string.Find(wxT("force_respawn = 1")))
          gamelaunch_forcerespawn = true;
		  else
        if (!temp_string.Find(wxT("force_respawn = 0")))
          gamelaunch_forcerespawn = false;
	  
      else
        if (!temp_string.Find(wxT("args_for_host = ")))
          gamelaunch_args_for_host = temp_string.Mid(16);
      else
        if (!temp_string.Find(wxT("args_for_all = ")))
          gamelaunch_args_for_all = temp_string.Mid(15);
      else
        if (!temp_string.Find(wxT("mod_name = ")))
          gamelaunch_modname = temp_string.Mid(11);
      else
        if (!temp_string.Find(wxT("show_mod_url = 1")))
          gamelaunch_show_modurl = true;
      else
        if (!temp_string.Find(wxT("show_mod_url = 0")))
          gamelaunch_show_modurl = false;
      else
        if (!temp_string.Find(wxT("mod_url = ")))
          gamelaunch_modurl = temp_string.Mid(10);
      else
        if (!temp_string.Find(wxT("use_masterslave = 1")))
          gamelaunch_use_masterslave = true;
      else
        if (!temp_string.Find(wxT("use_masterslave = 0")))
          gamelaunch_use_masterslave = false;
      else
        if (!temp_string.Find(wxT("advertise = 1")))
          gamelaunch_advertise = true;
      else
        if (!temp_string.Find(wxT("advertise = 0")))
          gamelaunch_advertise = false;
      else
        if (!temp_string.Find(wxT("skip_settings = 1")))
          gamelaunch_skip_settings = true;
      else
        if (!temp_string.Find(wxT("skip_settings = 0")))
          gamelaunch_skip_settings = false;
      else
        if (!temp_string.Find(wxT("last_serverlist_in_cycle = ")))
        {
          if (!temp_string.Mid(27).ToULong(&last_serverlist_in_cycle))
            last_serverlist_in_cycle = 0;
        }
/*    else
        if (!temp_string.Find(wxT("enable_natfree = 1")))
          gamelaunch_enable_natfree = true;
      else
        if (!temp_string.Find(wxT("enable_natfree = 0")))
          gamelaunch_enable_natfree = false;*/
      // Source ports settings - Duke Nukem 3D
      else
        if (!temp_string.Find(wxT("eduke32 = 1")))
          have_eduke32 = true;
      else
        if (!temp_string.Find(wxT("eduke32 = 0")))
          have_eduke32 = false;
      else
        if (!temp_string.Find(wxT("jfduke3d = 1")))
          have_jfduke3d = true;
      else
        if (!temp_string.Find(wxT("jfduke3d = 0")))
          have_jfduke3d = false;
      else
        if (!temp_string.Find(wxT("iduke3d = 1")))
          have_iduke3d = true;
      else
        if (!temp_string.Find(wxT("iduke3d = 0")))
          have_iduke3d = false;
      else
        if (!temp_string.Find(wxT("duke3dw32 = 1")))
          have_duke3dw32 = true;
      else
        if (!temp_string.Find(wxT("duke3dw32 = 0")))
          have_duke3dw32 = false;
      else
        if (!temp_string.Find(wxT("xduke = 1")))
          have_xduke = true;
      else
        if (!temp_string.Find(wxT("xduke = 0")))
          have_xduke = false;
      else
        if (!temp_string.Find(wxT("dosduke = 1")))
          have_dosduke = true;
      else
        if (!temp_string.Find(wxT("dosduke = 0")))
          have_dosduke = false;
      else
        if (!temp_string.Find(wxT("eduke32_exec = ")))
          eduke32_exec = temp_string.Mid(15);
      else
        if (!temp_string.Find(wxT("jfduke3d_exec = ")))
          jfduke3d_exec = temp_string.Mid(16);
      else
        if (!temp_string.Find(wxT("iduke3d_exec = ")))
          iduke3d_exec = temp_string.Mid(15);
      else
        if (!temp_string.Find(wxT("duke3dw32_exec = ")))
          duke3dw32_exec = temp_string.Mid(17);
      else
        if (!temp_string.Find(wxT("xduke_exec = ")))
          xduke_exec = temp_string.Mid(13);
      else
        if (!temp_string.Find(wxT("dosduke_exec = ")))
          dosduke_exec = temp_string.Mid(15);
      else
        if (!temp_string.Find(wxT("dn3d_maps_dir = ")))
          dn3d_maps_dir = temp_string.Mid(16);
      else
        if (!temp_string.Find(wxT("dn3d_mods_dir = ")))
          dn3d_mods_dir = temp_string.Mid(16);
      // Source ports settings - Shadow Warrior
      else
        if (!temp_string.Find(wxT("jfsw = 1")))
          have_jfsw = true;
      else
        if (!temp_string.Find(wxT("jfsw = 0")))
          have_jfsw = false;
      else
        if (!temp_string.Find(wxT("swp = 1")))
          have_swp = true;
      else
        if (!temp_string.Find(wxT("swp = 0")))
          have_swp = false;
      else
        if (!temp_string.Find(wxT("dossw = 1")))
          have_dossw = true;
      else
        if (!temp_string.Find(wxT("dossw = 0")))
          have_dossw = false;
      else
        if (!temp_string.Find(wxT("jfsw_exec = ")))
          jfsw_exec = temp_string.Mid(12);
      else
        if (!temp_string.Find(wxT("swp_exec = ")))
          swp_exec = temp_string.Mid(11);
      else
        if (!temp_string.Find(wxT("dossw_exec = ")))
          dossw_exec = temp_string.Mid(13);
      else
        if (!temp_string.Find(wxT("sw_maps_dir = ")))
          sw_maps_dir = temp_string.Mid(14);
      else
        if (!temp_string.Find(wxT("sw_mods_dir = ")))
          sw_mods_dir = temp_string.Mid(14);
      // DOSBox settings
      else
        if (!temp_string.Find(wxT("dosbox = 1")))
          have_dosbox = true;
      else
        if (!temp_string.Find(wxT("dosbox = 0")))
          have_dosbox = false;
      else
        if (!temp_string.Find(wxT("dosbox_use_conf = 1")))
          dosbox_use_conf = true;
      else
        if (!temp_string.Find(wxT("dosbox_use_conf = 0")))
          dosbox_use_conf = false;
      else
        if (!temp_string.Find(wxT("dosbox_cdrom_mount = none")))
          dosbox_cdmount = CDROMMOUNT_NONE;
      else
        if (!temp_string.Find(wxT("dosbox_cdrom_mount = dir")))
          dosbox_cdmount = CDROMMOUNT_DIR;
      else
        if (!temp_string.Find(wxT("dosbox_cdrom_mount = image")))
          dosbox_cdmount = CDROMMOUNT_IMG;
      else
        if (!temp_string.Find(wxT("dosbox_exec = ")))
          dosbox_exec = temp_string.Mid(14);
      else
        if (!temp_string.Find(wxT("dosbox_conf = ")))
          dosbox_conf = temp_string.Mid(14);
      else
        if (!temp_string.Find(wxT("dosbox_cdrom_location = ")))
          dosbox_cdrom_location = temp_string.Mid(24);
      else
        if (!temp_string.Find(wxT("dosbox_cdrom_image = ")))
          dosbox_cdrom_image = temp_string.Mid(21);
#ifndef __WXMSW__
      // Windows compatibility (e.g. Wine) settings
      else
        if (!temp_string.Find(wxT("wine = 1")))
          have_wine = true;
      else
        if (!temp_string.Find(wxT("wine = 0")))
          have_wine = false;
      else
        if (!temp_string.Find(wxT("wine_exec = ")))
          wine_exec = temp_string.Mid(12);
#endif
      // Multiplayer and network settings
      else
        if (!temp_string.Find(wxT("play_snd_join = 1")))
          play_snd_join = true;
      else
        if (!temp_string.Find(wxT("play_snd_join = 0")))
          play_snd_join = false;
      else
        if (!temp_string.Find(wxT("play_snd_leave = 1")))
          play_snd_leave = true;
      else
        if (!temp_string.Find(wxT("play_snd_leave = 0")))
          play_snd_leave = false;
      else
        if (!temp_string.Find(wxT("play_snd_send = 1")))
          play_snd_send = true;
      else
        if (!temp_string.Find(wxT("play_snd_send = 0")))
          play_snd_send = false;
      else
        if (!temp_string.Find(wxT("play_snd_receive = 1")))
          play_snd_receive = true;
      else
        if (!temp_string.Find(wxT("play_snd_receive = 0")))
          play_snd_receive = false;
      else
        if (!temp_string.Find(wxT("play_snd_error = 1")))
          play_snd_error = true;
      else
        if (!temp_string.Find(wxT("play_snd_error = 0")))
          play_snd_error = false;
      else
        if (!temp_string.Find(wxT("mute_sounds_while_ingame = 1")))
          mute_sounds_while_ingame = true;
      else
        if (!temp_string.Find(wxT("mute_sounds_while_ingame = 0")))
          mute_sounds_while_ingame = false;
      else
        if (!temp_string.Find(wxT("show_timestamp = 1")))
          show_timestamp = true;
      else
        if (!temp_string.Find(wxT("show_timestamp = 0")))
          show_timestamp = false;
      else
        if (!temp_string.Find(wxT("nickname = ")))
          nickname = temp_string.Mid(11);
      else
        if (!temp_string.Find(wxT("game_nickname = ")))
          game_nickname = temp_string.Mid(16);
      else
        if (!temp_string.Find(wxT("snd_join_file = ")))
          snd_join_file = temp_string.Mid(16);
      else
        if (!temp_string.Find(wxT("snd_leave_file = ")))
          snd_leave_file = temp_string.Mid(17);
      else
        if (!temp_string.Find(wxT("snd_send_file = ")))
          snd_send_file = temp_string.Mid(16);
      else
        if (!temp_string.Find(wxT("snd_receive_file = ")))
          snd_receive_file = temp_string.Mid(19);
      else
        if (!temp_string.Find(wxT("snd_error_file = ")))
          snd_error_file = temp_string.Mid(17);
      else
        if (!temp_string.Find(wxT("game_port_number = ")))
        {
          if (!temp_string.Mid(19).ToLong(&game_port_number))
            game_port_number = DEFAULT_GAME_PORT_NUM;
        }
      else
        if (!temp_string.Find(wxT("server_port_number = ")))
        {
          if (!temp_string.Mid(21).ToLong(&server_port_number))
            server_port_number = DEFAULT_SERVER_PORT_NUM;
        }
      // Advanced options
      else
        if (!temp_string.Find(wxT("eduke32_userpath_use = 1")))
          eduke32_userpath_use = true;
      else
        if (!temp_string.Find(wxT("eduke32_userpath_use = 0")))
          eduke32_userpath_use = false;
      else
        if (!temp_string.Find(wxT("jfduke3d_userpath_use = 1")))
          jfduke3d_userpath_use = true;
      else
        if (!temp_string.Find(wxT("jfduke3d_userpath_use = 0")))
          jfduke3d_userpath_use = false;
      else
        if (!temp_string.Find(wxT("iduke3d_userpath_use = 1")))
          iduke3d_userpath_use = true;
      else
        if (!temp_string.Find(wxT("iduke3d_userpath_use = 0")))
          iduke3d_userpath_use = false;
      else
        if (!temp_string.Find(wxT("jfsw_userpath_use = 1")))
          jfsw_userpath_use = true;
      else
        if (!temp_string.Find(wxT("jfsw_userpath_use = 0")))
          jfsw_userpath_use = false;
      else
        if (!temp_string.Find(wxT("eduke32_userpath = ")))
          eduke32_userpath = temp_string.Mid(19);
      else
        if (!temp_string.Find(wxT("jfduke3d_userpath = ")))
          jfduke3d_userpath = temp_string.Mid(20);
      else
        if (!temp_string.Find(wxT("iduke3d_userpath = ")))
          iduke3d_userpath = temp_string.Mid(19);
      else
        if (!temp_string.Find(wxT("jfsw_userpath = ")))
          jfsw_userpath = temp_string.Mid(16);
      else
        if (!temp_string.Find(wxT("override_browser = 1")))
          override_browser = true;
      else
        if (!temp_string.Find(wxT("override_browser = 0")))
          override_browser = false;
      else
        if (!temp_string.Find(wxT("browser_exec = ")))
          browser_exec = temp_string.Mid(15);
      else
        if (!temp_string.Find(wxT("override_soundcmd = 1")))
          override_soundcmd = true;
      else
        if (!temp_string.Find(wxT("override_soundcmd = 0")))
          override_soundcmd = false;
      else
        if (!temp_string.Find(wxT("playsound_cmd = ")))
          playsound_cmd = temp_string.Mid(16);
      else
        if (!temp_string.Find(wxT("enable_localip_optimization = 1")))
          enable_localip_optimization = true;
      else
        if (!temp_string.Find(wxT("enable_localip_optimization = 0")))
          enable_localip_optimization = false;
#ifndef __WXMSW__
      else
        if (!temp_string.Find(wxT("terminal_fullcmd = ")))
          terminal_fullcmd = temp_string.Mid(19);
#endif
      // A host in-room option
      else
        if (!temp_string.Find(wxT("host_autoaccept_downloads = 1")))
          host_autoaccept_downloads = true;
      else
        if (!temp_string.Find(wxT("host_autoaccept_downloads = 0")))
          host_autoaccept_downloads = false;
      else
        if (!temp_string.Find(wxT("lookup_on_startup = 1")))
          lookup_on_startup = true;
      else
        if (!temp_string.Find(wxT("lookup_on_startup = 0")))
          lookup_on_startup = false;
      // UPDATE: THE RELEVANT ERROR MAY STILL BE REPRODUCED.
      // Adding the "else" keyword would soon cause the following error on MSVC++ 2005 Express:
      // "compiler limit : blocks nested too deeply"
      // For that reason we stop here. BUT temp_string might be modified.
      // For that reason, every place where it may be modified is below. The rest is above.

      // MOD files
//    else
        if (!temp_string.Find(wxT("dn3d_modfiles = ")))
        {
          l_temp_int1 = 16;
          l_temp_int2 = 0;
          l_temp_length = temp_string.Len();
          while (l_temp_int1 < l_temp_length)
          {
            if (temp_string[l_temp_int1] != wxT('"'))
              do
              {
                l_temp_int1++;
              }
              while ((l_temp_int1 < l_temp_length)
                     && (temp_string[l_temp_int1] != wxT('"')));
            if (l_temp_int1 < l_temp_length)
            {
              l_temp_int2 = l_temp_int1 + 1;
              while ((l_temp_int2 < l_temp_length)
                     && (temp_string[l_temp_int2] != wxT('"')))
                l_temp_int2++;
              if (l_temp_int2 < l_temp_length)
                gamelaunch_dn3dmodfiles.Add(temp_string.Mid(l_temp_int1+1, l_temp_int2-l_temp_int1-1));
              l_temp_int1 = l_temp_int2 + 1;
            }
          }
          // BACKWARDS COMPATIBILITY
          if ((!l_temp_int2) && (l_temp_length > 16) &&
              (temp_string.Mid(16).ToLong(&l_temp_long1)))
          { // Let's count l_temp_long1 lines which aren't comments.
            while ((l_temp_long1 > 0) && (l_loop_var < l_num_of_lines))
            {
              l_loop_var++;
              if (l_loop_var < l_num_of_lines)
              {
                temp_string = l_settingsfile[l_loop_var];
                if (temp_string.Left(1) != wxT("#")) // Not a comment?
                {
                  l_temp_long1--;
                  gamelaunch_dn3dmodfiles.Add(temp_string);
                }
              }
            }
          }
        }
      else
        if (!temp_string.Find(wxT("sw_modfiles = ")))
        {
          l_temp_int1 = 14;
          l_temp_int2 = 0;
          l_temp_length = temp_string.Len();
          while (l_temp_int1 < l_temp_length)
          {
            if (temp_string[l_temp_int1] != wxT('"'))
              do
              {
                l_temp_int1++;
              }
              while ((l_temp_int1 < l_temp_length)
                     && (temp_string[l_temp_int1] != wxT('"')));
            if (l_temp_int1 < l_temp_length)
            {
              l_temp_int2 = l_temp_int1 + 1;
              while ((l_temp_int2 < l_temp_length)
                     && (temp_string[l_temp_int2] != wxT('"')))
                l_temp_int2++;
              if (l_temp_int2 < l_temp_length)
                gamelaunch_dn3dmodfiles.Add(temp_string.Mid(l_temp_int1+1, l_temp_int2-l_temp_int1-1));
              l_temp_int1 = l_temp_int2 + 1;
            }
          }
          // BACKWARDS COMPATIBILITY
          if ((!l_temp_int2) && (l_temp_length > 14) &&
              (temp_string.Mid(14).ToLong(&l_temp_long1)))
          { // Let's count l_temp_log1 lines which aren't comments.
            while ((l_temp_long1 > 0) && (l_loop_var < l_num_of_lines))
            {
              l_loop_var++;
              if (l_loop_var < l_num_of_lines)
              {
                temp_string = l_settingsfile[l_loop_var];
                if (temp_string.Left(1) != wxT("#")) // Not a comment?
                {
                  l_temp_long1--;
                  gamelaunch_swmodfiles.Add(temp_string);
                }
              }
            }
          }
        }
      // Extra server lists.
      else
        if (!temp_string.Find(wxT("extra_serverlists = ")))
        {
          l_temp_int1 = 20;
          l_temp_int2 = 0;
          l_temp_length = temp_string.Len();
          while (l_temp_int1 < l_temp_length)
          {
            if (temp_string[l_temp_int1] != wxT('"'))
              do
              {
                l_temp_int1++;
              }
              while ((l_temp_int1 < l_temp_length)
                     && (temp_string[l_temp_int1] != wxT('"')));
            if (l_temp_int1 < l_temp_length)
            {
              l_hostaddr = wxEmptyString;
              l_temp_int1++;
              while ((l_temp_int1 < l_temp_length)
                     && (temp_string[l_temp_int1] != wxT(':')))
                if (temp_string[l_temp_int1] == wxT('\\'))
                {
                  l_temp_int1++;
                  if (l_temp_int1 < l_temp_length)
                  {
                    l_hostaddr << temp_string[l_temp_int1];
                    l_temp_int1++;
                  }
                }
                else
                {
                  l_hostaddr << temp_string[l_temp_int1];
                  l_temp_int1++;
                }
              if (l_temp_int1 < l_temp_length)
              {
                l_temp_int2 = l_temp_int1 + 1;
                while ((l_temp_int2 < l_temp_length)
                       && (temp_string[l_temp_int2] != wxT('"')))
                  l_temp_int2++;
                if (l_temp_int2 < l_temp_length)
                {
                  extraservers_addrs.Add(l_hostaddr);
                  if ((temp_string.Mid(l_temp_int1+1, l_temp_int2-l_temp_int1-1)).ToLong(&l_temp_long1))
                    extraservers_portnums.Add(l_temp_long1);
                  else
                    extraservers_portnums.Add(0);
                }
                l_temp_int1 = l_temp_int2 + 1;
              }
            }
          }
        }
      // Aliases, IP addresses and port numbers for manual joining.
      else
        if (!temp_string.Find(wxT("manualjoin_items = ")))
        {
          l_temp_int1 = 19;
          l_temp_int2 = 0;
          l_temp_length = temp_string.Len();
          while (l_temp_int1 < l_temp_length)
          {
            if (temp_string[l_temp_int1] != wxT('"'))
              do
              {
                l_temp_int1++;
              }
              while ((l_temp_int1 < l_temp_length)
                     && (temp_string[l_temp_int1] != wxT('"')));
            if (l_temp_int1 < l_temp_length)
            {
              l_nickname = wxEmptyString;
              l_temp_int1++;
              while ((l_temp_int1 < l_temp_length)
                     && (temp_string[l_temp_int1] != wxT(':')))
                if (temp_string[l_temp_int1] == wxT('\\'))
                {
                  l_temp_int1++;
                  if (l_temp_int1 < l_temp_length)
                  {
                    l_nickname << temp_string[l_temp_int1];
                    l_temp_int1++;
                  }
                }
                else
                {
                  l_nickname << temp_string[l_temp_int1];
                  l_temp_int1++;
                }
              if (l_temp_int1 < l_temp_length)
              {
                l_hostaddr = wxEmptyString;
                l_temp_int1++;
                while ((l_temp_int1 < l_temp_length)
                       && (temp_string[l_temp_int1] != wxT(':')))
                  if (temp_string[l_temp_int1] == wxT('\\'))
                  {
                    l_temp_int1++;
                    if (l_temp_int1 < l_temp_length)
                    {
                      l_hostaddr << temp_string[l_temp_int1];
                      l_temp_int1++;
                    }
                  }
                  else
                  {
                    l_hostaddr << temp_string[l_temp_int1];
                    l_temp_int1++;
                  }
                if (l_temp_int1 < l_temp_length)
                {
                  l_temp_int2 = l_temp_int1 + 1;
                  while ((l_temp_int2 < l_temp_length)
                         && (temp_string[l_temp_int2] != wxT('"')))
                    l_temp_int2++;
                  if (l_temp_int2 < l_temp_length)
                  {
                    manualjoin_names.Add(l_nickname);
                    manualjoin_addrs.Add(l_hostaddr);
                    if ((temp_string.Mid(l_temp_int1+1, l_temp_int2-l_temp_int1-1)).ToLong(&l_temp_long1))
                      manualjoin_portnums.Add(l_temp_long1);
                    else
                      manualjoin_portnums.Add(server_port_number);
                  }
                  l_temp_int1 = l_temp_int2 + 1;
                }
              }
            }
          }

          // BACKWARDS COMPATIBILITY
          if ((!l_temp_int2) && (l_temp_length > 19) &&
              (temp_string.Mid(19).ToLong(&l_temp_long1)))
          { // Let's count 3*l_temp_long1 lines which aren't comments.
            while ((l_temp_long1 > 0) && (l_loop_var < l_num_of_lines))
            {
              l_loop_var++;
              if (l_loop_var < l_num_of_lines)
              {
                do
                {
                  l_temp_str1 = l_settingsfile[l_loop_var];
                  l_loop_var++;
                }
                while ((l_loop_var < l_num_of_lines) && (l_temp_str1.Left(1) == wxT("#"))); // A comment?
                if (l_loop_var < l_num_of_lines)
                {
                  do
                  {
                    l_temp_str2 = l_settingsfile[l_loop_var];
                    l_loop_var++;
                  }
                  while ((l_loop_var < l_num_of_lines) && (l_temp_str2.Left(1) == wxT("#")));
                  if (l_loop_var < l_num_of_lines)
                  {
                    do
                    {
                      temp_string = l_settingsfile[l_loop_var];
                      l_loop_var++;
                    }
                    while ((l_loop_var < l_num_of_lines) && (temp_string.Left(1) == wxT("#")));
                    if (temp_string.Left(1) != wxT("#"))
                    {
                      l_loop_var--; // Required to not miss the following line if there is any!
                      manualjoin_names.Add(l_temp_str1);
                      manualjoin_addrs.Add(l_temp_str2);
                      if (temp_string.ToLong(&l_temp_long2))
                        manualjoin_portnums.Add(l_temp_long2);
                      else
                        manualjoin_portnums.Add(server_port_number);
                      l_temp_long1--;
                    }
                  }
                }
              }
            }
          }
        }
      l_loop_var++;
    }
    l_settingsfile.Close();
    // Latest checks for all cases of invalid values (e.g. a skill of -2)

    if ((gamelaunch_dn3d_skill > DN3D_MAX_SKILLNUM) || (gamelaunch_dn3d_skill < 0))
      gamelaunch_dn3d_skill = DN3D_DEFAULT_SKILL;
    if ((gamelaunch_sw_skill > SW_MAX_SKILLNUM) || (gamelaunch_sw_skill < 0))
      gamelaunch_sw_skill = DN3D_DEFAULT_SKILL;
    if (
        (gamelaunch_dn3dlvl < 0) ||
        ((gamelaunch_dn3dlvl > 0) && (gamelaunch_dn3dlvl <= 100)) ||
        ((gamelaunch_dn3dlvl > 107) && (gamelaunch_dn3dlvl <= 200)) ||
        ((gamelaunch_dn3dlvl > 211) && (gamelaunch_dn3dlvl <= 300)) ||
        ((gamelaunch_dn3dlvl > 311) && (gamelaunch_dn3dlvl <= 400)) ||
        (gamelaunch_dn3dlvl > 411))
      gamelaunch_dn3dlvl = DN3D_DEFAULT_LEVEL;

    if ((gamelaunch_swlvl < 0) ||
        (gamelaunch_swlvl > 22))
      gamelaunch_swlvl = SW_DEFAULT_LEVEL;

    if (!gamelaunch_dn3dusermap.IsEmpty())
      if (!wxFileExists(dn3d_maps_dir + wxFILE_SEP_PATH + gamelaunch_dn3dusermap))
        gamelaunch_dn3dusermap = wxEmptyString;
    if (!gamelaunch_swusermap.IsEmpty())
      if (!wxFileExists(sw_maps_dir + wxFILE_SEP_PATH + gamelaunch_swusermap))
        gamelaunch_swusermap = wxEmptyString;

    return true;
  }
  return false;
}

#if (!((defined __WXMSW__) || (defined __WXMAC__) || (defined __WXCOCOA__)))
void LauncherConfig::DetectTerminal()
{
  if (wxFileExists(wxT("/usr/bin/gnome-terminal")))
    terminal_fullcmd = wxT("/usr/bin/gnome-terminal -x");
  else
    if (wxFileExists(wxT("/usr/bin/konsole")))
      terminal_fullcmd = wxT("/usr/bin/konsole -e");
  else
    if (wxFileExists(wxT("/usr/bin/xfce4-terminal")))
      terminal_fullcmd = wxT("/usr/bin/xfce4-terminal -x");
  else
    if (wxFileExists(wxT("/usr/bin/mlterm")))
      terminal_fullcmd = wxT("/usr/bin/mlterm -e");
  else
    if (wxFileExists(wxT("/usr/bin/xterm")))
      terminal_fullcmd = wxT("/usr/bin/xterm -e");
  else
    if (wxFileExists(wxT("/usr/bin/rxvt")))
      terminal_fullcmd = wxT("/usr/bin/rxvt -e");
  else
    if (wxFileExists(wxT("/usr/local/bin/gnome-terminal")))
      terminal_fullcmd = wxT("/usr/local/bin/gnome-terminal -x");
  else
    if (wxFileExists(wxT("/usr/local/bin/konsole")))
      terminal_fullcmd = wxT("/usr/local/bin/konsole -e");
  else
    if (wxFileExists(wxT("/usr/local/bin/xfce4-terminal")))
      terminal_fullcmd = wxT("/usr/local/bin/xfce4-terminal -x");
  else
    if (wxFileExists(wxT("/usr/local/bin/mlterm")))
      terminal_fullcmd = wxT("/usr/local/bin/mlterm -e");
  else
    if (wxFileExists(wxT("/usr/local/bin/xterm")))
      terminal_fullcmd = wxT("/usr/local/bin/xterm -e");
  else
    if (wxFileExists(wxT("/usr/local/bin/rxvt")))
      terminal_fullcmd = wxT("/usr/local/bin/rxvt -e");
  else
    if (wxFileExists(wxT("/usr/X11R6/bin/gnome-terminal")))
      terminal_fullcmd = wxT("/usr/X11R6/bin/gnome-terminal -x");
  else
    if (wxFileExists(wxT("/usr/X11R6/bin/konsole")))
      terminal_fullcmd = wxT("/usr/X11R6/bin/konsole -e");
  else
    if (wxFileExists(wxT("/usr/X11R6/bin/xfce4-terminal")))
      terminal_fullcmd = wxT("/usr/X11R6/bin/xfce4-terminal -x");
  else
    if (wxFileExists(wxT("/usr/X11R6/bin/mlterm")))
      terminal_fullcmd = wxT("/usr/X11R6/bin/mlterm -e");
  else
    if (wxFileExists(wxT("/usr/X11R6/bin/xterm")))
      terminal_fullcmd = wxT("/usr/X11R6/bin/xterm -e");
  else
    if (wxFileExists(wxT("/usr/X11R6/bin/rxvt")))
      terminal_fullcmd = wxT("/usr/X11R6/bin/rxvt -e");
  else
    if (wxFileExists(wxT("/usr/local/X11R6/bin/gnome-terminal")))
      terminal_fullcmd = wxT("/usr/local/X11R6/bin/gnome-terminal -x");
  else
    if (wxFileExists(wxT("/usr/local/X11R6/bin/konsole")))
      terminal_fullcmd = wxT("/usr/local/X11R6/bin/konsole -e");
  else
    if (wxFileExists(wxT("/usr/local/X11R6/bin/xfce4-terminal")))
      terminal_fullcmd = wxT("/usr/local/X11R6/bin/xfce4-terminal -x");
  else
    if (wxFileExists(wxT("/usr/local/X11R6/bin/mlterm")))
      terminal_fullcmd = wxT("/usr/local/X11R6/bin/mlterm -e");
  else
    if (wxFileExists(wxT("/usr/local/X11R6/bin/xterm")))
      terminal_fullcmd = wxT("/usr/local/X11R6/bin/xterm -e");
  else
    if (wxFileExists(wxT("/usr/local/X11R6/bin/rxvt")))
      terminal_fullcmd = wxT("/usr/local/X11R6/bin/rxvt -e");
  else
    if (wxFileExists(wxT("/usr/X11/bin/gnome-terminal")))
      terminal_fullcmd = wxT("/usr/X11/bin/gnome-terminal -x");
  else
    if (wxFileExists(wxT("/usr/X11/bin/konsole")))
      terminal_fullcmd = wxT("/usr/X11/bin/konsole -e");
  else
    if (wxFileExists(wxT("/usr/X11/bin/xfce4-terminal")))
      terminal_fullcmd = wxT("/usr/X11/bin/xfce4-terminal -x");
  else
    if (wxFileExists(wxT("/usr/X11/bin/mlterm")))
      terminal_fullcmd = wxT("/usr/X11/bin/mlterm -e");
  else
    if (wxFileExists(wxT("/usr/X11/bin/xterm")))
      terminal_fullcmd = wxT("/usr/X11/bin/xterm -e");
  else
    if (wxFileExists(wxT("/usr/X11/bin/rxvt")))
      terminal_fullcmd = wxT("/usr/X11/bin/rxvt -e");
  else
    if (wxFileExists(wxT("/usr/local/X11/bin/gnome-terminal")))
      terminal_fullcmd = wxT("/usr/local/X11/bin/gnome-terminal -x");
  else
    if (wxFileExists(wxT("/usr/local/X11/bin/konsole")))
      terminal_fullcmd = wxT("/usr/local/X11/bin/konsole -e");
  else
    if (wxFileExists(wxT("/usr/local/X11/bin/xfce4-terminal")))
      terminal_fullcmd = wxT("/usr/local/X11/bin/xfce4-terminal -x");
  else
    if (wxFileExists(wxT("/usr/local/X11/bin/mlterm")))
      terminal_fullcmd = wxT("/usr/local/X11/bin/mlterm -e");
  else
    if (wxFileExists(wxT("/usr/local/X11/bin/xterm")))
      terminal_fullcmd = wxT("/usr/local/X11/bin/xterm -e");
  else
    if (wxFileExists(wxT("/usr/local/X11/bin/rxvt")))
      terminal_fullcmd = wxT("/usr/local/X11/bin/rxvt -e");
}
#endif

bool LauncherConfig::Save()
{
#if 0
  if (!wxDirExists(*g_launcher_user_profile_path))
    if (!wxFileName::Mkdir(*g_launcher_user_profile_path, 0777, wxPATH_MKDIR_FULL))
      return false;
#endif
  wxTextFile settings_file;
  wxString l_str_line, l_str_curr;
  size_t l_loop_var, l_loop_var2, l_num_of_items, l_str_length;
// Future compatibility with wxWidgets v3.00.
#ifdef wxUSE_UNICODE_UTF8
  wxUniChar l_curr_char;
#else
  wxChar l_curr_char;
#endif
  if (((!wxFileExists(DUKEMATCHER_CONFIG_FULLFILEPATH)) || (wxRemoveFile(DUKEMATCHER_CONFIG_FULLFILEPATH)))
      && (settings_file.Create(DUKEMATCHER_CONFIG_FULLFILEPATH)))
  {
    settings_file.AddLine(wxT("# This is the DUKEMATCHER configuration file."));
    // Theme
    settings_file.AddLine(wxString::Format(wxT("theme_is_dark = %d"), theme_is_dark));
    // Check for updates on startup
    settings_file.AddLine(wxString::Format(wxT("check_for_updates_on_startup = %d"), check_for_updates_on_startup));
    // Single Player / Host settings
    switch (gamelaunch_source_port)
    {
//    case SOURCEPORT_EDUKE32: settings_file.AddLine(wxT("source_port = eduke32")); break;
      case SOURCEPORT_JFDUKE3D: settings_file.AddLine(wxT("source_port = jfduke3d")); break;
      case SOURCEPORT_IDUKE3D: settings_file.AddLine(wxT("source_port = iduke3d")); break;
      case SOURCEPORT_DUKE3DW32: settings_file.AddLine(wxT("source_port = duke3dw32")); break;
      case SOURCEPORT_XDUKE: settings_file.AddLine(wxT("source_port = xduke")); break;
      case SOURCEPORT_DOSDUKE: settings_file.AddLine(wxT("source_port = dosduke")); break;
       case SOURCEPORT_JFSW: settings_file.AddLine(wxT("source_port = jfsw")); break;
      case SOURCEPORT_SWP: settings_file.AddLine(wxT("source_port = swp")); break;
      case SOURCEPORT_DOSSW: settings_file.AddLine(wxT("source_port = dossw")); break;
      default: settings_file.AddLine(wxT("source_port = eduke32"));
    }
    settings_file.AddLine(wxString::Format(wxT("dn3d_skill = %d"), gamelaunch_dn3d_skill));
    settings_file.AddLine(wxString::Format(wxT("sw_skill = %d"), gamelaunch_sw_skill));
    settings_file.AddLine(wxT("args_in_sp = ") + gamelaunch_args_in_sp);
    settings_file.AddLine(wxString::Format(wxT("dn3d_level = %d"), gamelaunch_dn3dlvl));
    settings_file.AddLine(wxString::Format(wxT("sw_level = %d"), gamelaunch_swlvl));
    settings_file.AddLine(wxT("dn3d_usermap = ") + gamelaunch_dn3dusermap);
    settings_file.AddLine(wxT("sw_usermap = ") + gamelaunch_swusermap);

    settings_file.AddLine(wxT("room_name = ") + gamelaunch_roomname);
    settings_file.AddLine(wxString::Format(wxT("num_of_players = %d"), gamelaunch_numofplayers));
    switch (gamelaunch_dn3dgametype)
    {
      case DN3DMPGAMETYPE_DMSPAWN: settings_file.AddLine(wxT("dn3d_mpgametype = dm_spawn")); break;
      case DN3DMPGAMETYPE_COOP: settings_file.AddLine(wxT("dn3d_mpgametype = coop")); break;
      case DN3DMPGAMETYPE_DMNOSPAWN: settings_file.AddLine(wxT("dn3d_mpgametype = dm_nospawn")); break;
      case DN3DMPGAMETYPE_TDMSPAWN: settings_file.AddLine(wxT("dn3d_mpgametype = tdm_spawn")); break;
      default: settings_file.AddLine(wxT("dn3d_mpgametype = tdm_nospawn")); break;
    }
    switch (gamelaunch_dn3dspawn)
    {
      case DN3DSPAWN_MONSTERS: settings_file.AddLine(wxT("dn3d_spawntype = monsters")); break;
      case DN3DSPAWN_ITEMS: settings_file.AddLine(wxT("dn3d_spawntype = items")); break;
      case DN3DSPAWN_INVENTORY: settings_file.AddLine(wxT("dn3d_spawntype = inventory")); break;
      default: settings_file.AddLine(wxT("dn3d_spawntype = all"));
    }
        settings_file.AddLine(wxString::Format(wxT("frag_limit = %d"), gamelaunch_fraglimit));
	settings_file.AddLine(wxString::Format(wxT("no_team_change = %d"), gamelaunch_noteamchange));
	settings_file.AddLine(wxString::Format(wxT("force_respawn = %d"), gamelaunch_forcerespawn));
    settings_file.AddLine(wxT("args_for_host = " + gamelaunch_args_for_host ));
    settings_file.AddLine(wxT("args_for_all = " + gamelaunch_args_for_all ));
    settings_file.AddLine(wxT("mod_name = " + gamelaunch_modname ));
    settings_file.AddLine(wxString::Format(wxT("show_mod_url = %d"), gamelaunch_show_modurl));
    settings_file.AddLine(wxT("mod_url = " + gamelaunch_modurl ));
    settings_file.AddLine(wxString::Format(wxT("use_masterslave = %d"), gamelaunch_use_masterslave));
    settings_file.AddLine(wxString::Format(wxT("advertise = %d"), gamelaunch_advertise));
    settings_file.AddLine(wxString::Format(wxT("skip_settings = %d"), gamelaunch_skip_settings));
    settings_file.AddLine(wxString::Format(wxT("last_serverlist_in_cycle = %d"), last_serverlist_in_cycle));
//  settings_file.AddLine(wxString::Format(wxT("enable_natfree = %d"), gamelaunch_enable_natfree));
    // MOD files
    l_num_of_items = gamelaunch_dn3dmodfiles.GetCount();
    l_str_line = wxT("dn3d_modfiles =");
    for (l_loop_var = 0; l_loop_var < l_num_of_items; l_loop_var++)
      l_str_line << wxT(" \"") << gamelaunch_dn3dmodfiles[l_loop_var] << wxT('"');
    settings_file.AddLine(l_str_line);

    l_num_of_items = gamelaunch_swmodfiles.GetCount();
    l_str_line = wxT("sw_modfiles =");
    for (l_loop_var = 0; l_loop_var < l_num_of_items; l_loop_var++)
      l_str_line << wxT(" \"") << gamelaunch_swmodfiles[l_loop_var] << wxT('"');
    settings_file.AddLine(l_str_line);
    // Source ports settings: Duke Nukem 3D
    settings_file.AddLine(wxString::Format(wxT("eduke32 = %d"), have_eduke32));
    settings_file.AddLine(wxT("eduke32_exec = ") + eduke32_exec);
    settings_file.AddLine(wxString::Format(wxT("jfduke3d = %d"), have_jfduke3d));
    settings_file.AddLine(wxT("jfduke3d_exec = ") + jfduke3d_exec);
    settings_file.AddLine(wxString::Format(wxT("iduke3d = %d"), have_iduke3d));
    settings_file.AddLine(wxT("iduke3d_exec = ") + iduke3d_exec);
    settings_file.AddLine(wxString::Format(wxT("duke3dw32 = %d"), have_duke3dw32));
    settings_file.AddLine(wxT("duke3dw32_exec = ") + duke3dw32_exec);
    settings_file.AddLine(wxString::Format(wxT("xduke = %d"), have_xduke));
    settings_file.AddLine(wxT("xduke_exec = ") + xduke_exec);
    settings_file.AddLine(wxString::Format(wxT("dosduke = %d"), have_dosduke));
    settings_file.AddLine(wxT("dosduke_exec = ") + dosduke_exec);
    settings_file.AddLine(wxT("dn3d_maps_dir = ") + dn3d_maps_dir);
    settings_file.AddLine(wxT("dn3d_mods_dir = ") + dn3d_mods_dir);
    // Source ports settings: Shadow Warrior
    settings_file.AddLine(wxString::Format(wxT("jfsw = %d"), have_jfsw));
    settings_file.AddLine(wxT("jfsw_exec = ") + jfsw_exec);
    settings_file.AddLine(wxString::Format(wxT("swp = %d"), have_swp));
    settings_file.AddLine(wxT("swp_exec = ") + swp_exec);
    settings_file.AddLine(wxString::Format(wxT("dossw = %d"), have_dossw));
    settings_file.AddLine(wxT("dossw_exec = ") + dossw_exec);
    settings_file.AddLine(wxT("sw_maps_dir = ") + sw_maps_dir);
    settings_file.AddLine(wxT("sw_mods_dir = ") + sw_mods_dir);
    // DOSBox settings
    settings_file.AddLine(wxString::Format(wxT("dosbox = %d"), have_dosbox));
    settings_file.AddLine(wxString::Format(wxT("dosbox_use_conf = %d"), dosbox_use_conf));
    switch (dosbox_cdmount)
    {
      case CDROMMOUNT_NONE: settings_file.AddLine(wxT("dosbox_cdrom_mount = none")); break;
      case CDROMMOUNT_DIR: settings_file.AddLine(wxT("dosbox_cdrom_mount = dir")); break;
      default: /*CDROMMOUNT_IMG*/ settings_file.AddLine(wxT("dosbox_cdrom_mount = image"));
    }
    settings_file.AddLine(wxT("dosbox_exec = ") + dosbox_exec);
    settings_file.AddLine(wxT("dosbox_conf = ") + dosbox_conf);
    settings_file.AddLine(wxT("dosbox_cdrom_location = ") + dosbox_cdrom_location);
    settings_file.AddLine(wxT("dosbox_cdrom_image = ") + dosbox_cdrom_image);
#ifndef __WXMSW__
    // Windows compatibility (e.g. Wine) settings
    settings_file.AddLine(wxString::Format(wxT("wine = %d"), have_wine));
    settings_file.AddLine(wxT("wine_exec = ") + wine_exec);
#endif
    // Multiplayer and network settings
    settings_file.AddLine(wxString::Format(wxT("play_snd_join = %d"), play_snd_join));
    settings_file.AddLine(wxT("snd_join_file = ") + snd_join_file);
    settings_file.AddLine(wxString::Format(wxT("play_snd_leave = %d"), play_snd_leave));
    settings_file.AddLine(wxT("snd_leave_file = ") + snd_leave_file);
    settings_file.AddLine(wxString::Format(wxT("play_snd_send = %d"), play_snd_send));
    settings_file.AddLine(wxT("snd_send_file = ") + snd_send_file);
    settings_file.AddLine(wxString::Format(wxT("play_snd_receive = %d"), play_snd_receive));
    settings_file.AddLine(wxT("snd_receive_file = ") + snd_receive_file);
    settings_file.AddLine(wxString::Format(wxT("play_snd_error = %d"), play_snd_error));
    settings_file.AddLine(wxT("snd_error_file = ") + snd_error_file);
    settings_file.AddLine(wxString::Format(wxT("mute_sounds_while_ingame = %d"), mute_sounds_while_ingame));
    settings_file.AddLine(wxT("nickname = ") + nickname);
    settings_file.AddLine(wxT("game_nickname = ") + game_nickname);
	settings_file.AddLine(wxString::Format(wxT("show_timestamp = %d"), show_timestamp));
    settings_file.AddLine(wxString::Format(wxT("game_port_number = %d"), game_port_number));
    settings_file.AddLine(wxString::Format(wxT("server_port_number = %d"), server_port_number));
    // Advanced options
    settings_file.AddLine(wxString::Format(wxT("eduke32_userpath_use = %d"), eduke32_userpath_use));
    settings_file.AddLine(wxT("eduke32_userpath = ") + eduke32_userpath);
    settings_file.AddLine(wxString::Format(wxT("jfduke3d_userpath_use = %d"), jfduke3d_userpath_use));
    settings_file.AddLine(wxT("jfduke3d_userpath = ") + jfduke3d_userpath);
    settings_file.AddLine(wxString::Format(wxT("iduke3d_userpath_use = %d"), iduke3d_userpath_use));
    settings_file.AddLine(wxT("iduke3d_userpath = ") + iduke3d_userpath);
    settings_file.AddLine(wxString::Format(wxT("jfsw_userpath_use = %d"), jfsw_userpath_use));
    settings_file.AddLine(wxT("jfsw_userpath = ") + jfsw_userpath);
    settings_file.AddLine(wxString::Format(wxT("override_browser = %d"), override_browser));
    settings_file.AddLine(wxT("browser_exec = ") + browser_exec);
    settings_file.AddLine(wxString::Format(wxT("override_soundcmd = %d"), override_soundcmd));
    settings_file.AddLine(wxT("playsound_cmd = ") + playsound_cmd);
    settings_file.AddLine(wxString::Format(wxT("enable_localip_optimization = %d"), enable_localip_optimization));
#ifndef __WXMSW__
    settings_file.AddLine(wxT("terminal_fullcmd = ") + terminal_fullcmd);
#endif
    settings_file.AddLine(wxString::Format(wxT("host_autoaccept_downloads = %d"), host_autoaccept_downloads));
    // Extra server lists.
    l_num_of_items = extraservers_portnums.GetCount();
    l_str_line = wxT("extra_serverlists =");
    for (l_loop_var = 0; l_loop_var < l_num_of_items; l_loop_var++)
    {
      l_str_line << wxT(" \"");
      l_str_curr = extraservers_addrs[l_loop_var];
      l_str_length = l_str_curr.Len();
      for (l_loop_var2 = 0; l_loop_var2 < l_str_length; l_loop_var2++)
      {
        l_curr_char = l_str_curr[l_loop_var2];
        if ((l_curr_char == wxT(':')) ||
            (l_curr_char == wxT('"')) || (l_curr_char == wxT('\\')))
          l_str_line << wxT('\\');
        l_str_line << l_curr_char;
      }
      l_str_line << wxString::Format(wxT(":%d\""), extraservers_portnums[l_loop_var]);
    }
    settings_file.AddLine(l_str_line);
    // Main window: Retrieve rooms on startup, and stuff for manual joining.
    settings_file.AddLine(wxString::Format(wxT("lookup_on_startup = %d"), lookup_on_startup));

    l_num_of_items = manualjoin_portnums.GetCount();
    l_str_line = wxT("manualjoin_items =");
    for (l_loop_var = 0; l_loop_var < l_num_of_items; l_loop_var++)
    {
      l_str_line << wxT(" \"");
      l_str_curr = manualjoin_names[l_loop_var];
      l_str_length = l_str_curr.Len();
      for (l_loop_var2 = 0; l_loop_var2 < l_str_length; l_loop_var2++)
      {
        l_curr_char = l_str_curr[l_loop_var2];
        if ((l_curr_char == wxT(':')) ||
            (l_curr_char == wxT('"')) || (l_curr_char == wxT('\\')))
          l_str_line << wxT('\\');
        l_str_line << l_curr_char;
      }
      l_str_line << wxT(':');
      l_str_curr = manualjoin_addrs[l_loop_var];
      l_str_length = l_str_curr.Len();
      for (l_loop_var2 = 0; l_loop_var2 < l_str_length; l_loop_var2++)
      {
        l_curr_char = l_str_curr[l_loop_var2];
        if ((l_curr_char == wxT(':')) ||
            (l_curr_char == wxT('"')) || (l_curr_char == wxT('\\')))
          l_str_line << wxT('\\');
        l_str_line << l_curr_char;
      }
      l_str_line << wxString::Format(wxT(":%d\""), manualjoin_portnums[l_loop_var]);
    }
    settings_file.AddLine(l_str_line);
    settings_file.Write();
    settings_file.Close();
    return true;
  }
  return false;
}

