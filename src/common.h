/*************************************************************************

Copyright 2008-2009 - NY00123

This file is part of DUKEMATCHER.

DUKEMATCHER is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

DUKEMATCHER is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DUKEMATCHER; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*************************************************************************/

#ifndef _DUKEMATCHER_COMMON_H_
#define _DUKEMATCHER_COMMON_H_

#define DUKEMATCHER_VERSION wxT("1.4 (unofficial edit)")
#define DUKEMATCHER_REVISION wxT("0") 

#define NICK_MAX_LENGTH 32
#define GAMENICK_MAX_LENGTH 10

#define MAX_NUM_PLAYERS 8
#define DEFAULT_NUM_PLAYERS 8

// Old definitions. Keep for compatibility with older versions.
#define DUKEMATCHER_STR_VERSION wxT("0.48n")
#define DUKEMATCHER_STR_NET_VERSION wxT("461")
#define DUKEMATCHERMASTER_STR_NET_VERSION wxT("101")

#endif
