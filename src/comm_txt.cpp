/*************************************************************************

Copyright 2008-2009 - NY00123

This file is part of DUKEMATCHER.

DUKEMATCHER is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

DUKEMATCHER is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DUKEMATCHER; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*************************************************************************/

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
//#include <wx/wx.h> // CAUSES ERRORS ON COMPILATION OF THE MASTER SERVER! (wxBase only)
#include <wx/socket.h>
#endif

#include <string.h>
#include "comm_txt.h"

// Routines to convert between a local wxArrayString list of strings
// and a string received/sent via a TCP/IP socket.

int l_FillBuffer(const char* input_str, char output_buffer[MAX_COMM_TEXT_LENGTH])
{
  size_t l_buff_length = strlen(input_str);
  if (l_buff_length <= MAX_COMM_TEXT_LENGTH)
  {
    memcpy(output_buffer, input_str, l_buff_length);
    return l_buff_length;
  }
  else
  {
    memcpy(output_buffer, input_str, MAX_COMM_TEXT_LENGTH);
    return 0;
  }
}

int g_ConvertStringListToCommText(const wxArrayString& str_list, char buffer[MAX_COMM_TEXT_LENGTH])
{
  wxString l_curr_str;
// Future compatibility with wxWidgets v3.00.
#ifdef wxUSE_UNICODE_UTF8
  wxUniChar l_curr_char;
#else
  wxChar l_curr_char;
#endif
  size_t l_str_loop_var, l_char_loop_var,
         l_num_of_strings = str_list.GetCount(), l_num_of_chars;
  wxString l_final_str = wxString::Format(wxT("%d:"), l_num_of_strings);
  for (l_str_loop_var = 0; l_str_loop_var < l_num_of_strings; l_str_loop_var++)
  {
    l_curr_str = str_list[l_str_loop_var];
    l_num_of_chars = l_curr_str.Len();
    for (l_char_loop_var = 0; l_char_loop_var < l_num_of_chars; l_char_loop_var++)
    {
      l_curr_char = l_curr_str[l_char_loop_var];
      if (l_curr_char == wxT('\\'))
        l_final_str << wxT("\\\\");
      else
        if (l_curr_char == wxT('\n'))
          l_final_str << wxT("\\n");
      else
        if (l_curr_char == wxT('\t'))
          l_final_str << wxT("\\t");
      else
        if (l_curr_char == wxT('\f'))
          l_final_str << wxT("\\f");
      else
        if (l_curr_char == wxT(':'))
          l_final_str << wxT("\\:");
      else
        l_final_str << l_curr_char;
    }
    l_final_str << wxT(':');
  }
  return (l_FillBuffer(l_final_str.mb_str(wxConvUTF8), buffer));
}
/*
int g_TestStringListToCommTextConversion(const wxArrayString& str_list)
{
  char buffer[MAX_COMM_TEXT_LENGTH];
  return g_ConvertStringListToCommText(str_list, buffer);
}

wxArrayString g_ConvertCommTextToStringList(char buffer[MAX_COMM_TEXT_LENGTH], size_t length)
{
  wxString l_initial_str, l_num_items_str, l_curr_str;
#ifdef wxUSE_UNICODE_UTF8
  wxUniChar l_curr_char = wxT('A');
#else
  wxChar l_curr_char = wxT('A');
#endif
  wxArrayString l_str_list;
  size_t l_char_loop_var = 0, l_str_length;
//       l_last_pos = MAX_COMM_TEXT_LENGTH-1;
  long l_num_of_strings_left = 0;
  // In order to avoid an "endless" loop!
//char l_buffer[MAX_COMM_TEXT_LENGTH];
//for (l_char_loop_var = 0; l_char_loop_var < l_last_pos; l_char_loop_var++)
//  l_buffer[l_char_loop_var] = buffer[l_char_loop_var];
//l_buffer[l_last_pos] = '\0';
//l_initial_str = wxString(l_buffer, wxConvUTF8);
  l_initial_str = wxString(buffer, wxConvUTF8, length);
//wxMessageBox(l_initial_str);
  l_str_length = l_initial_str.Len();
  while ((l_char_loop_var < l_str_length) && (l_curr_char != wxT(':')))
  {
    l_curr_char = l_initial_str[l_char_loop_var];
    if (l_curr_char == wxT(':'))
    {
      if (!l_num_items_str.ToLong(&l_num_of_strings_left))
        return wxArrayString();
      if (l_num_of_strings_left <= 0)
        return wxArrayString();
    }
    else
      l_num_items_str << l_curr_char;
    l_char_loop_var++;
  }
  if (!l_num_of_strings_left)
    return wxArrayString();
  while ((l_char_loop_var < l_str_length) && (l_num_of_strings_left > 0))
  {
    l_curr_char = l_initial_str[l_char_loop_var];
    if (l_curr_char == wxT('\\'))
    {
      l_char_loop_var++;
      if (l_char_loop_var < l_str_length)
      {
        l_curr_char = l_initial_str[l_char_loop_var];
        if (l_curr_char == wxT('n'))
          l_curr_str << wxT('\n');
        else
          if (l_curr_char == wxT('t'))
            l_curr_str << wxT('\t');
        else
          if (l_curr_char == wxT('f'))
            l_curr_str << wxT('\f');
        else
          if (l_curr_char == wxT(':'))
            l_curr_str << wxT(':');
        else // Includes the case of a backslash.
          l_curr_str << l_curr_char;
        l_char_loop_var++;
      }
    }
    else
    {
      if (l_curr_char == wxT(':'))
      {
        l_str_list.Add(l_curr_str);
        l_curr_str = wxEmptyString;
        l_num_of_strings_left--;
      }
      else
        l_curr_str << l_curr_char;
      l_char_loop_var++;
    }
  }
  if (!l_curr_str.IsEmpty())
    l_str_list.Add(l_curr_str);
  return l_str_list;
}
*/
void g_SendStringListToSocket(wxSocketBase* socket, const wxArrayString& str_list)
{
  char l_buffer[MAX_COMM_TEXT_LENGTH];
  int l_length = g_ConvertStringListToCommText(str_list, l_buffer);
  if (l_length)
    socket->Write(l_buffer, l_length);
  else
    socket->Write(l_buffer, MAX_COMM_TEXT_LENGTH);
}

/* *** A DEFINITION IS NOW USED IN THE HEADER FILE. ***
void g_SendCStringToSocket(wxSocketBase* socket, const char* str)
{
  socket->Write(str, strlen(str));
}

wxArrayString g_GetStringListFromSocket(wxSocketBase *socket)
{
  char l_buffer[MAX_COMM_TEXT_LENGTH];
  socket->Read(l_buffer, MAX_COMM_TEXT_LENGTH);
  return g_ConvertCommTextToStringList(l_buffer, socket->LastCount());
}
*/
size_t g_ConvertStringBufToStringList(wxString& str, wxArrayString& str_list)
{
  wxString l_num_items_str, l_curr_str;
#ifdef wxUSE_UNICODE_UTF8
  wxUniChar l_curr_char = wxT('A');
#else
  wxChar l_curr_char = wxT('A');
#endif
  wxArrayString l_str_list;
  size_t l_char_loop_var = 0, l_str_length, l_final_num_of_strings;
  long l_num_of_strings_left = 0;
//str = wxString(buffer, wxConvUTF8, length);
  l_str_length = str.Len();
  while ((l_char_loop_var < l_str_length) && (l_curr_char != wxT(':')))
  {
    l_curr_char = str[l_char_loop_var];
    if (l_curr_char == wxT(':'))
    {
      if (!l_num_items_str.ToLong(&l_num_of_strings_left))
      {
        str = wxEmptyString;
        return 0;
      }
      if (l_num_of_strings_left <= 0)
      {
        str = wxEmptyString;
        return 0;
      }
    }
    else
      l_num_items_str << l_curr_char;
    l_char_loop_var++;
  }
  if (!l_num_of_strings_left)
  {
    str = wxEmptyString;
    return 0;
  }
  l_final_num_of_strings = l_num_of_strings_left;
  while ((l_char_loop_var < l_str_length) && (l_num_of_strings_left > 0))
  {
    l_curr_char = str[l_char_loop_var];
    if (l_curr_char == wxT('\\'))
    {
      l_char_loop_var++;
      if (l_char_loop_var < l_str_length)
      {
        l_curr_char = str[l_char_loop_var];
        if (l_curr_char == wxT('n'))
          l_curr_str << wxT('\n');
        else
          if (l_curr_char == wxT('t'))
            l_curr_str << wxT('\t');
        else
          if (l_curr_char == wxT('f'))
            l_curr_str << wxT('\f');
        else // Includes the cases of a backslash '\\' and a colon ':'.
          l_curr_str << l_curr_char;
        l_char_loop_var++;
      }
    }
    else
    {
      if (l_curr_char == wxT(':'))
      {
        l_str_list.Add(l_curr_str);
        l_curr_str = wxEmptyString;
        l_num_of_strings_left--;
      }
      else
        l_curr_str << l_curr_char;
      l_char_loop_var++;
    }
  }
  if (l_num_of_strings_left > 0)
    return 0;
  str_list = l_str_list;
  if (l_char_loop_var < l_str_length)
    str = str.Mid(l_char_loop_var);
  else
    str = wxEmptyString;
  return l_final_num_of_strings;
}
