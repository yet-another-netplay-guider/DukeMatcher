/*************************************************************************

Copyright 2008-2009 - NY00123

This file is part of DUKEMATCHER.

DUKEMATCHER is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

DUKEMATCHER is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DUKEMATCHER; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*************************************************************************/

#ifndef _DUKEMATCHER_MPDIALOG_H_
#define _DUKEMATCHER_MPDIALOG_H_

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "theme.h"

#define ID_MPSELECTGAME 1000
#define ID_MPSELECTSRCPORT 1001
#define ID_MPRECORDDEMO 1002
#define ID_MPADVERTISE 1003
#define ID_MPUSESETTINGS 1004
//#define ID_MPFORCEIP 1005
#define ID_MPUSEEPIMAP 1006
#define ID_MPUSEUSERMAP 1007
#define ID_MPUSEMODFILE 1008
#define ID_MPREMMODFILE 1009
#define ID_MPUSEMODURL 1010

class MPDialog : public DUKEMATCHERDialog
{
public:
  MPDialog();
//~MPDialog();

  void OnGameSelect(wxCommandEvent& event);
  void OnSrcPortSelect(wxCommandEvent& event);
  void OnCheckBoxClick(wxCommandEvent& event);
  void OnRadioBtnClick(wxCommandEvent& event);

  void OnAddModFile(wxCommandEvent& event);
  void OnRemModFile(wxCommandEvent& event);

  void OnCancel(wxCommandEvent& event);
  void OnOk(wxCommandEvent& event);
  void ApplySettings();

private:
  void PartialDialogControlsReset();

  DUKEMATCHERPanel* m_mppanel;
  wxStaticText* m_mpgamestaticText;
  DUKEMATCHERChoice* m_mpgamechoice;
  wxStaticText* m_mpsourceportstaticText;
  DUKEMATCHERChoice* m_mpsourceportchoice;
  wxStaticText* m_mproomnamestaticText;
  DUKEMATCHERTextCtrl* m_mproomnametextCtrl;
  wxStaticText* m_mpmaxnumplayersstaticText;
  DUKEMATCHERChoice* m_mpmaxnumplayerschoice;
  wxStaticText* m_mpskillstaticText;
  DUKEMATCHERChoice* m_mpskillchoice;
  wxStaticText* m_mpgametypestaticText;
  DUKEMATCHERChoice* m_mpgametypechoice;
  wxStaticText* m_mpspawnstaticText;
  DUKEMATCHERChoice* m_mpspawnchoice;
  wxStaticText* m_mpfraglimitstaticText;
  DUKEMATCHERTextCtrl* m_mpfraglimittextCtrl;
  DUKEMATCHERCheckBox* m_mprecorddemocheckBox;
  DUKEMATCHERTextCtrl* m_mprecorddemotextCtrl;
  DUKEMATCHERCheckBox* m_mpnoteamchangecheckBox;
  DUKEMATCHERCheckBox* m_mpforcerespawncheckBox;
  wxStaticText* m_mpextrahostargsstaticText;
  DUKEMATCHERTextCtrl* m_mpextrahostargstextCtrl;
  wxStaticText* m_mpextraallargsstaticText;
  DUKEMATCHERTextCtrl* m_mpextraallargstextCtrl;
  DUKEMATCHERCheckBox* m_mpadvertiseroomcheckBox;
  DUKEMATCHERCheckBox* m_mpnodialogcheckBox;
/*DUKEMATCHERCheckBox* m_mpforceipcheckBox;
  wxComboBox* m_mpforceipcomboBox;*/
  DUKEMATCHERRadioButton* m_mpepimapradioBtn;
  DUKEMATCHERChoice* m_mpepimapchoice;
  DUKEMATCHERRadioButton* m_mpusermapradioBtn;
  DUKEMATCHERChoice* m_mpusermapchoice;
  wxStaticText* m_mpmodstaticText;
  DUKEMATCHERListBox* m_mpavailmodfileslistBox;
  DUKEMATCHERListBox* m_mpusedmodfileslistBox;
  DUKEMATCHERButton* m_mpusemodfilebutton;
  DUKEMATCHERButton* m_mpremmodfilebutton;
  wxStaticText* m_mpmodnamestaticText;
  DUKEMATCHERTextCtrl* m_mpmodnametextCtrl;
  DUKEMATCHERCheckBox* m_mpmodurlcheckBox;
  DUKEMATCHERTextCtrl* m_mpmodurltextCtrl;
  DUKEMATCHERCheckBox* m_mpusemasterslavecheckBox;
  wxStaticText* m_mptdmnotestaticText;
  wxStdDialogButtonSizer* m_mpsdbSizer;
  DUKEMATCHERButton* m_mpsdbSizerOK;
  DUKEMATCHERButton* m_mpsdbSizerCancel;

  DECLARE_EVENT_TABLE()
};

#endif
