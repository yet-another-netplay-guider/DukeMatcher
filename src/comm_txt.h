/*************************************************************************

Copyright 2008-2009 - NY00123

This file is part of DUKEMATCHER.

DUKEMATCHER is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

DUKEMATCHER is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DUKEMATCHER; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*************************************************************************/

#ifndef _DUKEMATCHER_COMMTXT_H_
#define _DUKEMATCHER_COMMTXT_H_

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
//#include <wx/wx.h> // CAUSES ERRORS ON COMPILATION OF THE MASTER SERVER! (wxBase only)
#include <wx/socket.h>
#endif

#define MAX_COMM_TEXT_LENGTH 1024
#define MAX_COMM_READ_ATTEMPTS 8

int g_ConvertStringListToCommText(const wxArrayString& str_list, char buffer[MAX_COMM_TEXT_LENGTH]);
//int g_TestStringListToCommTextConversion(const wxArrayString& str_list, size_t length);
//wxArrayString g_ConvertCommTextToStringList(char buffer[MAX_COMM_TEXT_LENGTH]);

#define g_SendCStringToSocket(socket, str) socket->Write(str, strlen(str))
//void g_SendCStringToSocket(wxSocketBase* socket, const char* str);
void g_SendStringListToSocket(wxSocketBase* socket, const wxArrayString& str_list);
//wxArrayString g_GetStringListFromSocket(wxSocketBase *socket);
size_t g_ConvertStringBufToStringList(wxString& str, wxArrayString& str_list);

#endif

