/*************************************************************************

Copyright 2008-2009 - NY00123

This file is part of DUKEMATCHER.

DUKEMATCHER is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

DUKEMATCHER is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DUKEMATCHER; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*************************************************************************/

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/wx.h>
#include <wx/filename.h>
#include <wx/textfile.h>
#include <wx/gbsizer.h>
#endif

#include "mp_dialog.h"
#include "config.h"
#include "host_frame.h"
#include "mp_common.h"
#include "dukematcher.h"
#include "theme.h"
#include "dukematcher.xpm"
#include "comm_txt.h"

BEGIN_EVENT_TABLE(MPDialog, DUKEMATCHERDialog)

EVT_BUTTON(wxID_OK, MPDialog::OnOk)
EVT_BUTTON(wxID_CANCEL, MPDialog::OnCancel)

EVT_BUTTON(ID_MPUSEMODFILE, MPDialog::OnAddModFile)
EVT_BUTTON(ID_MPREMMODFILE, MPDialog::OnRemModFile)

EVT_CHOICE(ID_MPSELECTGAME, MPDialog::OnGameSelect)
EVT_CHOICE(ID_MPSELECTSRCPORT, MPDialog::OnSrcPortSelect)

EVT_CHECKBOX(ID_MPRECORDDEMO, MPDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_MPUSEMODURL, MPDialog::OnCheckBoxClick)
//EVT_CHECKBOX(ID_MPADVERTISE, MPDialog::OnCheckBoxClick)
//EVT_CHECKBOX(ID_MPFORCEIP, MPDialog::OnCheckBoxClick)

EVT_RADIOBUTTON(ID_MPUSEEPIMAP, MPDialog::OnRadioBtnClick)
EVT_RADIOBUTTON(ID_MPUSEUSERMAP, MPDialog::OnRadioBtnClick)
//EVT_RADIOBUTTON(ID_MPUSEMODFILES, MPDialog::OnRadioBtnClick)

END_EVENT_TABLE()

MPDialog::MPDialog() : DUKEMATCHERDialog(NULL, wxID_ANY, wxT("Create a room"), wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE|wxDIALOG_NO_PARENT, wxEmptyString)
{
	wxBoxSizer* MPDialogbSizer = new wxBoxSizer( wxVERTICAL );
	
	m_mppanel = new DUKEMATCHERPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* mppanelmainbSizer = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* mppanelsubbSizer = new wxBoxSizer( wxHORIZONTAL );
	
	wxGridBagSizer* mppanelleftgbSizer = new wxGridBagSizer( 0, 0 );
	mppanelleftgbSizer->SetFlexibleDirection( wxBOTH );
	mppanelleftgbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	m_mpgamestaticText = new wxStaticText( m_mppanel, wxID_ANY, wxT("Game:"), wxDefaultPosition, wxDefaultSize, 0 );
	mppanelleftgbSizer->Add( m_mpgamestaticText, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_mpgamechoice = new DUKEMATCHERChoice( m_mppanel, ID_MPSELECTGAME, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	mppanelleftgbSizer->Add( m_mpgamechoice, wxGBPosition( 0, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );
	
	m_mpsourceportstaticText = new wxStaticText( m_mppanel, wxID_ANY, wxT("Source port:"), wxDefaultPosition, wxDefaultSize, 0 );
	mppanelleftgbSizer->Add( m_mpsourceportstaticText, wxGBPosition( 1, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_mpsourceportchoice = new DUKEMATCHERChoice( m_mppanel, ID_MPSELECTSRCPORT, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	mppanelleftgbSizer->Add( m_mpsourceportchoice, wxGBPosition( 1, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );
	
	m_mproomnamestaticText = new wxStaticText( m_mppanel, wxID_ANY, wxT("Room name:"), wxDefaultPosition, wxDefaultSize, 0 );
	mppanelleftgbSizer->Add( m_mproomnamestaticText, wxGBPosition( 2, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_mproomnametextCtrl = new DUKEMATCHERTextCtrl( m_mppanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	mppanelleftgbSizer->Add( m_mproomnametextCtrl, wxGBPosition( 2, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );
	
	m_mpmaxnumplayersstaticText = new wxStaticText( m_mppanel, wxID_ANY, wxT("Max. number of players:"), wxDefaultPosition, wxDefaultSize, 0 );
	mppanelleftgbSizer->Add( m_mpmaxnumplayersstaticText, wxGBPosition( 3, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_mpmaxnumplayerschoice = new DUKEMATCHERChoice( m_mppanel, wxID_ANY, wxDefaultPosition, wxDefaultSize );
	mppanelleftgbSizer->Add( m_mpmaxnumplayerschoice, wxGBPosition( 3, 1 ), wxGBSpan( 1, 1 ), wxALL, 5 );
	
	m_mprecorddemocheckBox = new DUKEMATCHERCheckBox( m_mppanel, ID_MPRECORDDEMO, wxT("Record demo"), wxDefaultPosition, wxDefaultSize, 0 );
	mppanelleftgbSizer->Add( m_mprecorddemocheckBox, wxGBPosition( 4, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_mprecorddemotextCtrl = new DUKEMATCHERTextCtrl( m_mppanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	mppanelleftgbSizer->Add( m_mprecorddemotextCtrl, wxGBPosition( 4, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_mpskillstaticText = new wxStaticText( m_mppanel, wxID_ANY, wxT("Skill:"), wxDefaultPosition, wxDefaultSize, 0 );
	mppanelleftgbSizer->Add( m_mpskillstaticText, wxGBPosition( 5, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_mpskillchoice = new DUKEMATCHERChoice( m_mppanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	mppanelleftgbSizer->Add( m_mpskillchoice, wxGBPosition( 5, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );
	
	m_mpgametypestaticText = new wxStaticText( m_mppanel, wxID_ANY, wxT("Game type:"), wxDefaultPosition, wxDefaultSize, 0 );
	mppanelleftgbSizer->Add( m_mpgametypestaticText, wxGBPosition( 6, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_mpgametypechoice = new DUKEMATCHERChoice( m_mppanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	mppanelleftgbSizer->Add( m_mpgametypechoice, wxGBPosition( 6, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );
	
	m_mptdmnotestaticText = new wxStaticText( m_mppanel, wxID_ANY, wxT("* NOTICE: Team DukeMatch gametypes are\ncurrently supported only by EDuke32 and hDuke."), wxDefaultPosition, wxDefaultSize, 0 );
	mppanelleftgbSizer->Add( m_mptdmnotestaticText, wxGBPosition( 7, 0 ), wxGBSpan( 1, 3 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_mpspawnstaticText = new wxStaticText( m_mppanel, wxID_ANY, wxT("Spawn:"), wxDefaultPosition, wxDefaultSize, 0 );
	mppanelleftgbSizer->Add( m_mpspawnstaticText, wxGBPosition( 8, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_mpspawnchoice = new DUKEMATCHERChoice( m_mppanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	mppanelleftgbSizer->Add( m_mpspawnchoice, wxGBPosition( 8, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_mpfraglimitstaticText = new wxStaticText( m_mppanel, wxID_ANY, wxT("Frag Limit (hDuke only):"), wxDefaultPosition, wxDefaultSize, 0 );
	mppanelleftgbSizer->Add( m_mpfraglimitstaticText, wxGBPosition( 9, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_mpfraglimittextCtrl = new DUKEMATCHERTextCtrl( m_mppanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	mppanelleftgbSizer->Add( m_mpfraglimittextCtrl, wxGBPosition( 9, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );
	
	m_mpnoteamchangecheckBox = new DUKEMATCHERCheckBox( m_mppanel, ID_MPADVERTISE, wxT("Don't allow team change in game (hDuke only)."), wxDefaultPosition, wxDefaultSize, 0 );
	mppanelleftgbSizer->Add( m_mpnoteamchangecheckBox, wxGBPosition( 10, 0 ), wxGBSpan( 1, 3 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_mpforcerespawncheckBox = new DUKEMATCHERCheckBox( m_mppanel, ID_MPUSESETTINGS, wxT("Force players to respawn (hDuke only)."), wxDefaultPosition, wxDefaultSize, 0 );
	mppanelleftgbSizer->Add( m_mpforcerespawncheckBox, wxGBPosition( 11, 0 ), wxGBSpan( 1, 3 ), wxALL, 5 );
	
	m_mpextrahostargsstaticText = new wxStaticText( m_mppanel, wxID_ANY, wxT("Extra args just for host:"), wxDefaultPosition, wxDefaultSize, 0 );
	mppanelleftgbSizer->Add( m_mpextrahostargsstaticText, wxGBPosition( 12, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_mpextrahostargstextCtrl = new DUKEMATCHERTextCtrl( m_mppanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	mppanelleftgbSizer->Add( m_mpextrahostargstextCtrl, wxGBPosition( 12, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_mpextraallargsstaticText = new wxStaticText( m_mppanel, wxID_ANY, wxT("Extra args for everyone:"), wxDefaultPosition, wxDefaultSize, 0 );
	mppanelleftgbSizer->Add( m_mpextraallargsstaticText, wxGBPosition( 13, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_mpextraallargstextCtrl = new DUKEMATCHERTextCtrl( m_mppanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	mppanelleftgbSizer->Add( m_mpextraallargstextCtrl, wxGBPosition( 13, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
/*
	m_mpforceipcheckBox = new DUKEMATCHERCheckBox( m_mppanel, ID_MPFORCEIP, wxT("Force IP address (e.g. for a local game):"), wxDefaultPosition, wxDefaultSize, 0 );

	mppanelleftgbSizer->Add( m_mpforceipcheckBox, wxGBPosition( 12, 0 ), wxGBSpan( 1, 3 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_mpforceipcomboBox = new wxComboBox( m_mppanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) ); 
	mppanelleftgbSizer->Add( m_mpforceipcomboBox, wxGBPosition( 13, 0 ), wxGBSpan( 1, 3 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
*/
	mppanelsubbSizer->Add( mppanelleftgbSizer, 1, wxEXPAND, 0 );

	wxGridBagSizer* mppanelrightgbSizer = new wxGridBagSizer( 0, 0 );
	mppanelrightgbSizer->SetFlexibleDirection( wxBOTH );
	mppanelrightgbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_mpepimapradioBtn = new DUKEMATCHERRadioButton( m_mppanel, ID_MPUSEEPIMAP, wxT("Episode Map:"), wxDefaultPosition, wxDefaultSize, 0 );
	mppanelrightgbSizer->Add( m_mpepimapradioBtn, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_mpepimapchoice = new DUKEMATCHERChoice( m_mppanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
	mppanelrightgbSizer->Add( m_mpepimapchoice, wxGBPosition( 0, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );
	
	m_mpusermapradioBtn = new DUKEMATCHERRadioButton( m_mppanel, ID_MPUSEUSERMAP, wxT("User Map:"), wxDefaultPosition, wxDefaultSize, 0 );
	mppanelrightgbSizer->Add( m_mpusermapradioBtn, wxGBPosition( 1, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_mpusermapchoice = new DUKEMATCHERChoice( m_mppanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)), 0, NULL, wxCB_SORT);
	mppanelrightgbSizer->Add( m_mpusermapchoice, wxGBPosition( 1, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_mpmodstaticText = new wxStaticText( m_mppanel, wxID_ANY, wxT("TC/MOD files:"), wxDefaultPosition, wxDefaultSize, 0 );
	mppanelrightgbSizer->Add( m_mpmodstaticText, wxGBPosition( 2, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_mpavailmodfileslistBox = new DUKEMATCHERListBox( m_mppanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(64, 80)), 0, NULL, wxLB_EXTENDED ); 
	mppanelrightgbSizer->Add( m_mpavailmodfileslistBox, wxGBPosition( 2, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_mpusedmodfileslistBox = new DUKEMATCHERListBox( m_mppanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(64, 80)), 0, NULL, wxLB_EXTENDED ); 
	mppanelrightgbSizer->Add( m_mpusedmodfileslistBox, wxGBPosition( 2, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );
	
	m_mpusemodfilebutton = new DUKEMATCHERButton( m_mppanel, ID_MPUSEMODFILE, wxT("Use"), wxDefaultPosition, wxDefaultSize, 0 );
	mppanelrightgbSizer->Add( m_mpusemodfilebutton, wxGBPosition( 3, 1 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL|wxALIGN_CENTER_HORIZONTAL, 5 );
	
	m_mpremmodfilebutton = new DUKEMATCHERButton( m_mppanel, ID_MPREMMODFILE, wxT("Remove"), wxDefaultPosition, wxDefaultSize, 0 );
	mppanelrightgbSizer->Add( m_mpremmodfilebutton, wxGBPosition( 3, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_mpmodnamestaticText = new wxStaticText( m_mppanel, wxID_ANY, wxT("TC/MOD name:"), wxDefaultPosition, wxDefaultSize, 0 );
	mppanelrightgbSizer->Add( m_mpmodnamestaticText, wxGBPosition( 4, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_mpmodnametextCtrl = new DUKEMATCHERTextCtrl( m_mppanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	mppanelrightgbSizer->Add( m_mpmodnametextCtrl, wxGBPosition( 4, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );
	
	m_mpmodurlcheckBox = new DUKEMATCHERCheckBox( m_mppanel, ID_MPUSEMODURL, wxT("TC/MOD URL:"), wxDefaultPosition, wxDefaultSize, 0 );
	
	mppanelrightgbSizer->Add( m_mpmodurlcheckBox, wxGBPosition( 5, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_mpmodurltextCtrl = new DUKEMATCHERTextCtrl( m_mppanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	mppanelrightgbSizer->Add( m_mpmodurltextCtrl, wxGBPosition( 5, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_mpusemasterslavecheckBox = new DUKEMATCHERCheckBox( m_mppanel, wxID_ANY, wxT("Use master/slave mode instead of p2p"), wxDefaultPosition, wxDefaultSize, 0 );
	mppanelrightgbSizer->Add( m_mpusemasterslavecheckBox, wxGBPosition( 7, 0 ), wxGBSpan( 1, 3 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_mpadvertiseroomcheckBox = new DUKEMATCHERCheckBox( m_mppanel, ID_MPADVERTISE, wxT("Advertise room in the rooms list."), wxDefaultPosition, wxDefaultSize, 0 );
	mppanelrightgbSizer->Add( m_mpadvertiseroomcheckBox, wxGBPosition( 9, 0 ), wxGBSpan( 1, 3 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_mpnodialogcheckBox = new DUKEMATCHERCheckBox( m_mppanel, ID_MPUSESETTINGS, wxT("Don't show this dialog in the following times."), wxDefaultPosition, wxDefaultSize, 0 );
	mppanelrightgbSizer->Add( m_mpnodialogcheckBox, wxGBPosition( 10, 0 ), wxGBSpan( 1, 3 ), wxALL, 5 );
	
	mppanelsubbSizer->Add( mppanelrightgbSizer, 1, wxEXPAND, 0 );
	
	mppanelmainbSizer->Add( mppanelsubbSizer, 1, wxEXPAND, 0 );
	
	m_mpsdbSizer = new wxStdDialogButtonSizer();
	m_mpsdbSizerOK = new DUKEMATCHERButton( m_mppanel, wxID_OK );
	m_mpsdbSizer->AddButton( m_mpsdbSizerOK );
	m_mpsdbSizerCancel = new DUKEMATCHERButton( m_mppanel, wxID_CANCEL );
	m_mpsdbSizer->AddButton( m_mpsdbSizerCancel );
	m_mpsdbSizer->Realize();
	mppanelmainbSizer->Add( m_mpsdbSizer, 0, wxALIGN_CENTER_HORIZONTAL, 5 );
	
	m_mppanel->SetSizer( mppanelmainbSizer );
	m_mppanel->Layout();
	mppanelmainbSizer->Fit( m_mppanel );
	MPDialogbSizer->Add( m_mppanel, 1, wxEXPAND | wxALL, 0 );
	
	SetSizer( MPDialogbSizer );
	Layout();
	MPDialogbSizer->Fit( this );

  Centre();
  SetIcon(wxIcon(dukematcher_xpm));

  for (size_t l_loop_var = 2; l_loop_var <= MAX_NUM_PLAYERS; l_loop_var++)
    m_mpmaxnumplayerschoice->Append(wxString::Format(wxT("%d"), l_loop_var));
  m_mpmaxnumplayerschoice->SetSelection(g_configuration->gamelaunch_numofplayers - 2);

  m_mpusermapchoice->Disable();

  if (g_configuration->have_eduke32 || g_configuration->have_jfduke3d || g_configuration->have_iduke3d
      || g_configuration->have_duke3dw32 || g_configuration->have_xduke || g_configuration->have_dosduke)
    m_mpgamechoice->Append(GAMENAME_DN3D);
  if (g_configuration->have_jfsw || g_configuration->have_swp || g_configuration->have_dossw)
    m_mpgamechoice->Append(GAMENAME_SW);
  if (((g_configuration->gamelaunch_source_port == SOURCEPORT_EDUKE32) && g_configuration->have_eduke32) ||
      ((g_configuration->gamelaunch_source_port == SOURCEPORT_JFDUKE3D) && g_configuration->have_jfduke3d) ||
      ((g_configuration->gamelaunch_source_port == SOURCEPORT_IDUKE3D) && g_configuration->have_iduke3d) ||
      ((g_configuration->gamelaunch_source_port == SOURCEPORT_DUKE3DW32) && g_configuration->have_duke3dw32) ||
      ((g_configuration->gamelaunch_source_port == SOURCEPORT_XDUKE) && g_configuration->have_xduke) ||
      ((g_configuration->gamelaunch_source_port == SOURCEPORT_DOSDUKE) && g_configuration->have_dosduke && g_configuration->have_dosbox))
  {
    m_mpgamechoice->SetStringSelection(GAMENAME_DN3D);
    if (g_configuration->have_eduke32)
      m_mpsourceportchoice->Append(SRCPORTNAME_EDUKE32);
    if (g_configuration->have_jfduke3d)
      m_mpsourceportchoice->Append(SRCPORTNAME_JFDUKE3D);
    if (g_configuration->have_iduke3d)
      m_mpsourceportchoice->Append(SRCPORTNAME_IDUKE3D);
    if (g_configuration->have_duke3dw32)
      m_mpsourceportchoice->Append(SRCPORTNAME_DUKE3DW32);
    if (g_configuration->have_xduke)
      m_mpsourceportchoice->Append(SRCPORTNAME_XDUKE);
    if (g_configuration->have_dosduke && g_configuration->have_dosbox)
      m_mpsourceportchoice->Append(SRCPORTNAME_DOSDUKE);
    switch (g_configuration->gamelaunch_source_port)
    {
      case SOURCEPORT_EDUKE32: m_mpsourceportchoice->SetStringSelection(SRCPORTNAME_EDUKE32); break;
      case SOURCEPORT_JFDUKE3D: m_mpsourceportchoice->SetStringSelection(SRCPORTNAME_JFDUKE3D); break;
      case SOURCEPORT_IDUKE3D: m_mpsourceportchoice->SetStringSelection(SRCPORTNAME_IDUKE3D); break;
      case SOURCEPORT_DUKE3DW32: m_mpsourceportchoice->SetStringSelection(SRCPORTNAME_DUKE3DW32); break;
      case SOURCEPORT_XDUKE: m_mpsourceportchoice->SetStringSelection(SRCPORTNAME_XDUKE); break;
      default: /*SOURCEPORT_DOSDUKE*/ m_mpsourceportchoice->SetStringSelection(SRCPORTNAME_DOSDUKE);
    }
  }
  else
    if (((g_configuration->gamelaunch_source_port == SOURCEPORT_JFSW) && g_configuration->have_jfsw) ||
        ((g_configuration->gamelaunch_source_port == SOURCEPORT_SWP) && g_configuration->have_swp) ||
        ((g_configuration->gamelaunch_source_port == SOURCEPORT_DOSSW) && g_configuration->have_dossw && g_configuration->have_dosbox))
    {
      m_mpgamechoice->SetStringSelection(GAMENAME_SW);
      if (g_configuration->have_jfsw)
        m_mpsourceportchoice->Append(SRCPORTNAME_JFSW);
      if (g_configuration->have_swp)
        m_mpsourceportchoice->Append(SRCPORTNAME_SWP);
      if (g_configuration->have_dossw && g_configuration->have_dosbox)
        m_mpsourceportchoice->Append(SRCPORTNAME_DOSSW);
      switch (g_configuration->gamelaunch_source_port)
      {
        case SOURCEPORT_JFSW: m_mpsourceportchoice->SetStringSelection(SRCPORTNAME_JFSW); break;
        case SOURCEPORT_SWP: m_mpsourceportchoice->SetStringSelection(SRCPORTNAME_SWP); break;
        default: /*SOURCEPORT_DOSSW*/ m_mpsourceportchoice->SetStringSelection(SRCPORTNAME_DOSSW);
      }
    }
    else
    {
      if (g_configuration->have_eduke32)
        m_mpsourceportchoice->Append(SRCPORTNAME_EDUKE32);
      if (g_configuration->have_jfduke3d)
        m_mpsourceportchoice->Append(SRCPORTNAME_JFDUKE3D);
      if (g_configuration->have_iduke3d)
        m_mpsourceportchoice->Append(SRCPORTNAME_IDUKE3D);
      if (g_configuration->have_duke3dw32)
        m_mpsourceportchoice->Append(SRCPORTNAME_DUKE3DW32);
      if (g_configuration->have_xduke)
        m_mpsourceportchoice->Append(SRCPORTNAME_XDUKE);
      if (g_configuration->have_dosduke && g_configuration->have_dosbox)
        m_mpsourceportchoice->Append(SRCPORTNAME_DOSDUKE);
      if (m_mpsourceportchoice->GetCount())
        m_mpgamechoice->SetStringSelection(GAMENAME_DN3D);
      else
      {
        m_mpgamechoice->SetStringSelection(GAMENAME_SW);
        if (g_configuration->have_jfsw)
          m_mpsourceportchoice->Append(SRCPORTNAME_JFSW);
        if (g_configuration->have_swp)
          m_mpsourceportchoice->Append(SRCPORTNAME_SWP);
        if (g_configuration->have_dossw && g_configuration->have_dosbox)
          m_mpsourceportchoice->Append(SRCPORTNAME_DOSSW);
      }
      m_mpsourceportchoice->SetSelection(0);
    }
  m_mproomnametextCtrl->SetValue(g_configuration->gamelaunch_roomname);
  m_mpgametypechoice->Append(*g_dn3d_gametype_list);
  m_mpspawnchoice->Append(*g_dn3d_spawn_list);
  m_mpgametypechoice->SetSelection(g_configuration->gamelaunch_dn3dgametype-1);
  m_mpspawnchoice->SetSelection(g_configuration->gamelaunch_dn3dspawn-1);
  m_mpfraglimittextCtrl->SetValue(wxString::Format(wxT("%d"), g_configuration->gamelaunch_fraglimit));
  m_mpnoteamchangecheckBox->SetValue(g_configuration->gamelaunch_noteamchange);
  m_mpforcerespawncheckBox->SetValue(g_configuration->gamelaunch_forcerespawn);
  m_mpextrahostargstextCtrl->SetValue(g_configuration->gamelaunch_args_for_host);
  m_mpextraallargstextCtrl->SetValue(g_configuration->gamelaunch_args_for_all);
  m_mpmodnametextCtrl->SetValue(g_configuration->gamelaunch_modname);
  m_mpmodurlcheckBox->SetValue(g_configuration->gamelaunch_show_modurl);
  m_mpmodurltextCtrl->SetValue(g_configuration->gamelaunch_modurl);
  m_mpmodurltextCtrl->Enable(m_mpmodurlcheckBox->GetValue());
  m_mpadvertiseroomcheckBox->SetValue(g_configuration->gamelaunch_advertise);
  m_mpnodialogcheckBox->SetValue(g_configuration->gamelaunch_skip_settings);
  m_mpusemasterslavecheckBox->SetValue(g_configuration->gamelaunch_use_masterslave);
  PartialDialogControlsReset();
}
/*
MPDialog::~MPDialog()
{
  g_main_frame->Show(true);
  g_mp_dialog = NULL;
}
*/
void MPDialog::PartialDialogControlsReset()
{
  wxArrayString l_file_list;
  int l_loop_var, l_num_of_files, l_temp_index;
//m_mpdemobutton->Enable(m_mpplaydemocheckBox->GetValue());
  if (m_mpgamechoice->GetStringSelection() == GAMENAME_DN3D)
  {
//  m_mpskillchoice->Clear();
    m_mpskillchoice->Append(*g_dn3d_skill_list);
    m_mpskillchoice->SetSelection(g_configuration->gamelaunch_dn3d_skill);
    m_mprecorddemotextCtrl->Disable();
    m_mpgametypestaticText->Enable();
    m_mpgametypechoice->Enable();
    m_mpspawnstaticText->Enable();
    m_mpspawnchoice->Enable();
//  m_mpepimapchoice->Clear();
    m_mpepimapchoice->Append(*g_dn3d_level_list);
    m_mpepimapchoice->SetSelection(g_GetDN3DLevelSelectionByNum(g_configuration->gamelaunch_dn3dlvl));
    g_AddAllDN3DMapFiles(&l_file_list);
//  m_mpusermapchoice->Clear();
    m_mpusermapchoice->Append(l_file_list);
    m_mpusermapchoice->SetStringSelection(g_configuration->gamelaunch_dn3dusermap);
    l_file_list.Empty();
    g_AddAllDN3DModFiles(&l_file_list);
//  m_mpavailmodfileslistBox->Clear();
    m_mpavailmodfileslistBox->Append(l_file_list);
//  m_mpusedmodfileslistBox->Clear();
    m_mpusedmodfileslistBox->Append(g_configuration->gamelaunch_dn3dmodfiles);
    m_mpusemasterslavecheckBox->Enable(((m_mpsourceportchoice->GetStringSelection()) == SRCPORTNAME_EDUKE32)
                                       || ((m_mpsourceportchoice->GetStringSelection()) == SRCPORTNAME_JFDUKE3D));
	m_mpfraglimittextCtrl->Enable((m_mpsourceportchoice->GetStringSelection()) == SRCPORTNAME_IDUKE3D);
	m_mpnoteamchangecheckBox->Enable((m_mpsourceportchoice->GetStringSelection()) == SRCPORTNAME_IDUKE3D);
	m_mpforcerespawncheckBox->Enable((m_mpsourceportchoice->GetStringSelection()) == SRCPORTNAME_IDUKE3D);
  }
  else
  {
//  m_mpskillchoice->Clear();
    m_mpskillchoice->Append(*g_sw_skill_list);
    m_mpskillchoice->SetSelection(g_configuration->gamelaunch_sw_skill);
    m_mprecorddemotextCtrl->Enable(m_mprecorddemocheckBox->GetValue());
    m_mpgametypestaticText->Disable();
    m_mpgametypechoice->Disable();
    m_mpspawnstaticText->Disable();
    m_mpspawnchoice->Disable();
//  m_mpepimapchoice->Clear();
    m_mpepimapchoice->Append(*g_sw_level_list);
    m_mpepimapchoice->SetSelection(g_GetSWLevelSelectionByNum(g_configuration->gamelaunch_swlvl));
    g_AddAllSWMapFiles(&l_file_list);
//  m_mpusermapchoice->Clear();
    m_mpusermapchoice->Append(l_file_list);
    m_mpusermapchoice->SetStringSelection(g_configuration->gamelaunch_swusermap);
    l_file_list.Empty();
    g_AddAllSWModFiles(&l_file_list);
//  m_mpavailmodfileslistBox->Clear();
    m_mpavailmodfileslistBox->Append(l_file_list);
//  m_mpusedmodfileslistBox->Clear();
    m_mpusedmodfileslistBox->Append(g_configuration->gamelaunch_swmodfiles);
    m_mpusemasterslavecheckBox->Disable();
	m_mpfraglimittextCtrl->Disable();
	m_mpnoteamchangecheckBox->Disable();
	m_mpforcerespawncheckBox->Disable();
  }
// Do select MOD files exist? No? Maybe partially?
  l_num_of_files = m_mpusedmodfileslistBox->GetCount();
  l_loop_var = 0;
  while (l_loop_var < l_num_of_files)
  {
    l_temp_index = m_mpavailmodfileslistBox->FindString(m_mpusedmodfileslistBox->GetString(l_loop_var));
    if (l_temp_index == wxNOT_FOUND)
    {
      m_mpusedmodfileslistBox->Delete(l_loop_var);
      // Number of items has decreased by 1, and now l_loop_var points to the
      // following item if there is one!
      l_num_of_files--;
    }
    else
    {
      m_mpavailmodfileslistBox->Delete(l_temp_index);
      l_loop_var++;
    }
  }
  if (m_mpusermapchoice->IsEmpty())
  {
    m_mpepimapradioBtn->SetValue(true);
    m_mpepimapchoice->Enable();
    m_mpusermapradioBtn->Disable();
    m_mpusermapchoice->Disable();
  }
  else
  {
    m_mpusermapradioBtn->Enable();
    if (m_mpusermapchoice->GetSelection() == wxNOT_FOUND)
    {
      m_mpepimapradioBtn->SetValue(true);
      m_mpepimapchoice->Enable();
      m_mpusermapchoice->SetSelection(0);
      m_mpusermapchoice->Disable();
    }
    else
    {
      m_mpusermapradioBtn->SetValue(true);
      m_mpepimapchoice->Disable();
      m_mpusermapchoice->Enable();
    }
  }
}

void MPDialog::OnGameSelect(wxCommandEvent& WXUNUSED(event))
{
  wxString l_game_name = m_mpgamechoice->GetStringSelection();
  wxString l_srcport_name = m_mpsourceportchoice->GetStringSelection();
  if (((l_game_name == GAMENAME_DN3D) &&
       ((l_srcport_name == SRCPORTNAME_JFSW) || (l_srcport_name == SRCPORTNAME_SWP)
        || (l_srcport_name == SRCPORTNAME_DOSSW))
      )
      ||
      ((l_game_name == GAMENAME_SW) &&
       ((l_srcport_name == SRCPORTNAME_EDUKE32) || (l_srcport_name == SRCPORTNAME_JFDUKE3D)
        || (l_srcport_name == SRCPORTNAME_IDUKE3D) || (l_srcport_name == SRCPORTNAME_DUKE3DW32)
        || (l_srcport_name == SRCPORTNAME_XDUKE) || (l_srcport_name == SRCPORTNAME_DOSDUKE))
      )
     )
  {
    m_mpskillchoice->Clear();
    m_mpepimapchoice->Clear();
    m_mpusermapchoice->Clear();
    m_mpavailmodfileslistBox->Clear();
    m_mpusedmodfileslistBox->Clear();
    m_mpsourceportchoice->Clear();
    if (l_game_name == GAMENAME_DN3D)
    {
      if (g_configuration->have_eduke32)
        m_mpsourceportchoice->Append(SRCPORTNAME_EDUKE32);
      if (g_configuration->have_jfduke3d)
        m_mpsourceportchoice->Append(SRCPORTNAME_JFDUKE3D);
      if (g_configuration->have_iduke3d)
        m_mpsourceportchoice->Append(SRCPORTNAME_IDUKE3D);
      if (g_configuration->have_duke3dw32)
        m_mpsourceportchoice->Append(SRCPORTNAME_DUKE3DW32);
      if (g_configuration->have_xduke)
        m_mpsourceportchoice->Append(SRCPORTNAME_XDUKE);
      if (g_configuration->have_dosduke && g_configuration->have_dosbox)
        m_mpsourceportchoice->Append(SRCPORTNAME_DOSDUKE);
    }
    else
    {
      if (g_configuration->have_jfsw)
        m_mpsourceportchoice->Append(SRCPORTNAME_JFSW);
      if (g_configuration->have_swp)
        m_mpsourceportchoice->Append(SRCPORTNAME_SWP);
      if (g_configuration->have_dossw && g_configuration->have_dosbox)
        m_mpsourceportchoice->Append(SRCPORTNAME_DOSSW);
    }
    m_mpsourceportchoice->SetSelection(0);
    PartialDialogControlsReset();
  }
}

void MPDialog::OnSrcPortSelect(wxCommandEvent& WXUNUSED(event))
{
  m_mpusemasterslavecheckBox->Enable(((m_mpsourceportchoice->GetStringSelection()) == SRCPORTNAME_EDUKE32)
                                     || ((m_mpsourceportchoice->GetStringSelection()) == SRCPORTNAME_JFDUKE3D));
// enable hduke fields if source port is set to hduke.
m_mpfraglimittextCtrl->Enable((m_mpsourceportchoice->GetStringSelection()) == SRCPORTNAME_IDUKE3D);
m_mpnoteamchangecheckBox->Enable((m_mpsourceportchoice->GetStringSelection()) == SRCPORTNAME_IDUKE3D);
m_mpforcerespawncheckBox->Enable((m_mpsourceportchoice->GetStringSelection()) == SRCPORTNAME_IDUKE3D);	
}

void MPDialog::OnCheckBoxClick(wxCommandEvent& event)
{
  switch (event.GetId())
  {
    case ID_MPRECORDDEMO: m_mprecorddemotextCtrl->Enable((m_mprecorddemocheckBox->GetValue()) &&
                                                         (m_mpgamechoice->GetStringSelection() == GAMENAME_SW));
                          break;
    case ID_MPUSEMODURL:  m_mpmodurltextCtrl->Enable(m_mpmodurlcheckBox->GetValue());
    // Default would be the master-slave checkbox.
  }
}

void MPDialog::OnRadioBtnClick(wxCommandEvent& WXUNUSED(event))
{
  m_mpepimapchoice->Enable(m_mpepimapradioBtn->GetValue());
  m_mpusermapchoice->Enable(m_mpusermapradioBtn->GetValue());
}

void MPDialog::OnAddModFile(wxCommandEvent& WXUNUSED(event))
{
  wxArrayInt l_file_index_list;
  // We'd get a list of the INDEX NUMBERS of the selected files;
  // Therefore, we should remove them in REVERSED order.
  // That's because on removal of an item, the index numbers of
  // the following items get DECREASED by 1;
  // And if we try removing from the beginning, then from the second item
  // we'd try removing using the WRONG index numbers.
  size_t i = 0, l_num_of_items = m_mpavailmodfileslistBox->GetCount();
  bool no_item_found = true;
  while ((i < l_num_of_items) && (no_item_found))
  {
    if (m_mpavailmodfileslistBox->IsSelected(i))
    {
      l_file_index_list.Add(i);
      m_mpusedmodfileslistBox->Append(m_mpavailmodfileslistBox->GetString(i));
      no_item_found = false;
    }
    i++;
  }
// Now, if we've found one item, the array l_file_index_list should contain
// its index number.
// We should insert the rest of the selected items' index numbers
// into the array in REVERSED order.
// If i = l_num_of_items we have nothing to do
// (nothing is selected, or only the latest item is selected).
// But if i < l_num_of_items, now that l_file_index_list contains an item,
// we can simply use the Insert function/procedure to insert right
// in the beginning of the array.
  while (i < l_num_of_items)
  {
    if (m_mpavailmodfileslistBox->IsSelected(i))
    {
      l_file_index_list.Insert(i, 0);
      m_mpusedmodfileslistBox->Append(m_mpavailmodfileslistBox->GetString(i));
    }
    i++;
  }
  l_num_of_items = l_file_index_list.GetCount();
  for (i = 0; i < l_num_of_items; i++)
    m_mpavailmodfileslistBox->Delete(l_file_index_list[i]);
}

void MPDialog::OnRemModFile(wxCommandEvent& WXUNUSED(event))
{
// Same story as with OnAddModFile.
  wxArrayInt l_file_index_list;
  size_t i = 0, l_num_of_items = m_mpusedmodfileslistBox->GetCount();
  bool no_item_found = true;
  while ((i < l_num_of_items) && (no_item_found))
  {
    if (m_mpusedmodfileslistBox->IsSelected(i))
    {
      l_file_index_list.Add(i);
      m_mpavailmodfileslistBox->Append(m_mpusedmodfileslistBox->GetString(i));
      no_item_found = false;
    }
    i++;
  }
  while (i < l_num_of_items)
  {
    if (m_mpusedmodfileslistBox->IsSelected(i))
    {
      l_file_index_list.Insert(i, 0);
      m_mpavailmodfileslistBox->Append(m_mpusedmodfileslistBox->GetString(i));
    }
    i++;
  }
  l_num_of_items = l_file_index_list.GetCount();
  for (i = 0; i < l_num_of_items; i++)
    m_mpusedmodfileslistBox->Delete(l_file_index_list[i]);
}

void MPDialog::OnCancel(wxCommandEvent& WXUNUSED(event))
{
  if (g_host_frame)
  {
    g_host_frame->Enable();
    g_host_frame->FocusFrame();
//  g_host_frame->SetFocus();
//  g_host_frame->Raise();
  }
  else
    g_main_frame->ShowMainFrame();
  Destroy();
}

void MPDialog::OnOk(wxCommandEvent& WXUNUSED(event))
{
 // show main frame
  g_main_frame->ShowMainFrame();
  GameType l_game;
  SourcePortType l_srcport;
  wxString l_roomname = m_mproomnametextCtrl->GetValue();
  size_t l_max_num_players = m_mpmaxnumplayerschoice->GetSelection()+2;
  bool l_recorddemo = m_mprecorddemocheckBox->GetValue();
  wxString l_demofilename = m_mprecorddemotextCtrl->GetValue();
  size_t l_skill = m_mpskillchoice->GetSelection();
  DN3DMPGameT_Type l_dn3dgametype = (DN3DMPGameT_Type)(m_mpgametypechoice->GetSelection()+1);
  DN3DSpawnType l_dn3dspawn = (DN3DSpawnType)(m_mpspawnchoice->GetSelection()+1);

  long l_fraglimit;
  if (m_mpfraglimittextCtrl->GetValue().ToLong(&l_fraglimit))
    l_fraglimit = 0;

  bool l_noteamchange = m_mpnoteamchangecheckBox->GetValue();
  bool l_forcerespawn = m_mpforcerespawncheckBox->GetValue();
  wxString l_args_for_host = m_mpextrahostargstextCtrl->GetValue(),
           l_args_for_all = m_mpextraallargstextCtrl->GetValue();
  bool l_launch_usermap = m_mpusermapradioBtn->GetValue();
  size_t l_epimap;
  wxString l_usermap = m_mpusermapchoice->GetStringSelection();
  wxArrayString l_modfiles = m_mpusedmodfileslistBox->GetStrings();
  wxString l_modname = m_mpmodnametextCtrl->GetValue();
  bool l_showmodurl = m_mpmodurlcheckBox->GetValue();
  wxString l_modurl = m_mpmodurltextCtrl->GetValue();
  bool l_advertise = m_mpadvertiseroomcheckBox->GetValue();
  bool l_usemasterslave = m_mpusemasterslavecheckBox->GetValue();

  wxString l_temp_str;
  wxArrayString l_str_list;
  size_t l_num_of_items;
  bool l_server_is_up = true;
  char l_buffer[MAX_COMM_TEXT_LENGTH];
#ifdef ENABLE_UPNP
  UPNPForwardThread* l_thread;
#endif

  if (m_mpgamechoice->GetStringSelection() == GAMENAME_DN3D)
    l_game = GAME_DN3D;
  else
    l_game = GAME_SW;
  if (l_game == GAME_DN3D)
  {
    l_epimap = g_GetDN3DLevelNumBySelection(m_mpepimapchoice->GetSelection());
    l_temp_str = m_mpsourceportchoice->GetStringSelection();
    if (l_temp_str == SRCPORTNAME_EDUKE32)
      l_srcport = SOURCEPORT_EDUKE32;
    else
      if (l_temp_str == SRCPORTNAME_JFDUKE3D)
        l_srcport = SOURCEPORT_JFDUKE3D;
    else
      if (l_temp_str == SRCPORTNAME_IDUKE3D)
        l_srcport = SOURCEPORT_IDUKE3D;
    else
      if (l_temp_str == SRCPORTNAME_DUKE3DW32)
        l_srcport = SOURCEPORT_DUKE3DW32;
    else
      if (l_temp_str == SRCPORTNAME_XDUKE)
        l_srcport = SOURCEPORT_XDUKE;
    else
     l_srcport = SOURCEPORT_DOSDUKE;
  }
  else
  {
    l_epimap = g_GetSWLevelNumBySelection(m_mpepimapchoice->GetSelection());
    l_temp_str = m_mpsourceportchoice->GetStringSelection();
    if (l_temp_str == SRCPORTNAME_JFSW)
      l_srcport = SOURCEPORT_JFSW;
    else
      if (l_temp_str == SRCPORTNAME_SWP)
        l_srcport = SOURCEPORT_SWP;
    else
      l_srcport = SOURCEPORT_DOSSW;
  }

  // Let's begin a short "simulation" first...
  // to check that the packets to send to the clients have enough room
  // to contain the given information.

  HostedRoomFrame* l_host_frame = new HostedRoomFrame;
  // Should work when the number of clients and the number of in-room players
  // are both initialized to 0.
  // IMPORTANT!!! Pass *false* instead of l_advertise!
  l_host_frame->UpdateGameSettings(l_game, l_srcport, l_roomname,
                                   l_max_num_players, l_recorddemo,
                                   l_demofilename, l_skill, l_dn3dgametype,
                                   l_dn3dspawn, l_fraglimit, l_noteamchange, 
								   l_forcerespawn, l_args_for_host,
                                   l_args_for_all, l_launch_usermap,
                                   l_epimap, l_usermap, l_modfiles,
                                   l_modname, l_showmodurl, l_modurl,
                                   false, l_usemasterslave);
  l_num_of_items = l_modfiles.GetCount();
  if (l_num_of_items)
  {
    l_str_list = l_host_frame->GetServerModFilesSockStrList(l_num_of_items);
    if (!g_ConvertStringListToCommText(l_str_list, l_buffer))
    {
      wxMessageBox(wxT("When sending clients information about the used MOD files, there won't be room for all of that information.\nYou can use less files, shorter filenames, but most important - avoid using non-English characters in filenames and folders as much as possible."), wxT("Too much MOD files info"), wxOK|wxICON_INFORMATION);
      l_host_frame->Destroy();
      return;
    }
  }
  l_str_list = l_host_frame->GetServerInfoSockStrList();
  if (!g_ConvertStringListToCommText(l_str_list, l_buffer))
  {
    wxMessageBox(wxT("When sending clients information about the room (excluding list of MOD files), there won't be room for all of that information.\nYou can use shorter names for the room name and/or the usermap file name, but most important - Avoid using non-English characters in filenames and folders as much as possible.\nIn title-names non-English characters may currently work, although internally they probably take more space."), wxT("Too much game room info"), wxOK|wxICON_INFORMATION);
    l_host_frame->Destroy();
    return;
  }
  l_host_frame->Destroy();

  ApplySettings();

  if (g_host_frame)
    g_host_frame->Enable();
  else
  {
    g_host_frame = new HostedRoomFrame;
    l_server_is_up = g_host_frame->StartServer();
    if (!l_server_is_up)
    {
      wxMessageBox(wxString::Format(wxT("ERROR: Couldn't start server on port %d for an unknown reason.\nPossible reasons:\n* The configured server port number is already in use by another listener.\n* The server has disconnected a client by itself. At least in that case you might have to wait for about a minute before you can rehost on the same port.\n* Somehow a disconnection hasn't been done probably."), g_configuration->server_port_number),
      wxT("Couldn't start listening"), wxOK|wxICON_EXCLAMATION);
      g_host_frame->Destroy();
      g_host_frame = NULL;
      g_main_frame->ShowMainFrame();
    }
    else
    {
#ifdef ENABLE_UPNP
      l_thread = new UPNPForwardThread(g_configuration->game_port_number, false);
      if (l_thread->Create() == wxTHREAD_NO_ERROR )
        l_thread->Run();
      else
        l_thread->Delete();
      l_thread = new UPNPForwardThread(g_configuration->server_port_number, true);
      if (l_thread->Create() == wxTHREAD_NO_ERROR )
        l_thread->Run();
      else
        l_thread->Delete();
#endif
      g_host_frame->Show(true);
//    g_host_frame->Raise();
    }
  }
  if (l_server_is_up)
  {
    g_host_frame->UpdateGameSettings(l_game, l_srcport, l_roomname,
                                     l_max_num_players, l_recorddemo,
                                     l_demofilename, l_skill, l_dn3dgametype,
                                     l_dn3dspawn, l_fraglimit, l_noteamchange,
									 l_forcerespawn, l_args_for_host,
                                     l_args_for_all, l_launch_usermap,
                                     l_epimap, l_usermap, l_modfiles,
                                     l_modname, l_showmodurl, l_modurl,
                                     l_advertise, l_usemasterslave);
    g_host_frame->FocusFrame();
//  g_host_frame->Raise();
//  g_host_frame->SetFocus();
  }
  Destroy();
}

void MPDialog::ApplySettings()
{
  wxString l_selected_port = m_mpsourceportchoice->GetStringSelection();
  if (l_selected_port == SRCPORTNAME_EDUKE32)
    g_configuration->gamelaunch_source_port = SOURCEPORT_EDUKE32;
  else
    if (l_selected_port == SRCPORTNAME_JFDUKE3D)
      g_configuration->gamelaunch_source_port = SOURCEPORT_JFDUKE3D;
  else
    if (l_selected_port == SRCPORTNAME_IDUKE3D)
      g_configuration->gamelaunch_source_port = SOURCEPORT_IDUKE3D;
  else
    if (l_selected_port == SRCPORTNAME_DUKE3DW32)
      g_configuration->gamelaunch_source_port = SOURCEPORT_DUKE3DW32;
  else
    if (l_selected_port == SRCPORTNAME_XDUKE)
      g_configuration->gamelaunch_source_port = SOURCEPORT_XDUKE;
  else
    if (l_selected_port == SRCPORTNAME_DOSDUKE)
      g_configuration->gamelaunch_source_port = SOURCEPORT_DOSDUKE;
  else
    if (l_selected_port == SRCPORTNAME_JFSW)
      g_configuration->gamelaunch_source_port = SOURCEPORT_JFSW;
  else
    if (l_selected_port == SRCPORTNAME_SWP)
      g_configuration->gamelaunch_source_port = SOURCEPORT_SWP;
  else // SRCPORTNAME_DOSSW
    g_configuration->gamelaunch_source_port = SOURCEPORT_DOSSW;

  g_configuration->gamelaunch_roomname = m_mproomnametextCtrl->GetValue();
  g_configuration->gamelaunch_numofplayers = m_mpmaxnumplayerschoice->GetSelection()+2;
  g_configuration->gamelaunch_dn3dgametype = (DN3DMPGameT_Type)(m_mpgametypechoice->GetSelection()+1);
  g_configuration->gamelaunch_dn3dspawn = (DN3DSpawnType)(m_mpspawnchoice->GetSelection()+1);
	if (!m_mpfraglimittextCtrl->GetValue().ToLong(&g_configuration->gamelaunch_fraglimit))
		g_configuration->gamelaunch_fraglimit = 0;
	g_configuration->gamelaunch_noteamchange = m_mpnoteamchangecheckBox->GetValue();
	g_configuration->gamelaunch_forcerespawn = m_mpforcerespawncheckBox->GetValue();
  g_configuration->gamelaunch_args_for_host = m_mpextrahostargstextCtrl->GetValue();
  g_configuration->gamelaunch_args_for_all = m_mpextraallargstextCtrl->GetValue();

  if (m_mpgamechoice->GetStringSelection() == GAMENAME_DN3D)
  {
    g_configuration->gamelaunch_dn3d_skill = m_mpskillchoice->GetSelection();
    g_configuration->gamelaunch_dn3dlvl = g_GetDN3DLevelNumBySelection(m_mpepimapchoice->GetSelection());
//  l_level_num = g_GetDN3DLevelNumBySelection(m_mpepimapchoice->GetSelection());
//  if (l_level_num)
//  g_configuration->gamelaunch_dn3dlvl = l_level_num;
    if (m_mpepimapradioBtn->GetValue())
      g_configuration->gamelaunch_dn3dusermap = wxEmptyString;
    else
      g_configuration->gamelaunch_dn3dusermap = m_mpusermapchoice->GetStringSelection();
//  l_map_filename = m_mpusermapchoice->GetStringSelection();
//  if (!l_map_filename.IsEmpty())
//    g_configuration->gamelaunch_dn3dusermap = l_map_filename;
    g_configuration->gamelaunch_dn3dmodfiles = m_mpusedmodfileslistBox->GetStrings();
  }
  else
  {
    g_configuration->gamelaunch_sw_skill = m_mpskillchoice->GetSelection();
    g_configuration->gamelaunch_swlvl = g_GetSWLevelNumBySelection(m_mpepimapchoice->GetSelection());
//  l_level_num = g_GetSWLevelNumBySelection(m_mpepimapchoice->GetSelection());
//  if (l_level_num)
//  g_configuration->gamelaunch_swlvl = l_level_num;
    if (m_mpepimapradioBtn->GetValue())
      g_configuration->gamelaunch_swusermap = wxEmptyString;
    else
      g_configuration->gamelaunch_swusermap = m_mpusermapchoice->GetStringSelection();
//  l_map_filename = m_mpusermapchoice->GetStringSelection();
//  if (!l_map_filename.IsEmpty())
//    g_configuration->gamelaunch_swusermap = l_map_filename;
    g_configuration->gamelaunch_swmodfiles = m_mpusedmodfileslistBox->GetStrings();
  }
  g_configuration->gamelaunch_modname = m_mpmodnametextCtrl->GetValue();
  g_configuration->gamelaunch_show_modurl = m_mpmodurlcheckBox->GetValue();
  g_configuration->gamelaunch_modurl = m_mpmodurltextCtrl->GetValue();
  g_configuration->gamelaunch_advertise = m_mpadvertiseroomcheckBox->GetValue();
  g_configuration->gamelaunch_skip_settings = m_mpnodialogcheckBox->GetValue();
  g_configuration->gamelaunch_use_masterslave = m_mpusemasterslavecheckBox->GetValue();
  g_configuration->Save();
}
