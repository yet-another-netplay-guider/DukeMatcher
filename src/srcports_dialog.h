/*************************************************************************

Copyright 2008-2009 - NY00123

This file is part of DUKEMATCHER.

DUKEMATCHER is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

DUKEMATCHER is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DUKEMATCHER; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*************************************************************************/

#ifndef _DUKEMATCHER_SRCPORTSDIALOG_H_
#define _DUKEMATCHER_SRCPORTSDIALOG_H_

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/wx.h>
#include <wx/notebook.h>
//#include <wx/listbook.h>
#include <wx/statline.h>
#endif

#include "theme.h"

#define ID_EDUKE32CHECKBOX 1000
#define ID_LOCATEEDUKE32 1001
#define ID_JFDUKE3DCHECKBOX 1002
#define ID_LOCATEJFDUKE3D 1003
#define ID_IDUKE3DCHECKBOX 1004
#define ID_LOCATEIDUKE3D 1005
#define ID_XDUKECHECKBOX 1006
#define ID_LOCATEXDUKE 1007
#define ID_DUKE3DW32CHECKBOX 1008
#define ID_LOCATEDUKE3DW32 1009
#define ID_DOSDUKECHECKBOX 1010
#define ID_LOCATEDOSDUKE 1011
#define ID_SELECTDUKEMAPSFOLDER 1012
#define ID_SELECTDUKEMODSFOLDER 1013
#define ID_JFSWCHECKBOX 1014
#define ID_LOCATEJFSW 1015
#define ID_SWPCHECKBOX 1016
#define ID_LOCATESWP 1017
#define ID_DOSSWCHECKBOX 1018
#define ID_LOCATEDOSSW 1019
#define ID_SELECTSWMAPSFOLDER 1020
#define ID_SELECTSWMODSFOLDER 1021
#define ID_DOSBOXCHECKBOX 1022
#define ID_LOCATEDOSBOX 1023
#define ID_DOSBOXCONFCHECKBOX 1024
#define ID_LOCATEDOSBOXCONF 1025
#define ID_DOSBOXNOCDRADIOBTN 1026
#define ID_DOSBOXCDLOCATIONRADIOBTN 1027
#define ID_SELECTDOSBOXCDLOCATION 1028
#define ID_DOSBOXCDIMAGERADIOBTN 1029
#define ID_LOCATEDOSBOXCDIMAGE 1030

#ifndef __WXMSW__
#define ID_WINECHECKBOX 1031
#define ID_LOCATEWINE 1032
#endif

class SRCPortsDialog : public DUKEMATCHERDialog
{
public:
  SRCPortsDialog();
  ~SRCPortsDialog();
  void OnLocateFile(wxCommandEvent& event);
  void OnSelectDir(wxCommandEvent& event);
  void OnCheckBoxClick(wxCommandEvent& event);
  void OnRadioBtnClick(wxCommandEvent& event);

//void OnApply(wxCommandEvent& event);
  void OnCancel(wxCommandEvent& event);
  void OnOk(wxCommandEvent& event);
  void ApplySettings();

private:
  DUKEMATCHERNotebook* m_srcportsnotebook;
  DUKEMATCHERPanel* m_srcportduke3dpanel;
  DUKEMATCHERCheckBox* m_eduke32availcheckBox;
  DUKEMATCHERTextCtrl* m_eduke32bintextCtrl;
  DUKEMATCHERButton* m_eduke32locatebutton;
  DUKEMATCHERCheckBox* m_jfduke3davailcheckBox;
  DUKEMATCHERTextCtrl* m_jfduke3dbintextCtrl;
  DUKEMATCHERButton* m_jfduke3dlocatebutton;
  DUKEMATCHERCheckBox* m_iduke3davailcheckBox;
  DUKEMATCHERTextCtrl* m_iduke3dbintextCtrl;
  DUKEMATCHERButton* m_iduke3dlocatebutton;
  DUKEMATCHERCheckBox* m_duke3dw32availcheckBox;
  DUKEMATCHERTextCtrl* m_duke3dw32bintextCtrl;
  DUKEMATCHERButton* m_duke3dw32locatebutton;
  DUKEMATCHERCheckBox* m_xdukeavailcheckBox;
  DUKEMATCHERTextCtrl* m_xdukebintextCtrl;
  DUKEMATCHERButton* m_xdukelocatebutton;
  DUKEMATCHERCheckBox* m_dosdukeavailcheckBox;
  DUKEMATCHERTextCtrl* m_dosdukebintextCtrl;
  DUKEMATCHERButton* m_dosdukelocatebutton;
  wxStaticLine* m_prefduke3dstaticline;
  wxStaticText* m_duke3dmapsstaticText;
  DUKEMATCHERTextCtrl* m_duke3dmapstextCtrl;
  DUKEMATCHERButton* m_duke3dmapsselectbutton;
  wxStaticText* m_duke3dmodsstaticText;
  DUKEMATCHERTextCtrl* m_duke3dmodstextCtrl;
  DUKEMATCHERButton* m_duke3dmodsselectbutton;
  DUKEMATCHERPanel* m_srcportswpanel;
  DUKEMATCHERCheckBox* m_jfswavailcheckBox;
  DUKEMATCHERTextCtrl* m_jfswbintextCtrl;
  DUKEMATCHERButton* m_jfswlocatebutton;
  DUKEMATCHERCheckBox* m_swpavailcheckBox;
  DUKEMATCHERTextCtrl* m_swpbintextCtrl;
  DUKEMATCHERButton* m_swplocatebutton;
  DUKEMATCHERCheckBox* m_dosswavailcheckBox;
  DUKEMATCHERTextCtrl* m_dosswbintextCtrl;
  DUKEMATCHERButton* m_dosswlocatebutton;
  wxStaticLine* m_prefswstaticline;
  wxStaticText* m_swmapsstaticText;
  DUKEMATCHERTextCtrl* m_swmapstextCtrl;
  DUKEMATCHERButton* m_swmapsselectbutton;
  wxStaticText* m_swmodsstaticText;
  DUKEMATCHERTextCtrl* m_swmodstextCtrl;
  DUKEMATCHERButton* m_swmodsselectbutton;
  DUKEMATCHERPanel* m_srcportdosboxpanel;
  DUKEMATCHERCheckBox* m_dosboxavailcheckBox;
  DUKEMATCHERTextCtrl* m_dosboxbintextCtrl;
  DUKEMATCHERButton* m_dosboxlocatebutton;
  DUKEMATCHERCheckBox* m_dosboxconfcheckBox;
  DUKEMATCHERTextCtrl* m_dosboxconftextCtrl;
  DUKEMATCHERButton* m_dosboxconflocatebutton;
  DUKEMATCHERRadioButton* m_dosboxnocdradioBtn;
  DUKEMATCHERRadioButton* m_dosboxcdlocationradioBtn;
  DUKEMATCHERTextCtrl* m_dosboxcdlocationtextCtrl;
  DUKEMATCHERButton* m_dosboxcdlocationselectbutton;
  DUKEMATCHERRadioButton* m_dosboxcdimageradioBtn;
  DUKEMATCHERTextCtrl* m_dosboxcdimagetextCtrl;
  DUKEMATCHERButton* m_dosboxcdimagelocatebutton;
#ifndef __WXMSW__
  DUKEMATCHERPanel* m_srcportwinepanel;
  DUKEMATCHERCheckBox* m_usewinecheckBox;
  DUKEMATCHERTextCtrl* m_winebintextCtrl;
  wxStaticText* m_winestaticText;
  DUKEMATCHERButton* m_winelocatebutton;
#endif
  wxStdDialogButtonSizer* m_srcportssdbSizer;
  DUKEMATCHERButton* m_srcportssdbSizerOK;
  DUKEMATCHERButton* m_srcportssdbSizerCancel;

  DECLARE_EVENT_TABLE()
};

#endif

