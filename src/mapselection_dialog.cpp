/*************************************************************************

Copyright 2008-2009 - NY00123

This file is part of DUKEMATCHER.

DUKEMATCHER is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

DUKEMATCHER is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DUKEMATCHER; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*************************************************************************/

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/wx.h>
#include <wx/filename.h>
#endif

#include "mapselection_dialog.h"
#include "config.h"
#include "dukematcher.h"
#include "theme.h"
#include "dukematcher.xpm"

BEGIN_EVENT_TABLE(MapSelectionDialog, DUKEMATCHERDialog)
  EVT_BUTTON(ID_ADDNEWMAP, MapSelectionDialog::OnAddNewMap)
  EVT_BUTTON(wxID_OK, MapSelectionDialog::OnOKOrCancel)
  EVT_LISTBOX_DCLICK(wxID_OK, MapSelectionDialog::OnOKOrCancel)
  EVT_BUTTON(wxID_CANCEL, MapSelectionDialog::OnOKOrCancel)
END_EVENT_TABLE()

MapSelectionDialog::MapSelectionDialog( wxWindow* parent, GameType game) : DUKEMATCHERDialog( parent, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE )
{
	wxBoxSizer* MapSelectionbSizer = new wxBoxSizer( wxVERTICAL );
	
	m_mapselectionpanel = new DUKEMATCHERPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );

	/****** WORKAROUND: Why should the button be created before, so with dark theme it isn't fully grey? ******/
	m_mapselectionsdbSizer = new wxStdDialogButtonSizer();
	m_mapselectionsdbSizerOK = new DUKEMATCHERButton( m_mapselectionpanel, wxID_OK );
	m_mapselectionsdbSizer->AddButton( m_mapselectionsdbSizerOK );
	m_mapselectionsdbSizerCancel = new DUKEMATCHERButton( m_mapselectionpanel, wxID_CANCEL );
	m_mapselectionsdbSizer->AddButton( m_mapselectionsdbSizerCancel );
	m_mapselectionsdbSizer->Realize();

	wxBoxSizer* mapselectionpanelbSizer = new wxBoxSizer( wxVERTICAL );
	
	m_mapselectionlistBox = new DUKEMATCHERListBox( m_mapselectionpanel, wxID_OK, wxDefaultPosition, wxDLG_UNIT(this, wxSize(64, 80)), 0, 0, wxLB_SORT);
	mapselectionpanelbSizer->Add( m_mapselectionlistBox, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxEXPAND, 5 );
	
	m_mapselectionaddbutton = new DUKEMATCHERButton( m_mapselectionpanel, ID_ADDNEWMAP, wxT("Add a new map file..."), wxDefaultPosition, wxDefaultSize, 0 );
	mapselectionpanelbSizer->Add( m_mapselectionaddbutton, 0, wxALL|wxALIGN_CENTER_HORIZONTAL, 5 );
	
	/****** WORKAROUND: Why should the button be created before, so with dark theme it isn't fully grey? ******/
/*	m_mapselectionsdbSizer = new wxStdDialogButtonSizer();
	m_mapselectionsdbSizerOK = new DUKEMATCHERButton( m_mapselectionpanel, wxID_OK );
	m_mapselectionsdbSizer->AddButton( m_mapselectionsdbSizerOK );
	m_mapselectionsdbSizerCancel = new DUKEMATCHERButton( m_mapselectionpanel, wxID_CANCEL );
	m_mapselectionsdbSizer->AddButton( m_mapselectionsdbSizerCancel );
	m_mapselectionsdbSizer->Realize();*/
	mapselectionpanelbSizer->Add( m_mapselectionsdbSizer, 0, wxALIGN_CENTER_HORIZONTAL, 5 );
	
	m_mapselectionpanel->SetSizer( mapselectionpanelbSizer );
	m_mapselectionpanel->Layout();
	mapselectionpanelbSizer->Fit( m_mapselectionpanel );
	MapSelectionbSizer->Add( m_mapselectionpanel, 1, wxEXPAND | wxALL, 5 );
	
	SetSizer( MapSelectionbSizer );
	Layout();
	MapSelectionbSizer->Fit( this );

  Centre();
  SetIcon(wxIcon(dukematcher_xpm));
  m_game = game;
  RefreshMapsList();
  m_parent = parent;
}

void MapSelectionDialog::ShowDialog()
{
  Show(true);
  m_mapselectionlistBox->SetFocus();
}

void MapSelectionDialog::RefreshMapsList()
{
  wxArrayString l_file_list;

  if (m_game == GAME_DN3D)
  {
    g_AddAllDN3DMapFiles(&l_file_list);
    SetTitle(wxT("Select a Duke Nukem 3D map file"));
  }
  else
  {
    g_AddAllSWMapFiles(&l_file_list);
    SetTitle(wxT("Select a Shadow Warrior map file"));
  }
  m_mapselectionlistBox->Set(l_file_list);
}

bool MapSelectionDialog::GetMapSelection(wxString& map_name)
{
  int l_selection = m_mapselectionlistBox->GetSelection();
  if (l_selection == wxNOT_FOUND)
    return false;
  map_name = m_mapselectionlistBox->GetString(l_selection);
  return true;
}

void MapSelectionDialog::OnAddNewMap(wxCommandEvent& WXUNUSED(event))
{
  GameType l_last_game = m_game; // Might change while the dialog appears.
  bool l_do_copy = true, l_file_is_new = true;
  wxFileDialog l_dialog(NULL, wxT("Select a Build MAP file"), wxEmptyString, wxEmptyString,
                        wxT("Build MAP files (*.map)|*.map|All files|*"), wxFD_OPEN|wxFD_FILE_MUST_EXIST);
  if (l_dialog.ShowModal() == wxID_OK)
  {
    wxString l_fullpath = l_dialog.GetPath(), l_newpath;
    wxString l_filename = ((wxFileName)l_fullpath).GetFullName();
    wxString* l_maps_dir;
    if (l_last_game == GAME_DN3D)
      l_maps_dir = &(g_configuration->dn3d_maps_dir);
    else
      l_maps_dir = &(g_configuration->sw_maps_dir);
    if (!wxDirExists(*l_maps_dir))
      wxFileName::Mkdir(*l_maps_dir, 0777, wxPATH_MKDIR_FULL);
    l_newpath = *l_maps_dir + wxFILE_SEP_PATH + l_filename;
    if (wxFileExists(l_newpath))
    {
      l_file_is_new = false;
      l_do_copy = (wxMessageBox(wxT("WARNING: A map file with an identical filename already exists in the configured maps directory for the game, ")
                                + *l_maps_dir + wxT("\nAre you sure you want to overwrite it?"),
                                wxT("Map file exists"), wxYES_NO|wxICON_EXCLAMATION) == wxYES);
    }
    if (l_do_copy)
    {
      if (wxCopyFile(l_fullpath, l_newpath))
      {
        if (l_last_game == m_game)
        {
          if (l_file_is_new)
          {
            wxArrayString l_file_list = m_mapselectionlistBox->GetStrings();
            l_file_list.Add(l_filename);
            l_file_list.Sort();
            m_mapselectionlistBox->Set(l_file_list);
          }
          m_mapselectionlistBox->SetStringSelection(l_filename);
        }
      }
      else
        wxLogError(wxT("ERROR: Couldn't make a copy of the map file in the configured maps directory."));
    }
  }
  if (l_last_game != m_game)
    wxMessageBox(wxT("ATTENTION: The host has changed the game. The list of maps to select has been refreshed."),
                 wxT("Host has changed game"), wxOK|wxICON_INFORMATION);
}

void MapSelectionDialog::OnOKOrCancel(wxCommandEvent& event)
{
  m_response = (event.GetId() == wxID_OK);
  if (m_response)
  {
    int l_selection = m_mapselectionlistBox->GetSelection();
    if (l_selection == wxNOT_FOUND)
    {
      wxMessageBox(wxT("Please select a map before confirming."), wxT("No map is selected"), wxOK|wxICON_INFORMATION);
      return;
    }
    m_response_filename = m_mapselectionlistBox->GetString(l_selection);
  }
  wxCommandEvent* l_close_event = new wxCommandEvent(wxEVT_COMMAND_BUTTON_CLICKED, ID_MAPSELECTIONCLOSE);
#if wxCHECK_VERSION(2, 9, 0)
  m_parent->GetEventHandler()->QueueEvent(l_close_event);
#else
  m_parent->AddPendingEvent(*l_close_event);
  delete l_close_event;
#endif
  Hide();
}
