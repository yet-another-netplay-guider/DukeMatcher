/*************************************************************************

Copyright 2008-2009 - NY00123

This file is part of DUKEMATCHER.

DUKEMATCHER is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

DUKEMATCHER is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DUKEMATCHER; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*************************************************************************/

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/wx.h>
#include <wx/gbsizer.h>
#endif

#include "theme.h"

#include "adv_dialog.h"
#include "main_frame.h"
#include "config.h"
#include "dukematcher.h"
#include "dukematcher.xpm"

BEGIN_EVENT_TABLE(AdvDialog, DUKEMATCHERDialog)

EVT_BUTTON(wxID_OK, AdvDialog::OnOk)
//EVT_BUTTON(wxID_APPLY, AdvDialog::OnApply)
EVT_BUTTON(wxID_CANCEL, AdvDialog::OnCancel)

EVT_BUTTON(ID_SELECTEDUKE32USERPATH, AdvDialog::OnSelectDir)
EVT_BUTTON(ID_SELECTJFDUKE3DUSERPATH, AdvDialog::OnSelectDir)
EVT_BUTTON(ID_SELECTIDUKE3DUSERPATH, AdvDialog::OnSelectDir)
EVT_BUTTON(ID_SELECTJFSWUSERPATH, AdvDialog::OnSelectDir)

EVT_BUTTON(ID_LOCATEBROWSER, AdvDialog::OnLocateExec)

EVT_CHECKBOX(ID_EDUKE32USERPATHCHECKBOX, AdvDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_JFDUKE3DUSERPATHCHECKBOX, AdvDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_IDUKE3DUSERPATHCHECKBOX, AdvDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_JFSWUSERPATHCHECKBOX, AdvDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_BROWSERCHECKBOX, AdvDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_SOUNDCMDCHECKBOX, AdvDialog::OnCheckBoxClick)

EVT_BUTTON(ID_SERVERLISTADD, AdvDialog::OnServerListAdd)
EVT_BUTTON(ID_SERVERLISTREM, AdvDialog::OnServerListRem)

END_EVENT_TABLE()

AdvDialog::AdvDialog() : DUKEMATCHERDialog(NULL, wxID_ANY, wxT("Advanced settings"), wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE|wxDIALOG_NO_PARENT, wxEmptyString)
{
	wxBoxSizer* AdvDialogSizer = new wxBoxSizer( wxVERTICAL );

	m_advancednotebook = new DUKEMATCHERNotebook( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0 );
	m_advpanel = new DUKEMATCHERPanel( m_advancednotebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* advpanelbSizer = new wxBoxSizer( wxVERTICAL );

	wxGridBagSizer* advpanelgbSizer = new wxGridBagSizer( 0, 0 );
	advpanelgbSizer->AddGrowableCol( 1 );
	advpanelgbSizer->SetFlexibleDirection( wxBOTH );
	advpanelgbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	m_userpathstaticText = new wxStaticText( m_advpanel, wxID_ANY, wxT("On multi-user systems it may be required to specify user folders\nwhere it's possible to put temporary files (e.g. MAP files).\nNOTE THAT THE SOURCE PORT MUST SUPPORT THAT\nUSING THE SELECTED FOLDER \"OUT OF THE BOX\"."), wxDefaultPosition, wxDefaultSize, 0 );
	advpanelgbSizer->Add( m_userpathstaticText, wxGBPosition( 0, 0 ), wxGBSpan( 1, 4 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_eduke32userpathcheckBox = new DUKEMATCHERCheckBox( m_advpanel, ID_EDUKE32USERPATHCHECKBOX, wxT("EDuke32"), wxDefaultPosition, wxDefaultSize, 0 );
	
	advpanelgbSizer->Add( m_eduke32userpathcheckBox, wxGBPosition( 1, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_eduke32userpathtextCtrl = new DUKEMATCHERTextCtrl( m_advpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
	advpanelgbSizer->Add( m_eduke32userpathtextCtrl, wxGBPosition( 1, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );
	
	m_eduke32userpathbutton = new DUKEMATCHERButton( m_advpanel, ID_SELECTEDUKE32USERPATH, wxT("Select"), wxDefaultPosition, wxDefaultSize, 0 );
	advpanelgbSizer->Add( m_eduke32userpathbutton, wxGBPosition( 1, 3 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_iduke3duserpathcheckBox = new DUKEMATCHERCheckBox( m_advpanel, ID_IDUKE3DUSERPATHCHECKBOX, wxT("hDuke"), wxDefaultPosition, wxDefaultSize, 0 );
	
	advpanelgbSizer->Add( m_iduke3duserpathcheckBox, wxGBPosition( 3, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_jfduke3duserpathcheckBox = new DUKEMATCHERCheckBox( m_advpanel, ID_JFDUKE3DUSERPATHCHECKBOX, wxT("JFDuke3D"), wxDefaultPosition, wxDefaultSize, 0 );
	
	advpanelgbSizer->Add( m_jfduke3duserpathcheckBox, wxGBPosition( 2, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_jfduke3duserpathtextCtrl = new DUKEMATCHERTextCtrl( m_advpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
	advpanelgbSizer->Add( m_jfduke3duserpathtextCtrl, wxGBPosition( 2, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );
	
	m_jfduke3duserpathbutton = new DUKEMATCHERButton( m_advpanel, ID_SELECTJFDUKE3DUSERPATH, wxT("Select"), wxDefaultPosition, wxDefaultSize, 0 );
	advpanelgbSizer->Add( m_jfduke3duserpathbutton, wxGBPosition( 2, 3 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_iduke3duserpathtextCtrl = new DUKEMATCHERTextCtrl( m_advpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
	advpanelgbSizer->Add( m_iduke3duserpathtextCtrl, wxGBPosition( 3, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_iduke3duserpathbutton = new DUKEMATCHERButton( m_advpanel, ID_SELECTIDUKE3DUSERPATH, wxT("Select"), wxDefaultPosition, wxDefaultSize, 0 );
	advpanelgbSizer->Add( m_iduke3duserpathbutton, wxGBPosition( 3, 3 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_jfswuserpathcheckBox = new DUKEMATCHERCheckBox( m_advpanel, ID_JFSWUSERPATHCHECKBOX, wxT("JFShadowWarrior"), wxDefaultPosition, wxDefaultSize, 0 );
	
	advpanelgbSizer->Add( m_jfswuserpathcheckBox, wxGBPosition( 4, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_jfswuserpathtextCtrl = new DUKEMATCHERTextCtrl( m_advpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
	advpanelgbSizer->Add( m_jfswuserpathtextCtrl, wxGBPosition( 4, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_jfswuserpathbutton = new DUKEMATCHERButton( m_advpanel, ID_SELECTJFSWUSERPATH, wxT("Select"), wxDefaultPosition, wxDefaultSize, 0 );
	advpanelgbSizer->Add( m_jfswuserpathbutton, wxGBPosition( 4, 3 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_overridebrowsercheckBox = new DUKEMATCHERCheckBox( m_advpanel, ID_BROWSERCHECKBOX, wxT("Override default browser for opening URLs:"), wxDefaultPosition, wxDefaultSize, 0 );
	
	advpanelgbSizer->Add( m_overridebrowsercheckBox, wxGBPosition( 5, 0 ), wxGBSpan( 1, 4 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_browsertextCtrl = new DUKEMATCHERTextCtrl( m_advpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
	advpanelgbSizer->Add( m_browsertextCtrl, wxGBPosition( 6, 0 ), wxGBSpan( 1, 3 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_browserlocatebutton = new DUKEMATCHERButton( m_advpanel, ID_LOCATEBROWSER, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	advpanelgbSizer->Add( m_browserlocatebutton, wxGBPosition( 6, 3 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_overridesoundcheckBox = new DUKEMATCHERCheckBox( m_advpanel, ID_SOUNDCMDCHECKBOX, wxT("Override sound playback using the following command:"), wxDefaultPosition, wxDefaultSize, 0 );
	
	advpanelgbSizer->Add( m_overridesoundcheckBox, wxGBPosition( 7, 0 ), wxGBSpan( 1, 4 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_soundcmdtextCtrl = new DUKEMATCHERTextCtrl( m_advpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
	advpanelgbSizer->Add( m_soundcmdtextCtrl, wxGBPosition( 8, 0 ), wxGBSpan( 1, 3 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_localipoptimizecheckBox = new DUKEMATCHERCheckBox( m_advpanel, wxID_ANY, wxT("In your hosted room, local players will connect to each other using their local IPs."), wxDefaultPosition, wxDefaultSize, 0 );
	
	advpanelgbSizer->Add( m_localipoptimizecheckBox, wxGBPosition( 9, 0 ), wxGBSpan( 1, 4 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
#ifndef __WXMSW__
	m_terminalstaticText = new wxStaticText( m_advpanel, wxID_ANY, wxT("When launching a game from a script, open a terminal \nusing the following command (may include any argument).\nExamples: gnome-terminal -x, konsole -e, open"), wxDefaultPosition, wxDefaultSize, 0 );
	advpanelgbSizer->Add( m_terminalstaticText, wxGBPosition( 10, 0 ), wxGBSpan( 1, 4 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_terminaltextCtrl = new DUKEMATCHERTextCtrl( m_advpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
	advpanelgbSizer->Add( m_terminaltextCtrl, wxGBPosition( 11, 0 ), wxGBSpan( 1, 3 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
#endif
	advpanelbSizer->Add( advpanelgbSizer, 1, wxEXPAND, 0 );
	
	m_advpanel->SetSizer( advpanelbSizer );
	m_advpanel->Layout();
	advpanelbSizer->Fit( m_advpanel );
	m_advancednotebook->AddPage( m_advpanel, wxT("General"), true );
	m_extraserverspanel = new DUKEMATCHERPanel( m_advancednotebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* extraserverbSizer = new wxBoxSizer( wxVERTICAL );
	
	wxFlexGridSizer* extraserverfgSizer = new wxFlexGridSizer( 2, 4, 0, 0 );
	extraserverfgSizer->AddGrowableCol( 0 );
	extraserverfgSizer->SetFlexibleDirection( wxBOTH );
	extraserverfgSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	m_extraserveraddrstaticText = new wxStaticText( m_extraserverspanel, wxID_ANY, wxT("Hostname or IP"), wxDefaultPosition, wxDefaultSize, 0 );
	extraserverfgSizer->Add( m_extraserveraddrstaticText, 0, wxALL|wxALIGN_CENTER_HORIZONTAL, 5 );
	
	
	extraserverfgSizer->Add( 0, 0, 1, wxEXPAND, 5 );
	
	m_extraserverportnumstaticText = new wxStaticText( m_extraserverspanel, wxID_ANY, wxT("Port Number"), wxDefaultPosition, wxDefaultSize, 0 );
	extraserverfgSizer->Add( m_extraserverportnumstaticText, 0, wxALL|wxALIGN_CENTER_HORIZONTAL, 5 );
	
	
	extraserverfgSizer->Add( 0, 0, 1, wxEXPAND, 5 );
	
	m_extraserverhostaddrtextCtrl = new DUKEMATCHERTextCtrl( m_extraserverspanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER );
	extraserverfgSizer->Add( m_extraserverhostaddrtextCtrl, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_extraserverseparatorstaticText = new wxStaticText( m_extraserverspanel, wxID_ANY, wxT(":"), wxDefaultPosition, wxDefaultSize, 0 );
	extraserverfgSizer->Add( m_extraserverseparatorstaticText, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_extraserverportnumtextCtrl = new DUKEMATCHERTextCtrl( m_extraserverspanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER );
	extraserverfgSizer->Add( m_extraserverportnumtextCtrl, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_extraserveraddbutton = new DUKEMATCHERButton( m_extraserverspanel, ID_SERVERLISTADD, wxT("Add"), wxDefaultPosition, wxDefaultSize, 0 );
	extraserverfgSizer->Add( m_extraserveraddbutton, 0, wxALL|wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL, 5 );
	
	extraserverbSizer->Add( extraserverfgSizer, 0, wxEXPAND, 5 );
	
	wxBoxSizer* extraserverslistbSizer = new wxBoxSizer( wxHORIZONTAL );
	
	m_extraserverlistBox = new DUKEMATCHERListBox( m_extraserverspanel, wxID_ANY, wxDefaultPosition, (wxDLG_UNIT(this, wxSize(-1, 80)))); 
	extraserverslistbSizer->Add( m_extraserverlistBox, 1, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_extraserverrembutton = new DUKEMATCHERButton( m_extraserverspanel, ID_SERVERLISTREM, wxT("Remove"), wxDefaultPosition, wxDefaultSize, 0 );
	extraserverslistbSizer->Add( m_extraserverrembutton, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	extraserverbSizer->Add( extraserverslistbSizer, 0, wxEXPAND, 5 );
	
	
	extraserverbSizer->Add( 0, 0, 1, wxEXPAND, 5 );
	
	m_extraserverspanel->SetSizer( extraserverbSizer );
	m_extraserverspanel->Layout();
	extraserverbSizer->Fit( m_extraserverspanel );
	m_advancednotebook->AddPage( m_extraserverspanel, wxT("Extra server lists"), false );
	
	AdvDialogSizer->Add( m_advancednotebook, 1, wxEXPAND | wxALL, 5 );
	
	m_advsdbSizer = new wxStdDialogButtonSizer();
	m_advsdbSizerOK = new DUKEMATCHERButton( this, wxID_OK );
	m_advsdbSizer->AddButton( m_advsdbSizerOK );
	m_advsdbSizerCancel = new DUKEMATCHERButton( this, wxID_CANCEL );
	m_advsdbSizer->AddButton( m_advsdbSizerCancel );
	m_advsdbSizer->Realize();
	AdvDialogSizer->Add( m_advsdbSizer, 0, wxALIGN_CENTER_HORIZONTAL, 5 );
	
	this->SetSizer( AdvDialogSizer );
	this->Layout();
	AdvDialogSizer->Fit( this );

  Centre();
  SetIcon(wxIcon(dukematcher_xpm));

  m_eduke32userpathcheckBox->SetValue(g_configuration->eduke32_userpath_use);
  m_eduke32userpathtextCtrl->SetValue(g_configuration->eduke32_userpath);
  m_eduke32userpathtextCtrl->Enable(g_configuration->eduke32_userpath_use);
  m_eduke32userpathbutton->Enable(g_configuration->eduke32_userpath_use);

  m_jfduke3duserpathcheckBox->SetValue(g_configuration->jfduke3d_userpath_use);
  m_jfduke3duserpathtextCtrl->SetValue(g_configuration->jfduke3d_userpath);
  m_jfduke3duserpathtextCtrl->Enable(g_configuration->jfduke3d_userpath_use);
  m_jfduke3duserpathbutton->Enable(g_configuration->jfduke3d_userpath_use);

  m_iduke3duserpathcheckBox->SetValue(g_configuration->iduke3d_userpath_use);
  m_iduke3duserpathtextCtrl->SetValue(g_configuration->iduke3d_userpath);
  m_iduke3duserpathtextCtrl->Enable(g_configuration->iduke3d_userpath_use);
  m_iduke3duserpathbutton->Enable(g_configuration->iduke3d_userpath_use);

  m_jfswuserpathcheckBox->SetValue(g_configuration->jfsw_userpath_use);
  m_jfswuserpathtextCtrl->SetValue(g_configuration->jfsw_userpath);
  m_jfswuserpathtextCtrl->Enable(g_configuration->jfsw_userpath_use);
  m_jfswuserpathbutton->Enable(g_configuration->jfsw_userpath_use);
#ifndef __WXMSW__
  m_terminaltextCtrl->SetValue(g_configuration->terminal_fullcmd);
#endif
  m_overridebrowsercheckBox->SetValue(g_configuration->override_browser);
  m_browsertextCtrl->SetValue(g_configuration->browser_exec);
  m_browsertextCtrl->Enable(g_configuration->override_browser);
  m_browserlocatebutton->Enable(g_configuration->override_browser);

  m_overridesoundcheckBox->SetValue(g_configuration->override_soundcmd);
  m_soundcmdtextCtrl->SetValue(g_configuration->playsound_cmd);
  m_soundcmdtextCtrl->Enable(g_configuration->override_soundcmd);

  m_localipoptimizecheckBox->SetValue(g_configuration->enable_localip_optimization);

  m_extra_addrs = g_configuration->extraservers_addrs;
  m_extra_portnums = g_configuration->extraservers_portnums;
  size_t l_num_of_items = m_extra_addrs.GetCount();
  for (size_t l_loop_var = 0; l_loop_var < l_num_of_items; l_loop_var++)
    m_extraserverlistBox->Append(m_extra_addrs[l_loop_var] + wxString::Format(wxT(":%d"), m_extra_portnums[l_loop_var]));
}

AdvDialog::~AdvDialog()
{
  if (!g_main_frame)
    g_main_frame = new MainFrame;
  g_main_frame->ShowMainFrame();
//g_adv_dialog = NULL;
}

void AdvDialog::OnSelectDir(wxCommandEvent& event)
{
  wxDirDialog l_dialog(NULL, wxT("Select a directory"), wxEmptyString,
                       wxDD_DEFAULT_STYLE|wxDD_DIR_MUST_EXIST);
  if (l_dialog.ShowModal() == wxID_OK)
  {
    wxString l_path = l_dialog.GetPath();
    switch (event.GetId())
    {
      case ID_SELECTEDUKE32USERPATH: m_eduke32userpathtextCtrl->SetValue(l_path); break;
      case ID_SELECTJFDUKE3DUSERPATH: m_jfduke3duserpathtextCtrl->SetValue(l_path); break;
      case ID_SELECTIDUKE3DUSERPATH: m_iduke3duserpathtextCtrl->SetValue(l_path); break;
      default: /*ID_SELECTJFSWUSERPATH*/ m_jfswuserpathtextCtrl->SetValue(l_path);
    }
  }
}

void AdvDialog::OnLocateExec(wxCommandEvent& WXUNUSED(event))
{
  wxFileDialog l_dialog(NULL, wxT("Select a file"), wxEmptyString, wxEmptyString,
                        wxT("All files|*"), wxFD_OPEN|wxFD_FILE_MUST_EXIST);
  if (l_dialog.ShowModal() == wxID_OK)
    m_browsertextCtrl->SetValue(l_dialog.GetPath());
}

void AdvDialog::OnCheckBoxClick(wxCommandEvent& event)
{
  bool l_selection = event.GetInt();
  switch (event.GetId())
  {
    case ID_EDUKE32USERPATHCHECKBOX:  m_eduke32userpathtextCtrl->Enable(l_selection);
                                      m_eduke32userpathbutton->Enable(l_selection);
                                      break;
    case ID_JFDUKE3DUSERPATHCHECKBOX: m_jfduke3duserpathtextCtrl->Enable(l_selection);
                                      m_jfduke3duserpathbutton->Enable(l_selection);
                                      break;
    case ID_IDUKE3DUSERPATHCHECKBOX:  m_iduke3duserpathtextCtrl->Enable(l_selection);
                                      m_iduke3duserpathbutton->Enable(l_selection);
                                      break;
    case ID_JFSWUSERPATHCHECKBOX:     m_jfswuserpathtextCtrl->Enable(l_selection);
                                      m_jfswuserpathbutton->Enable(l_selection);
                                      break;
    case ID_BROWSERCHECKBOX:          m_browsertextCtrl->Enable(l_selection);
                                      m_browserlocatebutton->Enable(l_selection);
                                      break;
    default: /*ID_SOUNDCMDCHECKBOX*/  m_soundcmdtextCtrl->Enable(l_selection);
  }
}

void AdvDialog::OnServerListAdd(wxCommandEvent& WXUNUSED(event))
{
  long l_temp_num;
  m_extra_addrs.Add(m_extraserverhostaddrtextCtrl->GetValue());
  if (!((m_extraserverportnumtextCtrl->GetValue()).ToLong(&l_temp_num)))
    l_temp_num = 0;
  m_extra_portnums.Add(l_temp_num);
  l_temp_num = m_extra_addrs.GetCount()-1;
  m_extraserverlistBox->Append(m_extra_addrs[l_temp_num] + wxString::Format(wxT(":%d"), m_extra_portnums[l_temp_num]));
}

void AdvDialog::OnServerListRem(wxCommandEvent& WXUNUSED(event))
{
  int l_selection = m_extraserverlistBox->GetSelection();
  if (l_selection >= 0)
  {
    m_extra_addrs.RemoveAt(l_selection);
    m_extra_portnums.RemoveAt(l_selection);
    m_extraserverlistBox->Delete(l_selection);
    m_extraserverlistBox->SetSelection(l_selection);
  }
}
/*
void AdvDialog::OnApply(wxCommandEvent& WXUNUSED(event))
{
  ApplySettings();
}
*/
void AdvDialog::OnOk(wxCommandEvent& WXUNUSED(event))
{
  ApplySettings();
  Destroy();
}

void AdvDialog::OnCancel(wxCommandEvent& WXUNUSED(event))
{
  Destroy();
}

void AdvDialog::ApplySettings()
{
//wxArrayString l_extra_servers = m_extraserverlistBox->GetStrings();
  g_configuration->eduke32_userpath_use = m_eduke32userpathcheckBox->GetValue();
  g_configuration->eduke32_userpath = m_eduke32userpathtextCtrl->GetValue();
  g_configuration->jfduke3d_userpath_use = m_jfduke3duserpathcheckBox->GetValue();
  g_configuration->jfduke3d_userpath = m_jfduke3duserpathtextCtrl->GetValue();
  g_configuration->iduke3d_userpath_use = m_iduke3duserpathcheckBox->GetValue();
  g_configuration->iduke3d_userpath = m_iduke3duserpathtextCtrl->GetValue();
  g_configuration->jfsw_userpath_use = m_jfswuserpathcheckBox->GetValue();
  g_configuration->jfsw_userpath = m_jfswuserpathtextCtrl->GetValue();
  g_configuration->override_browser = m_overridebrowsercheckBox->GetValue();
  g_configuration->browser_exec = m_browsertextCtrl->GetValue();
  g_configuration->override_soundcmd = m_overridesoundcheckBox->GetValue();
  g_configuration->playsound_cmd = m_soundcmdtextCtrl->GetValue();
  g_configuration->enable_localip_optimization = m_localipoptimizecheckBox->GetValue();
#ifndef __WXMSW__
  g_configuration->terminal_fullcmd = m_terminaltextCtrl->GetValue();
#endif
  g_configuration->extraservers_addrs = m_extra_addrs;
  g_configuration->extraservers_portnums = m_extra_portnums;
  g_configuration->Save();
}
