/*************************************************************************

Copyright 2008-2009 - NY00123

This file is part of DUKEMATCHER.

DUKEMATCHER is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

DUKEMATCHER is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DUKEMATCHER; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*************************************************************************/

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "mp_common.h"

RoomPlayersTable::RoomPlayersTable()
{
  num_players = 0;
}

bool RoomPlayersTable::Add(const wxString& nickname,
                           const wxString& ingameip, long game_port_number)
{
  if (num_players < MAX_NUM_PLAYERS)
  {
    players[num_players].nickname = nickname;
    players[num_players].ingameip = ingameip;
    players[num_players].game_port_number = game_port_number;
    num_players++;
    return true;
  }
  return false;
}

bool RoomPlayersTable::DelByIndex(size_t index)
{
  if (index < num_players)
  {
    index++;
    while (index < num_players)
    {
      players[index-1] = players[index];
      index++;
    }
    num_players--;
    return true;
  }
  return false;
}

size_t RoomPlayersTable::FindIndexByNick(const wxString& nickname)
{
  size_t l_loop_var = 0;
  while (l_loop_var < num_players)
  {
    if (players[l_loop_var].nickname == nickname)
      return l_loop_var;
    l_loop_var++;
  }
  return l_loop_var; // An indication for not finding.
}

// For the host only, at least for now.

ExtendedRoomPlayersTable::ExtendedRoomPlayersTable() : RoomPlayersTable()
{
}

bool ExtendedRoomPlayersTable::Add(const wxString& nickname,
                                   const wxString& ingameip, long game_port_number)
{
  if (num_players < MAX_NUM_PLAYERS)
  {
    players[num_players].nickname = nickname;
    players[num_players].ingameip = ingameip;
    players[num_players].game_port_number = game_port_number;
    requests[num_players].transferrequest = TRANSFERREQUEST_NONE;
//  requests[num_players].awaiting_download = false;
    num_players++;
    return true;
  }
  return false;
}

bool ExtendedRoomPlayersTable::DelByIndex(size_t index)
{
  if (index < num_players)
  {
    index++;
    while (index < num_players)
    {
      players[index-1] = players[index];
      requests[index-1] = requests[index];
      index++;
    }
    num_players--;
    return true;
  }
  return false;
}

/*
ExtendedRoomPlayersTable::ExtendedRoomPlayersTable()
{
  num_players = 0;
}

bool ExtendedRoomPlayersTable::Add(const wxString& nickname,
                                   const wxString& ingameip, long game_port_number)
{
  if (num_players < MAX_NUM_PLAYERS)
  {
    players[num_players].nickname = nickname;
    players[num_players].ingameip = ingameip;
    players[num_players].game_port_number = game_port_number;
    num_players++;
    return true;
  }
  return false;
}

bool ExtendedRoomPlayersTable::DelByIndex(size_t index)
{
  if (index < num_players)
  {
    index++;
    while (index < num_players)
    {
      players[index-1] = players[index];
      index++;
    }
    num_players--;
    return true;
  }
  return false;
}

size_t ExtendedRoomPlayersTable::FindIndexByNick(const wxString& nickname)
{
  size_t l_loop_var = 0;
  while (l_loop_var < num_players)
  {
    if (players[l_loop_var].nickname == nickname)
      return l_loop_var;
    l_loop_var++;
  }
  return l_loop_var; // An indication for not finding.
}
*/

// For the host only as well.
HostRoomClientsTable::HostRoomClientsTable()
{
  num_clients = 0;
}

bool HostRoomClientsTable::Add(wxSocketBase* sock,
                               const wxString& peerdetectedip,
//                             const wxString& selfdetectedip,
                               wxTimer* timer)
{
  if (num_clients < MAX_NUM_PLAYERS)
  {
    clients[num_clients].sock = sock;
    clients[num_clients].peerdetectedip = peerdetectedip;
//  clients[num_clients].selfdetectedip = selfdetectedip;
    clients[num_clients].timer = timer;
    num_clients++;
    return true;
  }
  return false;
}

bool HostRoomClientsTable::PutInRoom(size_t index, size_t inroom_num_clients)
{
  HostRoomClientData l_temp_client;
  size_t l_loop_var;
  if ((index >= num_clients) || (inroom_num_clients >= num_clients))
    return false;
  // Because timers are used, we must "move" the rest of the clients
  // which are not in-room by 1.
//l_temp_client = clients[inroom_num_clients];
  l_temp_client = clients[index];
  for (l_loop_var = index; l_loop_var > inroom_num_clients; l_loop_var--)
    clients[l_loop_var] = clients[l_loop_var-1];
  clients[inroom_num_clients] = l_temp_client;
//clients[index] = l_temp_client;
  return true;
}

bool HostRoomClientsTable::DelBySock(wxSocketBase* sock)
{
  size_t l_loop_var = 0;
  while (l_loop_var < num_clients)
    if (clients[l_loop_var].sock == sock)
    {
      l_loop_var++;
      while (l_loop_var < num_clients)
      {
        clients[l_loop_var-1] = clients[l_loop_var];
        l_loop_var++;
      }
      num_clients--;
      return true;
    }
    else
      l_loop_var++;
  return false;
}

bool HostRoomClientsTable::DelByIndex(size_t index)
{
  if (index < num_clients)
  {
    index++;
    while (index < num_clients)
    {
      clients[index-1] = clients[index];
      index++;
    }
    num_clients--;
    return true;
  }
  return false;
}

size_t HostRoomClientsTable::FindIndexBySock(wxSocketBase* sock)
{
  size_t l_loop_var = 0;
  while (l_loop_var < num_clients)
  {
    if (clients[l_loop_var].sock == sock)
      return l_loop_var;
    l_loop_var++;
  }
  return l_loop_var; // An indication for not finding.
}
