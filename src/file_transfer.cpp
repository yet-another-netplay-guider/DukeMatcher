/*************************************************************************

Copyright 2008-2009 - NY00123

This file is part of DUKEMATCHER.

DUKEMATCHER is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

DUKEMATCHER is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DUKEMATCHER; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*************************************************************************/

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/wx.h>
#include <wx/socket.h>
#include <wx/sckstrm.h>
#include <wx/wfstream.h>
#include <wx/zstream.h>
#endif

#include "file_transfer.h"

#define FILE_TRANSFER_BUFFER_SIZE 16384

FileSendThread::FileSendThread(wxSocketBase* sock, wxWindow* window,
                               wxFile* fp, TransferStateType* transfer_state_ptr)
{
  m_sock = sock;
  m_window = window;
  m_fp = fp;
  m_transfer_state_ptr = transfer_state_ptr;
}

void* FileSendThread::Entry()
{
  char l_buffer[FILE_TRANSFER_BUFFER_SIZE];
  wxInt32 l_num_of_bytes, l_num_of_bytes_left;
//m_sock->SetFlags(wxSOCKET_NOWAIT);
  m_sock->SetFlags(wxSOCKET_NONE|wxSOCKET_BLOCK);
  m_sock->SetTimeout(1);

  while ((!m_fp->Eof()) && (m_sock->IsConnected()) && (*m_transfer_state_ptr == TRANSFERSTATE_BUSY))
  {
    l_num_of_bytes = m_fp->Read(l_buffer, FILE_TRANSFER_BUFFER_SIZE);
    l_num_of_bytes_left = l_num_of_bytes;
    while (l_num_of_bytes_left && (m_sock->IsConnected()) && (*m_transfer_state_ptr == TRANSFERSTATE_BUSY))
    {
      m_sock->Write(l_buffer+l_num_of_bytes-l_num_of_bytes_left, l_num_of_bytes_left);
/*    if ((m_sock->Error()) && (m_sock->LastError() != wxSOCKET_NOERROR)
          && (m_sock->LastError() != wxSOCKET_WOULDBLOCK) && (m_sock->LastError() != wxSOCKET_TIMEDOUT))
      {
        *m_transfer_state_ptr = TRANSFERSTATE_ABORTED;
        wxLogError(wxString::Format(wxT("File transfer error: %d"), m_sock->LastError()));
      }
      else*/
      l_num_of_bytes_left -= m_sock->LastCount();
    }
  }

// Don't close and delete here - May cause a race condition regarding UI updates.
//delete m_fp;
  m_sock->Destroy();

  wxCommandEvent* l_end_event = new wxCommandEvent(wxEVT_COMMAND_BUTTON_CLICKED, ID_TRANSFERTHREADEND);
#if wxCHECK_VERSION(2, 9, 0)
  m_window->GetEventHandler()->QueueEvent(l_end_event);
#else
  m_window->AddPendingEvent(*l_end_event);
  delete l_end_event;
#endif
  return NULL;
}

FileReceiveThread::FileReceiveThread(wxSocketBase* sock, wxWindow* window,
                                     wxFile* fp, wxULongLong filesize,
                                     TransferStateType* transfer_state_ptr)
{
  m_sock = sock;
  m_window = window;
  m_fp = fp;
  m_filesize = filesize;
  m_transfer_state_ptr = transfer_state_ptr;
}

void* FileReceiveThread::Entry()
{
  char l_buffer[FILE_TRANSFER_BUFFER_SIZE];
  size_t l_total_read = 0, l_curr_read;
//m_sock->SetFlags(wxSOCKET_NOWAIT);
  m_sock->SetFlags(wxSOCKET_NONE|wxSOCKET_BLOCK);
  m_sock->SetTimeout(1);

  while ((l_total_read < m_filesize) && (*m_transfer_state_ptr == TRANSFERSTATE_BUSY))
  {
    m_sock->Read(l_buffer, FILE_TRANSFER_BUFFER_SIZE);
/*  if ((m_sock->Error()) && (m_sock->LastError() != wxSOCKET_NOERROR)
        && (m_sock->LastError() != wxSOCKET_WOULDBLOCK) && (m_sock->LastError() != wxSOCKET_TIMEDOUT))
    {
      *m_transfer_state_ptr = TRANSFERSTATE_ABORTED;
      wxLogError(wxString::Format(wxT("File transfer error: %d"), m_sock->LastError()));
    }
    else*/
    l_curr_read = m_sock->LastCount();
    l_total_read += l_curr_read;
    if (l_curr_read)
      m_fp->Write(l_buffer, l_curr_read);
  }

// Don't close and delete here - May cause a race condition regarding UI updates.
//delete fp;
  m_sock->Destroy();

  wxCommandEvent* l_end_event = new wxCommandEvent(wxEVT_COMMAND_BUTTON_CLICKED, ID_TRANSFERTHREADEND);
#if wxCHECK_VERSION(2, 9, 0)
  m_window->GetEventHandler()->QueueEvent(l_end_event);
#else
  m_window->AddPendingEvent(*l_end_event);
  delete l_end_event;
#endif

  return NULL;
}
